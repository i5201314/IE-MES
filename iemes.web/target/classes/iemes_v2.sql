/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50721
Source Host           : 47.106.70.27:3306
Source Database       : iemes_v2

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-07-09 21:55:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mds_buttom
-- ----------------------------
DROP TABLE IF EXISTS `mds_buttom`;
CREATE TABLE `mds_buttom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `buttom` varchar(200) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_buttom
-- ----------------------------
INSERT INTO `mds_buttom` VALUES ('1', '新增', '<button type=\"button\" id=\"addFun\" class=\"btn btn-primary marR10\"><span class=\"glyphicon glyphicon-plus\"></span> 新增</button>', '');
INSERT INTO `mds_buttom` VALUES ('2', '编辑', '<button type=\"button\" id=\"editFun\" class=\"btn btn-info marR10\"><span class=\"glyphicon glyphicon-edit\"></span> 编辑</button>', null);
INSERT INTO `mds_buttom` VALUES ('3', '删除', '<button type=\"button\" id=\"delFun\" class=\"btn btn-danger marR10\"><span class=\"glyphicon glyphicon-trash\"></span> 删除</button>', null);
INSERT INTO `mds_buttom` VALUES ('4', '上传', '<button type=\"button\" id=\"upLoad\" class=\"btn btn-primary marR10\"><span class=\"glyphicon glyphicon-cloud-upload\"></span> 上传</button>', null);
INSERT INTO `mds_buttom` VALUES ('5', '下载', '<button type=\"button\" id=\"downLoad\" class=\"btn btn-primary marR10\"><span class=\"glyphicon glyphicon-cloud-download\"></span> 下载</button>', null);
INSERT INTO `mds_buttom` VALUES ('6', '上移', '<button type=\"button\" id=\"lyGridUp\" class=\"btn btn-success marR10\"><span class=\"glyphicon glyphicon-arrow-up\"></span> 上移</button>', null);
INSERT INTO `mds_buttom` VALUES ('7', '下移', '<button type=\"button\" id=\"lyGridDown\" class=\"btn btn btn-grey marR10\"><span class=\"glyphicon glyphicon-arrow-down\"></span> 下移</button>', null);
INSERT INTO `mds_buttom` VALUES ('8', '分配权限', '<button type=\"button\" id=\"permissions\" class=\"btn btn btn-grey marR10\"><span class=\"glyphicon glyphicon-cog\"></span> 分配权限</button>', null);
INSERT INTO `mds_buttom` VALUES ('9', '检索', '<button type=\"button\" class=\"btn btn-default\" id=\"search\"><span class=\"glyphicon glyphicon-search\"></span> 检索</button>', null);
INSERT INTO `mds_buttom` VALUES ('10', '保存', '<button type=\"button\" class=\"btn btn-success\" id=\"save\"><span class=\"glyphicon glyphicon-floppy-save\"></span> 保存</button>', null);

-- ----------------------------
-- Table structure for mds_collection
-- ----------------------------
DROP TABLE IF EXISTS `mds_collection`;
CREATE TABLE `mds_collection` (
  `ID` varchar(100) NOT NULL,
  `COLLECTION` varchar(50) NOT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `VERSION` varchar(1) DEFAULT NULL,
  `CURRENT_VERSION` varchar(5) DEFAULT NULL,
  `CREATED_TIME` datetime DEFAULT NULL,
  `CREATED_USER` varchar(100) DEFAULT NULL,
  `MODIFIED_TIME` datetime DEFAULT NULL,
  `MODIFIED_USER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_collection
-- ----------------------------

-- ----------------------------
-- Table structure for mds_collection_parameter
-- ----------------------------
DROP TABLE IF EXISTS `mds_collection_parameter`;
CREATE TABLE `mds_collection_parameter` (
  `ID` varchar(100) NOT NULL,
  `COLLECTION_ID` varchar(100) DEFAULT NULL,
  `PARAMETER` varchar(50) DEFAULT NULL,
  `PARAMETER_TITLE` varchar(100) DEFAULT NULL,
  `REQUIRED` varchar(5) DEFAULT NULL,
  `CREATED_TIME` datetime DEFAULT NULL,
  `CREATED_USER` varchar(100) DEFAULT NULL,
  `MODIFIED_TIME` datetime DEFAULT NULL,
  `MODIFIED_USER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_collection_parameter
-- ----------------------------

-- ----------------------------
-- Table structure for mds_collection_sfc
-- ----------------------------
DROP TABLE IF EXISTS `mds_collection_sfc`;
CREATE TABLE `mds_collection_sfc` (
  `ID` varchar(100) NOT NULL,
  `SFC_ID` varchar(100) DEFAULT NULL,
  `COLLECTION_ID` varchar(100) DEFAULT NULL,
  `PARAMETER_ID` varchar(100) DEFAULT NULL,
  `PARAMETER_VALUE` varchar(100) DEFAULT NULL,
  `VERSION` varchar(1) DEFAULT NULL,
  `CURRENT_VERSION` varchar(5) DEFAULT NULL,
  `CREATED_TIME` datetime DEFAULT NULL,
  `CREATED_USER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_collection_sfc
-- ----------------------------

-- ----------------------------
-- Table structure for mds_container
-- ----------------------------
DROP TABLE IF EXISTS `mds_container`;
CREATE TABLE `mds_container` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `container_id` varchar(64) NOT NULL COMMENT '容器ID',
  `container_type_id` varchar(64) NOT NULL COMMENT '容器类型ID',
  `container_status` int(1) NOT NULL COMMENT '容器状态；1：打开；-1：关闭',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_container
-- ----------------------------

-- ----------------------------
-- Table structure for mds_container_sfc_relation
-- ----------------------------
DROP TABLE IF EXISTS `mds_container_sfc_relation`;
CREATE TABLE `mds_container_sfc_relation` (
  `id` varchar(64) NOT NULL,
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `container_id` varchar(64) NOT NULL COMMENT '容器ID',
  `container_type_id` varchar(255) NOT NULL COMMENT '容器类型ID',
  `create_user` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wrappage` varchar(64) NOT NULL COMMENT '包装货物；当容器类型为box时，里面装的是SFC，当类型为中箱时，里面装的是礼盒；当类型为栈板时，里面装的是中箱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_container_sfc_relation
-- ----------------------------

-- ----------------------------
-- Table structure for mds_container_type
-- ----------------------------
DROP TABLE IF EXISTS `mds_container_type`;
CREATE TABLE `mds_container_type` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `container_type_no` varchar(255) NOT NULL COMMENT '容器类型编号',
  `container_type_desc` varchar(255) DEFAULT NULL COMMENT '容器类型描述',
  `container_level` varchar(255) NOT NULL COMMENT '容器层级：box、盒子；carton、中箱；pallet、栈板',
  `status` int(1) NOT NULL COMMENT '状态：1、可用；-1、不可用',
  `min_number` int(11) NOT NULL COMMENT '最小总数量',
  `max_number` int(11) NOT NULL COMMENT '最大总数量',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `container_length` double DEFAULT NULL COMMENT '容器长度',
  `container_width` double DEFAULT NULL COMMENT '容器宽度',
  `container_height` double DEFAULT NULL COMMENT '容器高度',
  `max_weight` double DEFAULT NULL COMMENT '最大重量',
  `container_weight` double DEFAULT NULL COMMENT '容器重量',
  `container_status` int(1) DEFAULT '1' COMMENT '容器状态：1,解包；-1 打包',
  `number_rule_id` varchar(64) NOT NULL COMMENT '编号规则',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_container_type
-- ----------------------------

-- ----------------------------
-- Table structure for mds_customer_reject_sfcinfo
-- ----------------------------
DROP TABLE IF EXISTS `mds_customer_reject_sfcinfo`;
CREATE TABLE `mds_customer_reject_sfcinfo` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `old_sfc` varchar(64) NOT NULL COMMENT '旧SFC',
  `new_sfc` varchar(64) NOT NULL COMMENT '新SFC',
  `old_shoporder_id` varchar(64) NOT NULL COMMENT '旧工单ID',
  `new_shoporder_id` varchar(64) NOT NULL COMMENT '新工单ID',
  `create_user` varchar(64) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_customer_reject_sfcinfo
-- ----------------------------

-- ----------------------------
-- Table structure for mds_flow_step
-- ----------------------------
DROP TABLE IF EXISTS `mds_flow_step`;
CREATE TABLE `mds_flow_step` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `process_workflow_id` varchar(64) NOT NULL COMMENT '工艺路线Id',
  `operation_id` varchar(255) NOT NULL COMMENT '操作',
  `next_operation_id` varchar(255) NOT NULL COMMENT '下一个操作',
  `status` int(1) NOT NULL COMMENT '状态，0：正常,1：开始节点，2：结束节点',
  `site_id` varchar(255) NOT NULL COMMENT '站点ID',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `process_workflow_version` varchar(255) NOT NULL COMMENT '工艺路线版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_flow_step
-- ----------------------------
INSERT INTO `mds_flow_step` VALUES ('43b854da5f3346beb04022a4d13ca3ef', '905f98228bfb440482d3bee89b139617', '01565f643509455498d6a00e65fbde51', '004f116b4dff48b5a013b56736af7860', '0', '0', 'admin', '2018-07-08 20:52:40', 'A');
INSERT INTO `mds_flow_step` VALUES ('9497a89acbdd455a894a8c692ac6b346', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', 'adaf2e8c2a314be5ac717e39c105936d', '1', '0', 'admin', '2018-07-08 20:52:40', 'A');
INSERT INTO `mds_flow_step` VALUES ('9a51de7f34dd41ef963fb61965917438', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '01565f643509455498d6a00e65fbde51', '1', '0', 'admin', '2018-07-08 20:52:40', 'A');
INSERT INTO `mds_flow_step` VALUES ('f6626f8b5ab347cd91bcadb1c5cdcb8f', '08266e4cea7148e4ad945c7541bc8a8d', 'cd1e8884810b4b59a7920d71d94a30c2', '2e17d8469fb44369a5f2c511b51f1ee2', '1', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'admin', '2018-07-07 17:49:35', 'A');

-- ----------------------------
-- Table structure for mds_item
-- ----------------------------
DROP TABLE IF EXISTS `mds_item`;
CREATE TABLE `mds_item` (
  `id` varchar(64) NOT NULL,
  `item_no` varchar(255) NOT NULL COMMENT '物料编号',
  `item_name` varchar(255) NOT NULL COMMENT '物料名称',
  `item_desc` varchar(255) DEFAULT NULL COMMENT '物料描述',
  `bom_no` varchar(255) DEFAULT NULL COMMENT '物料的BOM',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `status` int(1) NOT NULL COMMENT '状态：1、可用；-1、不可用',
  `item_type` varchar(255) NOT NULL COMMENT '物料类型；外采购、purchase；自生产、produce',
  `item_version` varchar(255) NOT NULL COMMENT '物料版本',
  `item_save_type` varchar(255) NOT NULL COMMENT '物料存储类型：unit、个体；batch、批次',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_item
-- ----------------------------
INSERT INTO `mds_item` VALUES ('05f9ec95adf24537a40c2037a2cc9367', 'w1', 'w1', 'e', null, 'admin', '2017-11-02 19:58:37', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('09b69b49ffb14e0db31fd1e4e3b662a8', 'w2', 'w112222', 'e', null, 'admin', '2017-11-02 19:55:48', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('0c1d619ea0a244deb8b7c538ab1a1da9', 'dz', '电阻', null, null, 'zoufa', '2017-11-08 14:21:23', 'a67033a4395543adb40a6dcd4c65fbd4', '1', 'produce', 'a', 'batch');
INSERT INTO `mds_item` VALUES ('20d3d921b6b24c4e828805802b50bf5d', 'mds_wk', 'MDS_WK', 'mds外壳', null, 'admin', '2017-10-25 11:28:05', 'b4ef0a63b39f48299795806d54cb17a5', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('2a02d37279fc43038dac8abc478df220', '001', '成品手机', '成品手机', '6189bfc819bc4da9bea9c9cf4c9991a9', 'admin', '2018-07-08 20:39:45', '0', '1', 'produce', 'A', 'unit');
INSERT INTO `mds_item` VALUES ('2b7175b17c6a4bc094c8bde93fc980bf', 'mds_laptop', 'mds_laptop', '迈鼎胜笔记本', 'eb6675f095d8486f978991ee152dbecb', 'admin', '2017-10-25 10:16:07', 'b4ef0a63b39f48299795806d54cb17a5', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('41c9f27847614c5689be29bbd5e2387b', 'sj1', '手机1', 'e', '1bd5146fefbe47b5b9650773575262f2', 'admin', '2017-11-08 23:04:05', '365fe34663c94f5c8d06f8be76b8501d', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('42a25f2ba40942aa8d1a2aae8a1757e2', '001', '001', null, null, 'admin', '2018-07-04 23:34:38', '4db61d7d29ed4c0c99b4580c22cc5bcf', '1', 'produce', 'A', 'unit');
INSERT INTO `mds_item` VALUES ('4558ecf93da84175b34bdb8f717a9228', '2017102701', 'PCB', 'PCB', '509e44b36d454cf394131fb399774423', '123', '2017-10-29 20:19:22', 'd31fe95b88804806b6cff4083090afd3', '1', 'produce', 'A', 'unit');
INSERT INTO `mds_item` VALUES ('4956dc1fbb274716b5b17ec8a1b9817f', 'mds_screen', 'MDS_SCREEN', 'mds屏幕', null, 'admin', '2017-10-25 11:27:45', 'b4ef0a63b39f48299795806d54cb17a5', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('50537e0557974c7f9a5a8627619847bd', 'applehk', '苹果后壳', 'APPLE后壳', null, 'admin', '2017-11-08 15:07:25', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('54ac891b98884d809960c06a6389fa00', 'dj', '电阻', '电阻', null, 'admin', '2017-11-01 20:50:19', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('575a57d508a24c719a46827caf0fdcf2', '001-02', '001-02', '001-02', null, 'admin', '2018-07-08 20:31:39', '0', '1', 'produce', 'A', 'unit');
INSERT INTO `mds_item` VALUES ('59b78d8ff10e405482fd68b9f8d59281', 'sj', '手机', null, '49032d48f3ce4196b7fbbd2bea209d8e', 'zoufa', '2017-11-08 10:32:19', 'a67033a4395543adb40a6dcd4c65fbd4', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('5cfa177565b44ac3939c1b894df18e06', 'w2', 'w112222', 'e', null, 'admin', '2017-11-02 19:55:56', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 'b', 'unit');
INSERT INTO `mds_item` VALUES ('68df1479eb3a4b79aa67addff920377f', 'mds_internal memory', 'MDS_INTERNAL MEMORY', '迈鼎胜内存', null, 'admin', '2017-10-25 10:08:38', 'b4ef0a63b39f48299795806d54cb17a5', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('72412099b9c64ef480ebb9dcc2eb3bdf', 'a1', 'a', 'a', null, 'admin', '2017-11-03 15:13:15', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('749eef16d1714f5888572d6b8ab3bf1f', 'huaweihk', '华为后壳', '华为后壳', null, 'admin', '2017-11-04 15:34:55', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('76a27a75fad24a88917ae5a2105a0579', 'samsungic', '三星芯片', '三星手机芯片', null, 'admin', '2017-11-04 15:29:25', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('7c03334ddba74acfa7ec2603728e059f', 'NE8', 'ea', 'e', null, 'admin', '2017-10-31 21:36:38', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('83cf2f1d308b49439f6621b5d11a2f2c', '2017102702', 'DZ', 'DZ', null, '123', '2017-10-29 20:16:14', 'd31fe95b88804806b6cff4083090afd3', '1', 'purchase', 'A', 'unit');
INSERT INTO `mds_item` VALUES ('88b6345eb19d47a89a57f6f29e9847be', 'luoshi2', '螺丝2', 'e', null, 'admin', '2017-11-09 10:44:29', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'batch');
INSERT INTO `mds_item` VALUES ('8b06f47f3b65414b93105ea8c1ef896d', 'mds_HD', 'MDS_HD', '迈鼎胜硬盘', null, 'admin', '2017-10-25 10:10:12', 'b4ef0a63b39f48299795806d54cb17a5', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('934ccacc383f47f0892f25334a54d241', 'sj', '手机', '手机', '54395cf81056400e8b775ab8f681f41b', 'admin', '2017-10-19 09:39:52', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('9bdd7c4632244d58af0b651a421a99b7', '001-01', '001-01', '001-01', null, 'admin', '2018-07-08 20:31:32', '0', '1', 'produce', 'A', 'unit');
INSERT INTO `mds_item` VALUES ('9c8375eb760f42f69a97f38a8bd6188d', 'hk', '后壳', '后壳', null, 'admin', '2017-11-01 19:56:33', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('b691f59b63d34b73a68bb2b59543afc2', 'luoshi1', '螺丝1', 'e', null, 'admin', '2017-11-08 23:05:05', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'batch');
INSERT INTO `mds_item` VALUES ('b9387c634ca1443aa379eae5f6e9fd24', 'miic', '小米芯片', '小米手机芯片', null, 'admin', '2017-11-04 15:39:49', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('bacd72e2e40d462aa0292808b4493ea5', 'sj', '手机', '智能手机', 'bf0399152259429b9fae22f72cd9ef17', 'admin', '2017-11-04 15:43:46', '365fe34663c94f5c8d06f8be76b8501d', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('cb1730b1033f4b8a95d94582ad63df97', 'cpu', 'CPU', null, null, 'zoufa', '2017-11-08 10:31:25', 'a67033a4395543adb40a6dcd4c65fbd4', '1', 'produce', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('d9d4e6b471054662b1fed2583ed14146', 'MI8', 'e', 'e', null, 'admin', '2017-10-31 21:32:41', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 'e', 'unit');
INSERT INTO `mds_item` VALUES ('dd71a5d8eee84308b52f43051303c354', 'hk1', '手机1', 'e', '1bd5146fefbe47b5b9650773575262f2', 'admin', '2017-11-08 23:05:17', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('e6b75710f52042169d2ced6e7d90e9ec', 'hk2', '后壳2', 'e', null, 'admin', '2017-11-08 23:05:26', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('e7ef68cac6a24025a49295a3517f8633', 'dz2', '电阻2', null, null, 'zoufa', '2017-11-08 18:16:06', 'a67033a4395543adb40a6dcd4c65fbd4', '1', 'produce', 'a', 'batch');
INSERT INTO `mds_item` VALUES ('f468496e3f2e4d66ab1b1e2fb76ddc87', 'mihk', '小米后壳', '小米后壳', null, 'admin', '2017-11-04 15:40:53', '365fe34663c94f5c8d06f8be76b8501d', '1', 'purchase', 'a', 'unit');
INSERT INTO `mds_item` VALUES ('f814df5670d141f9aaa0ea88625c7ad1', '111', '11', null, null, 'admin', '2017-11-03 15:12:13', '6d70fed36bd94bdab1a71cde63954fa6', '1', 'produce', 's', 'unit');
INSERT INTO `mds_item` VALUES ('f926aa7f5187447b91c0d3b90ecbe084', 'mds_cpu', 'mds_cpu', '迈鼎胜处理器', null, 'admin', '2017-10-25 10:05:43', 'b4ef0a63b39f48299795806d54cb17a5', '1', 'purchase', 'a', 'unit');

-- ----------------------------
-- Table structure for mds_item_bom
-- ----------------------------
DROP TABLE IF EXISTS `mds_item_bom`;
CREATE TABLE `mds_item_bom` (
  `id` varchar(64) NOT NULL,
  `item_bom_no` varchar(255) NOT NULL COMMENT '物料清单NO',
  `item_bom_name` varchar(255) NOT NULL COMMENT '物料清单名称',
  `item_bom_desc` varchar(255) DEFAULT NULL COMMENT '物料清单描述',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `status` int(1) NOT NULL COMMENT '状态-1:不可用;1:可用',
  `item_bom_version` varchar(64) NOT NULL COMMENT '物料清单版本',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_item_bom
-- ----------------------------
INSERT INTO `mds_item_bom` VALUES ('6189bfc819bc4da9bea9c9cf4c9991a9', 'BOM001', 'BOM001', 'BOM001', '0', '1', 'A', 'admin', '2018-07-08 20:32:02');

-- ----------------------------
-- Table structure for mds_item_bom_relation
-- ----------------------------
DROP TABLE IF EXISTS `mds_item_bom_relation`;
CREATE TABLE `mds_item_bom_relation` (
  `id` varchar(64) NOT NULL,
  `item_bom_id` varchar(255) NOT NULL COMMENT '物料清单PK_ID',
  `item_id` varchar(255) NOT NULL COMMENT '物料号PK_ID',
  `item_seq` varchar(255) NOT NULL COMMENT '物料在物料清单里的顺序',
  `use_number` varchar(255) DEFAULT '1' COMMENT '消耗数量',
  `site_id` varchar(64) NOT NULL COMMENT '站点PK_ID',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_item_bom_relation
-- ----------------------------
INSERT INTO `mds_item_bom_relation` VALUES ('c11a50dbd3c44cec8d097e5e167670fa', '6189bfc819bc4da9bea9c9cf4c9991a9', '9bdd7c4632244d58af0b651a421a99b7', '10', '1', '0', 'admin', '2018-07-08 20:32:02');
INSERT INTO `mds_item_bom_relation` VALUES ('dbeec2c5c77b46988f4d8621f1c57453', '6189bfc819bc4da9bea9c9cf4c9991a9', '575a57d508a24c719a46827caf0fdcf2', '20', '2', '0', 'admin', '2018-07-08 20:32:02');

-- ----------------------------
-- Table structure for mds_item_surrenal
-- ----------------------------
DROP TABLE IF EXISTS `mds_item_surrenal`;
CREATE TABLE `mds_item_surrenal` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `item_surrenal_level` int(4) NOT NULL COMMENT '顺序',
  `item_surrenal_id` varchar(64) NOT NULL COMMENT '替代物料ID',
  `item_surrenal_version` varchar(1) NOT NULL COMMENT '替代物料的版本',
  `item_surrenal_usage` varchar(64) NOT NULL COMMENT '有效装配物料ID，即替代物料用在哪些物料上才有用',
  `item_surrenal_usage_version` varchar(1) NOT NULL COMMENT '有效版本',
  `start_validity_term` datetime NOT NULL COMMENT '有效期自',
  `end_validity_term` datetime NOT NULL COMMENT '有效期至',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `item_id` varchar(64) NOT NULL COMMENT '被替代的物料ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_item_surrenal
-- ----------------------------

-- ----------------------------
-- Table structure for mds_log
-- ----------------------------
DROP TABLE IF EXISTS `mds_log`;
CREATE TABLE `mds_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountName` varchar(30) DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `methods` varchar(30) DEFAULT NULL,
  `actionTime` varchar(30) DEFAULT NULL,
  `userIP` varchar(30) DEFAULT NULL,
  `operTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_log
-- ----------------------------

-- ----------------------------
-- Table structure for mds_nc_code
-- ----------------------------
DROP TABLE IF EXISTS `mds_nc_code`;
CREATE TABLE `mds_nc_code` (
  `id` varchar(64) NOT NULL,
  `nc_code` varchar(255) NOT NULL COMMENT '不合格代码',
  `nc_code_desc` varchar(255) DEFAULT NULL COMMENT '不合格代码描述',
  `status` int(1) NOT NULL COMMENT '不合格代码状态:-1:不可用；1：可用；',
  `site_id` varchar(255) NOT NULL COMMENT '站点',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_nc_code
-- ----------------------------
INSERT INTO `mds_nc_code` VALUES ('01b60f7d115546cd8849612ec2b98efd', '20171027PCB', '20171027PCB', '1', 'd31fe95b88804806b6cff4083090afd3', '123', '2017-10-29 20:20:32');
INSERT INTO `mds_nc_code` VALUES ('09eccf55e86f4f53bbb147e69699d563', 'nc1', 'eae', '1', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '2017-11-03 10:23:25');
INSERT INTO `mds_nc_code` VALUES ('1dc8b0915e894af9a53767136c61e85c', 'nc_shuailiao', '摔料', '1', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '2017-10-25 11:59:20');
INSERT INTO `mds_nc_code` VALUES ('1eaaf0d20a5a4b258e23bfeaf7fc7312', 'NC_CODE_01', 'NC_CODE_01', '1', '0', 'admin', '2018-07-08 20:44:55');
INSERT INTO `mds_nc_code` VALUES ('51ecdeeb7e1141b9b9f76f3fe8be351d', 'nc_qiegespeed', '切割速度不良', '1', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '2017-10-25 11:57:12');
INSERT INTO `mds_nc_code` VALUES ('7dc283d26c3149a9ad8f45668a2ed7de', 'ss', '摔碎', '1', 'a67033a4395543adb40a6dcd4c65fbd4', 'zoufa', '2017-11-08 10:44:31');
INSERT INTO `mds_nc_code` VALUES ('8cac5e23de1540d184eb1b93ef845947', 'nc2', 'eae', '1', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '2017-11-03 10:23:33');
INSERT INTO `mds_nc_code` VALUES ('9fb221e3b9a044f7be3e53f31345437f', 'NC_CODE_01', 'NC_CODE_01', '1', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'admin', '2018-07-07 17:11:45');
INSERT INTO `mds_nc_code` VALUES ('a0e01b1e244642d586faea27daa4057a', 'shuailiao', '摔料', '1', '365fe34663c94f5c8d06f8be76b8501d', 'admin', '2017-11-08 16:09:14');
INSERT INTO `mds_nc_code` VALUES ('b0569d50e7b640b3896585352d1e708f', 'pm', '屏幕不良', '1', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '2017-10-19 09:49:22');
INSERT INTO `mds_nc_code` VALUES ('b85a0fbf5e374eae93e0b39f9f580dc1', 'weightoutoflimit', '重量超标', '1', '365fe34663c94f5c8d06f8be76b8501d', 'z0001', '2017-11-04 15:54:42');
INSERT INTO `mds_nc_code` VALUES ('d25dcb18fe5247849f276ddee87c4070', 'nc_hanjietemp', '焊接温度不良', '1', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '2017-10-25 11:58:41');
INSERT INTO `mds_nc_code` VALUES ('ebd69217b87543a4b008b2d45b97eff1', 'lengthoutoflimit', '长度超标', '1', '365fe34663c94f5c8d06f8be76b8501d', 'z0001', '2017-11-04 15:55:25');

-- ----------------------------
-- Table structure for mds_nc_code_group
-- ----------------------------
DROP TABLE IF EXISTS `mds_nc_code_group`;
CREATE TABLE `mds_nc_code_group` (
  `id` varchar(64) NOT NULL,
  `nc_code_group_no` varchar(255) NOT NULL COMMENT '不合格组代码',
  `nc_code_group_desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `site_id` varchar(255) NOT NULL COMMENT '站点',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '不合格组代码状态：-1：不可用；1:可用',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_nc_code_group
-- ----------------------------
INSERT INTO `mds_nc_code_group` VALUES ('00419afeaf8743a1b1d9fc39f0bb0008', 'ncg_operation', '操作不当', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '1', '2017-10-25 11:55:58');
INSERT INTO `mds_nc_code_group` VALUES ('252c1c9615244585bddb6576016a9fce', 'ncg_process', '工艺不良', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '1', '2017-10-25 11:55:36');
INSERT INTO `mds_nc_code_group` VALUES ('35caf90337124b64a9f17f9a6dd3dd6c', 'WG', '外观不良', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '1', '2017-10-19 09:49:41');
INSERT INTO `mds_nc_code_group` VALUES ('4039adf32d384e798a1e96e3802d5b93', 'PL', '破裂', 'a67033a4395543adb40a6dcd4c65fbd4', 'zoufa', '1', '2017-11-08 10:44:46');
INSERT INTO `mds_nc_code_group` VALUES ('5317dd8bc448408c85e38317dd8fc3c5', 'pelpleissue', '人为问题', '365fe34663c94f5c8d06f8be76b8501d', 'admin', '1', '2017-11-08 16:23:28');
INSERT INTO `mds_nc_code_group` VALUES ('5419bd79304343c1b26e093e6c42ab6e', '20171027PCB', '20171027PCB', 'd31fe95b88804806b6cff4083090afd3', '123', '1', '2017-10-29 20:19:58');
INSERT INTO `mds_nc_code_group` VALUES ('719e1872495f405980782c1243155a43', 'ng2', 'ae', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '1', '2017-11-03 10:24:34');
INSERT INTO `mds_nc_code_group` VALUES ('7bbe4af2502647f9878f5a3e28b2c4e8', 'ng3', 'eee', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '1', '2017-11-03 10:24:42');
INSERT INTO `mds_nc_code_group` VALUES ('9507688dfa6b43658d542e317d450062', 'NC_G_01', 'NC_G_01', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'admin', '1', '2018-07-07 17:11:27');
INSERT INTO `mds_nc_code_group` VALUES ('c78213d8267840cc88b219ddf1241a87', 'ng1', 'ae', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '1', '2017-11-03 10:24:28');
INSERT INTO `mds_nc_code_group` VALUES ('d063f72ba7a540b5b8695806d685bec1', 'processissue', '工艺问题', '365fe34663c94f5c8d06f8be76b8501d', 'z0001', '1', '2017-11-04 15:59:05');
INSERT INTO `mds_nc_code_group` VALUES ('e364d12c945c4c5782b32d8eb86237df', 'NC_G_01', 'NC_G_01', '0', 'admin', '1', '2018-07-08 20:43:59');

-- ----------------------------
-- Table structure for mds_nc_code_relation
-- ----------------------------
DROP TABLE IF EXISTS `mds_nc_code_relation`;
CREATE TABLE `mds_nc_code_relation` (
  `id` varchar(64) NOT NULL,
  `nc_code_group_id` varchar(255) NOT NULL COMMENT '不合格代码组PK_ID',
  `nc_code_id` varchar(255) NOT NULL COMMENT '不合格代码PK_ID',
  `site_id` varchar(255) NOT NULL COMMENT '站点PK_ID',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`nc_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_nc_code_relation
-- ----------------------------
INSERT INTO `mds_nc_code_relation` VALUES ('0c8e3877f88c441b83acf6eb3f462e39', '5317dd8bc448408c85e38317dd8fc3c5', 'a0e01b1e244642d586faea27daa4057a', '365fe34663c94f5c8d06f8be76b8501d', 'admin', '2017-11-08 16:23:28');
INSERT INTO `mds_nc_code_relation` VALUES ('251693a5da5845469e1d6f23e88e94ba', '35caf90337124b64a9f17f9a6dd3dd6c', 'b0569d50e7b640b3896585352d1e708f', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '2017-10-19 09:49:41');
INSERT INTO `mds_nc_code_relation` VALUES ('2c6ef5ebfe6d4f7d81d658f0f722db63', '35caf90337124b64a9f17f9a6dd3dd6c', '09eccf55e86f4f53bbb147e69699d563', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '2017-11-03 10:23:25');
INSERT INTO `mds_nc_code_relation` VALUES ('37116a3c1dbd4177a840a9c6267c2a0b', '252c1c9615244585bddb6576016a9fce', 'd25dcb18fe5247849f276ddee87c4070', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '2017-10-25 11:58:42');
INSERT INTO `mds_nc_code_relation` VALUES ('408e05c835e44f47be9b9b2bf3018b6e', '00419afeaf8743a1b1d9fc39f0bb0008', '1dc8b0915e894af9a53767136c61e85c', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '2017-10-25 11:59:21');
INSERT INTO `mds_nc_code_relation` VALUES ('4e78964289214423a8d29bbc62cf017d', '9507688dfa6b43658d542e317d450062', '9fb221e3b9a044f7be3e53f31345437f', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'admin', '2018-07-07 17:11:45');
INSERT INTO `mds_nc_code_relation` VALUES ('5dd3a9387eba41d2a67fbb0f4f0b6534', 'd063f72ba7a540b5b8695806d685bec1', 'b85a0fbf5e374eae93e0b39f9f580dc1', '365fe34663c94f5c8d06f8be76b8501d', 'z0001', '2017-11-04 15:59:05');
INSERT INTO `mds_nc_code_relation` VALUES ('960c808a1038466a8e3fb5d79cee697e', '35caf90337124b64a9f17f9a6dd3dd6c', '8cac5e23de1540d184eb1b93ef845947', '6d70fed36bd94bdab1a71cde63954fa6', 'admin', '2017-11-03 10:23:34');
INSERT INTO `mds_nc_code_relation` VALUES ('c15d55840c3d4bf8b44fc55630db8cbe', 'e364d12c945c4c5782b32d8eb86237df', '1eaaf0d20a5a4b258e23bfeaf7fc7312', '0', 'admin', '2018-07-08 20:44:55');
INSERT INTO `mds_nc_code_relation` VALUES ('d952072a4b8f470d852846a20c1bbc87', '4039adf32d384e798a1e96e3802d5b93', '7dc283d26c3149a9ad8f45668a2ed7de', 'a67033a4395543adb40a6dcd4c65fbd4', 'zoufa', '2017-11-08 10:44:46');
INSERT INTO `mds_nc_code_relation` VALUES ('dd11b7c3ef6a4c538d4eadf9ac9c1488', '252c1c9615244585bddb6576016a9fce', '51ecdeeb7e1141b9b9f76f3fe8be351d', 'b4ef0a63b39f48299795806d54cb17a5', 'admin', '2017-10-25 11:57:12');
INSERT INTO `mds_nc_code_relation` VALUES ('e163f621adcd42ac8f2e14f9dd033796', 'd063f72ba7a540b5b8695806d685bec1', 'ebd69217b87543a4b008b2d45b97eff1', '365fe34663c94f5c8d06f8be76b8501d', 'z0001', '2017-11-04 15:59:05');
INSERT INTO `mds_nc_code_relation` VALUES ('e25ab0b7fe0e49b1aadc0c8c995d57f4', '5419bd79304343c1b26e093e6c42ab6e', '01b60f7d115546cd8849612ec2b98efd', 'd31fe95b88804806b6cff4083090afd3', '123', '2017-10-29 20:20:32');

-- ----------------------------
-- Table structure for mds_nc_repair
-- ----------------------------
DROP TABLE IF EXISTS `mds_nc_repair`;
CREATE TABLE `mds_nc_repair` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `sfc` varchar(255) NOT NULL COMMENT 'sfc',
  `shoporder_id` varchar(255) NOT NULL COMMENT '工单ID',
  `workshop_id` varchar(255) NOT NULL COMMENT '车间ID',
  `workline_id` varchar(255) NOT NULL COMMENT '产线ID',
  `operation_id` varchar(255) NOT NULL COMMENT '操作ID',
  `repair_desc` varchar(255) NOT NULL COMMENT '处置说明',
  `site_id` varchar(255) NOT NULL COMMENT '站点ID',
  `nc_code_id` varchar(255) NOT NULL COMMENT '不良代码ID',
  `nc_code_group_id` varchar(255) NOT NULL COMMENT '不良代码组ID',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_nc_repair
-- ----------------------------
INSERT INTO `mds_nc_repair` VALUES ('2fa55543fb5f4577ac1814eece2a43b1', '201820180708203522000000000002XC', '37ac54169ef244658abbe13bb4927335', '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '01565f643509455498d6a00e65fbde51', 'FGTH', '0', '1eaaf0d20a5a4b258e23bfeaf7fc7312', 'e364d12c945c4c5782b32d8eb86237df', 'admin', '2018-07-08 20:54:33');
INSERT INTO `mds_nc_repair` VALUES ('3590360e2fdd4caf909dce74e6a05bc6', 'MDS_SFC2017102411274400000000001', '9fc911a781b64755b3f737852ad9ef70', 'e78f5429e2974807a6b93d9e03f60307', 'bc3ea23aab814c6bb310c29b5832648c', '3aba6a9d08484b01826628359dced8fd', '3333', '6d70fed36bd94bdab1a71cde63954fa6', 'b0569d50e7b640b3896585352d1e708f', '35caf90337124b64a9f17f9a6dd3dd6c', 'admin', '2017-10-24 14:36:05');
INSERT INTO `mds_nc_repair` VALUES ('9505b705dfd64fce8c370ef0ccbecda4', 'MDS_SFC2017102410374600000000001', '9fc911a781b64755b3f737852ad9ef70', 'e78f5429e2974807a6b93d9e03f60307', 'bc3ea23aab814c6bb310c29b5832648c', '3aba6a9d08484b01826628359dced8fd', '报废喽', '6d70fed36bd94bdab1a71cde63954fa6', 'b0569d50e7b640b3896585352d1e708f', '35caf90337124b64a9f17f9a6dd3dd6c', 'admin', '2017-10-24 10:46:08');
INSERT INTO `mds_nc_repair` VALUES ('f22212292f6b46b29bba44717677b819', 'MDS_SFC2017102410490400000000001', '9fc911a781b64755b3f737852ad9ef70', 'e78f5429e2974807a6b93d9e03f60307', 'bc3ea23aab814c6bb310c29b5832648c', '3aba6a9d08484b01826628359dced8fd', 'MDS_SFC2017102410490400000000001', '6d70fed36bd94bdab1a71cde63954fa6', 'b0569d50e7b640b3896585352d1e708f', '35caf90337124b64a9f17f9a6dd3dd6c', 'admin', '2017-10-24 11:21:54');

-- ----------------------------
-- Table structure for mds_number_rule
-- ----------------------------
DROP TABLE IF EXISTS `mds_number_rule`;
CREATE TABLE `mds_number_rule` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `site_id` varchar(255) NOT NULL COMMENT '站点PK_id',
  `number_rule` varchar(255) NOT NULL COMMENT '编号规则',
  `number_type` varchar(255) DEFAULT NULL,
  `number_rule_desc` varchar(255) NOT NULL COMMENT '编号规则描述',
  `prefix` varchar(255) DEFAULT NULL COMMENT '前缀',
  `postfix` varchar(255) DEFAULT NULL COMMENT '后缀',
  `base_number` int(11) NOT NULL COMMENT '基数',
  `current_number` int(11) NOT NULL COMMENT '当前序号',
  `last_generate_number_time` datetime DEFAULT NULL COMMENT '最近一次编号生成时间，精确到秒',
  `order_length` int(11) NOT NULL COMMENT '序列长度',
  `increment` int(11) NOT NULL COMMENT '增量',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_number_rule
-- ----------------------------
INSERT INTO `mds_number_rule` VALUES ('5ecf98a2adab47a1b8b83a0c54899977', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'sfc', null, 'SFC', '2018', null, '0', '10', '2018-07-04 23:39:27', '32', '1', 'admin', '2018-07-04 23:38:59');
INSERT INTO `mds_number_rule` VALUES ('6097c2950ee44178b7924b1956f6fa72', '365fe34663c94f5c8d06f8be76b8501d', 'sfc', 'sfc', 'sf生成规则', 'sfc_', '_end', '0', '0', null, '32', '1', 'admin', '2017-11-09 09:24:17');
INSERT INTO `mds_number_rule` VALUES ('6097c2950ee44178b7924b1956f6fa73', 'a67033a4395543adb40a6dcd4c65fbd4', 'container_rule1', 'type', '容器规则', 'con_', '_end', '0', '1', '2017-11-14 16:28:04', '32', '1', 'admin', '1990-01-01 00:00:00');
INSERT INTO `mds_number_rule` VALUES ('8d4676ea494c41d88f587bacb1f5cb75', '0', 'sfc', null, 'SFC', '2018', 'XC', '0', '5', '2018-07-08 20:35:22', '32', '1', 'admin', '2018-07-08 20:34:30');
INSERT INTO `mds_number_rule` VALUES ('a299632f0afa40ffa849e10972615999', '365fe34663c94f5c8d06f8be76b8501d', 'container01', null, '1', 'c_', null, '0', '0', null, '32', '1', 'admin', '2017-11-10 09:23:44');
INSERT INTO `mds_number_rule` VALUES ('ce1688f9a0a54a1981a523dda772d69c', '365fe34663c94f5c8d06f8be76b8501d', 'sfc02', null, 'sf生成规则', 'sfc_2_', '_end', '0', '2', '2017-11-11 15:24:28', '32', '1', 'admin', '2017-11-10 09:22:51');

-- ----------------------------
-- Table structure for mds_operation
-- ----------------------------
DROP TABLE IF EXISTS `mds_operation`;
CREATE TABLE `mds_operation` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `operation_no` varchar(255) NOT NULL COMMENT '操作编号',
  `operation_desc` varchar(255) DEFAULT NULL COMMENT '操作描述',
  `site_id` varchar(255) NOT NULL COMMENT '站点',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `status` int(1) NOT NULL COMMENT '1：可用，-1：不可用',
  `operation_type` varchar(255) NOT NULL COMMENT '操作类型（normal正常、test维修、repair测试）；',
  `default_resource` varchar(255) DEFAULT NULL COMMENT '缺省资源',
  `operation_version` varchar(1) NOT NULL COMMENT '操作版本',
  `loop_num` int(5) DEFAULT '0' COMMENT '循环次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_operation
-- ----------------------------
INSERT INTO `mds_operation` VALUES ('004f116b4dff48b5a013b56736af7860', 'OP001', '装配第一工序', '0', 'admin', '2018-07-08 19:48:44', '1', 'normal', null, 'A', '0');
INSERT INTO `mds_operation` VALUES ('01565f643509455498d6a00e65fbde51', 'WX004', '维修工序', '0', 'admin', '2018-07-08 19:51:39', '1', 'repair', null, 'A', '0');
INSERT INTO `mds_operation` VALUES ('1a1f48f1588742618176a8ca89bd29a8', 'WX', 'WX', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'admin', '2018-07-07 17:13:15', '1', 'repair', null, 'A', '0');
INSERT INTO `mds_operation` VALUES ('2e17d8469fb44369a5f2c511b51f1ee2', 'OP002', 'OP002', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'test', '2018-07-02 11:47:01', '1', 'normal', null, 'A', '0');
INSERT INTO `mds_operation` VALUES ('adaf2e8c2a314be5ac717e39c105936d', 'OP002', '装配第二工序', '0', 'admin', '2018-07-08 19:48:58', '1', 'normal', null, 'A', '0');
INSERT INTO `mds_operation` VALUES ('cbe81d11aa69425698672f77dd5582ca', 'TEST003', '测试工序', '0', 'admin', '2018-07-08 19:49:39', '1', 'test', null, 'A', '0');
INSERT INTO `mds_operation` VALUES ('cd1e8884810b4b59a7920d71d94a30c2', 'OP001', 'OP001', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'test', '2018-07-02 11:46:51', '1', 'normal', null, 'A', '0');

-- ----------------------------
-- Table structure for mds_operation_pod
-- ----------------------------
DROP TABLE IF EXISTS `mds_operation_pod`;
CREATE TABLE `mds_operation_pod` (
  `operation_no` varchar(255) NOT NULL COMMENT '操作',
  `pod_button_no` varchar(255) NOT NULL COMMENT 'POD功能按钮',
  PRIMARY KEY (`operation_no`,`pod_button_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_operation_pod
-- ----------------------------
INSERT INTO `mds_operation_pod` VALUES ('100', '25');
INSERT INTO `mds_operation_pod` VALUES ('100', '26');
INSERT INTO `mds_operation_pod` VALUES ('100', '27');
INSERT INTO `mds_operation_pod` VALUES ('101', '25');
INSERT INTO `mds_operation_pod` VALUES ('101', '26');
INSERT INTO `mds_operation_pod` VALUES ('101', '27');
INSERT INTO `mds_operation_pod` VALUES ('102', '25');
INSERT INTO `mds_operation_pod` VALUES ('102', '26');
INSERT INTO `mds_operation_pod` VALUES ('102', '27');
INSERT INTO `mds_operation_pod` VALUES ('103', '25');
INSERT INTO `mds_operation_pod` VALUES ('103', '26');
INSERT INTO `mds_operation_pod` VALUES ('104', '25');
INSERT INTO `mds_operation_pod` VALUES ('104', '26');
INSERT INTO `mds_operation_pod` VALUES ('104', '27');
INSERT INTO `mds_operation_pod` VALUES ('105', '25');
INSERT INTO `mds_operation_pod` VALUES ('105', '26');
INSERT INTO `mds_operation_pod` VALUES ('105', '27');
INSERT INTO `mds_operation_pod` VALUES ('105', '28');
INSERT INTO `mds_operation_pod` VALUES ('106', '25');
INSERT INTO `mds_operation_pod` VALUES ('106', '26');
INSERT INTO `mds_operation_pod` VALUES ('107', '25');
INSERT INTO `mds_operation_pod` VALUES ('107', '26');
INSERT INTO `mds_operation_pod` VALUES ('107', '27');
INSERT INTO `mds_operation_pod` VALUES ('108', '25');
INSERT INTO `mds_operation_pod` VALUES ('108', '26');
INSERT INTO `mds_operation_pod` VALUES ('108', '28');
INSERT INTO `mds_operation_pod` VALUES ('90', '25');
INSERT INTO `mds_operation_pod` VALUES ('90', '26');
INSERT INTO `mds_operation_pod` VALUES ('90', '27');
INSERT INTO `mds_operation_pod` VALUES ('90', '28');
INSERT INTO `mds_operation_pod` VALUES ('99', '25');
INSERT INTO `mds_operation_pod` VALUES ('99', '26');

-- ----------------------------
-- Table structure for mds_page_save
-- ----------------------------
DROP TABLE IF EXISTS `mds_page_save`;
CREATE TABLE `mds_page_save` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `page_name` varchar(255) NOT NULL COMMENT '页面名称',
  `page_res_id` varchar(64) NOT NULL COMMENT '页面对应的资源id',
  `page_file_jsp_path` varchar(255) DEFAULT NULL COMMENT '生成jsp文件的路径',
  `page_file_js_path` varchar(255) DEFAULT NULL COMMENT '生成js文件的路径',
  `page_file_created` int(11) NOT NULL COMMENT '是否生成过页面文件',
  `page_source` text NOT NULL COMMENT '保存的页面源数据',
  `create_user` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL,
  `page_source_in_draw` text NOT NULL COMMENT '保存的画布内的页面源数据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_page_save
-- ----------------------------
INSERT INTO `mds_page_save` VALUES ('09a604d3d32b4ea7873074b768c44174', 'testName2', 'test2', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><link rel=\"stylesheet\" href=\"css/jquery_ui/jquery-ui.min.css\"><script src=\"js/jquery/jquery-3.2.1.min.js\"></script><script src=\"js/jquery_ui/jquery-ui.min.js\"></script><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form ui-droppable\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li></ul><div id=\"content\"><div id=\"tab1\"><div class=\"inputForm_content ui-droppable\">tab1<div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div></div></div></form></body></html>', 'admin', '2017-12-12 10:26:00', '');
INSERT INTO `mds_page_save` VALUES ('0ef810ea3faf4b77922da4f5cf7d4d9d', 'testName', '4becb66dc41c4de9be2b264f2622114d', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form ui-droppable\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"测试按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>菜单:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">基本信息</a></li></ul><div id=\"content\"><div id=\"tab1\" style=\"\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>呵呵:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>哈哈:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div></div></form></body></html>', 'admin', '2017-12-13 15:08:17', '<form class=\"mds_tab_form ui-droppable\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"测试按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>菜单:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">基本信息</a></li></ul><div id=\"content\"><div id=\"tab1\" style=\"\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>呵呵:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>哈哈:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('4305935f12dd4b4094939d369eb6beb2', '1', '1234', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><link rel=\"stylesheet\" href=\"css/jquery_ui/jquery-ui.min.css\"><script src=\"js/jquery/jquery-3.2.1.min.js\"></script><script src=\"js/jquery_ui/jquery-ui.min.js\"></script><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form ui-droppable\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li></ul><div id=\"content\"><div id=\"tab1\"><div class=\"inputForm_content ui-droppable\">tab1<div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div></div></form></body></html>', 'admin', '2017-12-12 11:10:47', '');
INSERT INTO `mds_page_save` VALUES ('482c9e6622bf4985a5ed2c988a4bb51d', 'test001', 'ec3e2ad1910d4b3d916672f06f5fbede', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form\" style=\"z-index: 0;\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn_search\" value=\"检索\">&nbsp;<input type=\"button\" id=\"btn_save\" value=\"保存\">&nbsp;<input type=\"button\" id=\"btn_clean\" value=\"清除\">&nbsp;<input type=\"button\" id=\"btn_del\" value=\"删除\">&nbsp;<input type=\"button\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div><ul id=\"tabs\" class=\"tabs_li_2tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">基础信息</a></li><li id=\"\"><a href=\"#\" tabid=\"tab2\">其他信息</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"text\" class=\"form_version\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><select><option>是</option></select></div></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody><tr><th>col1</th><th>col2</th></tr></tbody></table></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"text\" class=\"form_version\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div></div></form></body></html>', 'admin', '2017-12-13 17:19:09', '<form class=\"mds_tab_form\" style=\"z-index: 0;\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn_search\" value=\"检索\">&nbsp;<input type=\"button\" id=\"btn_save\" value=\"保存\">&nbsp;<input type=\"button\" id=\"btn_clean\" value=\"清除\">&nbsp;<input type=\"button\" id=\"btn_del\" value=\"删除\">&nbsp;<input type=\"button\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div><ul id=\"tabs\" class=\"tabs_li_2tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">基础信息</a></li><li id=\"\"><a href=\"#\" tabid=\"tab2\">其他信息</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"text\" class=\"form_version\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><select><option>是</option></select></div></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody><tr><th>col1</th><th>col2</th></tr></tbody></table></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"text\" class=\"form_version\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('68f1665116d041a593637d413632cf91', '1235', '0b7987134a54476b84bae196faa05a2g', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn_search\" value=\"检索\">&nbsp;<input type=\"button\" id=\"btn_save\" value=\"保存\">&nbsp;<input type=\"button\" id=\"btn_clean\" value=\"清除\">&nbsp;<input type=\"button\" id=\"btn_del\" value=\"删除\">&nbsp;<input type=\"button\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_2tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li><li><a href=\"#\" tabid=\"tab2\">tab2</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\">tab1<div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\">tab2</div></div></div></form></body></html>', 'admin', '2017-12-13 14:41:37', '<form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn_search\" value=\"检索\">&nbsp;<input type=\"button\" id=\"btn_save\" value=\"保存\">&nbsp;<input type=\"button\" id=\"btn_clean\" value=\"清除\">&nbsp;<input type=\"button\" id=\"btn_del\" value=\"删除\">&nbsp;<input type=\"button\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_2tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li><li><a href=\"#\" tabid=\"tab2\">tab2</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\">tab1<div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\">tab2</div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('74b901d4db8b43abb040e33480611d0d', '1234', '0b7987134a54476b84bae196faa05a2g', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><link rel=\"stylesheet\" href=\"css/jquery_ui/jquery-ui.min.css\"><script src=\"js/jquery/jquery-3.2.1.min.js\"></script><script src=\"js/jquery_ui/jquery-ui.min.js\"></script><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form\" style=\"z-index: 0;\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"查询\">&nbsp;<input type=\"button\" id=\"btn\" value=\"保存\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>站点:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_3tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\" class=\"\">基本信息</a></li><li id=\"\"><a href=\"#\" tabid=\"tab2\" class=\"\">替代品</a></li><li id=\"\"><a href=\"#\" tabid=\"tab3\">tab3</a></li></ul><div id=\"content\" class=\"\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody><tr></tr></tbody></table></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"></div></div><div id=\"tab3\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\">tab3</div></div></div></form></body></html>', 'admin', '2017-12-12 19:31:23', '<form class=\"mds_tab_form\" style=\"z-index: 0;\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"查询\">&nbsp;<input type=\"button\" id=\"btn\" value=\"保存\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>站点:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_3tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\" class=\"\">基本信息</a></li><li id=\"\"><a href=\"#\" tabid=\"tab2\" class=\"\">替代品</a></li><li id=\"\"><a href=\"#\" tabid=\"tab3\">tab3</a></li></ul><div id=\"content\" class=\"\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody><tr></tr></tbody></table></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"></div></div><div id=\"tab3\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\">tab3</div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('9def66a592244e3dace2dc5d8dd406b8', 'send_test_import', '920417da21ad40b9bcd8f7a0a1f0d9c6', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8\" pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script></head><body><form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" value=\"导入\">&nbsp;<input type=\"button\" value=\"清除\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>送测记录表文件：</label><input type=\"text\">&nbsp;<input type=\"file\" value=\"选择文件\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">文件内容</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody>\n<tr>\n					\n					<th data-value=\"shoporder_no\">序号</th>\n					<th data-value=\"sfc\">送测日期</th>\n					<th data-value=\"sfc_status\">机型</th>\n					<th data-value=\"main_item\">品名</th>\n					<th data-value=\"process_flow_no\">模号</th>\n					<th data-value=\"shoporder_status\">颜色</th>\n					<th data-value=\"workshop_no\">送测部门</th>\n					<th data-value=\"workline_no\">送测人</th>\n					<th data-value=\"shoporder_create_time\">送测数量</th>\n					<th data-value=\"shoporder_create_user\">CPK</th>\n					<th data-value=\"more_info\">FAI</th>\n					<th data-value=\"more_info\">重点尺寸</th>\n					<th data-value=\"more_info\">其它</th>\n					<th data-value=\"more_info\">测量数量</th>\n					<th data-value=\"more_info\">测量总量 </th>\n					<th data-value=\"more_info\">测量员</th>\n					<th data-value=\"more_info\">备注</th>\n					<th data-value=\"more_info\">状态</th>\n					<th data-value=\"more_info\">检测报告</th>\n				</tr>\n				<tr>\n					<td>1</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36/36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td bgcolor=\"green\">已完成</td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">查看报告</a></td>\n				</tr>\n				<tr>\n					<td>2</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">12/36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td bgcolor=\"yellow\">检测中</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>3</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>4</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>5</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>6</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>7</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n</tbody></table></div></div></div></form></body></html>', 'admin', '2018-01-08 14:43:00', '<form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" value=\"导入\">&nbsp;<input type=\"button\" value=\"清除\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>送测记录表文件：</label><input type=\"text\">&nbsp;<input type=\"file\" value=\"选择文件\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">文件内容</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody>\n<tr>\n					\n					<th data-value=\"shoporder_no\">序号</th>\n					<th data-value=\"sfc\">送测日期</th>\n					<th data-value=\"sfc_status\">机型</th>\n					<th data-value=\"main_item\">品名</th>\n					<th data-value=\"process_flow_no\">模号</th>\n					<th data-value=\"shoporder_status\">颜色</th>\n					<th data-value=\"workshop_no\">送测部门</th>\n					<th data-value=\"workline_no\">送测人</th>\n					<th data-value=\"shoporder_create_time\">送测数量</th>\n					<th data-value=\"shoporder_create_user\">CPK</th>\n					<th data-value=\"more_info\">FAI</th>\n					<th data-value=\"more_info\">重点尺寸</th>\n					<th data-value=\"more_info\">其它</th>\n					<th data-value=\"more_info\">测量数量</th>\n					<th data-value=\"more_info\">测量总量 </th>\n					<th data-value=\"more_info\">测量员</th>\n					<th data-value=\"more_info\">备注</th>\n					<th data-value=\"more_info\">状态</th>\n					<th data-value=\"more_info\">检测报告</th>\n				</tr>\n				<tr>\n					<td>1</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36/36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td bgcolor=\"green\">已完成</td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">查看报告</a></td>\n				</tr>\n				<tr>\n					<td>2</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">12/36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td bgcolor=\"yellow\">检测中</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>3</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>4</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>5</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>6</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n				<tr>\n					<td>7</td>\n					<td>12月1日</td>\n					<td>W5</td>\n					<td>底壳</td>\n					<td></td>\n					<td>黑</td>\n					<td>IQC</td>\n					<td>杨镰优</td>\n					<td>5</td>\n					<td></td>\n					<td></td>\n					<td><a style=\"text-decoration: underline;color: #06c;\">36项</a></td>\n					<td></td>\n					<td></td>\n					<td></td>\n					<td>谢桂丽</td>\n					<td></td>\n					<td>待检测</td>\n					<td>暂无报告</td>\n				</tr>\n</tbody></table></div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('ea3c36c6df7a47d6b42874da87856cfd', 'test_resource', '9949fd5c1c7545e3bd6ca41459b5729b', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8\" pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script></head><body><form class=\"mds_tab_form\" style=\"z-index: 0;\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn_search\" value=\"检索\">&nbsp;<input type=\"button\" id=\"btn_save\" value=\"保存\">&nbsp;<input type=\"button\" id=\"btn_clean\" value=\"清除\">&nbsp;<input type=\"button\" id=\"btn_del\" value=\"删除\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>站点:</label><span>$(site)</span></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>资源编号:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div><ul id=\"tabs\" class=\"tabs_li_4tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">基本信息</a></li><li id=\"\"><a href=\"#\" tabid=\"tab2\">自定义数据</a></li><li id=\"\"><a href=\"#\" tabid=\"tab3\">测试</a></li><li id=\"\"><a href=\"#\" tabid=\"tab4\">这</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable ui-draggable-dragging\" style=\"position: relative;\"><label>描述:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>资源类型:</label><select><option>是</option></select></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>状态:</label><select><option>是</option></select></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>工作中心:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>创建人:</label><span></span></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>创建时间:</label><span></span></div></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody><tr><th>数据字段</th><th>值</th></tr></tbody></table></div></div><div id=\"tab3\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"></div></div><div id=\"tab4\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"></div></div></div></form></body></html>', 'admin', '2017-12-18 10:23:43', '<form class=\"mds_tab_form\" style=\"z-index: 0;\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn_search\" value=\"检索\">&nbsp;<input type=\"button\" id=\"btn_save\" value=\"保存\">&nbsp;<input type=\"button\" id=\"btn_clean\" value=\"清除\">&nbsp;<input type=\"button\" id=\"btn_del\" value=\"删除\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>站点:</label><span>$(site)</span></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>资源编号:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div><ul id=\"tabs\" class=\"tabs_li_4tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">基本信息</a></li><li id=\"\"><a href=\"#\" tabid=\"tab2\">自定义数据</a></li><li id=\"\"><a href=\"#\" tabid=\"tab3\">测试</a></li><li id=\"\"><a href=\"#\" tabid=\"tab4\">这</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable ui-draggable-dragging\" style=\"position: relative;\"><label>描述:</label><input type=\"text\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>资源类型:</label><select><option>是</option></select></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>状态:</label><select><option>是</option></select></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>工作中心:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>创建人:</label><span></span></div><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><label>创建时间:</label><span></span></div></div></div><div id=\"tab2\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-droppable ui-draggable\" style=\"position: relative;\"><table class=\"mtable\"><tbody><tr><th>数据字段</th><th>值</th></tr></tbody></table></div></div><div id=\"tab3\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"></div></div><div id=\"tab4\" class=\"ui-droppable\" style=\"min-height: 200px; display: none;\"><div class=\"inputForm_content ui-droppable\"></div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('ee0b1c1302dd4bedb015730f81b6ff77', '1232', '', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><link rel=\"stylesheet\" href=\"css/jquery_ui/jquery-ui.min.css\"><script src=\"js/jquery/jquery-3.2.1.min.js\"></script><script src=\"js/jquery_ui/jquery-ui.min.js\"></script><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\">tab1</div></div></div></form></body></html>', 'admin', '2017-12-12 17:00:03', '<form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\">tab1</div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('f20d17ff3d9f4617b5e672e119544baa', '123', '', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><link rel=\"stylesheet\" href=\"css/jquery_ui/jquery-ui.min.css\"><script src=\"js/jquery/jquery-3.2.1.min.js\"></script><script src=\"js/jquery_ui/jquery-ui.min.js\"></script><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\">tab1</div></div></div></form></body></html>', 'admin', '2017-12-12 16:48:44', '<form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\" style=\"min-height: 200px;\"><div class=\"inputForm_content ui-droppable\">tab1</div></div></div></form>');
INSERT INTO `mds_page_save` VALUES ('fb52b4914fcb4f2f9a706cdc7c55583e', '1111', '0b7987134a54476b84bae196faa05a2f', null, null, '-1', '<%@ page language=\"java\" contentType=\"text/html; charset=utf-8pageEncoding=\"utf-8\"%><%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%><!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>功能页面</title><link rel=\"stylesheet\" href=\"css/jquery_ui/jquery-ui.min.css\"><script src=\"js/jquery/jquery-3.2.1.min.js\"></script><script src=\"js/jquery_ui/jquery-ui.min.js\"></script><script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script><script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>\"</head><body><form class=\"mds_tab_form\"><div id=\"commanButtonsDiv\" class=\"iemes_style_commanButtonsDiv ui-droppable\"> <input type=\"button\" id=\"btn\" value=\"按钮\">&nbsp;</div><div class=\"inputForm_query ui-droppable\"><div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\"></div></div><ul id=\"tabs\" class=\"tabs_li_1tabs\"><li id=\"current\"><a href=\"#\" tabid=\"tab1\">tab1</a></li></ul><div id=\"content\"><div id=\"tab1\" class=\"ui-droppable\"><div class=\"inputForm_content ui-droppable\">tab1<div class=\"formField_query ui_widget_content ui-draggable-dragging ui-draggable\" style=\"position: relative;\"><label>标签:</label><input type=\"text\">&nbsp;<input type=\"button\" submit=\"N\" value=\"检索\" onclick=\"operationBrowse(this)\"></div></div></div></div></form></body></html>', 'admin', '2017-12-12 11:48:23', '');

-- ----------------------------
-- Table structure for mds_panel_button
-- ----------------------------
DROP TABLE IF EXISTS `mds_panel_button`;
CREATE TABLE `mds_panel_button` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `pod_panel_button_id` varchar(64) NOT NULL COMMENT 'POD按钮ID',
  `pod_panel_id` varchar(64) NOT NULL COMMENT 'POD面板ID',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `pod_panel_button_level` varchar(64) DEFAULT NULL COMMENT 'POD按钮顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_panel_button
-- ----------------------------
INSERT INTO `mds_panel_button` VALUES ('0428be736c8d473bbb2925eb9fd44325', 'acf27ca03c21421cad2857f370e30379', '4c3571295a3e460e9bb6bdfcabf762ea', '6d70fed36bd94bdab1a71cde63954fa6', '10');
INSERT INTO `mds_panel_button` VALUES ('05dc176b4d624783a95a5d9ab9dc0955', 'ea49f6cd1c594a4aa0c3dc0603915880', 'bedca045ce7b42c29661a047df119bfe', '365fe34663c94f5c8d06f8be76b8501d', '40');
INSERT INTO `mds_panel_button` VALUES ('061ab2dac2ac403b8fe0e70c22a7c166', '1f6a428fbe034850bd8e6cd4da001717', '4c3571295a3e460e9bb6bdfcabf762ea', '6d70fed36bd94bdab1a71cde63954fa6', '40');
INSERT INTO `mds_panel_button` VALUES ('091de567c9b145aca001de45a1dcb412', '8a7a556a685049dba261e7b1f0aa012a', '28097bd512b94d8d83e172c22bcbf819', '0', '50');
INSERT INTO `mds_panel_button` VALUES ('1d2a53aa99ee4281af5ed7720d7c3535', 'cb312f502d08413f9d28c8059f5fd553', '61f53ba561704000bc04b9c81e94fb0e', '6d70fed36bd94bdab1a71cde63954fa6', '20');
INSERT INTO `mds_panel_button` VALUES ('1e27902e7a1a481995a1f3490199ae5f', '45c644e69c4446cd84f51017fd11fc68', 'bedca045ce7b42c29661a047df119bfe', '365fe34663c94f5c8d06f8be76b8501d', '50');
INSERT INTO `mds_panel_button` VALUES ('21c07f5ef3284d589a87a6422edc0cd9', '1642786257f4485d9206242784455719', 'bedca045ce7b42c29661a047df119bfe', '365fe34663c94f5c8d06f8be76b8501d', '10');
INSERT INTO `mds_panel_button` VALUES ('2635bee3352845879341dad41de864be', '61389f416c8e492897bf171a33b9c484', 'd354b595857a4174a03eafbf35ad0cd8', 'a67033a4395543adb40a6dcd4c65fbd4', '20');
INSERT INTO `mds_panel_button` VALUES ('275a4539aa1d44bc81f1845f187f9ada', '1642786257f4485d9206242784455719', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '50');
INSERT INTO `mds_panel_button` VALUES ('30e0bb5796b74c7bb6fc76e6672cea64', '61389f416c8e492897bf171a33b9c484', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '10');
INSERT INTO `mds_panel_button` VALUES ('39c545c453c945b2af628348eeaabe45', 'acf27ca03c21421cad2857f370e30379', '61f53ba561704000bc04b9c81e94fb0e', '6d70fed36bd94bdab1a71cde63954fa6', '10');
INSERT INTO `mds_panel_button` VALUES ('3f2b2590cbab4ee2a40737caf247a820', 'f9be4db2c552416288e963fe32694369', '61f53ba561704000bc04b9c81e94fb0e', '6d70fed36bd94bdab1a71cde63954fa6', '40');
INSERT INTO `mds_panel_button` VALUES ('41589dfefbb54ed6b7e1d0cb63bf07d1', 'f117d903564b4824b067b834a6fd737f', '4c3571295a3e460e9bb6bdfcabf762ea', '6d70fed36bd94bdab1a71cde63954fa6', '50');
INSERT INTO `mds_panel_button` VALUES ('42d9e49007eb46ebb2ade838bd24a446', '61389f416c8e492897bf171a33b9c484', 'eb0a14fef16644be97e25169b1076599', '365fe34663c94f5c8d06f8be76b8501d', '20');
INSERT INTO `mds_panel_button` VALUES ('4e216543d7574f6392bf88a713c26c1b', 'd5dc97f9ba964fcb86676a52bae08115', '4c3571295a3e460e9bb6bdfcabf762ea', '6d70fed36bd94bdab1a71cde63954fa6', '20');
INSERT INTO `mds_panel_button` VALUES ('52c5e6ff521b47b38d2512638e0d2ab6', 'f3fc2af6f3de4d11b9beefc4784da36e', 'bedca045ce7b42c29661a047df119bfe', '365fe34663c94f5c8d06f8be76b8501d', '60');
INSERT INTO `mds_panel_button` VALUES ('5c84572ca1694dfca8e06ef452626a2e', '61389f416c8e492897bf171a33b9c484', '28097bd512b94d8d83e172c22bcbf819', '0', '10');
INSERT INTO `mds_panel_button` VALUES ('64587b5309bd48ec8b0902f09277b58c', 'f3fc2af6f3de4d11b9beefc4784da36e', 'eb0a14fef16644be97e25169b1076599', '365fe34663c94f5c8d06f8be76b8501d', '10');
INSERT INTO `mds_panel_button` VALUES ('66c82eddce034331b1763d7c9f52c7b0', 'f3fc2af6f3de4d11b9beefc4784da36e', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '90');
INSERT INTO `mds_panel_button` VALUES ('719d7aa2a5904d7aa6448a9d8490c9f0', 'c9311162e1d44baf887bb2994d93acef', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '100');
INSERT INTO `mds_panel_button` VALUES ('758af3cf94764f569fa9df3474ac829f', '61389f416c8e492897bf171a33b9c484', 'bedca045ce7b42c29661a047df119bfe', '365fe34663c94f5c8d06f8be76b8501d', '20');
INSERT INTO `mds_panel_button` VALUES ('7bba0c1a5d134daabde665d840aeeac8', 'f8675caaecb046d6b7d9bd603a9e655c', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '80');
INSERT INTO `mds_panel_button` VALUES ('8146ee09e71b4ed28684ca9a1adc12f3', 'eab8a81c636c4774a124060e34802feb', '61f53ba561704000bc04b9c81e94fb0e', '6d70fed36bd94bdab1a71cde63954fa6', '30');
INSERT INTO `mds_panel_button` VALUES ('81fdebf4f2cb4b1e80c3a88f914cdb2b', '3ebc64b4ab44489bb992b3deacf1badf', 'f17d6f64bab74039951a380ff3b6f1bd', 'd31fe95b88804806b6cff4083090afd3', '10');
INSERT INTO `mds_panel_button` VALUES ('85dd533676d54a3f8bc5ebce35e23586', 'ea49f6cd1c594a4aa0c3dc0603915880', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '30');
INSERT INTO `mds_panel_button` VALUES ('86b333a4bd5744d0bfda0f1a7a9c1d3c', 'f3fc2af6f3de4d11b9beefc4784da36e', 'd354b595857a4174a03eafbf35ad0cd8', 'a67033a4395543adb40a6dcd4c65fbd4', '10');
INSERT INTO `mds_panel_button` VALUES ('93d6228be1164daca5f87199be0ffcfd', 'c9311162e1d44baf887bb2994d93acef', 'eb0a14fef16644be97e25169b1076599', '365fe34663c94f5c8d06f8be76b8501d', '50');
INSERT INTO `mds_panel_button` VALUES ('9a6ce891de774497a472c271424f2290', '45c644e69c4446cd84f51017fd11fc68', '28097bd512b94d8d83e172c22bcbf819', '0', '30');
INSERT INTO `mds_panel_button` VALUES ('a3623e6d98544a6f98adb27d8ff89288', '1642786257f4485d9206242784455719', 'd354b595857a4174a03eafbf35ad0cd8', 'a67033a4395543adb40a6dcd4c65fbd4', '60');
INSERT INTO `mds_panel_button` VALUES ('a5aca059848b46959889a5e2b172e632', '44ada4c988b2462a9daf60a6eb8f2aac', 'f17d6f64bab74039951a380ff3b6f1bd', 'd31fe95b88804806b6cff4083090afd3', '30');
INSERT INTO `mds_panel_button` VALUES ('a847fdceb0b9400c96dcb975957cc990', 'c9311162e1d44baf887bb2994d93acef', '28097bd512b94d8d83e172c22bcbf819', '0', '40');
INSERT INTO `mds_panel_button` VALUES ('aa1b5bd9362d433db726e7942493f65d', '0d241ba1de784c7eb4ca1311b5aca1a2', '4c3571295a3e460e9bb6bdfcabf762ea', '6d70fed36bd94bdab1a71cde63954fa6', '60');
INSERT INTO `mds_panel_button` VALUES ('ac2f00d6aa8542efa5cb1f22734eae54', '45c644e69c4446cd84f51017fd11fc68', 'd354b595857a4174a03eafbf35ad0cd8', 'a67033a4395543adb40a6dcd4c65fbd4', '50');
INSERT INTO `mds_panel_button` VALUES ('ad3e60d814ab4034ba02af40aaecd7fa', 'f8675caaecb046d6b7d9bd603a9e655c', '28097bd512b94d8d83e172c22bcbf819', '0', '70');
INSERT INTO `mds_panel_button` VALUES ('afe345e71da04ca08042685e71fe7a6b', 'ea49f6cd1c594a4aa0c3dc0603915880', 'd354b595857a4174a03eafbf35ad0cd8', 'a67033a4395543adb40a6dcd4c65fbd4', '40');
INSERT INTO `mds_panel_button` VALUES ('b2ffb3893b364c5c8617ae7d7190500c', 'ea49f6cd1c594a4aa0c3dc0603915880', '28097bd512b94d8d83e172c22bcbf819', '0', '20');
INSERT INTO `mds_panel_button` VALUES ('b8e6a2312a014a7cadcb0c9fd6af53ed', 'f8675caaecb046d6b7d9bd603a9e655c', 'bedca045ce7b42c29661a047df119bfe', '365fe34663c94f5c8d06f8be76b8501d', '30');
INSERT INTO `mds_panel_button` VALUES ('c6c5e48b656e499eb43186f4f560b6c8', '45c644e69c4446cd84f51017fd11fc68', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '40');
INSERT INTO `mds_panel_button` VALUES ('d7b0a9c2fe6145eb82500e49c924dccb', 'cb312f502d08413f9d28c8059f5fd553', '4c3571295a3e460e9bb6bdfcabf762ea', '6d70fed36bd94bdab1a71cde63954fa6', '30');
INSERT INTO `mds_panel_button` VALUES ('d9ac9450742942caa3040b14dd12f8b8', 'f8675caaecb046d6b7d9bd603a9e655c', 'd354b595857a4174a03eafbf35ad0cd8', 'a67033a4395543adb40a6dcd4c65fbd4', '30');
INSERT INTO `mds_panel_button` VALUES ('dca4f81c17eb4f8b973e0c7a0a143b6d', 'f8675caaecb046d6b7d9bd603a9e655c', 'eb0a14fef16644be97e25169b1076599', '365fe34663c94f5c8d06f8be76b8501d', '30');
INSERT INTO `mds_panel_button` VALUES ('eaf60c9f23174a15b54f60af4f2d398e', '1642786257f4485d9206242784455719', '28097bd512b94d8d83e172c22bcbf819', '0', '60');
INSERT INTO `mds_panel_button` VALUES ('fb5074e72a3b41a681fd38a531aae359', '8a7a556a685049dba261e7b1f0aa012a', 'ffc99a602cd34360b12ecbbc2868b833', '4db61d7d29ed4c0c99b4580c22cc5bcf', '60');
INSERT INTO `mds_panel_button` VALUES ('fb8666376e8048ae81d27a09f6c32429', '8a7a556a685049dba261e7b1f0aa012a', 'eb0a14fef16644be97e25169b1076599', '365fe34663c94f5c8d06f8be76b8501d', '40');
INSERT INTO `mds_panel_button` VALUES ('fcab9040c9914833ab8eaef3d6a5a091', '1e946cd02eec4f9a83c3db2f413dd56e', 'f17d6f64bab74039951a380ff3b6f1bd', 'd31fe95b88804806b6cff4083090afd3', '20');

-- ----------------------------
-- Table structure for mds_pod_button
-- ----------------------------
DROP TABLE IF EXISTS `mds_pod_button`;
CREATE TABLE `mds_pod_button` (
  `id` varchar(64) NOT NULL,
  `pod_button_no` varchar(255) NOT NULL COMMENT 'POD按钮编号',
  `pod_button_name` varchar(255) NOT NULL COMMENT 'POD按钮名称',
  `pod_function` varchar(255) NOT NULL COMMENT 'POD按钮方法地址（POD功能的ID）',
  `pod_icon` varchar(255) DEFAULT NULL COMMENT 'POD按钮样式',
  `site_id` varchar(255) NOT NULL COMMENT '站点',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_pod_button
-- ----------------------------
INSERT INTO `mds_pod_button` VALUES ('1642786257f4485d9206242784455719', 'pod_bindingshoporder', '绑定工单', 'fbf5388e4af9475eb63f973d06a72d6a', null, '0', 'admin', '2017-11-06 17:29:49');
INSERT INTO `mds_pod_button` VALUES ('45c644e69c4446cd84f51017fd11fc68', 'pod_nocode', '记录不良', '95c7234b06c64f50b02c77dbc3eef523', null, '0', 'admin', '2017-11-06 17:29:23');
INSERT INTO `mds_pod_button` VALUES ('61389f416c8e492897bf171a33b9c484', 'pod_start', '开始', '501508ab9e0246a880c95765b5b478b5', null, '0', 'admin', '2017-11-06 17:27:37');
INSERT INTO `mds_pod_button` VALUES ('8a7a556a685049dba261e7b1f0aa012a', 'pod_scrap', '报废', '193c78e5384e480094d587c60fdb05d8', null, '0', 'admin', '2017-11-06 17:30:14');
INSERT INTO `mds_pod_button` VALUES ('c9311162e1d44baf887bb2994d93acef', 'pod_repair', '维修', '566394fcc10a4e41be7767f4ab51a63a', null, '0', 'admin', '2017-11-06 17:30:27');
INSERT INTO `mds_pod_button` VALUES ('ea49f6cd1c594a4aa0c3dc0603915880', 'pod_assemble', '装配', '7ea13e59226a4dd6b3137de92d3d012e', null, '0', 'admin', '2017-11-06 17:29:05');
INSERT INTO `mds_pod_button` VALUES ('f3fc2af6f3de4d11b9beefc4784da36e', 'pod_refresh', '刷新', '095c5809d57f4d47b0c439210519e725', null, '0', 'admin', '2017-11-06 17:28:30');
INSERT INTO `mds_pod_button` VALUES ('f8675caaecb046d6b7d9bd603a9e655c', 'pod_finish', '完成', '6fcb9a0607284ae8b23edd7dc7e0b4fe', null, '0', 'admin', '2017-11-06 17:27:55');

-- ----------------------------
-- Table structure for mds_pod_function
-- ----------------------------
DROP TABLE IF EXISTS `mds_pod_function`;
CREATE TABLE `mds_pod_function` (
  `id` varchar(64) NOT NULL,
  `pod_function_no` varchar(255) NOT NULL COMMENT 'POD功能NO',
  `pod_function_name` varchar(255) NOT NULL COMMENT 'POD功能名称',
  `pod_function_url` varchar(255) NOT NULL COMMENT 'PDD资源路径',
  `site_id` varchar(64) NOT NULL COMMENT '站点Id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_pod_function
-- ----------------------------
INSERT INTO `mds_pod_function` VALUES ('095c5809d57f4d47b0c439210519e725', 'pod_refresh', '刷新功能', 'pod_refresh', '0', '2017-10-19 20:57:09', 'zs');
INSERT INTO `mds_pod_function` VALUES ('193c78e5384e480094d587c60fdb05d8', 'scrapfun', '报废方法', 'scrapfun', '0', '2017-10-23 19:00:59', 'admin');
INSERT INTO `mds_pod_function` VALUES ('501508ab9e0246a880c95765b5b478b5', 'startfun', '开始功能', 'startfun', '0', '2017-11-01 21:01:30', 'admin');
INSERT INTO `mds_pod_function` VALUES ('566394fcc10a4e41be7767f4ab51a63a', 'repairfun', '维修方法', 'repairfun', '0', '2017-10-23 18:56:42', 'admin');
INSERT INTO `mds_pod_function` VALUES ('6fcb9a0607284ae8b23edd7dc7e0b4fe', 'finishfun', '完成功能', 'finishfun', '0', '2017-11-01 21:01:40', 'admin');
INSERT INTO `mds_pod_function` VALUES ('7ea13e59226a4dd6b3137de92d3d012e', 'assemblefun', '装配功能', 'assemblefun', '0', '2017-11-01 21:01:58', 'admin');
INSERT INTO `mds_pod_function` VALUES ('95c7234b06c64f50b02c77dbc3eef523', 'nocodefun', '记录不良功能', 'nocodefun', '0', '2017-11-01 21:02:05', 'admin');
INSERT INTO `mds_pod_function` VALUES ('fbf5388e4af9475eb63f973d06a72d6a', 'bindingshoporder', '绑定工单功能', 'bindingshoporder', '0', '2017-11-06 17:30:53', 'admin');

-- ----------------------------
-- Table structure for mds_pod_panel
-- ----------------------------
DROP TABLE IF EXISTS `mds_pod_panel`;
CREATE TABLE `mds_pod_panel` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `pod_panel_no` varchar(64) NOT NULL COMMENT 'POD面板编号',
  `pod_panel_name` varchar(255) NOT NULL COMMENT 'pod面板名称',
  `pod_panel_desc` varchar(255) DEFAULT NULL COMMENT 'pod面板描述',
  `status` int(1) NOT NULL COMMENT '状态：1、启用；2、禁用',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `default_operation` varchar(64) DEFAULT NULL COMMENT '默认操作ID',
  `default_resource` varchar(64) DEFAULT NULL COMMENT '默认资源ID',
  `changeOperation` int(1) DEFAULT NULL COMMENT '是否可以修改操作：1、允许；2、不允许',
  `changeResource` int(1) DEFAULT NULL COMMENT '是否可以修改资源：1、允许；2、不允许',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_pod_panel
-- ----------------------------
INSERT INTO `mds_pod_panel` VALUES ('28097bd512b94d8d83e172c22bcbf819', '测试面板', '测试', null, '1', 'admin', '2018-07-07 22:22:14', null, null, '1', '1', '0');
INSERT INTO `mds_pod_panel` VALUES ('4c3571295a3e460e9bb6bdfcabf762ea', 'workpanel', '生产面板', '生产面板', '1', 'admin', '2017-10-24 10:36:08', '49efc9bb14fb42728ebda76e68b5536d', '950d2c8610d44947bd55b74884312475', '1', '1', '6d70fed36bd94bdab1a71cde63954fa6');
INSERT INTO `mds_pod_panel` VALUES ('4d1f54dbf1da437fbe958da03695ed53', 'p2', 'ae', 'aeee', '1', 'admin', '2017-11-03 14:27:32', null, null, '1', '1', '6d70fed36bd94bdab1a71cde63954fa6');
INSERT INTO `mds_pod_panel` VALUES ('61f53ba561704000bc04b9c81e94fb0e', 'repair', '维修面板', '维修面板', '1', 'admin', '2017-10-24 10:37:09', '3aba6a9d08484b01826628359dced8fd', 'c2b8110a5dc84c44ab77f9073e19840b', '1', '1', '6d70fed36bd94bdab1a71cde63954fa6');
INSERT INTO `mds_pod_panel` VALUES ('bedca045ce7b42c29661a047df119bfe', 'pod001', '组装POD', '组装POD面板', '1', 'admin', '2017-11-06 17:34:05', null, null, '1', '1', '365fe34663c94f5c8d06f8be76b8501d');
INSERT INTO `mds_pod_panel` VALUES ('d354b595857a4174a03eafbf35ad0cd8', 'work', '生产面板', '生产面板', '1', 'zoufa', '2017-11-08 10:51:45', '55f7ea3bf3af454b9ab313b4b8134ab8', '1c69e7f308c3461dbef07592142227d2', '1', '1', 'a67033a4395543adb40a6dcd4c65fbd4');
INSERT INTO `mds_pod_panel` VALUES ('eb0a14fef16644be97e25169b1076599', 'weixiu', '维修面板', '维修面板界面', '1', 'admin', '2017-11-06 17:34:31', null, null, '1', '1', '365fe34663c94f5c8d06f8be76b8501d');
INSERT INTO `mds_pod_panel` VALUES ('f12ee49876ad43e99935d98a09e7c1cf', 'p1', 'ae', 'aeee', '1', 'admin', '2017-11-03 14:27:24', null, null, '1', '1', '6d70fed36bd94bdab1a71cde63954fa6');
INSERT INTO `mds_pod_panel` VALUES ('f17d6f64bab74039951a380ff3b6f1bd', 'smt01', '生产面板', '生产面板', '1', '123', '2017-10-29 23:04:16', '165cd54e0934466c991599c34ff9822e', '4fd41655351a477d89f26dce6205e277', '1', '1', 'd31fe95b88804806b6cff4083090afd3');
INSERT INTO `mds_pod_panel` VALUES ('ffc99a602cd34360b12ecbbc2868b833', 'POD001', 'POD001', 'POD001', '1', 'admin', '2018-07-07 17:14:59', null, null, '1', '1', '4db61d7d29ed4c0c99b4580c22cc5bcf');

-- ----------------------------
-- Table structure for mds_process_workflow
-- ----------------------------
DROP TABLE IF EXISTS `mds_process_workflow`;
CREATE TABLE `mds_process_workflow` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `process_workflow` varchar(255) NOT NULL COMMENT '工艺路线',
  `process_workflow_type` varchar(255) DEFAULT NULL COMMENT '工艺路线类型',
  `process_workflow_desc` varchar(255) DEFAULT NULL COMMENT '工艺路线描述',
  `status` int(1) NOT NULL COMMENT '状态',
  `allowpass` int(1) NOT NULL COMMENT '是否允许跳过：1、允许；2、不允许',
  `data` longtext NOT NULL COMMENT '工艺路线数据',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(255) DEFAULT NULL COMMENT '修改人',
  `process_workflow_version` varchar(255) NOT NULL COMMENT '工艺路线版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_process_workflow
-- ----------------------------
INSERT INTO `mds_process_workflow` VALUES ('08266e4cea7148e4ad945c7541bc8a8d', 'TEST', 'normal', 'TEST', '1', '1', '{\"nodes\":[{\"no\":\"开始\",\"id\":\"1\",\"left\":170,\"top\":74,\"zIndex\":\"4\",\"className\":\"ui_widget_content other ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"from\",\"id\":1530956969729}]},{\"no\":\"OP001\",\"id\":\"cd1e8884810b4b59a7920d71d94a30c2\",\"left\":176,\"top\":174,\"zIndex\":\"4\",\"className\":\"ui_widget_content ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"to\",\"id\":1530956969729},{\"beari\":\"from\",\"id\":1530956971455}]},{\"no\":\"OP002\",\"id\":\"2e17d8469fb44369a5f2c511b51f1ee2\",\"left\":178,\"top\":275,\"zIndex\":\"4\",\"className\":\"ui_widget_content ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"to\",\"id\":1530956971455},{\"beari\":\"from\",\"id\":1530956973447}]},{\"no\":\"结束\",\"id\":\"-1\",\"left\":\"180px\",\"top\":\"365px\",\"zIndex\":\"4\",\"className\":\"ui_widget_content other ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"to\",\"id\":1530956973447}]}],\"lines\":[{\"id\":1530956969729,\"x1\":273,\"x2\":273,\"y1\":104,\"y2\":161},{\"id\":1530956971455,\"x1\":277,\"x2\":277,\"y1\":204,\"y2\":262},{\"id\":1530956973447,\"x1\":279,\"x2\":279,\"y1\":305,\"y2\":352}],\"nexus\":[{\"from\":\"开始\",\"fromId\":\"1\",\"to\":\"OP001\",\"toId\":\"cd1e8884810b4b59a7920d71d94a30c2\",\"line\":1530956969729},{\"from\":\"OP001\",\"fromId\":\"cd1e8884810b4b59a7920d71d94a30c2\",\"to\":\"OP002\",\"toId\":\"2e17d8469fb44369a5f2c511b51f1ee2\",\"line\":1530956971455},{\"from\":\"OP002\",\"fromId\":\"2e17d8469fb44369a5f2c511b51f1ee2\",\"to\":\"结束\",\"toId\":\"-1\",\"line\":1530956973447}]}', '4db61d7d29ed4c0c99b4580c22cc5bcf', '2018-07-07 17:49:35', 'admin', null, null, 'A');
INSERT INTO `mds_process_workflow` VALUES ('905f98228bfb440482d3bee89b139617', 'TEST', 'normal', '组装工艺', '1', '1', '{\"nodes\":[{\"no\":\"开始\",\"id\":\"1\",\"left\":328,\"top\":85,\"zIndex\":\"4\",\"className\":\"ui_widget_content other ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"from\",\"id\":1531051063450}]},{\"no\":\"OP001\",\"id\":\"004f116b4dff48b5a013b56736af7860\",\"left\":333,\"top\":189,\"zIndex\":\"4\",\"className\":\"ui_widget_content ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"to\",\"id\":1531051063450},{\"beari\":\"from\",\"id\":1531051065195},{\"beari\":\"from\",\"id\":1531054346951},{\"beari\":\"to\",\"id\":1531054358123}]},{\"no\":\"OP002\",\"id\":\"adaf2e8c2a314be5ac717e39c105936d\",\"left\":334,\"top\":308,\"zIndex\":\"4\",\"className\":\"ui_widget_content ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"to\",\"id\":1531051065195},{\"beari\":\"from\",\"id\":1531051066797}]},{\"no\":\"结束\",\"id\":\"-1\",\"left\":342,\"top\":436,\"zIndex\":\"4\",\"className\":\"ui_widget_content other ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"to\",\"id\":1531051066797}]},{\"no\":\"WX004\",\"id\":\"01565f643509455498d6a00e65fbde51\",\"left\":576,\"top\":243,\"zIndex\":\"4\",\"className\":\"ui_widget_content repair ui-draggable-dragging ui-draggable\",\"lines\":[{\"beari\":\"to\",\"id\":1531054346951},{\"beari\":\"from\",\"id\":1531054358123}]}],\"lines\":[{\"id\":1531051063450,\"x1\":430.5,\"x2\":430.5,\"y1\":115,\"y2\":176},{\"id\":1531051065195,\"x1\":433.5,\"x2\":433.5,\"y1\":219,\"y2\":295},{\"id\":1531051066797,\"x1\":438,\"x2\":438,\"y1\":338,\"y2\":426},{\"id\":1531054346951,\"x1\":533,\"x2\":576,\"y1\":219,\"y2\":233},{\"id\":1531054358123,\"x1\":576,\"x2\":538,\"y1\":243,\"y2\":221}],\"nexus\":[{\"from\":\"开始\",\"fromId\":\"1\",\"to\":\"OP001\",\"toId\":\"004f116b4dff48b5a013b56736af7860\",\"line\":1531051063450},{\"from\":\"OP001\",\"fromId\":\"004f116b4dff48b5a013b56736af7860\",\"to\":\"OP002\",\"toId\":\"adaf2e8c2a314be5ac717e39c105936d\",\"line\":1531051065195},{\"from\":\"OP002\",\"fromId\":\"adaf2e8c2a314be5ac717e39c105936d\",\"to\":\"结束\",\"toId\":\"-1\",\"line\":1531051066797},{\"from\":\"OP001\",\"fromId\":\"004f116b4dff48b5a013b56736af7860\",\"to\":\"WX004\",\"toId\":\"01565f643509455498d6a00e65fbde51\",\"line\":1531054346951},{\"from\":\"WX004\",\"fromId\":\"01565f643509455498d6a00e65fbde51\",\"to\":\"OP001\",\"toId\":\"004f116b4dff48b5a013b56736af7860\",\"line\":1531054358123}]}', '0', '2018-07-08 20:52:40', 'admin', null, null, 'A');

-- ----------------------------
-- Table structure for mds_resources
-- ----------------------------
DROP TABLE IF EXISTS `mds_resources`;
CREATE TABLE `mds_resources` (
  `id` varchar(64) NOT NULL,
  `res_name` varchar(50) DEFAULT NULL COMMENT '资源名称',
  `parent_id` varchar(64) DEFAULT NULL COMMENT '上层目录',
  `res_key` varchar(50) DEFAULT NULL COMMENT '资源关键字',
  `type` varchar(40) DEFAULT NULL COMMENT '类型：0：菜单；1：目录；2：按钮',
  `res_url` varchar(200) DEFAULT NULL COMMENT 'url',
  `level` int(4) DEFAULT NULL COMMENT '层级，用于对菜单的位置排序',
  `icon` varchar(100) DEFAULT '""' COMMENT '图标',
  `is_hide` int(3) DEFAULT '0' COMMENT '是否隐藏:-1:不隐藏；1：隐藏',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_user` varchar(50) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_resources
-- ----------------------------
INSERT INTO `mds_resources` VALUES ('0418eae6ccc2422f9f2f345cb55baeb3', '资源维护', '37', 'work_resource', '1', '/workcenter_model/work_resource/work_resource_maintenance.shtml', '4', null, '-1', '资源维护', 'admin', '2017-09-25 09:24:38');
INSERT INTO `mds_resources` VALUES ('0418eae6ccc2422f9f2f345cb55baeb4', '检索', '57', 'site_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 11:24:45');
INSERT INTO `mds_resources` VALUES ('08070d9f91824eb087bc9c9f022a6bc5', '删除', '50', 'workcenter_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-07 11:39:00');
INSERT INTO `mds_resources` VALUES ('08504ae6679048938c97163a8beaec90', '删除', '176', 'work_resource_type_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 11:30:52');
INSERT INTO `mds_resources` VALUES ('094eb5ab98b74b4193eaa571e0b83927', '保存', '53', 'process_workflow_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 14:31:29');
INSERT INTO `mds_resources` VALUES ('1', '系统基础管理', '0', 'system', '0', 'system', '1', 'fa fa-gear', '-1', '系统基础管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('130', '生产操作员面板-功能维护', '39', 'pod_function_maintenance', '1', '/work_process/pod_function/pod_function_maintenance.shtml', '1', null, '-1', 'pod功能维护菜单', 'admin', '2017-10-14 11:42:49');
INSERT INTO `mds_resources` VALUES ('134', '生产操作员面板-按钮维护', '39', 'pod_button_maintenance', '1', '/work_process/pod_button/pod_button_maintenance.shtml', '2', null, '-1', 'pod按钮维护', 'admin', '2017-10-14 11:48:51');
INSERT INTO `mds_resources` VALUES ('151', '生产操作员面板-操作', '39', 'pod_opeartion', '1', '/work_process/pod_operation/pod_operation.shtml', '4', null, '-1', '生产操作员面板-操作', 'admin', '2017-10-16 16:32:21');
INSERT INTO `mds_resources` VALUES ('169', '工单下达', '38', 'shoporder_issuing', '1', '/work_plan/shoporder_issuing/shoporder_issuing.shtml', '2', null, '-1', '工单下达', 'admin', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('174', '生产记录报表', '46', 'prouction_record_report', '1', '/report/production_record_report.shtml', '1', null, '-1', '生产记录报表', 'admin', '2017-10-25 09:31:10');
INSERT INTO `mds_resources` VALUES ('175', 'SFC装配信息追溯报表', '46', 'prouction_record_report', '1', '/report/sfc_assembly_report.shtml', '2', 'scheduler_reportUi', '-1', 'SFC装配信息追溯报表', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('176', '资源类型维护', '37', 'work_resource_type', '1', '/workcenter_model/work_resource_type/work_resource_type_maintenance.shtml', '3', null, '-1', '资源类型维护', '', '2017-09-21 10:41:49');
INSERT INTO `mds_resources` VALUES ('177', '容器类型管理', '37', 'container_manager', '1', '/workcenter_model/container/container_manager.shtml', '12', null, '-1', '容器类型管理', 'admin', '2017-11-09 16:35:54');
INSERT INTO `mds_resources` VALUES ('178', '自定义数据维护', '37', 'user_defined_data_maintenance', '1', '/workcenter_model/user_defined_data/udefined_data_maintenance.shtml', '13', null, '-1', '自定义数据维护', '', '2017-09-21 20:12:36');
INSERT INTO `mds_resources` VALUES ('179', '编号规则管理', '37', 'number_rule_manager', '1', '/workcenter_model/number_rule/number_rule_manager.shtml', '11', null, '-1', '编号规则管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('181', '条码打印', '39', 'shop_order_manager', '1', '/work_process/barcode_printing/barcode_printing.shtml', '5', null, '-1', '条码打印', 'admin', '2017-10-13 11:58:06');
INSERT INTO `mds_resources` VALUES ('182', '生产操作员面板-维护', '39', 'pod_panel_maintenance', '1', '/work_process/pod_panel/pod_panel_maintenance.shtml', '3', null, '-1', '生产操作员面板维护', 'admin', '2017-11-07 17:13:08');
INSERT INTO `mds_resources` VALUES ('183', '生产调度管理', '0', 'work_scheduling_manager', '0', 'work_scheduling_manager', '7', 'fa fa-hourglass-o', '-1', '生产操作员面板维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('184', '车间作业步骤调整', '183', 'work_step_change', '1', '/work_scheduling/work_step_change/work_step_change.shtml', '1', null, '-1', '车间作业步骤调整', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('185', 'SOP管理', '183', 'sop_manager', '1', '/work_scheduling/sop_manager/sop_manager.shtml', '2', null, '-1', 'SOP管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('186', '隔离品管理', '40', 'sop_manager', '1', '/quality/hold_product_manager.shtml', '1', null, '-1', '隔离品管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('187', '产品包装管理', '0', 'packing_manager', '0', 'packing_manager', '8', 'fa fa-life-bouy', '-1', '包装管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('188', '包装管理', '187', 'packing_manager', '1', '/packing/packing_manager.shtml', '1', null, '-1', '包装管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('19264a90df7d431a83dbfae41b9cb44c', '清除', '561', 'CUSTOMER_REJECT_clean', '2', 'btnClear', '3', '\"\"', '-1', '客退单界面清除按钮', 'admin', '2017-11-15 14:33:26');
INSERT INTO `mds_resources` VALUES ('1a3bd2f4070945e4b999d540f92997ff', '保存', '56', 'shoporder_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 17:40:46');
INSERT INTO `mds_resources` VALUES ('2', '用户管理', '1', 'user_manager', '1', '/system/user/list.shtml', '2', null, '-1', null, '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('210d0a230de34423be666a11de0f23c3', '删除', '7499b45af0dd42e187279d8f191ba0c9', 'item_maintenance_btndelete', '2', 'btnDelete', '7', '\"\"', '-1', '物料维护-删除', 'admin', '2018-01-06 11:40:21');
INSERT INTO `mds_resources` VALUES ('21d616905b794b59a7efc3557600d96e', '删除', '561', 'CUSTOMER_REJECT_delete', '2', 'btnDelete', '3', '\"\"', '-1', '客退单界面删除按钮', 'admin', '2017-11-15 14:33:50');
INSERT INTO `mds_resources` VALUES ('22d7058913c8417c9b89fbf6776f510e', '保存', '50', 'workcenter_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-07 11:37:42');
INSERT INTO `mds_resources` VALUES ('23870d1f8fdd4711a6356520894cfba4', '保存', '57', 'site_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-07 11:35:33');
INSERT INTO `mds_resources` VALUES ('24bbe91ddf9043f29d9ee6e71a4df44a', '删除', '51', 'operation_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 11:48:56');
INSERT INTO `mds_resources` VALUES ('25', '登陆信息管理', '0', 'ly_login', '0', 'ly_login', '14', 'fa fa-calendar', '1', '登陆信息管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('26', '用户登录记录', '25', 'ly_log_list', '1', '/userlogin/listUI.shtml', '1', null, '1', '用户登录记录', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('27', '操作日志管理', '0', 'ly_log', '0', 'ly_log', '15', 'fa fa-picture-o', '1', '操作日志管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('28', '日志查询', '27', 'ly_log', '1', '/log/list.shtml', '1', null, '-1', null, '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('2d0f005f17914a4f993f190134bfb1c7', '保存', '178', 'udefined_data_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 17:12:13');
INSERT INTO `mds_resources` VALUES ('2f1b9139a5a941a3a980f431768a05a1', '保存', '177', 'container_manager_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 16:54:43');
INSERT INTO `mds_resources` VALUES ('3', '角色管理', '1', 'role_manager', '1', '/system/role/list.shtml', '1', 'fa-list', '-1', '组管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('37', '车间基础建模', '0', 'base_data', '0', 'base_data', '2', 'fa fa-dashboard', '-1', '车间数据维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('38', '生产计划管理', '0', 'product_plan', '0', 'product_plan', '3', 'fa fa-bars', '-1', '生产计划管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('39', '生产过程管理', '0', 'process_manager', '0', 'process_manager', '4', 'fa fa-home', '-1', '生产过程管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('3a255d4578a048e18453168488f13448', '保存', '88', 'shift_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 17:22:48');
INSERT INTO `mds_resources` VALUES ('3c7a2952248241dabc4030dbe16cbc63', '清除', '2', 'user_manager_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 18:17:21');
INSERT INTO `mds_resources` VALUES ('4', '菜单管理', '1', 'menu_manager', '1', '/system/menu/menu_manager.shtml', '3', 'fa-list-alt', '-1', '菜单管理', '', '2017-09-19 15:43:38');
INSERT INTO `mds_resources` VALUES ('40', '产品质量管理', '0', 'quality', '0', 'quality', '6', 'fa fa-files-o', '1', '产品质量管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('41', '车间库存管理', '0', 'inventory', '0', 'inventory', '5', 'fa fa-inbox', '-1', '车间库存管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('4290b40fc84f4654a6b635ba1d897770', '删除', '56', 'shoporder_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 17:41:59');
INSERT INTO `mds_resources` VALUES ('45', '车间看板管理', '0', 'pad', '0', 'pad', '12', 'fa fa-crosshairs', '-1', '车间看板管理', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('46', '数据分析报表', '0', 'report', '0', 'report', '13', 'fa fa-line-chart', '-1', '数据分析报表', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('465cecd6b5404057878402c4cfe0582a', '保存', '2', 'user_manager_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 18:16:50');
INSERT INTO `mds_resources` VALUES ('4c8b51e70e4246f3b5b2a93c4151bcef', '清除', '177', 'container_manager_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 16:58:32');
INSERT INTO `mds_resources` VALUES ('50', '工作中心维护', '37', 'workcenter', '1', '/workcenter_model/workcenter/workcenter_maintenance.shtml', '2', null, '-1', '工作中心', 'admin', '2017-09-20 20:25:34');
INSERT INTO `mds_resources` VALUES ('51', '操作维护', '37', 'operation_maintenance', '1', '/workcenter_model/operation/operation_maintenance.shtml', '5', null, '-1', '操作维护', 'admin', '2017-09-26 11:12:57');
INSERT INTO `mds_resources` VALUES ('5166d79c2bbe4494b7fdb53202e0d040', '检索', '178', 'udefined_data_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 17:11:45');
INSERT INTO `mds_resources` VALUES ('5213841377034b8db06c0f53723cc960', '清除', '3', 'role_manager_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 17:51:55');
INSERT INTO `mds_resources` VALUES ('52ad936d5d564240907c458695a3d09e', '清除', '51', 'operation_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 11:48:15');
INSERT INTO `mds_resources` VALUES ('53', '工艺路线', '37', 'process_workflow', '1', '/workcenter_model/process_workflow/process_workflow.shtml', '6', null, '-1', '工艺路线', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('53207fdc51e545f888deef1b21fd4bf0', '清除', 'd4b3779430c64d0681b7b0ab34a90574', 'BOM_CLEAR', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2018-07-08 20:29:36');
INSERT INTO `mds_resources` VALUES ('542bf59974fd450899e163cc6b1c6daa', '保存', '0418eae6ccc2422f9f2f345cb55baeb3', 'work_resource_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 11:28:54');
INSERT INTO `mds_resources` VALUES ('547062bd5e694aeab5827664f1faea45', '删除', '53', 'process_workflow_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 14:32:56');
INSERT INTO `mds_resources` VALUES ('5513d6f9c52146d4b7173e7ed63146d4', '检索', '179', 'number_rule_manager_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 16:41:04');
INSERT INTO `mds_resources` VALUES ('558f6a2f21d74b7b9665cf2efeabcaf9', '删除', '0418eae6ccc2422f9f2f345cb55baeb3', 'work_resource_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 11:05:20');
INSERT INTO `mds_resources` VALUES ('56', '工单维护', '38', 'shoporder_maintenance', '1', '/work_plan/shoporder_maintenance/shoporder_maintenance.shtml', '1', null, '-1', '工单维护', 'admin', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('561', '客退工单维护', '38', 'customer_reject_shoporder_maintenance', '1', '/work_plan/customer_reject_shoporder_maintenance/customer_reject_shoporder_maintenance.shtml', '3', null, '-1', '客退工单维护', 'admin', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('56cc753ce2a94f82935f3f5bde06eb49', '检索', '50', 'WORKCENTER_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2018-06-11 23:23:14');
INSERT INTO `mds_resources` VALUES ('57', '站点维护', '37', 'site_maintenance', '1', '/workcenter_model/site/site_maintenance.shtml', '1', null, '-1', '站点维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('5f2ba2b9feb94e74a5e52f4dde6f51ac', '保存', '7499b45af0dd42e187279d8f191ba0c9', 'item_maintenance_btnsave', '2', 'btnSave', '5', null, '-1', '物料维护-保存', 'admin', '2018-01-06 11:39:29');
INSERT INTO `mds_resources` VALUES ('6278f9b7d8d3487397d0223fd44cebba', '清除', '53', 'process_workflow_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 14:32:21');
INSERT INTO `mds_resources` VALUES ('62e8525166eb4e978b29dd1f5c2d7c8a', '清除', '57', 'site_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-07 11:39:58');
INSERT INTO `mds_resources` VALUES ('62fad4526ac14435a22490f7156444c2', '保存', '179', 'number_rule_manager_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 16:41:32');
INSERT INTO `mds_resources` VALUES ('6b42d8eac8de4a54bed75b4a255f20f0', '检索', '82', 'nc_code_group_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 16:21:04');
INSERT INTO `mds_resources` VALUES ('6cb279d4b1af4c52b83dce98ed6d256e', '删除', '2', 'user_manager_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 18:17:52');
INSERT INTO `mds_resources` VALUES ('70ca116cd9834a6d87fa408fd98643c5', '保存', '83', 'nc_code_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 16:06:30');
INSERT INTO `mds_resources` VALUES ('73a45daf5bcd4f14877070848ec22c3c', '检索', '83', 'nc_code_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 16:06:03');
INSERT INTO `mds_resources` VALUES ('7499b45af0dd42e187279d8f191ba0c9', '物料维护', '37', 'item_maintenance', '1', '/workcenter_model/item/item_maintenance.shtml', '4', '\"\"', '-1', '物料维护', 'admin', '2018-01-06 11:35:04');
INSERT INTO `mds_resources` VALUES ('75', '车间库存接收', '41', 'workshop_inventory_reception', '1', '/workshop_inventory/workshop_inventory_reception/workshop_inventory_reception.shtml', '1', null, '-1', '车间领料操作面板', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('78eea5b7890f44afbdd8a86a5993bce5', '删除', '179', 'NUMBER_RULE_MANAGER_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-09 16:50:43');
INSERT INTO `mds_resources` VALUES ('7adc654dfa184c2f9ae6a4359019c969', '检索', '561', 'CUSTOMER_REJECT_search', '2', 'btnQuery', '1', '\"\"', '-1', '客退单界面检索按钮', 'admin', '2017-11-15 14:31:11');
INSERT INTO `mds_resources` VALUES ('7e1aa439f0aa472bbef816830763db26', '删除', '83', 'nc_code_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 16:08:17');
INSERT INTO `mds_resources` VALUES ('82', '不良代码组维护', '37', 'nc_code_group_maintenance', '1', '/workcenter_model/nc_code_group/nc_code_group_maintenance.shtml', '10', null, '-1', '不合格组维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('83', '不良代码维护', '37', 'nc_code_maintenance', '1', '/workcenter_model/nc_code/nc_code_maintenance.shtml', '9', null, '-1', '不合格代码维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('84a2aa295f124546930264c4b61fb80b', '清除', '179', 'number_rule_manager_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 16:41:57');
INSERT INTO `mds_resources` VALUES ('85', '设备数据采集维护', '43', 'data_gather', '1', '/data_gather', null, null, '-1', '数据采集点维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('87', '设备基础信息维护', '43', 'machine', '1', '/machine', null, null, '-1', '设备维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('88', '班次维护', '37', 'shift', '1', '/workcenter_model/shift/shift_maintenance.shtml', '14', null, '-1', '班次维护', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('89123aac7bef4152a01c82af68d5ab9f', '清除', '178', 'udefined_data_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 17:12:33');
INSERT INTO `mds_resources` VALUES ('896ce4c63dd9434aa73d9f80f7adc15d', '检索', '3', 'role_manager_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 17:50:01');
INSERT INTO `mds_resources` VALUES ('8a15ee24afaa41308883db64fe226625', '删除', 'd4b3779430c64d0681b7b0ab34a90574', 'BOM_DELETE', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2018-07-08 20:30:00');
INSERT INTO `mds_resources` VALUES ('90', '车间物料消耗看板', '45', 'workshop_item_consumption_kanban', '1', '/report/workshop_item_consumption_kanban_query.shtml', '1', null, '-1', '看板', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('9017966460e246b8bf9081b21a3a341e', '清除', '56', 'shoporder_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 17:41:14');
INSERT INTO `mds_resources` VALUES ('90a29c531bd647b39d4b9aeff1d9ce8c', '清除', '88', 'shift_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 17:23:46');
INSERT INTO `mds_resources` VALUES ('925bba00d0e54dde97a020a76dbbb01e', '检索', '53', 'process_workflow_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 14:30:18');
INSERT INTO `mds_resources` VALUES ('92681665b6d74caabb1bffddad5550c6', '保存', '51', 'operation_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 11:47:37');
INSERT INTO `mds_resources` VALUES ('94', '生产进度报表', '46', 'scheduler_report', '1', '/report/work_schedule_report.shtml', '3', null, '-1', '生产进度报表', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('97897a439c3946b3b98149408e500d30', '删除', '57', 'site_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-09-23 14:28:15');
INSERT INTO `mds_resources` VALUES ('99', '车间库存报表', '46', 'workshop_inventory_report', '1', '/report/workshop_inventory_report.shtml', '4', null, '-1', '车间库存报表', '', '2017-09-19 15:15:48');
INSERT INTO `mds_resources` VALUES ('9a3728e959d7408a8f3db6593550b3d1', '重置密码', '2', 'user_manager_btnresetpass', '2', 'btnResetPass', '5', '无', '-1', '重置密码', 'admin', '2017-11-08 18:19:38');
INSERT INTO `mds_resources` VALUES ('9c42efa2ce64407486b38b02c9b625a2', '清除', '7499b45af0dd42e187279d8f191ba0c9', 'item_maintenance_btnclear', '2', 'btnClear', '6', '\"\"', '-1', '物料维护-清除', 'admin', '2018-01-06 11:40:01');
INSERT INTO `mds_resources` VALUES ('9fac5a6f4e2c4b6f8bc09fd87ef5d2cd', '保存', '176', 'work_resource_type_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 11:02:55');
INSERT INTO `mds_resources` VALUES ('a9a08de361124fc697a3951cff21848f', '保存', 'd4b3779430c64d0681b7b0ab34a90574', 'BOM_SAVE', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2018-07-08 20:28:45');
INSERT INTO `mds_resources` VALUES ('aafa1ba6540c4ec496c6b7b641adfd8c', '检索', '0418eae6ccc2422f9f2f345cb55baeb3', 'work_resource_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 11:25:04');
INSERT INTO `mds_resources` VALUES ('b04b9d52423e4661b88a4da27600b34e', '检索', '176', 'work_resource_type_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 11:01:02');
INSERT INTO `mds_resources` VALUES ('b23d7e9bd3ea4fc59f2bd02923ac1997', '保存', '3', 'role_manager_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 17:51:22');
INSERT INTO `mds_resources` VALUES ('b741c0ad7b8641ea885d3bd18feb0142', '删除', '177', 'container_manager_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 16:59:04');
INSERT INTO `mds_resources` VALUES ('bcbf8d6d448f48d1a0d19109665d52de', '保存', '82', 'nc_code_group_maintenance_btnsave', '2', 'btnSave', '2', '无', '-1', '保存', 'admin', '2017-11-08 16:21:27');
INSERT INTO `mds_resources` VALUES ('beea151b615c4e569b459198f9c5fac0', '删除', '82', 'nc_code_group_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 16:22:17');
INSERT INTO `mds_resources` VALUES ('c0c7ae9e12f340dc8a851b89290dde62', '清除', '176', 'work_resource_type_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 11:04:14');
INSERT INTO `mds_resources` VALUES ('c1189a7130c94cddafcbdd574287af4f', '清除', '82', 'nc_code_group_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 16:21:52');
INSERT INTO `mds_resources` VALUES ('c36e35a372ba4015b19ecb2f60e13c76', '检索', 'd4b3779430c64d0681b7b0ab34a90574', 'BOM_QUERY', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2018-07-08 20:10:15');
INSERT INTO `mds_resources` VALUES ('c4c2e920a45f49a4a9c5555c87b28717', '删除', '88', 'shift_maintenance_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 17:24:21');
INSERT INTO `mds_resources` VALUES ('c56fe45b04cd4632afda887711d7d6c6', '检索', '88', 'shift_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 17:22:06');
INSERT INTO `mds_resources` VALUES ('c67f4c965cdc4d53961fdb19a8371990', '保存', '561', 'CUSTOMER_REJECT_save', '2', 'btnSave', '2', '\"\"', '-1', '客退单界面保存按钮', 'admin', '2017-11-15 14:33:03');
INSERT INTO `mds_resources` VALUES ('ccbe69ba1ec4482aa8a21d6aa5a61279', '检索', '2', 'user_manager_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 18:16:24');
INSERT INTO `mds_resources` VALUES ('cf47a02ee06f49829b6e5fecaea749a6', '删除', '3', 'role_manager_btndelete', '2', 'btnDelete', '4', '无', '-1', '删除', 'admin', '2017-11-08 17:52:31');
INSERT INTO `mds_resources` VALUES ('d4b3779430c64d0681b7b0ab34a90574', '物料清单维护', '37', 'bom', '1', '/workcenter_model/item_bom/item_bom_maintenance.shtml', '14', '\"\"', '-1', null, 'admin', '2018-07-08 20:05:13');
INSERT INTO `mds_resources` VALUES ('d6f3859a07c842e4925dcf9e976838a5', '检索', '7499b45af0dd42e187279d8f191ba0c9', 'item_maintenance_btnquery', '2', 'btnQuery', '4', '\"\"', '-1', '物料维护-检索', 'admin', '2018-01-06 11:37:23');
INSERT INTO `mds_resources` VALUES ('ecd744107771494780e59ed0f7445446', '检索', '51', 'operation_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 11:46:48');
INSERT INTO `mds_resources` VALUES ('f551c4af8d394d1b804da63300a179cc', '检索', '56', 'shoporder_maintenance_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 17:40:10');
INSERT INTO `mds_resources` VALUES ('feeebdb497fb43d39d5000034d5b133e', '清除', '83', 'nc_code_maintenance_btnclear', '2', 'btnClear', '3', '无', '-1', '清除', 'admin', '2017-11-08 16:07:00');
INSERT INTO `mds_resources` VALUES ('ffd84152db974f94ac6aa9b5a7fa61ca', '检索', '177', 'container_manager_btnquery', '2', 'btnQuery', '1', '无', '-1', '检索', 'admin', '2017-11-08 16:52:51');

-- ----------------------------
-- Table structure for mds_res_role
-- ----------------------------
DROP TABLE IF EXISTS `mds_res_role`;
CREATE TABLE `mds_res_role` (
  `resId` varchar(64) NOT NULL,
  `roleId` varchar(64) NOT NULL,
  PRIMARY KEY (`roleId`,`resId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_res_role
-- ----------------------------
INSERT INTO `mds_res_role` VALUES ('0418eae6ccc2422f9f2f345cb55baeb3', '0');
INSERT INTO `mds_res_role` VALUES ('0418eae6ccc2422f9f2f345cb55baeb4', '0');
INSERT INTO `mds_res_role` VALUES ('08070d9f91824eb087bc9c9f022a6bc5', '0');
INSERT INTO `mds_res_role` VALUES ('08504ae6679048938c97163a8beaec90', '0');
INSERT INTO `mds_res_role` VALUES ('094eb5ab98b74b4193eaa571e0b83927', '0');
INSERT INTO `mds_res_role` VALUES ('1', '0');
INSERT INTO `mds_res_role` VALUES ('130', '0');
INSERT INTO `mds_res_role` VALUES ('134', '0');
INSERT INTO `mds_res_role` VALUES ('151', '0');
INSERT INTO `mds_res_role` VALUES ('169', '0');
INSERT INTO `mds_res_role` VALUES ('174', '0');
INSERT INTO `mds_res_role` VALUES ('175', '0');
INSERT INTO `mds_res_role` VALUES ('176', '0');
INSERT INTO `mds_res_role` VALUES ('177', '0');
INSERT INTO `mds_res_role` VALUES ('178', '0');
INSERT INTO `mds_res_role` VALUES ('179', '0');
INSERT INTO `mds_res_role` VALUES ('181', '0');
INSERT INTO `mds_res_role` VALUES ('182', '0');
INSERT INTO `mds_res_role` VALUES ('183', '0');
INSERT INTO `mds_res_role` VALUES ('184', '0');
INSERT INTO `mds_res_role` VALUES ('185', '0');
INSERT INTO `mds_res_role` VALUES ('187', '0');
INSERT INTO `mds_res_role` VALUES ('188', '0');
INSERT INTO `mds_res_role` VALUES ('19264a90df7d431a83dbfae41b9cb44c', '0');
INSERT INTO `mds_res_role` VALUES ('1a3bd2f4070945e4b999d540f92997ff', '0');
INSERT INTO `mds_res_role` VALUES ('2', '0');
INSERT INTO `mds_res_role` VALUES ('210d0a230de34423be666a11de0f23c3', '0');
INSERT INTO `mds_res_role` VALUES ('21d616905b794b59a7efc3557600d96e', '0');
INSERT INTO `mds_res_role` VALUES ('22d7058913c8417c9b89fbf6776f510e', '0');
INSERT INTO `mds_res_role` VALUES ('23870d1f8fdd4711a6356520894cfba4', '0');
INSERT INTO `mds_res_role` VALUES ('24bbe91ddf9043f29d9ee6e71a4df44a', '0');
INSERT INTO `mds_res_role` VALUES ('2d0f005f17914a4f993f190134bfb1c7', '0');
INSERT INTO `mds_res_role` VALUES ('2f1b9139a5a941a3a980f431768a05a1', '0');
INSERT INTO `mds_res_role` VALUES ('3', '0');
INSERT INTO `mds_res_role` VALUES ('37', '0');
INSERT INTO `mds_res_role` VALUES ('38', '0');
INSERT INTO `mds_res_role` VALUES ('39', '0');
INSERT INTO `mds_res_role` VALUES ('3a255d4578a048e18453168488f13448', '0');
INSERT INTO `mds_res_role` VALUES ('3c7a2952248241dabc4030dbe16cbc63', '0');
INSERT INTO `mds_res_role` VALUES ('4', '0');
INSERT INTO `mds_res_role` VALUES ('41', '0');
INSERT INTO `mds_res_role` VALUES ('4290b40fc84f4654a6b635ba1d897770', '0');
INSERT INTO `mds_res_role` VALUES ('45', '0');
INSERT INTO `mds_res_role` VALUES ('46', '0');
INSERT INTO `mds_res_role` VALUES ('465cecd6b5404057878402c4cfe0582a', '0');
INSERT INTO `mds_res_role` VALUES ('4c8b51e70e4246f3b5b2a93c4151bcef', '0');
INSERT INTO `mds_res_role` VALUES ('50', '0');
INSERT INTO `mds_res_role` VALUES ('51', '0');
INSERT INTO `mds_res_role` VALUES ('5166d79c2bbe4494b7fdb53202e0d040', '0');
INSERT INTO `mds_res_role` VALUES ('5213841377034b8db06c0f53723cc960', '0');
INSERT INTO `mds_res_role` VALUES ('52ad936d5d564240907c458695a3d09e', '0');
INSERT INTO `mds_res_role` VALUES ('53', '0');
INSERT INTO `mds_res_role` VALUES ('53207fdc51e545f888deef1b21fd4bf0', '0');
INSERT INTO `mds_res_role` VALUES ('542bf59974fd450899e163cc6b1c6daa', '0');
INSERT INTO `mds_res_role` VALUES ('547062bd5e694aeab5827664f1faea45', '0');
INSERT INTO `mds_res_role` VALUES ('5513d6f9c52146d4b7173e7ed63146d4', '0');
INSERT INTO `mds_res_role` VALUES ('558f6a2f21d74b7b9665cf2efeabcaf9', '0');
INSERT INTO `mds_res_role` VALUES ('56', '0');
INSERT INTO `mds_res_role` VALUES ('561', '0');
INSERT INTO `mds_res_role` VALUES ('56cc753ce2a94f82935f3f5bde06eb49', '0');
INSERT INTO `mds_res_role` VALUES ('57', '0');
INSERT INTO `mds_res_role` VALUES ('5f2ba2b9feb94e74a5e52f4dde6f51ac', '0');
INSERT INTO `mds_res_role` VALUES ('6278f9b7d8d3487397d0223fd44cebba', '0');
INSERT INTO `mds_res_role` VALUES ('62e8525166eb4e978b29dd1f5c2d7c8a', '0');
INSERT INTO `mds_res_role` VALUES ('62fad4526ac14435a22490f7156444c2', '0');
INSERT INTO `mds_res_role` VALUES ('6b42d8eac8de4a54bed75b4a255f20f0', '0');
INSERT INTO `mds_res_role` VALUES ('6cb279d4b1af4c52b83dce98ed6d256e', '0');
INSERT INTO `mds_res_role` VALUES ('70ca116cd9834a6d87fa408fd98643c5', '0');
INSERT INTO `mds_res_role` VALUES ('73a45daf5bcd4f14877070848ec22c3c', '0');
INSERT INTO `mds_res_role` VALUES ('7499b45af0dd42e187279d8f191ba0c9', '0');
INSERT INTO `mds_res_role` VALUES ('75', '0');
INSERT INTO `mds_res_role` VALUES ('78eea5b7890f44afbdd8a86a5993bce5', '0');
INSERT INTO `mds_res_role` VALUES ('7adc654dfa184c2f9ae6a4359019c969', '0');
INSERT INTO `mds_res_role` VALUES ('7e1aa439f0aa472bbef816830763db26', '0');
INSERT INTO `mds_res_role` VALUES ('82', '0');
INSERT INTO `mds_res_role` VALUES ('83', '0');
INSERT INTO `mds_res_role` VALUES ('84a2aa295f124546930264c4b61fb80b', '0');
INSERT INTO `mds_res_role` VALUES ('88', '0');
INSERT INTO `mds_res_role` VALUES ('89123aac7bef4152a01c82af68d5ab9f', '0');
INSERT INTO `mds_res_role` VALUES ('896ce4c63dd9434aa73d9f80f7adc15d', '0');
INSERT INTO `mds_res_role` VALUES ('8a15ee24afaa41308883db64fe226625', '0');
INSERT INTO `mds_res_role` VALUES ('90', '0');
INSERT INTO `mds_res_role` VALUES ('9017966460e246b8bf9081b21a3a341e', '0');
INSERT INTO `mds_res_role` VALUES ('90a29c531bd647b39d4b9aeff1d9ce8c', '0');
INSERT INTO `mds_res_role` VALUES ('925bba00d0e54dde97a020a76dbbb01e', '0');
INSERT INTO `mds_res_role` VALUES ('92681665b6d74caabb1bffddad5550c6', '0');
INSERT INTO `mds_res_role` VALUES ('94', '0');
INSERT INTO `mds_res_role` VALUES ('97897a439c3946b3b98149408e500d30', '0');
INSERT INTO `mds_res_role` VALUES ('99', '0');
INSERT INTO `mds_res_role` VALUES ('9a3728e959d7408a8f3db6593550b3d1', '0');
INSERT INTO `mds_res_role` VALUES ('9c42efa2ce64407486b38b02c9b625a2', '0');
INSERT INTO `mds_res_role` VALUES ('9fac5a6f4e2c4b6f8bc09fd87ef5d2cd', '0');
INSERT INTO `mds_res_role` VALUES ('a9a08de361124fc697a3951cff21848f', '0');
INSERT INTO `mds_res_role` VALUES ('aafa1ba6540c4ec496c6b7b641adfd8c', '0');
INSERT INTO `mds_res_role` VALUES ('b04b9d52423e4661b88a4da27600b34e', '0');
INSERT INTO `mds_res_role` VALUES ('b23d7e9bd3ea4fc59f2bd02923ac1997', '0');
INSERT INTO `mds_res_role` VALUES ('b741c0ad7b8641ea885d3bd18feb0142', '0');
INSERT INTO `mds_res_role` VALUES ('bcbf8d6d448f48d1a0d19109665d52de', '0');
INSERT INTO `mds_res_role` VALUES ('beea151b615c4e569b459198f9c5fac0', '0');
INSERT INTO `mds_res_role` VALUES ('c0c7ae9e12f340dc8a851b89290dde62', '0');
INSERT INTO `mds_res_role` VALUES ('c1189a7130c94cddafcbdd574287af4f', '0');
INSERT INTO `mds_res_role` VALUES ('c36e35a372ba4015b19ecb2f60e13c76', '0');
INSERT INTO `mds_res_role` VALUES ('c4c2e920a45f49a4a9c5555c87b28717', '0');
INSERT INTO `mds_res_role` VALUES ('c56fe45b04cd4632afda887711d7d6c6', '0');
INSERT INTO `mds_res_role` VALUES ('c67f4c965cdc4d53961fdb19a8371990', '0');
INSERT INTO `mds_res_role` VALUES ('ccbe69ba1ec4482aa8a21d6aa5a61279', '0');
INSERT INTO `mds_res_role` VALUES ('cf47a02ee06f49829b6e5fecaea749a6', '0');
INSERT INTO `mds_res_role` VALUES ('d4b3779430c64d0681b7b0ab34a90574', '0');
INSERT INTO `mds_res_role` VALUES ('d6f3859a07c842e4925dcf9e976838a5', '0');
INSERT INTO `mds_res_role` VALUES ('ecd744107771494780e59ed0f7445446', '0');
INSERT INTO `mds_res_role` VALUES ('f551c4af8d394d1b804da63300a179cc', '0');
INSERT INTO `mds_res_role` VALUES ('feeebdb497fb43d39d5000034d5b133e', '0');
INSERT INTO `mds_res_role` VALUES ('ffd84152db974f94ac6aa9b5a7fa61ca', '0');
INSERT INTO `mds_res_role` VALUES ('151', '16e141945d1644cc83610b71190a8a4c');
INSERT INTO `mds_res_role` VALUES ('39', '16e141945d1644cc83610b71190a8a4c');
INSERT INTO `mds_res_role` VALUES ('0418eae6ccc2422f9f2f345cb55baeb3', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('1', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('151', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('169', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('174', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('175', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('176', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('177', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('178', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('179', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('181', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('182', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('183', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('184', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('187', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('188', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('2', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('3', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('37', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('38', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('39', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('41', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('45', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('46', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('50', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('51', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('53', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('56', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('75', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('82', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('83', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('88', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('94', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('99', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_res_role` VALUES ('0418eae6ccc2422f9f2f345cb55baeb3', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('0418eae6ccc2422f9f2f345cb55baeb4', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('1', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('10', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('101', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('102', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('103', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('104', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('105', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('106', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('107', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('108', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('109', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('11', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('110', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('111', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('112', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('113', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('114', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('115', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('120', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('121', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('122', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('123', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('128', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('129', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('130', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('131', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('132', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('133', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('134', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('135', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('136', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('137', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('139', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('140', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('141', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('142', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('143', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('144', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('145', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('146', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('147', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('148', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('149', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('150', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('151', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('152', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('153', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('169', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('170', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('174', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('175', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('176', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('177', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('178', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('179', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('181', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('182', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('183', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('184', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('185', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('186', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('187', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('188', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('2', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('23870d1f8fdd4711a6356520894cfba4', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('25', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('26', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('27', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('28', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('29', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('3', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('30', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('31', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('34', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('37', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('38', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('39', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('4', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('40', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('41', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('45', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('46', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('5', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('50', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('51', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('53', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('56', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('57', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('6', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('62e8525166eb4e978b29dd1f5c2d7c8a', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('7', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('75', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('8', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('82', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('83', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('88', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('9', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('94', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('97897a439c3946b3b98149408e500d30', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('99', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_res_role` VALUES ('0418eae6ccc2422f9f2f345cb55baeb3', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('0418eae6ccc2422f9f2f345cb55baeb4', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('08070d9f91824eb087bc9c9f022a6bc5', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('08504ae6679048938c97163a8beaec90', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('094eb5ab98b74b4193eaa571e0b83927', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('1', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('130', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('134', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('151', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('169', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('174', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('175', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('176', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('177', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('178', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('179', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('181', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('182', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('183', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('184', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('185', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('186', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('187', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('188', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('19264a90df7d431a83dbfae41b9cb44c', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('1a3bd2f4070945e4b999d540f92997ff', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('2', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('210d0a230de34423be666a11de0f23c3', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('21d616905b794b59a7efc3557600d96e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('22d7058913c8417c9b89fbf6776f510e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('23870d1f8fdd4711a6356520894cfba4', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('24bbe91ddf9043f29d9ee6e71a4df44a', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('25', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('26', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('27', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('28', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('2d0f005f17914a4f993f190134bfb1c7', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('2f1b9139a5a941a3a980f431768a05a1', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('3', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('37', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('38', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('39', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('3a255d4578a048e18453168488f13448', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('3c7a2952248241dabc4030dbe16cbc63', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('4', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('40', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('41', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('4290b40fc84f4654a6b635ba1d897770', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('45', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('46', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('465cecd6b5404057878402c4cfe0582a', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('4c8b51e70e4246f3b5b2a93c4151bcef', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('50', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('51', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('5166d79c2bbe4494b7fdb53202e0d040', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('5213841377034b8db06c0f53723cc960', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('52ad936d5d564240907c458695a3d09e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('53', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('542bf59974fd450899e163cc6b1c6daa', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('547062bd5e694aeab5827664f1faea45', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('5513d6f9c52146d4b7173e7ed63146d4', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('558f6a2f21d74b7b9665cf2efeabcaf9', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('56', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('561', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('57', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('5f2ba2b9feb94e74a5e52f4dde6f51ac', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('6278f9b7d8d3487397d0223fd44cebba', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('62e8525166eb4e978b29dd1f5c2d7c8a', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('62fad4526ac14435a22490f7156444c2', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('6b42d8eac8de4a54bed75b4a255f20f0', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('6cb279d4b1af4c52b83dce98ed6d256e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('70ca116cd9834a6d87fa408fd98643c5', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('73a45daf5bcd4f14877070848ec22c3c', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('7499b45af0dd42e187279d8f191ba0c9', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('75', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('78eea5b7890f44afbdd8a86a5993bce5', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('7adc654dfa184c2f9ae6a4359019c969', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('7e1aa439f0aa472bbef816830763db26', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('82', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('83', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('84a2aa295f124546930264c4b61fb80b', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('88', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('89123aac7bef4152a01c82af68d5ab9f', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('896ce4c63dd9434aa73d9f80f7adc15d', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('90', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('9017966460e246b8bf9081b21a3a341e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('90a29c531bd647b39d4b9aeff1d9ce8c', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('925bba00d0e54dde97a020a76dbbb01e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('92681665b6d74caabb1bffddad5550c6', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('94', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('97897a439c3946b3b98149408e500d30', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('99', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('9a3728e959d7408a8f3db6593550b3d1', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('9c42efa2ce64407486b38b02c9b625a2', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('9fac5a6f4e2c4b6f8bc09fd87ef5d2cd', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('aafa1ba6540c4ec496c6b7b641adfd8c', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('b04b9d52423e4661b88a4da27600b34e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('b23d7e9bd3ea4fc59f2bd02923ac1997', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('b741c0ad7b8641ea885d3bd18feb0142', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('bcbf8d6d448f48d1a0d19109665d52de', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('beea151b615c4e569b459198f9c5fac0', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('c0c7ae9e12f340dc8a851b89290dde62', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('c1189a7130c94cddafcbdd574287af4f', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('c4c2e920a45f49a4a9c5555c87b28717', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('c56fe45b04cd4632afda887711d7d6c6', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('c67f4c965cdc4d53961fdb19a8371990', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('ccbe69ba1ec4482aa8a21d6aa5a61279', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('cf47a02ee06f49829b6e5fecaea749a6', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('d6f3859a07c842e4925dcf9e976838a5', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('ecd744107771494780e59ed0f7445446', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('f551c4af8d394d1b804da63300a179cc', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('feeebdb497fb43d39d5000034d5b133e', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_res_role` VALUES ('ffd84152db974f94ac6aa9b5a7fa61ca', 'e28f46fa492a4fa2a080ea1914ee71ca');

-- ----------------------------
-- Table structure for mds_role
-- ----------------------------
DROP TABLE IF EXISTS `mds_role`;
CREATE TABLE `mds_role` (
  `id` varchar(64) NOT NULL,
  `status` varchar(3) NOT NULL COMMENT '状态：0、可用；1、不可用；',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `role_key` varchar(50) NOT NULL COMMENT '角色ID',
  `description` varchar(50) DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `site_id` varbinary(255) NOT NULL COMMENT '站点',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_role
-- ----------------------------
INSERT INTO `mds_role` VALUES ('0', '0', '管理员', 'admin', '管理员', '2018-07-08 20:30:15', 'admin', 0x30);
INSERT INTO `mds_role` VALUES ('16e141945d1644cc83610b71190a8a4c', '0', 'z站工程师', 'zengineer', 'z站的工程师', '2017-11-08 17:57:44', 'admin', 0x3336356665333436363363393466356338643036663862653736623835303164);
INSERT INTO `mds_role` VALUES ('5f53ad5cb5cf467ba843ba995b513cd2', '0', 'ree', 'r2', 'ee', '2017-11-02 16:13:56', 'admin', 0x3664373066656433366264393462646162316137316364653633393534666136);
INSERT INTO `mds_role` VALUES ('6907f56e454345b1bde76f9281175fd5', '0', 'Z站管理员', 'z_admin', 'Z站的管理员', '2017-11-06 16:17:50', 'admin', 0x3336356665333436363363393466356338643036663862653736623835303164);
INSERT INTO `mds_role` VALUES ('6bc48f44b596403891135e9d482eb4f6', '0', '1234', '1234', null, '2017-10-29 20:08:38', 'admin', 0x6433316665393562383838303438303662366366663430383330393061666433);
INSERT INTO `mds_role` VALUES ('df932dc0abf248469418a563b7752195', '0', 'ree', 'r1', 'ee', '2017-11-02 16:13:45', 'admin', 0x3664373066656433366264393462646162316137316364653633393534666136);
INSERT INTO `mds_role` VALUES ('e28f46fa492a4fa2a080ea1914ee71ca', '0', 'TEST', 'test', 'TEST', '2018-06-11 22:36:35', 'admin', 0x3464623631643764323965643463306339396234353830633232636335626366);

-- ----------------------------
-- Table structure for mds_sfc_assembly
-- ----------------------------
DROP TABLE IF EXISTS `mds_sfc_assembly`;
CREATE TABLE `mds_sfc_assembly` (
  `id` varchar(64) NOT NULL,
  `site_id` varchar(64) NOT NULL COMMENT '站点PK_id',
  `operation_id` varbinary(64) NOT NULL COMMENT '操作PK_id',
  `sfc` varchar(255) NOT NULL COMMENT 'SFC',
  `item_sfc` varchar(255) DEFAULT NULL COMMENT '装配的物料SFC',
  `item_id` varchar(255) NOT NULL COMMENT '装配的物料id',
  `use_number` int(11) NOT NULL COMMENT '消耗数量',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `shoporder_id` varchar(255) NOT NULL COMMENT '工单ID',
  `workshop_id` varchar(255) NOT NULL COMMENT '车间ID',
  `workline_id` varchar(255) NOT NULL COMMENT '产线ID',
  `work_resource_id` varchar(255) NOT NULL COMMENT '资源ID',
  `main_item_id` varchar(255) NOT NULL COMMENT '成品物料ID',
  `item_inner_batch` varchar(255) DEFAULT NULL COMMENT '物料批次',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_sfc_assembly
-- ----------------------------
INSERT INTO `mds_sfc_assembly` VALUES ('071d94a9b8054630b40a6006bf14e3e6', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711052342380000000001_end', 'sfc_samsung_0002', 'b9387c634ca1443aa379eae5f6e9fd24', '1', 'admin', '2017-11-06 10:05:17', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIIC_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('130d971853894099aeca072bffe35080', 'a67033a4395543adb40a6dcd4c65fbd4', 0x3535663765613362663361663435346239616233313362346238313334616238, 'sfc20171108105043000000000001mds', 'CPU11111111111', 'cb1730b1033f4b8a95d94582ad63df97', '1', 'zoufa', '2017-11-08 15:42:54', 'cb7e6699daa447d2990f1c5d795464ee', '53d4ae49a85846f99be097dea826281e', '1e0783fbfe6b46faa47577f6b037f873', '1c69e7f308c3461dbef07592142227d2', '59b78d8ff10e405482fd68b9f8d59281', null);
INSERT INTO `mds_sfc_assembly` VALUES ('1a702fbb5cd2407f9fb76883fb6818ed', 'a67033a4395543adb40a6dcd4c65fbd4', 0x3535663765613362663361663435346239616233313362346238313334616238, 'sfc20171108105043000000000001mds', null, '0c1d619ea0a244deb8b7c538ab1a1da9', '8', 'zoufa', '2017-11-08 17:33:39', 'cb7e6699daa447d2990f1c5d795464ee', '53d4ae49a85846f99be097dea826281e', '1e0783fbfe6b46faa47577f6b037f873', '1c69e7f308c3461dbef07592142227d2', '59b78d8ff10e405482fd68b9f8d59281', 'DZ999999999999');
INSERT INTO `mds_sfc_assembly` VALUES ('4321476a6cf946a09cafcbe296f0343e', '6d70fed36bd94bdab1a71cde63954fa6', 0x3439656663396262313466623432373238656264613736653638623535333664, 'MDS_SFC2017102816330000000000001', null, '54ac891b98884d809960c06a6389fa00', '1', 'admin', '2017-10-28 16:33:48', '6cf7c14e855c4d5b9fd4ae4d1dc5b144', 'e78f5429e2974807a6b93d9e03f60307', 'bc3ea23aab814c6bb310c29b5832648c', '950d2c8610d44947bd55b74884312475', '934ccacc383f47f0892f25334a54d241', '3333333333333');
INSERT INTO `mds_sfc_assembly` VALUES ('4a321e66a10342d9909640c25e3a1552', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711052342380000000001_end', 'sfc_huaweihk_0004', 'f468496e3f2e4d66ab1b1e2fb76ddc87', '1', 'admin', '2017-11-06 10:05:46', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIHK_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('4d9dfcf8b2064558a4705a6196ef1cfb', '6d70fed36bd94bdab1a71cde63954fa6', 0x6333303537303461626634633465613361643032323538393035353233313462, 'MDS_SFC2017102411274400000000001', 'fffffffff', '54ac891b98884d809960c06a6389fa00', '1', 'admin', '2017-10-24 11:30:33', '9fc911a781b64755b3f737852ad9ef70', 'e78f5429e2974807a6b93d9e03f60307', 'bc3ea23aab814c6bb310c29b5832648c', 'f3bd14ffc26742da9ecb724655bbb6d6', '934ccacc383f47f0892f25334a54d241', '3333333333333');
INSERT INTO `mds_sfc_assembly` VALUES ('526ff6dc459b42fc8f91c15a9e48c56b', 'a67033a4395543adb40a6dcd4c65fbd4', 0x3535663765613362663361663435346239616233313362346238313334616238, 'sfc20171108105043000000000001mds', null, '0c1d619ea0a244deb8b7c538ab1a1da9', '2', 'zoufa', '2017-11-08 17:56:48', 'cb7e6699daa447d2990f1c5d795464ee', '53d4ae49a85846f99be097dea826281e', '1e0783fbfe6b46faa47577f6b037f873', '1c69e7f308c3461dbef07592142227d2', '59b78d8ff10e405482fd68b9f8d59281', 'DZ999999999999');
INSERT INTO `mds_sfc_assembly` VALUES ('5287560a5892452d8a7d365e8fae2d98', '0', 0x3030346631313662346466663438623561303133623536373336616637383630, '201820180708203522000000000001XC', 'BATCH001', '9bdd7c4632244d58af0b651a421a99b7', '1', 'admin', '2018-07-08 20:42:32', '37ac54169ef244658abbe13bb4927335', '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null);
INSERT INTO `mds_sfc_assembly` VALUES ('5963259653e44460b1ff01a08ec8de66', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711061148410000000001_end', 'sfc_mihk_0005', 'f468496e3f2e4d66ab1b1e2fb76ddc87', '1', 'z0001', '2017-11-06 14:15:44', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIHK_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('5bd2500fe83d40be8fd11e9b8a824137', 'd31fe95b88804806b6cff4083090afd3', 0x3136356364353465303933343436366339393135393963333466663938323265, '20172017103015061200000000102017', '4444', '83cf2f1d308b49439f6621b5d11a2f2c', '1', 'admin', '2017-10-30 18:40:05', '4125600930ae4ebd9fbcc2b1767fa54a', '9838055c5c6646ac813c1237ef514344', '0d23c200f5dd471a97d379b94dda4b55', '4fd41655351a477d89f26dce6205e277', '4558ecf93da84175b34bdb8f717a9228', 'DZ');
INSERT INTO `mds_sfc_assembly` VALUES ('71bd6af6a2a642abb2920d1897e7439d', 'a67033a4395543adb40a6dcd4c65fbd4', 0x3535663765613362663361663435346239616233313362346238313334616238, 'sfc20171108105043000000000001mds', null, '0c1d619ea0a244deb8b7c538ab1a1da9', '2', 'zoufa', '2017-11-08 18:22:14', 'cb7e6699daa447d2990f1c5d795464ee', '53d4ae49a85846f99be097dea826281e', '1e0783fbfe6b46faa47577f6b037f873', '1c69e7f308c3461dbef07592142227d2', '59b78d8ff10e405482fd68b9f8d59281', 'hhhhhhhhhhhhhhh');
INSERT INTO `mds_sfc_assembly` VALUES ('7a32319b997849b6a20831735520bf0f', '365fe34663c94f5c8d06f8be76b8501d', 0x3462373164306263643236363435333462663963643630343364396633353035, 'sfc_201711041609210000000001_end', 'sfc_miic_0003', 'b9387c634ca1443aa379eae5f6e9fd24', '1', 'admin', '2017-11-06 10:58:36', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', '979eddd9793345b7acce4d014790e880', 'bacd72e2e40d462aa0292808b4493ea5', 'MIIC_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('8089de230e4c485888ce874e4e37d7a9', 'a67033a4395543adb40a6dcd4c65fbd4', 0x3535663765613362663361663435346239616233313362346238313334616238, 'sfc20171108105043000000000001mds', null, '0c1d619ea0a244deb8b7c538ab1a1da9', '20', 'zoufa', '2017-11-08 15:44:55', 'cb7e6699daa447d2990f1c5d795464ee', '53d4ae49a85846f99be097dea826281e', '1e0783fbfe6b46faa47577f6b037f873', '1c69e7f308c3461dbef07592142227d2', '59b78d8ff10e405482fd68b9f8d59281', 'DZ999999999999');
INSERT INTO `mds_sfc_assembly` VALUES ('85ad832e43d84895b59f056a9e480678', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711041609210000000001_end', 'sfc_mihk_0001', 'f468496e3f2e4d66ab1b1e2fb76ddc87', '1', 'z0001', '2017-11-04 17:04:55', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIHK_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('893a6ebc73fc43579a2c995ad78dbeac', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711041609210000000003_end', 'sfc_miic_0002', 'b9387c634ca1443aa379eae5f6e9fd24', '1', 'z0001', '2017-11-04 17:28:27', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIIC_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('8a44d6aa800b4328bf9cdbce26fb309f', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711082306470000000005_end', 'hk1sfc001', 'dd71a5d8eee84308b52f43051303c354', '1', 'admin', '2017-11-08 23:09:25', 'db8b14efe0154c0c837a8a45f03083a4', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', '41c9f27847614c5689be29bbd5e2387b', null);
INSERT INTO `mds_sfc_assembly` VALUES ('98260cf8b2b24d1eaaa056bdd1b67f32', 'a67033a4395543adb40a6dcd4c65fbd4', 0x3535663765613362663361663435346239616233313362346238313334616238, 'sfc20171108105043000000000001mds', null, '0c1d619ea0a244deb8b7c538ab1a1da9', '1', 'zoufa', '2017-11-08 18:22:23', 'cb7e6699daa447d2990f1c5d795464ee', '53d4ae49a85846f99be097dea826281e', '1e0783fbfe6b46faa47577f6b037f873', '1c69e7f308c3461dbef07592142227d2', '59b78d8ff10e405482fd68b9f8d59281', 'hhhhhhhhhhhhhhh');
INSERT INTO `mds_sfc_assembly` VALUES ('a39d493c1d3846d5888882b7d8d980b4', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711041609210000000003_end', 'sfc_mihk_0002', 'f468496e3f2e4d66ab1b1e2fb76ddc87', '1', 'z0001', '2017-11-04 17:28:38', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIHK_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('a7b28a8851ce415ca906031a0196b5f5', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711041609210000000001_end', 'sfc_miic_0001', 'b9387c634ca1443aa379eae5f6e9fd24', '1', 'z0001', '2017-11-04 17:04:32', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIIC_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('b413f71378a340f5906c9bd1a5b5c227', '365fe34663c94f5c8d06f8be76b8501d', 0x3462373164306263643236363435333462663963643630343364396633353035, 'sfc_201711041609210000000002_end', 'sfc_miic_0004', 'b9387c634ca1443aa379eae5f6e9fd24', '1', 'admin', '2017-11-04 17:21:55', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', '979eddd9793345b7acce4d014790e880', 'bacd72e2e40d462aa0292808b4493ea5', 'MIIC_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('b9e853387574490f8da659a90b4c5c18', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711061148410000000001_end', 'sfc_miic_0005', 'b9387c634ca1443aa379eae5f6e9fd24', '1', 'z0001', '2017-11-06 14:15:31', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIIC_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('d3a2ffb59a824a92915e73126b9e6800', '6d70fed36bd94bdab1a71cde63954fa6', 0x3439656663396262313466623432373238656264613736653638623535333664, 'MDS_SFC2017102816330000000000001', null, '9c8375eb760f42f69a97f38a8bd6188d', '1', 'admin', '2017-10-28 16:34:13', '6cf7c14e855c4d5b9fd4ae4d1dc5b144', 'e78f5429e2974807a6b93d9e03f60307', 'bc3ea23aab814c6bb310c29b5832648c', '950d2c8610d44947bd55b74884312475', '934ccacc383f47f0892f25334a54d241', '11111');
INSERT INTO `mds_sfc_assembly` VALUES ('d4258f70f8274ef7b9185f3091f96c2a', '6d70fed36bd94bdab1a71cde63954fa6', 0x6333303537303461626634633465613361643032323538393035353233313462, 'MDS_SFC2017102411274400000000001', 'bbbbbbbbbbb', '9c8375eb760f42f69a97f38a8bd6188d', '1', 'admin', '2017-10-24 11:31:32', '9fc911a781b64755b3f737852ad9ef70', 'e78f5429e2974807a6b93d9e03f60307', 'bc3ea23aab814c6bb310c29b5832648c', 'f3bd14ffc26742da9ecb724655bbb6d6', '934ccacc383f47f0892f25334a54d241', '11111');
INSERT INTO `mds_sfc_assembly` VALUES ('e12ecccf941843a293eb315cae107233', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711052342380000000001_end', 'sfc_samsung_0003', 'b9387c634ca1443aa379eae5f6e9fd24', '1', 'admin', '2017-11-06 10:05:26', '2b56185157f24acd8045532382ae7d39', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', 'bacd72e2e40d462aa0292808b4493ea5', 'MIIC_inb1');
INSERT INTO `mds_sfc_assembly` VALUES ('f628ed24f91b403fbdad03b52030203e', '365fe34663c94f5c8d06f8be76b8501d', 0x6666656238316161373862393435623362623461656433373761343965323332, 'sfc_201711082306470000000005_end', null, 'b691f59b63d34b73a68bb2b59543afc2', '5', 'admin', '2017-11-08 23:09:39', 'db8b14efe0154c0c837a8a45f03083a4', '1f616801ede24c1a8f40786eecef6a40', 'bf25d584510c454594e4e9227f169648', 'c103462ff6a44800bfa4a442ed1888e1', '41c9f27847614c5689be29bbd5e2387b', 'b001');

-- ----------------------------
-- Table structure for mds_sfc_nc
-- ----------------------------
DROP TABLE IF EXISTS `mds_sfc_nc`;
CREATE TABLE `mds_sfc_nc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sfc` varchar(255) NOT NULL COMMENT 'SFC',
  `site_id` varchar(255) NOT NULL COMMENT '站点ID',
  `operation_id` varchar(255) NOT NULL COMMENT '操作ID',
  `nc_code_id` varchar(255) NOT NULL COMMENT '不合格代码ID',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `shoporder_id` varchar(255) NOT NULL COMMENT '工单ID',
  `work_resource_id` varchar(255) NOT NULL COMMENT '设备、资源ID',
  `nc_code_group_id` varchar(255) NOT NULL COMMENT '不合格代码组ID',
  `status` int(1) NOT NULL COMMENT '状态：-1、未处理；1、已处理',
  `repair_type` int(1) DEFAULT NULL COMMENT '处置方式：0、维修；1、报废',
  `repair_id` varchar(255) DEFAULT NULL COMMENT '处置表id',
  `nc_desc` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_sfc_nc
-- ----------------------------
INSERT INTO `mds_sfc_nc` VALUES ('32', 'MDS_SFC2017102410374600000000001', '6d70fed36bd94bdab1a71cde63954fa6', 'c305704abf4c4ea3ad0225890552314b', 'b0569d50e7b640b3896585352d1e708f', 'admin', '2017-10-24 10:40:23', '9fc911a781b64755b3f737852ad9ef70', 'f3bd14ffc26742da9ecb724655bbb6d6', '35caf90337124b64a9f17f9a6dd3dd6c', '1', '1', '9505b705dfd64fce8c370ef0ccbecda4', '456456');
INSERT INTO `mds_sfc_nc` VALUES ('33', 'MDS_SFC2017102410490400000000001', '6d70fed36bd94bdab1a71cde63954fa6', 'c305704abf4c4ea3ad0225890552314b', 'b0569d50e7b640b3896585352d1e708f', 'admin', '2017-10-24 11:20:52', '9fc911a781b64755b3f737852ad9ef70', 'f3bd14ffc26742da9ecb724655bbb6d6', '35caf90337124b64a9f17f9a6dd3dd6c', '1', '1', 'f22212292f6b46b29bba44717677b819', '	MDS_SFC2017102410490400000000001');
INSERT INTO `mds_sfc_nc` VALUES ('34', 'MDS_SFC2017102411274400000000001', '6d70fed36bd94bdab1a71cde63954fa6', 'c305704abf4c4ea3ad0225890552314b', 'b0569d50e7b640b3896585352d1e708f', 'admin', '2017-10-24 11:31:52', '9fc911a781b64755b3f737852ad9ef70', 'f3bd14ffc26742da9ecb724655bbb6d6', '35caf90337124b64a9f17f9a6dd3dd6c', '1', '1', '3590360e2fdd4caf909dce74e6a05bc6', '5555555555');
INSERT INTO `mds_sfc_nc` VALUES ('35', 'sfc_201711041609210000000003_end', '365fe34663c94f5c8d06f8be76b8501d', 'a3ef54723c00433b9091700dec59e866', 'a0e01b1e244642d586faea27daa4057a', 'admin', '2017-11-06 11:28:48', '2b56185157f24acd8045532382ae7d39', '883d5dd43e7940be9e6d80129aed79ee', '5317dd8bc448408c85e38317dd8fc3c5', '-1', null, null, '33333');
INSERT INTO `mds_sfc_nc` VALUES ('36', 'sfc_201711052342380000000001_end', '365fe34663c94f5c8d06f8be76b8501d', 'ffeb81aa78b945b3bb4aed377a49e232', 'b85a0fbf5e374eae93e0b39f9f580dc1', 'z0001', '2017-11-06 16:53:15', '2b56185157f24acd8045532382ae7d39', 'c103462ff6a44800bfa4a442ed1888e1', 'd063f72ba7a540b5b8695806d685bec1', '-1', null, null, '测试');
INSERT INTO `mds_sfc_nc` VALUES ('37', 'sfc_201711082053510000000002_end', '365fe34663c94f5c8d06f8be76b8501d', 'ffeb81aa78b945b3bb4aed377a49e232', 'a0e01b1e244642d586faea27daa4057a', 'admin', '2017-11-09 13:15:55', 'b7dfaa804f4a4c60a64d1cc542b1ecf2', 'c103462ff6a44800bfa4a442ed1888e1', '5317dd8bc448408c85e38317dd8fc3c5', '-1', null, null, 'tttttt');
INSERT INTO `mds_sfc_nc` VALUES ('38', 'sfc_2_2017111011281300000001_end', '365fe34663c94f5c8d06f8be76b8501d', 'ffeb81aa78b945b3bb4aed377a49e232', 'ebd69217b87543a4b008b2d45b97eff1', 'admin', '2017-11-12 13:12:17', 'db8b14efe0154c0c837a8a45f03083a4', 'c103462ff6a44800bfa4a442ed1888e1', 'd063f72ba7a540b5b8695806d685bec1', '-1', null, null, '1111');
INSERT INTO `mds_sfc_nc` VALUES ('39', '20182018070423392700000000000001', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'cd1e8884810b4b59a7920d71d94a30c2', '9fb221e3b9a044f7be3e53f31345437f', 'admin', '2018-07-07 17:14:10', '0092562100b64c6b8b1de8cd87bcea06', '044891f491ee4f5ea183361fea3cdaf4', '9507688dfa6b43658d542e317d450062', '-1', null, null, 'DF');
INSERT INTO `mds_sfc_nc` VALUES ('40', '201820180708203522000000000002XC', '0', '004f116b4dff48b5a013b56736af7860', '1eaaf0d20a5a4b258e23bfeaf7fc7312', 'admin', '2018-07-08 20:53:04', '37ac54169ef244658abbe13bb4927335', '597175d91a6949ec9db7d68a9a99add6', 'e364d12c945c4c5782b32d8eb86237df', '1', '1', '2fa55543fb5f4577ac1814eece2a43b1', 'DFC');

-- ----------------------------
-- Table structure for mds_sfc_step
-- ----------------------------
DROP TABLE IF EXISTS `mds_sfc_step`;
CREATE TABLE `mds_sfc_step` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `seq` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'sequence',
  `sfc` varchar(64) NOT NULL COMMENT 'sfc',
  `process_workflow_id` varchar(64) NOT NULL COMMENT '工艺路线PK_id',
  `operation_id` varchar(255) NOT NULL COMMENT '操作PK_id',
  `status` int(1) NOT NULL COMMENT '状态：0、排队中；1、生产中；2、已完成；',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `finish_time` datetime DEFAULT NULL COMMENT '完成时间',
  `workline_id` varchar(64) NOT NULL COMMENT '工作中心PK_id',
  `shoporder_id` varchar(64) NOT NULL COMMENT '工单PK_id',
  `site_id` varchar(64) NOT NULL COMMENT '站点PK_id',
  `work_resource_id` varchar(64) DEFAULT NULL COMMENT '资源ID',
  `item_id` varchar(64) NOT NULL COMMENT '成品物料ID',
  `shift_id` varchar(64) DEFAULT NULL COMMENT '班次ID',
  `workshop_id` varchar(64) DEFAULT NULL COMMENT '车间ID',
  `shoporder_sfc_id` varchar(64) NOT NULL COMMENT 'shoporder_sfc_id表PK',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `seq_id` (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_sfc_step
-- ----------------------------
INSERT INTO `mds_sfc_step` VALUES ('0de5bfd283224e4dba57b63c1b5e1625', '350', '201820180708203522000000000004XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '0', 'admin', '2018-07-08 20:35:22', null, 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', null, '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', '30a6f0fb7b5540c9be272a129acc5efb', null);
INSERT INTO `mds_sfc_step` VALUES ('3286d42d46ed4438a0996379f6310e3c', '353', '201820180708203522000000000001XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '2', 'admin', '2018-07-08 20:43:30', '2018-07-08 20:43:30', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'f79aa9a8163046ae9649928732aaa88a', null);
INSERT INTO `mds_sfc_step` VALUES ('3583585b3e46465db7492ed4e7611f71', '356', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '2', 'admin', '2018-07-08 20:53:04', '2018-07-08 20:53:04', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', 'DFC');
INSERT INTO `mds_sfc_step` VALUES ('35ccfbc15bc944198f6274824c761d35', '359', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '01565f643509455498d6a00e65fbde51', '2', 'admin', '2018-07-08 20:54:33', '2018-07-08 20:54:33', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', 'FGTH');
INSERT INTO `mds_sfc_step` VALUES ('425d5866e43e43da8c74cc2614def6bd', '360', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '0', 'admin', '2018-07-08 20:54:33', '2018-07-08 20:54:53', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', null);
INSERT INTO `mds_sfc_step` VALUES ('5625f32f5845465090600eb2b40a3bbe', '355', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '1', 'admin', '2018-07-08 20:43:41', '2018-07-08 20:53:04', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', null);
INSERT INTO `mds_sfc_step` VALUES ('5669139741e649da982d1be86129d59a', '347', '201820180708203522000000000001XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '0', 'admin', '2018-07-08 20:35:22', '2018-07-08 20:39:00', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'f79aa9a8163046ae9649928732aaa88a', null);
INSERT INTO `mds_sfc_step` VALUES ('5c197b1c8b6a4bfeb257dfa5546b264c', '361', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '1', 'admin', '2018-07-08 20:54:53', '2018-07-08 20:54:53', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', null);
INSERT INTO `mds_sfc_step` VALUES ('80a80d826b91419baeb56ccc57144402', '349', '201820180708203522000000000003XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '0', 'admin', '2018-07-08 20:35:22', null, 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', null, '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', '287dc01abaff4233b0b44e1d243371c4', null);
INSERT INTO `mds_sfc_step` VALUES ('98c4089e454c41cfbf5bbd3e6ea5e5f1', '351', '201820180708203522000000000005XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '0', 'admin', '2018-07-08 20:35:22', null, 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', null, '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', '368add97138f4996a35cd3af42924adb', null);
INSERT INTO `mds_sfc_step` VALUES ('ac8b3eff19da46b8aee086f8c67389dd', '357', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '01565f643509455498d6a00e65fbde51', '0', 'admin', '2018-07-08 20:53:04', '2018-07-08 20:53:56', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', null);
INSERT INTO `mds_sfc_step` VALUES ('b4022d7a156046ef87fb1633b1740e93', '358', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '01565f643509455498d6a00e65fbde51', '1', 'admin', '2018-07-08 20:53:56', '2018-07-08 20:54:33', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', null);
INSERT INTO `mds_sfc_step` VALUES ('b60ce02f25184a71ab133018d40b798a', '352', '201820180708203522000000000001XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '1', 'admin', '2018-07-08 20:39:00', '2018-07-08 20:43:30', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'f79aa9a8163046ae9649928732aaa88a', null);
INSERT INTO `mds_sfc_step` VALUES ('c2faff71a1a94c30a5c5568e53090817', '354', '201820180708203522000000000001XC', '905f98228bfb440482d3bee89b139617', 'adaf2e8c2a314be5ac717e39c105936d', '0', 'admin', '2018-07-08 20:43:30', null, 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', null, '2a02d37279fc43038dac8abc478df220', '', '7220466201db48f3b5f21672f44ee8fd', 'f79aa9a8163046ae9649928732aaa88a', null);
INSERT INTO `mds_sfc_step` VALUES ('ea92f34cf6f24c95aee285f7e21c7712', '348', '201820180708203522000000000002XC', '905f98228bfb440482d3bee89b139617', '004f116b4dff48b5a013b56736af7860', '0', 'admin', '2018-07-08 20:35:22', '2018-07-08 20:43:41', 'b6f24eb126274a69830f544e9d97556b', '37ac54169ef244658abbe13bb4927335', '0', '597175d91a6949ec9db7d68a9a99add6', '2a02d37279fc43038dac8abc478df220', null, '7220466201db48f3b5f21672f44ee8fd', 'a5ee60f5782240d1880a9379c132392a', null);

-- ----------------------------
-- Table structure for mds_shift
-- ----------------------------
DROP TABLE IF EXISTS `mds_shift`;
CREATE TABLE `mds_shift` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `site_id` varchar(255) NOT NULL COMMENT '站点PK_id',
  `shift_no` varchar(255) NOT NULL COMMENT '班次编号',
  `shift_desc` varchar(255) DEFAULT NULL COMMENT '班次描述',
  `shift_start_time` time NOT NULL COMMENT '班次开始时间',
  `shift_end_time` time NOT NULL COMMENT '班次结束时间',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_shift
-- ----------------------------
INSERT INTO `mds_shift` VALUES ('17f9895ec47b4f4f808fe5590527e83d', '0', 'shift_A', 'A班', '00:00:00', '08:00:00', 'admin', '2017-10-11 15:50:01');
INSERT INTO `mds_shift` VALUES ('243095541efd48b2bfd8d276531d9fd7', '6d70fed36bd94bdab1a71cde63954fa6', 's1', 'e', '11:18:34', '11:17:35', 'admin', '2017-11-03 11:16:39');
INSERT INTO `mds_shift` VALUES ('3d87fd98a90e44d3821046d6065cafe8', '6d70fed36bd94bdab1a71cde63954fa6', 'wb', '晚班', '12:00:00', '00:00:00', 'admin', '2017-10-19 10:01:13');
INSERT INTO `mds_shift` VALUES ('3eae7524c0c1409e839eba52920b2369', '0', 'shift_c', 'C班', '19:00:00', '22:00:00', 'admin', '2017-10-11 15:11:56');
INSERT INTO `mds_shift` VALUES ('4fb57fda986b46f4a73a2273c0240a36', 'b4ef0a63b39f48299795806d54cb17a5', 'night', '夜班', '18:00:00', '08:00:00', 'admin', '2017-10-25 14:13:42');
INSERT INTO `mds_shift` VALUES ('830d1afe93d94fff9f2b2cfadffbbd24', '365fe34663c94f5c8d06f8be76b8501d', 'night', '本班', '18:00:00', '06:00:00', 'z0001', '2017-11-04 16:05:08');
INSERT INTO `mds_shift` VALUES ('9490eb421d5a440086a78a9ee7f9e5fd', '365fe34663c94f5c8d06f8be76b8501d', 'day', '白班', '06:01:00', '18:00:00', 'admin', '2017-11-09 09:33:20');
INSERT INTO `mds_shift` VALUES ('99ff49d1d7de4e5bbea6053c10fe4255', '0', 'shift_b', 'B班', '08:00:00', '19:00:00', 'admin', '2017-10-11 15:12:18');
INSERT INTO `mds_shift` VALUES ('ab82b04b83f1425ea0dd1def4b662a2b', 'b4ef0a63b39f48299795806d54cb17a5', 'day', '白班', '08:00:00', '18:00:00', 'admin', '2017-10-25 14:12:57');
INSERT INTO `mds_shift` VALUES ('b10cab37636b461d9297e5fcece34c88', 'a67033a4395543adb40a6dcd4c65fbd4', 'bb', 'bb', '00:00:04', '08:00:12', 'zoufa', '2017-11-08 10:49:40');
INSERT INTO `mds_shift` VALUES ('c85555b21e42487ca79bbb9520d2d58c', '6d70fed36bd94bdab1a71cde63954fa6', 'tes', 'e', '19:33:46', '19:35:48', 'admin', '2017-10-25 19:32:55');
INSERT INTO `mds_shift` VALUES ('dbc5360f8a2c4a5cb6b0d70a676b7c9b', '6d70fed36bd94bdab1a71cde63954fa6', 's2', 'e', '11:18:34', '11:17:35', 'admin', '2017-11-03 11:16:49');
INSERT INTO `mds_shift` VALUES ('e16b8aff69ed424e99a6cf1af713bca0', '0', 'shift_d', '班次D', '08:00:00', '23:00:00', 'admin', '2017-10-11 15:49:10');

-- ----------------------------
-- Table structure for mds_shoporder
-- ----------------------------
DROP TABLE IF EXISTS `mds_shoporder`;
CREATE TABLE `mds_shoporder` (
  `id` varchar(64) NOT NULL,
  `shoporder_no` varchar(255) NOT NULL COMMENT '工单号',
  `shoporder_plan_start_time` datetime DEFAULT NULL COMMENT '工单计划开始时间',
  `shoporder_plan_end_time` datetime DEFAULT NULL COMMENT '工单计划结束时间',
  `shoporder_actual_start_time` datetime DEFAULT NULL COMMENT '工单实际开始时间',
  `shoporder_actual_end_date` datetime DEFAULT NULL COMMENT '工单实际结束时间',
  `process_workflow_id` varchar(255) NOT NULL COMMENT '工艺路线PK_id',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `shoporder_type` varchar(255) DEFAULT NULL COMMENT '工单类型',
  `shoporder_item_id` varchar(255) NOT NULL COMMENT '成品物料PK_id',
  `shoporder_number` int(11) NOT NULL COMMENT '工单数量',
  `shoporder_issued_number` int(11) NOT NULL COMMENT '工单已下达数量',
  `site_id` varchar(255) NOT NULL COMMENT '站点PK_id',
  `status` int(1) NOT NULL COMMENT '工单状态：0、创建；1、已下达；2、生产中；3、已完成；4、关闭；5、挂起；',
  `is_sfc_auto_generate` int(1) NOT NULL COMMENT 'sfc是否自动生成：-1:不自动生成；1:自动生成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_shoporder
-- ----------------------------
INSERT INTO `mds_shoporder` VALUES ('37ac54169ef244658abbe13bb4927335', '001', '2018-07-09 20:33:00', '2018-07-12 20:33:00', null, null, '905f98228bfb440482d3bee89b139617', 'admin', '2018-07-08 20:33:31', null, '2a02d37279fc43038dac8abc478df220', '10', '5', '0', '1', '1');

-- ----------------------------
-- Table structure for mds_shoporder_issuing_history
-- ----------------------------
DROP TABLE IF EXISTS `mds_shoporder_issuing_history`;
CREATE TABLE `mds_shoporder_issuing_history` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `issuing_shoporder_id` varchar(64) NOT NULL COMMENT '下达的工单PK_id',
  `issuing_number` int(11) NOT NULL COMMENT '当次下达数量',
  `issuing_workshop_id` varchar(64) NOT NULL COMMENT '当次下达车间PK_id',
  `issuing_workline_id` varchar(64) NOT NULL COMMENT '当次下达产线PK_id',
  `issuing_time` datetime NOT NULL COMMENT '当次下达时间',
  `site_id` varchar(64) NOT NULL COMMENT '站点PK_id',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_shoporder_issuing_history
-- ----------------------------
INSERT INTO `mds_shoporder_issuing_history` VALUES ('21bdaf56f90b4223850caec95c26d009', '37ac54169ef244658abbe13bb4927335', '5', '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '2018-07-08 20:35:22', '0', 'admin', '2018-07-08 20:35:22');

-- ----------------------------
-- Table structure for mds_shoporder_sfc
-- ----------------------------
DROP TABLE IF EXISTS `mds_shoporder_sfc`;
CREATE TABLE `mds_shoporder_sfc` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `shoporder_id` varchar(255) NOT NULL COMMENT '工单号PK_id',
  `sfc` varchar(255) NOT NULL COMMENT 'SFC',
  `sfc_status` int(1) NOT NULL COMMENT 'SFC状态：0、创建；1、生产中；2、已完成；3、报废；4、维修中',
  `site_id` varchar(64) NOT NULL COMMENT '站点PK_id',
  `sfc_finish_time` datetime DEFAULT NULL COMMENT '完成时间',
  `workshop_id` varchar(64) NOT NULL COMMENT '工作中心PK_id',
  `workline_id` varchar(64) NOT NULL COMMENT '工作中心PK_id',
  `is_printed` int(1) NOT NULL COMMENT '是否打印过:-1:未打印;1:已打印',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_shoporder_sfc
-- ----------------------------
INSERT INTO `mds_shoporder_sfc` VALUES ('287dc01abaff4233b0b44e1d243371c4', '37ac54169ef244658abbe13bb4927335', '201820180708203522000000000003XC', '0', '0', null, '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '-1', 'admin', '2018-07-08 20:35:22');
INSERT INTO `mds_shoporder_sfc` VALUES ('30a6f0fb7b5540c9be272a129acc5efb', '37ac54169ef244658abbe13bb4927335', '201820180708203522000000000004XC', '0', '0', null, '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '-1', 'admin', '2018-07-08 20:35:22');
INSERT INTO `mds_shoporder_sfc` VALUES ('368add97138f4996a35cd3af42924adb', '37ac54169ef244658abbe13bb4927335', '201820180708203522000000000005XC', '0', '0', null, '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '-1', 'admin', '2018-07-08 20:35:22');
INSERT INTO `mds_shoporder_sfc` VALUES ('a5ee60f5782240d1880a9379c132392a', '37ac54169ef244658abbe13bb4927335', '201820180708203522000000000002XC', '1', '0', null, '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '-1', 'admin', '2018-07-08 20:35:22');
INSERT INTO `mds_shoporder_sfc` VALUES ('f79aa9a8163046ae9649928732aaa88a', '37ac54169ef244658abbe13bb4927335', '201820180708203522000000000001XC', '1', '0', null, '7220466201db48f3b5f21672f44ee8fd', 'b6f24eb126274a69830f544e9d97556b', '-1', 'admin', '2018-07-08 20:35:22');

-- ----------------------------
-- Table structure for mds_shoporder_statuschange_history
-- ----------------------------
DROP TABLE IF EXISTS `mds_shoporder_statuschange_history`;
CREATE TABLE `mds_shoporder_statuschange_history` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `shoporder_id` varchar(64) NOT NULL COMMENT '工单PK_id',
  `shoporder_status_before_change` int(1) NOT NULL COMMENT '调整前工单状态',
  `shoporder_status_after_change` int(11) NOT NULL COMMENT '调整后工单状态',
  `shoporder_status_change_reason` varchar(255) DEFAULT NULL COMMENT '调整原因',
  `shoporder_status_change_user` varchar(255) NOT NULL COMMENT '调整人员',
  `shoporder_status_change_time` datetime NOT NULL COMMENT '调整时间',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_shoporder_statuschange_history
-- ----------------------------

-- ----------------------------
-- Table structure for mds_site
-- ----------------------------
DROP TABLE IF EXISTS `mds_site`;
CREATE TABLE `mds_site` (
  `id` varchar(64) NOT NULL,
  `site` varchar(255) NOT NULL,
  `site_description` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) NOT NULL,
  `status` int(1) DEFAULT NULL COMMENT '-1不可用，1可用',
  `create_user` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_site
-- ----------------------------
INSERT INTO `mds_site` VALUES ('0', '*', '默认站点', '*', '1', '', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for mds_sop
-- ----------------------------
DROP TABLE IF EXISTS `mds_sop`;
CREATE TABLE `mds_sop` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `resources_id` varchar(64) NOT NULL COMMENT 'resourcesPK_id',
  `sop_content` longtext COMMENT 'sop内容',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_sop
-- ----------------------------

-- ----------------------------
-- Table structure for mds_step_change_history
-- ----------------------------
DROP TABLE IF EXISTS `mds_step_change_history`;
CREATE TABLE `mds_step_change_history` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `sfc_id` varchar(64) NOT NULL COMMENT 'sfcPK_id',
  `step_id_before_change` varchar(64) NOT NULL COMMENT '调整前mds_sfc_stepPK_id',
  `step_id_after_change` varchar(64) NOT NULL COMMENT '调整后mds_sfc_stepPK_id',
  `change_remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `change_user` varchar(255) NOT NULL COMMENT '调整人员',
  `change_time` datetime NOT NULL COMMENT '调整时间',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_step_change_history
-- ----------------------------

-- ----------------------------
-- Table structure for mds_udefined_data
-- ----------------------------
DROP TABLE IF EXISTS `mds_udefined_data`;
CREATE TABLE `mds_udefined_data` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `data_type` varchar(255) NOT NULL COMMENT '自定义数据类型。如：工作中心，物料',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_udefined_data
-- ----------------------------
INSERT INTO `mds_udefined_data` VALUES ('157c3793f2f640668f0ecc7cec6c0da1', '365fe34663c94f5c8d06f8be76b8501d', 'routing', 'admin', '2017-11-08 17:12:47');
INSERT INTO `mds_udefined_data` VALUES ('24e59cd1519c4b0fa0e926154e7bfbde', '365fe34663c94f5c8d06f8be76b8501d', 'item_bom_maintenance', 'z0001', '2017-11-04 10:50:33');
INSERT INTO `mds_udefined_data` VALUES ('2f4cf0da1ecb4b129cbce54a8c32358f', '365fe34663c94f5c8d06f8be76b8501d', 'item_maintenance', 'admin', '2017-11-04 15:22:13');
INSERT INTO `mds_udefined_data` VALUES ('555b7d589add4bd296d16999285eaf35', '365fe34663c94f5c8d06f8be76b8501d', 'shop_order_maintenance', 'z0001', '2017-11-04 16:07:07');
INSERT INTO `mds_udefined_data` VALUES ('57925b0c218643a7802f7314dda54517', '365fe34663c94f5c8d06f8be76b8501d', 'workcenter', 'z0001', '2017-11-04 10:36:02');
INSERT INTO `mds_udefined_data` VALUES ('6654d8f2c8194855b6e0e44f79a356d3', '365fe34663c94f5c8d06f8be76b8501d', 'shift', 'z0001', '2017-11-04 16:04:20');
INSERT INTO `mds_udefined_data` VALUES ('678aff0e73b742478c7131b604bbfe5e', '0', 'item_maintenance', 'admin', '2017-10-29 18:56:06');
INSERT INTO `mds_udefined_data` VALUES ('8f6db14da69b4b90a4efb54673599536', '365fe34663c94f5c8d06f8be76b8501d', 'work_resource', 'admin', '2017-11-04 14:13:53');
INSERT INTO `mds_udefined_data` VALUES ('b5bf1951d0f0451885d4ee32f72f5b8e', '365fe34663c94f5c8d06f8be76b8501d', 'container_manager', 'z0001', '2017-11-04 16:02:23');
INSERT INTO `mds_udefined_data` VALUES ('ba4365df5e8b4c90a62594f2c8b7e46f', '0', 'workcenter', 'admin', '2018-06-11 23:24:41');
INSERT INTO `mds_udefined_data` VALUES ('ca51c0a0ab7d4921b0cb6ad217b909b9', '365fe34663c94f5c8d06f8be76b8501d', 'nc_code_maintenance', 'z0001', '2017-11-04 15:54:30');
INSERT INTO `mds_udefined_data` VALUES ('d1e401d0d7a246baa534e261416f832c', '365fe34663c94f5c8d06f8be76b8501d', 'operation_maintenance', 'admin', '2017-11-04 15:14:50');
INSERT INTO `mds_udefined_data` VALUES ('edcf43dccfed4e55897ba48da2935fc3', '6d70fed36bd94bdab1a71cde63954fa6', 'work_resource', 'admin', '2017-11-03 10:43:35');

-- ----------------------------
-- Table structure for mds_udefined_data_field
-- ----------------------------
DROP TABLE IF EXISTS `mds_udefined_data_field`;
CREATE TABLE `mds_udefined_data_field` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `udefined_data_id` varchar(64) NOT NULL COMMENT 'mds_udefined_data表的id',
  `data_key` varchar(255) NOT NULL COMMENT '字段的key',
  `site_id` varchar(255) NOT NULL COMMENT '站点',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `data_level` varchar(255) NOT NULL COMMENT '字段的顺序',
  `data_label` varchar(255) NOT NULL COMMENT '字段显示的标签',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_udefined_data_field
-- ----------------------------

-- ----------------------------
-- Table structure for mds_udefined_data_value
-- ----------------------------
DROP TABLE IF EXISTS `mds_udefined_data_value`;
CREATE TABLE `mds_udefined_data_value` (
  `id` varchar(64) NOT NULL,
  `data_type_detail_id` varchar(64) NOT NULL COMMENT '具体到某个功能下的具体实例的ID,如资源管理下某个资源的ID',
  `udefined_data_filed_id` varchar(64) NOT NULL COMMENT '自定义字段的ID',
  `value` varchar(255) DEFAULT NULL COMMENT '自定义字段的值',
  `site_id` varchar(64) NOT NULL COMMENT '站点ID',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_udefined_data_value
-- ----------------------------

-- ----------------------------
-- Table structure for mds_user
-- ----------------------------
DROP TABLE IF EXISTS `mds_user`;
CREATE TABLE `mds_user` (
  `id` varchar(64) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `account_name` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `credentials_salt` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `locked` varchar(3) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_status` int(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除状态0:存在1:删除',
  `site_id` varchar(255) NOT NULL,
  `create_user` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_user
-- ----------------------------
INSERT INTO `mds_user` VALUES ('0', 'abc', 'admin', '1c4935d0836639b0b71accd5d500ba27', 'aaacaf8c420b87eb1241422ec4339268', null, '0', '2018-03-29 16:30:47', '0', '0', 'admin');
INSERT INTO `mds_user` VALUES ('b59c30100aba4836baad49621e9678a6', 'TEST', 'test', 'b68300dc24b531ac5dd635a21cc1c75a', '3271d5e80244fb3835ab6449db4e2f2c', 'TEST', '0', '2018-07-02 11:45:03', '0', '4db61d7d29ed4c0c99b4580c22cc5bcf', 'admin');

-- ----------------------------
-- Table structure for mds_userlogin
-- ----------------------------
DROP TABLE IF EXISTS `mds_userlogin`;
CREATE TABLE `mds_userlogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(64) DEFAULT NULL COMMENT '用户id',
  `accountName` varchar(20) DEFAULT NULL COMMENT '用户名',
  `loginTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '登录时间',
  `loginIP` varchar(40) DEFAULT NULL COMMENT '登录IP',
  PRIMARY KEY (`id`),
  KEY `ly_user_loginlist` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5009 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_userlogin
-- ----------------------------
INSERT INTO `mds_userlogin` VALUES ('143', '3', 'admin', '2016-11-30 23:14:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('144', '3', 'admin', '2016-11-30 23:21:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('145', '3', 'admin', '2016-11-30 23:38:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('146', '3', 'admin', '2016-11-30 23:47:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('147', '3', 'admin', '2016-12-01 00:10:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('148', '3', 'admin', '2016-12-01 00:20:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('149', '3', 'admin', '2016-12-01 00:44:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('150', '3', 'admin', '2016-12-01 00:47:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('151', '3', 'admin', '2016-12-01 01:18:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('152', '3', 'admin', '2016-12-01 01:19:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('153', '3', 'admin', '2016-12-01 15:34:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('154', '3', 'admin', '2016-12-01 15:34:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('155', '3', 'admin', '2016-12-01 17:03:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('156', '3', 'admin', '2016-12-02 09:23:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('157', '3', 'admin', '2016-12-02 10:37:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('158', '3', 'admin', '2016-12-02 14:53:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('159', '3', 'admin', '2016-12-02 14:54:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('160', '3', 'admin', '2016-12-03 17:07:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('161', '3', 'admin', '2016-12-03 17:57:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('162', '3', 'admin', '2016-12-03 19:51:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('163', '3', 'admin', '2016-12-04 18:04:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('164', '3', 'admin', '2016-12-06 20:17:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('165', '3', 'admin', '2016-12-06 22:35:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('166', '3', 'admin', '2016-12-06 22:47:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('167', '3', 'admin', '2016-12-09 12:46:24', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('168', '3', 'admin', '2016-12-09 12:46:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('169', '3', 'admin', '2016-12-09 16:05:10', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('170', '3', 'admin', '2016-12-09 16:16:25', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('171', '3', 'admin', '2016-12-09 16:35:57', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('172', '3', 'admin', '2016-12-09 18:24:58', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('173', '3', 'admin', '2016-12-09 20:32:08', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('174', '3', 'admin', '2016-12-09 20:42:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('175', '3', 'admin', '2016-12-09 20:52:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('176', '3', 'admin', '2016-12-09 23:52:07', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('177', '3', 'admin', '2016-12-09 23:53:33', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('178', '3', 'admin', '2016-12-09 23:59:25', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('179', '3', 'admin', '2016-12-10 00:07:48', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('180', '3', 'admin', '2016-12-10 00:07:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('181', '3', 'admin', '2016-12-10 00:08:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('182', '3', 'admin', '2016-12-10 00:10:41', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('183', '3', 'admin', '2016-12-10 00:11:44', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('184', '3', 'admin', '2016-12-10 00:15:13', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('185', '3', 'admin', '2016-12-10 00:17:55', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('186', '3', 'admin', '2016-12-10 00:18:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('187', '3', 'admin', '2016-12-10 00:19:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('188', '3', 'admin', '2016-12-10 00:21:52', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('189', '3', 'admin', '2016-12-10 00:26:39', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('190', '3', 'admin', '2016-12-10 00:26:56', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('191', '3', 'admin', '2016-12-10 00:57:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('192', '3', 'admin', '2016-12-10 01:00:24', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('193', '3', 'admin', '2016-12-10 01:03:29', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('194', '3', 'admin', '2016-12-10 01:06:05', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('195', '3', 'admin', '2016-12-10 01:21:42', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('196', '3', 'admin', '2016-12-10 01:26:29', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('197', '3', 'admin', '2016-12-10 01:31:31', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('198', '3', 'admin', '2016-12-10 17:17:45', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('199', '3', 'admin', '2016-12-10 17:30:53', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('200', '3', 'admin', '2016-12-10 18:02:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('201', '3', 'admin', '2016-12-10 18:15:20', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('202', '3', 'admin', '2016-12-10 18:35:39', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('203', '3', 'admin', '2016-12-10 19:10:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('204', '3', 'admin', '2016-12-10 19:13:52', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('205', '3', 'admin', '2016-12-10 19:28:27', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('206', '3', 'admin', '2016-12-11 02:31:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('207', '3', 'admin', '2016-12-11 02:40:05', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('208', '3', 'admin', '2016-12-11 02:42:52', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('209', '3', 'admin', '2016-12-11 02:43:44', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('210', '3', 'admin', '2016-12-11 02:52:25', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('211', '3', 'admin', '2016-12-11 03:01:58', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('212', '3', 'admin', '2016-12-11 03:16:43', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('213', '3', 'admin', '2016-12-11 04:53:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('214', '3', 'admin', '2016-12-11 19:58:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('215', '3', 'admin', '2016-12-11 21:36:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('216', '3', 'admin', '2016-12-12 12:38:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('217', '3', 'admin', '2016-12-12 12:38:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('218', '3', 'admin', '2016-12-13 00:45:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('219', '3', 'admin', '2016-12-13 23:04:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('220', '3', 'admin', '2016-12-13 23:06:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('221', '3', 'admin', '2016-12-14 22:39:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('222', '3', 'admin', '2016-12-14 23:03:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('223', '3', 'admin', '2016-12-14 23:07:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('224', '3', 'admin', '2016-12-14 23:11:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('225', '3', 'admin', '2016-12-14 23:21:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('226', '3', 'admin', '2016-12-14 23:22:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('227', '3', 'admin', '2016-12-14 23:22:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('228', '3', 'admin', '2016-12-14 23:27:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('229', '5', 'zhanghao', '2016-12-14 23:35:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('230', '3', 'admin', '2016-12-14 23:36:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('231', '6', 'huahao', '2016-12-14 23:38:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('232', '3', 'admin', '2016-12-16 23:04:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('233', '3', 'admin', '2016-12-17 02:08:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('234', '3', 'admin', '2016-12-17 22:40:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('235', '3', 'admin', '2016-12-17 23:12:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('236', '3', 'admin', '2016-12-17 23:37:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('237', '3', 'admin', '2016-12-17 23:50:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('238', '3', 'admin', '2016-12-17 23:58:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('239', '3', 'admin', '2016-12-18 09:12:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('240', '3', 'admin', '2016-12-18 09:21:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('241', '3', 'admin', '2016-12-18 12:48:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('242', '3', 'admin', '2016-12-18 15:30:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('243', '3', 'admin', '2016-12-18 15:37:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('244', '3', 'admin', '2016-12-18 15:47:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('245', '3', 'admin', '2016-12-18 15:48:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('246', '3', 'admin', '2016-12-18 15:50:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('247', '3', 'admin', '2016-12-18 15:50:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('248', '3', 'admin', '2016-12-18 16:18:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('249', '3', 'admin', '2016-12-18 16:29:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('250', '3', 'admin', '2016-12-18 16:35:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('251', '3', 'admin', '2016-12-18 17:36:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('252', '3', 'admin', '2016-12-18 18:08:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('253', '3', 'admin', '2016-12-18 20:50:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('254', '3', 'admin', '2016-12-18 21:13:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('255', '3', 'admin', '2016-12-18 21:15:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('256', '3', 'admin', '2016-12-18 21:17:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('257', '3', 'admin', '2016-12-18 21:19:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('258', '3', 'admin', '2016-12-18 21:34:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('259', '3', 'admin', '2016-12-18 22:10:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('260', '3', 'admin', '2016-12-18 22:43:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('261', '3', 'admin', '2016-12-18 22:58:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('262', '3', 'admin', '2016-12-18 23:29:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('263', '3', 'admin', '2016-12-18 23:40:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('264', '3', 'admin', '2016-12-18 23:41:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('265', '3', 'admin', '2016-12-19 00:50:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('266', '3', 'admin', '2016-12-19 01:16:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('267', '3', 'admin', '2016-12-19 23:56:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('268', '3', 'admin', '2016-12-20 16:25:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('269', '3', 'admin', '2016-12-20 17:23:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('270', '3', 'admin', '2016-12-20 17:57:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('271', '3', 'admin', '2016-12-20 18:21:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('272', '3', 'admin', '2016-12-20 20:00:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('273', '3', 'admin', '2016-12-20 20:00:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('274', '3', 'admin', '2016-12-20 20:00:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('275', '3', 'admin', '2016-12-20 20:02:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('276', '3', 'admin', '2016-12-20 20:06:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('277', '3', 'admin', '2016-12-20 20:12:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('278', '3', 'admin', '2016-12-20 20:19:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('279', '3', 'admin', '2016-12-20 20:27:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('280', '3', 'admin', '2016-12-20 20:30:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('281', '3', 'admin', '2016-12-20 20:35:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('282', '3', 'admin', '2016-12-20 20:47:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('283', '3', 'admin', '2016-12-20 21:38:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('284', '3', 'admin', '2016-12-20 22:12:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('285', '3', 'admin', '2016-12-20 22:22:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('286', '3', 'admin', '2016-12-20 22:40:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('287', '3', 'admin', '2016-12-20 23:15:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('288', '3', 'admin', '2016-12-21 01:10:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('289', '3', 'admin', '2016-12-21 01:58:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('290', '3', 'admin', '2016-12-21 02:03:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('291', '3', 'admin', '2016-12-21 02:05:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('292', '3', 'admin', '2016-12-21 02:23:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('293', '3', 'admin', '2016-12-21 02:27:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('294', '3', 'admin', '2016-12-21 02:44:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('295', '3', 'admin', '2016-12-21 02:47:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('296', '3', 'admin', '2016-12-21 02:49:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('297', '3', 'admin', '2016-12-21 03:16:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('298', '3', 'admin', '2016-12-21 03:22:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('299', '3', 'admin', '2016-12-21 12:35:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('300', '3', 'admin', '2016-12-21 14:32:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('301', '3', 'admin', '2016-12-21 14:36:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('302', '3', 'admin', '2016-12-21 14:40:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('303', '3', 'admin', '2016-12-21 14:45:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('304', '3', 'admin', '2016-12-21 19:34:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('305', '3', 'admin', '2016-12-21 19:50:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('306', '3', 'admin', '2016-12-21 20:07:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('307', '3', 'admin', '2016-12-21 20:07:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('308', '3', 'admin', '2016-12-21 20:08:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('309', '3', 'admin', '2016-12-21 20:30:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('310', '3', 'admin', '2016-12-21 20:47:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('311', '3', 'admin', '2016-12-21 20:52:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('312', '3', 'admin', '2016-12-21 20:52:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('313', '3', 'admin', '2016-12-21 21:19:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('314', '3', 'admin', '2016-12-21 21:20:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('315', '3', 'admin', '2016-12-21 21:28:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('316', '3', 'admin', '2016-12-21 22:08:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('317', '3', 'admin', '2016-12-21 22:28:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('318', '3', 'admin', '2016-12-21 22:30:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('319', '3', 'admin', '2016-12-21 22:41:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('320', '3', 'admin', '2016-12-21 23:14:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('321', '3', 'admin', '2016-12-21 23:29:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('322', '3', 'admin', '2016-12-21 23:41:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('323', '3', 'admin', '2016-12-22 00:13:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('324', '3', 'admin', '2016-12-22 01:33:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('325', '3', 'admin', '2016-12-22 02:16:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('326', '3', 'admin', '2016-12-22 02:37:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('327', '3', 'admin', '2016-12-22 02:42:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('328', '3', 'admin', '2016-12-22 02:44:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('329', '3', 'admin', '2016-12-22 02:50:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('330', '3', 'admin', '2016-12-22 02:56:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('331', '3', 'admin', '2016-12-22 03:03:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('332', '3', 'admin', '2016-12-22 03:10:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('333', '3', 'admin', '2016-12-22 03:13:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('334', '3', 'admin', '2016-12-22 03:22:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('335', '3', 'admin', '2016-12-22 03:33:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('336', '3', 'admin', '2016-12-22 04:21:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('337', '3', 'admin', '2016-12-22 04:23:47', '116.4.154.116');
INSERT INTO `mds_userlogin` VALUES ('338', '3', 'admin', '2016-12-22 04:28:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('339', '3', 'admin', '2016-12-22 05:22:29', '223.104.22.54');
INSERT INTO `mds_userlogin` VALUES ('340', '3', 'admin', '2016-12-22 05:22:50', '223.104.22.54');
INSERT INTO `mds_userlogin` VALUES ('341', '3', 'admin', '2016-12-22 05:23:26', '223.104.22.54');
INSERT INTO `mds_userlogin` VALUES ('342', '3', 'admin', '2016-12-22 05:23:34', '223.104.22.54');
INSERT INTO `mds_userlogin` VALUES ('343', '3', 'admin', '2016-12-22 05:23:42', '223.104.22.54');
INSERT INTO `mds_userlogin` VALUES ('344', '3', 'admin', '2016-12-22 05:29:45', '113.16.150.1');
INSERT INTO `mds_userlogin` VALUES ('345', '3', 'admin', '2016-12-22 08:35:33', '113.16.150.136');
INSERT INTO `mds_userlogin` VALUES ('346', '3', 'admin', '2016-12-22 09:17:28', '113.16.150.1');
INSERT INTO `mds_userlogin` VALUES ('347', '3', 'admin', '2016-12-22 09:59:21', '223.104.22.54');
INSERT INTO `mds_userlogin` VALUES ('348', '3', 'admin', '2016-12-22 12:52:40', '117.136.39.233');
INSERT INTO `mds_userlogin` VALUES ('349', '3', 'admin', '2016-12-22 12:53:24', '14.152.69.151');
INSERT INTO `mds_userlogin` VALUES ('350', '3', 'admin', '2016-12-22 18:27:20', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('351', '3', 'admin', '2016-12-24 20:46:59', '223.104.1.131');
INSERT INTO `mds_userlogin` VALUES ('352', '3', 'admin', '2016-12-24 21:46:25', '59.40.14.10');
INSERT INTO `mds_userlogin` VALUES ('353', '3', 'admin', '2016-12-24 22:33:16', '223.104.1.131');
INSERT INTO `mds_userlogin` VALUES ('354', '3', 'admin', '2016-12-24 23:28:51', '223.104.1.131');
INSERT INTO `mds_userlogin` VALUES ('355', '3', 'admin', '2016-12-25 00:50:35', '223.104.1.131');
INSERT INTO `mds_userlogin` VALUES ('356', '3', 'admin', '2016-12-25 01:32:47', '223.104.1.131');
INSERT INTO `mds_userlogin` VALUES ('357', '3', 'admin', '2016-12-25 02:04:16', '223.104.1.131');
INSERT INTO `mds_userlogin` VALUES ('358', '3', 'admin', '2016-12-26 02:04:53', '14.156.106.74');
INSERT INTO `mds_userlogin` VALUES ('359', '3', 'admin', '2016-12-26 23:13:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('360', '3', 'admin', '2016-12-27 00:13:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('361', '3', 'admin', '2016-12-27 12:46:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('362', '3', 'admin', '2016-12-27 13:07:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('363', '3', 'admin', '2016-12-27 23:16:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('364', '3', 'admin', '2016-12-28 00:01:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('365', '3', 'admin', '2016-12-28 00:01:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('366', '3', 'admin', '2016-12-28 16:35:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('367', '3', 'admin', '2016-12-28 18:04:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('368', '3', 'admin', '2016-12-28 20:11:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('369', '3', 'admin', '2016-12-28 20:49:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('370', '3', 'admin', '2016-12-28 20:54:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('371', '3', 'admin', '2016-12-28 20:59:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('372', '3', 'admin', '2016-12-28 21:01:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('373', '3', 'admin', '2016-12-28 21:07:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('374', '3', 'admin', '2016-12-28 21:10:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('375', '3', 'admin', '2016-12-28 21:22:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('376', '3', 'admin', '2016-12-28 21:25:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('377', '3', 'admin', '2016-12-28 21:33:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('378', '3', 'admin', '2016-12-30 20:46:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('379', '3', 'admin', '2016-12-30 20:52:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('380', '3', 'admin', '2016-12-30 20:53:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('381', '3', 'admin', '2016-12-30 21:08:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('382', '3', 'admin', '2016-12-30 22:35:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('383', '3', 'admin', '2016-12-30 23:09:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('384', '3', 'admin', '2016-12-31 19:59:03', '14.156.107.71');
INSERT INTO `mds_userlogin` VALUES ('385', '3', 'admin', '2016-12-31 20:05:00', '120.197.198.37');
INSERT INTO `mds_userlogin` VALUES ('386', '3', 'admin', '2016-12-31 20:05:28', '120.197.198.37');
INSERT INTO `mds_userlogin` VALUES ('387', '3', 'admin', '2016-12-31 20:06:13', '120.197.198.37');
INSERT INTO `mds_userlogin` VALUES ('388', '3', 'admin', '2016-12-31 20:06:20', '120.197.198.38');
INSERT INTO `mds_userlogin` VALUES ('389', '3', 'admin', '2016-12-31 20:06:43', '120.197.198.38');
INSERT INTO `mds_userlogin` VALUES ('390', '3', 'admin', '2016-12-31 20:40:19', '120.197.198.49');
INSERT INTO `mds_userlogin` VALUES ('391', '3', 'admin', '2016-12-31 20:48:24', '120.197.198.61');
INSERT INTO `mds_userlogin` VALUES ('392', '3', 'admin', '2016-12-31 21:20:41', '14.156.107.71');
INSERT INTO `mds_userlogin` VALUES ('393', '3', 'admin', '2017-01-01 09:51:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('394', '3', 'admin', '2017-01-01 10:41:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('395', '3', 'admin', '2017-01-01 14:52:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('396', '3', 'admin', '2017-01-01 14:57:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('397', '3', 'admin', '2017-01-01 20:46:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('398', '3', 'admin', '2017-01-01 20:46:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('399', '3', 'admin', '2017-01-01 20:47:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('400', '3', 'admin', '2017-01-01 20:55:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('401', '3', 'admin', '2017-01-01 20:57:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('402', '3', 'admin', '2017-01-01 21:02:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('403', '3', 'admin', '2017-01-02 02:42:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('404', '3', 'admin', '2017-01-02 08:33:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('405', '3', 'admin', '2017-01-02 08:48:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('406', '3', 'admin', '2017-01-02 08:48:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('407', '3', 'admin', '2017-01-02 08:48:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('408', '3', 'admin', '2017-01-02 08:51:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('409', '3', 'admin', '2017-01-02 08:57:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('410', '3', 'admin', '2017-01-02 09:00:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('411', '3', 'admin', '2017-01-02 09:04:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('412', '3', 'admin', '2017-01-02 09:09:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('413', '3', 'admin', '2017-01-02 09:11:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('414', '3', 'admin', '2017-01-02 09:12:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('415', '3', 'admin', '2017-01-02 09:13:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('416', '3', 'admin', '2017-01-02 09:17:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('417', '3', 'admin', '2017-01-02 09:27:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('418', '3', 'admin', '2017-01-02 09:50:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('419', '3', 'admin', '2017-01-02 09:59:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('420', '3', 'admin', '2017-01-02 10:01:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('421', '3', 'admin', '2017-01-02 10:14:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('422', '3', 'admin', '2017-01-02 10:31:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('423', '3', 'admin', '2017-01-02 10:32:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('424', '3', 'admin', '2017-01-02 10:33:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('425', '3', 'admin', '2017-01-02 10:35:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('426', '3', 'admin', '2017-01-02 10:36:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('427', '3', 'admin', '2017-01-02 10:39:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('428', '3', 'admin', '2017-01-02 10:40:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('429', '3', 'admin', '2017-01-02 10:43:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('430', '3', 'admin', '2017-01-02 10:45:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('431', '3', 'admin', '2017-01-02 10:47:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('432', '3', 'admin', '2017-01-02 10:52:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('433', '3', 'admin', '2017-01-02 10:54:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('434', '3', 'admin', '2017-01-02 10:58:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('435', '3', 'admin', '2017-01-02 11:04:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('436', '3', 'admin', '2017-01-02 11:27:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('437', '3', 'admin', '2017-01-02 11:40:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('438', '3', 'admin', '2017-01-02 11:46:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('439', '3', 'admin', '2017-01-02 11:48:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('440', '3', 'admin', '2017-01-02 11:50:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('441', '3', 'admin', '2017-01-02 11:51:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('442', '3', 'admin', '2017-01-02 11:52:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('443', '3', 'admin', '2017-01-02 11:56:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('444', '3', 'admin', '2017-01-02 11:57:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('445', '3', 'admin', '2017-01-02 11:57:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('446', '3', 'admin', '2017-01-02 12:08:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('447', '3', 'admin', '2017-01-02 12:13:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('448', '3', 'admin', '2017-01-02 12:31:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('449', '3', 'admin', '2017-01-02 13:15:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('450', '3', 'admin', '2017-01-02 14:56:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('451', '3', 'admin', '2017-01-02 15:04:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('452', '3', 'admin', '2017-01-02 15:29:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('453', '3', 'admin', '2017-01-02 16:17:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('454', '3', 'admin', '2017-01-02 16:17:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('455', '3', 'admin', '2017-01-02 16:17:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('456', '3', 'admin', '2017-01-02 16:17:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('457', '3', 'admin', '2017-01-02 17:49:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('458', '3', 'admin', '2017-01-02 17:55:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('459', '3', 'admin', '2017-01-02 18:19:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('460', '3', 'admin', '2017-01-02 18:28:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('461', '3', 'admin', '2017-01-02 18:30:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('462', '3', 'admin', '2017-01-02 18:33:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('463', '3', 'admin', '2017-01-02 18:36:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('464', '3', 'admin', '2017-01-02 18:43:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('465', '3', 'admin', '2017-01-02 20:42:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('466', '3', 'admin', '2017-01-02 20:58:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('467', '3', 'admin', '2017-01-02 21:08:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('468', '3', 'admin', '2017-01-02 21:29:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('469', '3', 'admin', '2017-01-02 21:35:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('470', '3', 'admin', '2017-01-02 21:46:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('471', '3', 'admin', '2017-01-02 21:47:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('472', '3', 'admin', '2017-01-02 21:57:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('473', '3', 'admin', '2017-01-02 22:18:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('474', '3', 'admin', '2017-01-02 22:46:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('475', '3', 'admin', '2017-01-02 22:51:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('476', '3', 'admin', '2017-01-02 22:53:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('477', '3', 'admin', '2017-01-02 22:54:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('478', '3', 'admin', '2017-01-03 11:25:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('479', '3', 'admin', '2017-01-03 14:22:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('480', '3', 'admin', '2017-01-03 14:43:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('481', '3', 'admin', '2017-01-03 15:15:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('482', '3', 'admin', '2017-01-03 16:16:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('483', '3', 'admin', '2017-01-03 17:06:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('484', '3', 'admin', '2017-01-03 17:56:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('485', '3', 'admin', '2017-01-03 21:00:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('486', '3', 'admin', '2017-01-03 21:33:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('487', '3', 'admin', '2017-01-03 21:37:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('488', '3', 'admin', '2017-01-03 21:39:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('489', '3', 'admin', '2017-01-03 21:55:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('490', '3', 'admin', '2017-01-03 21:59:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('491', '3', 'admin', '2017-01-03 22:31:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('492', '3', 'admin', '2017-01-03 22:34:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('493', '3', 'admin', '2017-01-03 22:46:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('494', '3', 'admin', '2017-01-03 22:52:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('495', '3', 'admin', '2017-01-03 22:58:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('496', '3', 'admin', '2017-01-03 23:04:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('497', '3', 'admin', '2017-01-03 23:09:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('498', '3', 'admin', '2017-01-03 23:11:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('499', '3', 'admin', '2017-01-03 23:21:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('500', '3', 'admin', '2017-01-03 23:36:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('501', '3', 'admin', '2017-01-04 00:19:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('502', '3', 'admin', '2017-01-04 00:49:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('503', '3', 'admin', '2017-01-04 00:51:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('504', '3', 'admin', '2017-01-04 00:51:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('505', '3', 'admin', '2017-01-04 01:10:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('506', '3', 'admin', '2017-01-04 01:27:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('507', '3', 'admin', '2017-01-04 02:01:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('508', '3', 'admin', '2017-01-04 02:07:32', '113.78.166.38');
INSERT INTO `mds_userlogin` VALUES ('509', '3', 'admin', '2017-01-04 02:15:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('510', '3', 'admin', '2017-01-04 10:58:08', '112.98.10.223');
INSERT INTO `mds_userlogin` VALUES ('511', '3', 'admin', '2017-01-04 11:02:02', '106.39.189.75');
INSERT INTO `mds_userlogin` VALUES ('512', '3', 'admin', '2017-01-04 15:06:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('513', '3', 'admin', '2017-01-04 15:55:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('514', '3', 'admin', '2017-01-04 16:27:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('515', '3', 'admin', '2017-01-04 17:15:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('516', '3', 'admin', '2017-01-04 17:25:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('517', '3', 'admin', '2017-01-04 17:44:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('518', '3', 'admin', '2017-01-04 17:57:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('519', '3', 'admin', '2017-01-04 18:14:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('520', '3', 'admin', '2017-01-04 19:09:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('521', '3', 'admin', '2017-01-04 21:35:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('522', '3', 'admin', '2017-01-04 21:49:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('523', '3', 'admin', '2017-01-04 21:51:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('524', '3', 'admin', '2017-01-04 22:12:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('525', '3', 'admin', '2017-01-04 22:17:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('526', '3', 'admin', '2017-01-04 22:21:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('527', '3', 'admin', '2017-01-04 22:31:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('528', '3', 'admin', '2017-01-04 22:37:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('529', '3', 'admin', '2017-01-04 23:05:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('530', '3', 'admin', '2017-01-04 23:06:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('531', '3', 'admin', '2017-01-04 23:31:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('532', '3', 'admin', '2017-01-04 23:41:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('533', '3', 'admin', '2017-01-04 23:44:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('534', '3', 'admin', '2017-01-04 23:59:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('535', '3', 'admin', '2017-01-05 00:07:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('536', '3', 'admin', '2017-01-05 00:11:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('537', '3', 'admin', '2017-01-05 00:11:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('538', '3', 'admin', '2017-01-05 00:16:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('539', '3', 'admin', '2017-01-05 01:58:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('540', '3', 'admin', '2017-01-05 03:08:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('541', '3', 'admin', '2017-01-05 03:12:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('542', '3', 'admin', '2017-01-05 03:30:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('543', '3', 'admin', '2017-01-05 03:34:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('544', '3', 'admin', '2017-01-05 03:42:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('545', '3', 'admin', '2017-01-05 03:49:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('546', '3', 'admin', '2017-01-05 03:52:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('547', '3', 'admin', '2017-01-05 03:55:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('548', '3', 'admin', '2017-01-05 03:57:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('549', '3', 'admin', '2017-01-05 04:10:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('550', '3', 'admin', '2017-01-05 04:20:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('551', '3', 'admin', '2017-01-05 04:25:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('552', '3', 'admin', '2017-01-05 07:22:59', '183.12.240.214');
INSERT INTO `mds_userlogin` VALUES ('553', '3', 'admin', '2017-01-05 07:30:57', '183.12.240.214');
INSERT INTO `mds_userlogin` VALUES ('554', '3', 'admin', '2017-01-05 09:22:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('555', '3', 'admin', '2017-01-05 09:25:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('556', '3', 'admin', '2017-01-05 10:24:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('557', '3', 'admin', '2017-01-05 10:26:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('558', '3', 'admin', '2017-01-05 10:38:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('559', '3', 'admin', '2017-01-05 10:41:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('560', '3', 'admin', '2017-01-05 10:52:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('561', '3', 'admin', '2017-01-05 11:48:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('562', '3', 'admin', '2017-01-05 11:53:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('563', '3', 'admin', '2017-01-05 11:53:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('564', '3', 'admin', '2017-01-05 11:54:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('565', '3', 'admin', '2017-01-05 14:26:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('566', '3', 'admin', '2017-01-05 14:33:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('567', '3', 'admin', '2017-01-05 14:38:22', '119.136.82.34');
INSERT INTO `mds_userlogin` VALUES ('568', '3', 'admin', '2017-01-05 14:38:47', '119.136.82.34');
INSERT INTO `mds_userlogin` VALUES ('569', '3', 'admin', '2017-01-05 14:55:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('570', '3', 'admin', '2017-01-05 15:49:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('571', '3', 'admin', '2017-01-05 18:08:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('572', '3', 'admin', '2017-01-05 18:11:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('573', '3', 'admin', '2017-01-05 18:15:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('574', '3', 'admin', '2017-01-05 18:19:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('575', '3', 'admin', '2017-01-05 18:24:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('576', '3', 'admin', '2017-01-05 18:34:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('577', '3', 'admin', '2017-01-05 18:39:32', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('578', '3', 'admin', '2017-01-05 18:39:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('579', '3', 'admin', '2017-01-05 18:52:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('580', '3', 'admin', '2017-01-05 20:02:42', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('581', '3', 'admin', '2017-01-05 20:02:42', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('582', '3', 'admin', '2017-01-05 20:02:43', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('583', '3', 'admin', '2017-01-05 20:02:44', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('584', '3', 'admin', '2017-01-05 20:02:44', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('585', '3', 'admin', '2017-01-05 20:02:44', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('586', '3', 'admin', '2017-01-05 20:02:44', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('587', '3', 'admin', '2017-01-05 20:02:45', '27.38.17.110');
INSERT INTO `mds_userlogin` VALUES ('588', '3', 'admin', '2017-01-05 21:23:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('589', '3', 'admin', '2017-01-05 21:30:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('590', '3', 'admin', '2017-01-05 22:50:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('591', '3', 'admin', '2017-01-05 22:52:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('592', '3', 'admin', '2017-01-05 22:57:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('593', '3', 'admin', '2017-01-05 23:08:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('594', '3', 'admin', '2017-01-05 23:14:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('595', '3', 'admin', '2017-01-06 01:14:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('596', '3', 'admin', '2017-01-06 01:18:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('597', '3', 'admin', '2017-01-06 10:03:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('598', '3', 'admin', '2017-01-06 12:25:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('599', '3', 'admin', '2017-01-06 12:36:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('600', '3', 'admin', '2017-01-06 12:55:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('601', '3', 'admin', '2017-01-06 13:03:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('602', '3', 'admin', '2017-01-08 09:36:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('603', '3', 'admin', '2017-01-08 10:05:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('604', '3', 'admin', '2017-01-08 10:26:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('605', '3', 'admin', '2017-01-08 10:37:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('606', '3', 'admin', '2017-01-08 10:41:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('607', '3', 'admin', '2017-01-08 10:53:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('608', '3', 'admin', '2017-01-08 11:02:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('609', '3', 'admin', '2017-01-08 11:04:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('610', '3', 'admin', '2017-01-08 11:25:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('611', '3', 'admin', '2017-01-08 11:32:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('612', '3', 'admin', '2017-01-08 11:54:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('613', '3', 'admin', '2017-01-08 11:56:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('614', '3', 'admin', '2017-01-08 11:57:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('615', '3', 'admin', '2017-01-08 11:57:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('616', '3', 'admin', '2017-01-08 12:12:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('617', '3', 'admin', '2017-01-08 13:06:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('618', '3', 'admin', '2017-01-08 21:24:22', '117.136.40.39');
INSERT INTO `mds_userlogin` VALUES ('619', '3', 'admin', '2017-01-08 21:30:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('620', '3', 'admin', '2017-01-09 10:55:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('621', '3', 'admin', '2017-01-09 22:57:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('622', '3', 'admin', '2017-01-10 00:30:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('623', '3', 'admin', '2017-01-10 01:13:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('624', '3', 'admin', '2017-01-10 01:33:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('625', '3', 'admin', '2017-01-10 01:41:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('626', '3', 'admin', '2017-01-10 23:05:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('627', '3', 'admin', '2017-01-10 23:37:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('628', '3', 'admin', '2017-01-10 23:48:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('629', '3', 'admin', '2017-01-10 23:49:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('630', '3', 'admin', '2017-01-10 23:50:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('631', '3', 'admin', '2017-01-10 23:54:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('632', '3', 'admin', '2017-01-10 23:59:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('633', '3', 'admin', '2017-01-11 00:01:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('634', '3', 'admin', '2017-01-11 00:10:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('635', '3', 'admin', '2017-01-11 00:12:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('636', '3', 'admin', '2017-01-11 00:16:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('637', '3', 'admin', '2017-01-11 00:17:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('638', '3', 'admin', '2017-01-11 00:30:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('639', '3', 'admin', '2017-01-11 01:18:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('640', '3', 'admin', '2017-01-11 01:29:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('641', '3', 'admin', '2017-01-11 03:17:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('642', '3', 'admin', '2017-01-11 03:27:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('643', '3', 'admin', '2017-01-11 03:57:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('644', '3', 'admin', '2017-01-11 16:33:04', '223.104.90.92');
INSERT INTO `mds_userlogin` VALUES ('645', '3', 'admin', '2017-01-11 16:33:04', '223.104.90.92');
INSERT INTO `mds_userlogin` VALUES ('646', '3', 'admin', '2017-01-11 16:33:04', '223.104.90.92');
INSERT INTO `mds_userlogin` VALUES ('647', '3', 'admin', '2017-01-11 16:33:04', '223.104.90.92');
INSERT INTO `mds_userlogin` VALUES ('648', '3', 'admin', '2017-01-11 16:33:05', '223.104.90.92');
INSERT INTO `mds_userlogin` VALUES ('649', '3', 'admin', '2017-01-11 16:33:08', '223.104.90.92');
INSERT INTO `mds_userlogin` VALUES ('650', '3', 'admin', '2017-01-11 22:48:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('651', '3', 'admin', '2017-01-11 23:22:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('652', '3', 'admin', '2017-01-11 23:28:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('653', '3', 'admin', '2017-01-11 23:33:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('654', '3', 'admin', '2017-01-12 06:15:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('655', '3', 'admin', '2017-01-12 06:16:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('656', '3', 'admin', '2017-01-12 06:16:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('657', '3', 'admin', '2017-01-12 06:33:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('658', '3', 'admin', '2017-01-12 06:45:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('659', '3', 'admin', '2017-01-12 07:27:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('660', '3', 'admin', '2017-01-12 07:31:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('661', '3', 'admin', '2017-01-12 07:44:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('662', '3', 'admin', '2017-01-12 07:50:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('663', '3', 'admin', '2017-01-12 08:42:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('664', '3', 'admin', '2017-01-12 10:41:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('665', '3', 'admin', '2017-01-12 10:49:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('666', '3', 'admin', '2017-01-12 10:50:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('667', '3', 'admin', '2017-01-12 12:52:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('668', '3', 'admin', '2017-01-12 12:54:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('669', '3', 'admin', '2017-01-12 14:02:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('670', '3', 'admin', '2017-01-12 14:27:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('671', '3', 'admin', '2017-01-12 14:29:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('672', '3', 'admin', '2017-01-12 14:45:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('673', '3', 'admin', '2017-01-12 14:49:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('674', '3', 'admin', '2017-01-14 23:18:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('675', '3', 'admin', '2017-01-15 03:02:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('676', '3', 'admin', '2017-01-15 08:10:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('677', '3', 'admin', '2017-01-15 10:25:05', '223.104.90.73');
INSERT INTO `mds_userlogin` VALUES ('678', '3', 'admin', '2017-01-16 00:30:23', '113.16.140.214');
INSERT INTO `mds_userlogin` VALUES ('679', '3', 'admin', '2017-01-16 08:44:26', '223.104.91.76');
INSERT INTO `mds_userlogin` VALUES ('680', '3', 'admin', '2017-01-23 22:02:10', '14.220.120.118');
INSERT INTO `mds_userlogin` VALUES ('681', '3', 'admin', '2017-01-23 22:10:58', '14.220.120.118');
INSERT INTO `mds_userlogin` VALUES ('682', '3', 'admin', '2017-01-23 22:38:13', '223.73.191.16');
INSERT INTO `mds_userlogin` VALUES ('683', '3', 'admin', '2017-02-03 15:57:44', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('684', '3', 'admin', '2017-02-03 15:58:01', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('685', '3', 'admin', '2017-02-09 21:10:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('686', '3', 'admin', '2017-02-09 21:30:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('687', '3', 'admin', '2017-02-09 21:30:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('688', '3', 'admin', '2017-02-14 22:51:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('689', '3', 'admin', '2017-02-14 23:13:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('690', '3', 'admin', '2017-02-15 21:02:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('691', '3', 'admin', '2017-02-15 22:06:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('692', '3', 'admin', '2017-02-15 22:09:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('693', '3', 'admin', '2017-02-15 22:49:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('694', '3', 'admin', '2017-02-15 23:09:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('695', '3', 'admin', '2017-02-15 23:59:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('696', '3', 'admin', '2017-02-16 00:02:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('697', '3', 'admin', '2017-02-16 00:04:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('698', '3', 'admin', '2017-02-16 00:11:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('699', '3', 'admin', '2017-02-16 00:15:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('700', '3', 'admin', '2017-02-16 00:21:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('701', '3', 'admin', '2017-02-16 15:27:52', '27.38.177.134');
INSERT INTO `mds_userlogin` VALUES ('702', '3', 'admin', '2017-02-16 15:39:28', '27.38.177.134');
INSERT INTO `mds_userlogin` VALUES ('703', '3', 'admin', '2017-02-16 16:22:34', '27.38.176.32');
INSERT INTO `mds_userlogin` VALUES ('704', '3', 'admin', '2017-02-16 16:22:40', '27.38.176.32');
INSERT INTO `mds_userlogin` VALUES ('705', '8', 'sukeyu', '2017-02-16 16:23:23', '27.38.176.32');
INSERT INTO `mds_userlogin` VALUES ('706', '3', 'admin', '2017-02-16 16:23:34', '163.125.210.32');
INSERT INTO `mds_userlogin` VALUES ('707', '3', 'admin', '2017-02-16 21:13:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('708', '3', 'admin', '2017-02-16 21:32:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('709', '3', 'admin', '2017-02-16 21:38:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('710', '3', 'admin', '2017-02-16 21:40:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('711', '3', 'admin', '2017-02-16 21:40:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('712', '3', 'admin', '2017-02-16 21:41:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('713', '3', 'admin', '2017-02-16 21:48:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('714', '3', 'admin', '2017-02-16 22:33:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('715', '3', 'admin', '2017-02-16 22:41:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('716', '3', 'admin', '2017-02-16 22:43:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('717', '3', 'admin', '2017-02-16 23:01:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('718', '3', 'admin', '2017-02-16 23:25:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('719', '3', 'admin', '2017-02-16 23:26:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('720', '3', 'admin', '2017-02-16 23:31:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('721', '3', 'admin', '2017-02-16 23:36:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('722', '3', 'admin', '2017-02-16 23:44:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('723', '3', 'admin', '2017-02-16 23:51:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('724', '3', 'admin', '2017-02-16 23:54:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('725', '3', 'admin', '2017-02-17 00:00:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('726', '3', 'admin', '2017-02-17 00:20:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('727', '3', 'admin', '2017-02-17 23:38:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('728', '3', 'admin', '2017-02-18 01:14:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('729', '3', 'admin', '2017-02-18 01:39:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('730', '3', 'admin', '2017-02-18 02:15:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('731', '3', 'admin', '2017-02-18 02:26:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('732', '3', 'admin', '2017-02-18 02:40:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('733', '3', 'admin', '2017-02-18 13:59:13', '27.38.176.32');
INSERT INTO `mds_userlogin` VALUES ('734', '3', 'admin', '2017-02-18 14:34:00', '27.46.7.169');
INSERT INTO `mds_userlogin` VALUES ('735', '3', 'admin', '2017-02-18 14:41:08', '58.251.169.64');
INSERT INTO `mds_userlogin` VALUES ('736', '3', 'admin', '2017-02-18 14:59:21', '58.251.169.64');
INSERT INTO `mds_userlogin` VALUES ('737', '3', 'admin', '2017-02-18 15:00:10', '27.46.7.186');
INSERT INTO `mds_userlogin` VALUES ('738', '3', 'admin', '2017-02-18 15:05:26', '27.46.7.180');
INSERT INTO `mds_userlogin` VALUES ('739', '3', 'admin', '2017-02-18 15:36:14', '27.46.7.180');
INSERT INTO `mds_userlogin` VALUES ('740', '3', 'admin', '2017-02-18 15:42:20', '27.46.7.180');
INSERT INTO `mds_userlogin` VALUES ('741', '3', 'admin', '2017-02-18 15:47:35', '27.46.7.180');
INSERT INTO `mds_userlogin` VALUES ('742', '3', 'admin', '2017-02-18 15:50:31', '27.46.7.180');
INSERT INTO `mds_userlogin` VALUES ('743', '3', 'admin', '2017-02-18 15:54:24', '27.46.7.180');
INSERT INTO `mds_userlogin` VALUES ('744', '3', 'admin', '2017-02-18 16:01:56', '27.46.7.180');
INSERT INTO `mds_userlogin` VALUES ('745', '3', 'admin', '2017-02-18 16:18:29', '27.38.176.116');
INSERT INTO `mds_userlogin` VALUES ('746', '3', 'admin', '2017-02-18 16:21:44', '27.38.176.116');
INSERT INTO `mds_userlogin` VALUES ('747', '3', 'admin', '2017-02-18 16:22:49', '27.38.176.116');
INSERT INTO `mds_userlogin` VALUES ('748', '3', 'admin', '2017-02-18 16:27:40', '27.38.176.116');
INSERT INTO `mds_userlogin` VALUES ('749', '3', 'admin', '2017-02-18 16:29:06', '163.125.243.51');
INSERT INTO `mds_userlogin` VALUES ('750', '3', 'admin', '2017-02-18 17:22:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('751', '3', 'admin', '2017-02-18 17:25:29', '163.125.243.25');
INSERT INTO `mds_userlogin` VALUES ('752', '3', 'admin', '2017-02-18 18:03:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('753', '3', 'admin', '2017-02-18 18:32:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('754', '3', 'admin', '2017-02-18 19:12:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('755', '3', 'admin', '2017-02-18 19:13:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('756', '3', 'admin', '2017-02-18 19:22:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('757', '3', 'admin', '2017-02-18 19:24:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('758', '3', 'admin', '2017-02-18 19:29:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('759', '3', 'admin', '2017-02-18 19:33:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('760', '3', 'admin', '2017-02-18 19:35:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('761', '3', 'admin', '2017-02-19 08:38:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('762', '3', 'admin', '2017-02-19 08:40:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('763', '3', 'admin', '2017-02-19 08:56:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('764', '3', 'admin', '2017-02-19 09:25:40', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('765', '3', 'admin', '2017-02-19 09:41:22', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('766', '3', 'admin', '2017-02-19 09:41:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('767', '3', 'admin', '2017-02-19 10:37:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('768', '3', 'admin', '2017-02-19 10:48:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('769', '3', 'admin', '2017-02-19 10:54:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('770', '3', 'admin', '2017-02-19 13:01:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('771', '3', 'admin', '2017-02-19 13:14:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('772', '3', 'admin', '2017-02-19 13:18:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('773', '3', 'admin', '2017-02-19 13:59:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('774', '3', 'admin', '2017-02-19 14:00:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('775', '3', 'admin', '2017-02-19 14:00:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('776', '3', 'admin', '2017-02-19 14:03:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('777', '3', 'admin', '2017-02-19 14:20:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('778', '3', 'admin', '2017-02-19 14:21:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('779', '3', 'admin', '2017-02-19 14:28:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('780', '3', 'admin', '2017-02-19 14:43:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('781', '3', 'admin', '2017-02-19 15:12:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('782', '3', 'admin', '2017-02-19 15:37:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('783', '3', 'admin', '2017-02-19 15:39:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('784', '3', 'admin', '2017-02-19 18:25:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('785', '3', 'admin', '2017-02-19 19:22:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('786', '3', 'admin', '2017-02-19 19:24:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('787', '3', 'admin', '2017-02-19 20:50:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('788', '3', 'admin', '2017-02-19 21:38:14', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('789', '3', 'admin', '2017-02-19 21:38:43', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('790', '3', 'admin', '2017-02-19 21:39:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('791', '3', 'admin', '2017-02-19 21:43:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('792', '3', 'admin', '2017-02-19 22:13:34', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('793', '3', 'admin', '2017-02-19 22:14:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('794', '3', 'admin', '2017-02-19 22:18:16', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('795', '3', 'admin', '2017-02-19 22:24:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('796', '3', 'admin', '2017-02-19 22:32:21', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('797', '3', 'admin', '2017-02-19 22:32:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('798', '3', 'admin', '2017-02-19 22:38:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('799', '3', 'admin', '2017-02-19 22:47:16', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('800', '3', 'admin', '2017-02-19 22:49:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('801', '3', 'admin', '2017-02-19 22:54:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('802', '3', 'admin', '2017-02-19 22:58:30', '59.40.86.147');
INSERT INTO `mds_userlogin` VALUES ('803', '3', 'admin', '2017-02-19 23:08:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('804', '3', 'admin', '2017-02-19 23:11:06', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('805', '3', 'admin', '2017-02-19 23:13:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('806', '3', 'admin', '2017-02-19 23:22:03', '59.40.86.204');
INSERT INTO `mds_userlogin` VALUES ('807', '3', 'admin', '2017-02-19 23:22:08', '59.40.86.204');
INSERT INTO `mds_userlogin` VALUES ('808', '3', 'admin', '2017-02-20 00:13:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('809', '3', 'admin', '2017-02-20 19:59:45', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('810', '3', 'admin', '2017-02-20 20:00:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('811', '3', 'admin', '2017-02-20 20:17:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('812', '3', 'admin', '2017-02-20 20:20:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('813', '3', 'admin', '2017-02-20 20:46:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('814', '3', 'admin', '2017-02-20 21:09:08', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('815', '3', 'admin', '2017-02-20 21:11:16', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('816', '3', 'admin', '2017-02-20 21:18:48', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('817', '3', 'admin', '2017-02-20 21:19:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('818', '3', 'admin', '2017-02-20 22:09:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('819', '3', 'admin', '2017-02-20 22:32:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('820', '3', 'admin', '2017-02-20 22:33:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('821', '3', 'admin', '2017-02-20 23:02:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('822', '3', 'admin', '2017-02-20 23:03:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('823', '3', 'admin', '2017-02-20 23:21:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('824', '3', 'admin', '2017-02-20 23:42:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('825', '3', 'admin', '2017-02-20 23:57:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('826', '3', 'admin', '2017-02-21 00:03:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('827', '3', 'admin', '2017-02-21 00:12:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('828', '3', 'admin', '2017-02-21 00:46:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('829', '3', 'admin', '2017-02-21 00:50:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('830', '3', 'admin', '2017-02-21 00:54:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('831', '3', 'admin', '2017-02-21 00:57:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('832', '3', 'admin', '2017-02-21 01:00:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('833', '3', 'admin', '2017-02-21 01:03:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('834', '3', 'admin', '2017-02-21 18:51:27', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('835', '3', 'admin', '2017-02-21 18:54:25', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('836', '3', 'admin', '2017-02-21 19:42:14', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('837', '3', 'admin', '2017-02-21 19:56:09', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('838', '3', 'admin', '2017-02-21 19:56:16', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('839', '3', 'admin', '2017-02-21 20:02:49', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('840', '3', 'admin', '2017-02-21 20:14:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('841', '3', 'admin', '2017-02-21 20:15:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('842', '3', 'admin', '2017-02-21 20:22:35', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('843', '3', 'admin', '2017-02-21 20:23:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('844', '3', 'admin', '2017-02-21 20:25:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('845', '3', 'admin', '2017-02-21 20:46:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('846', '3', 'admin', '2017-02-21 21:03:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('847', '3', 'admin', '2017-02-21 21:04:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('848', '3', 'admin', '2017-02-21 21:06:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('849', '3', 'admin', '2017-02-21 21:08:49', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('850', '3', 'admin', '2017-02-21 21:14:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('851', '3', 'admin', '2017-02-21 21:20:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('852', '3', 'admin', '2017-02-21 21:20:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('853', '3', 'admin', '2017-02-21 21:25:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('854', '3', 'admin', '2017-02-21 21:30:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('855', '3', 'admin', '2017-02-21 21:31:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('856', '3', 'admin', '2017-02-21 21:45:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('857', '3', 'admin', '2017-02-21 21:46:17', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('858', '3', 'admin', '2017-02-21 21:48:47', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('859', '3', 'admin', '2017-02-21 22:00:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('860', '3', 'admin', '2017-02-21 22:25:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('861', '3', 'admin', '2017-02-21 22:34:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('862', '3', 'admin', '2017-02-21 22:34:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('863', '3', 'admin', '2017-02-21 23:06:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('864', '3', 'admin', '2017-02-21 23:12:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('865', '3', 'admin', '2017-02-21 23:14:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('866', '3', 'admin', '2017-02-21 23:16:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('867', '3', 'admin', '2017-02-21 23:25:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('868', '3', 'admin', '2017-02-21 23:31:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('869', '3', 'admin', '2017-02-21 23:59:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('870', '3', 'admin', '2017-02-22 00:04:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('871', '3', 'admin', '2017-02-22 00:13:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('872', '3', 'admin', '2017-02-22 00:18:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('873', '3', 'admin', '2017-02-22 00:20:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('874', '3', 'admin', '2017-02-22 00:25:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('875', '3', 'admin', '2017-02-22 00:32:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('876', '3', 'admin', '2017-02-22 00:35:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('877', '3', 'admin', '2017-02-22 00:37:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('878', '3', 'admin', '2017-02-22 19:09:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('879', '3', 'admin', '2017-02-22 20:01:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('880', '3', 'admin', '2017-02-22 20:30:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('881', '3', 'admin', '2017-02-22 20:39:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('882', '3', 'admin', '2017-02-22 20:40:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('883', '3', 'admin', '2017-02-22 20:43:20', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('884', '3', 'admin', '2017-02-22 21:09:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('885', '3', 'admin', '2017-02-22 21:12:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('886', '3', 'admin', '2017-02-22 21:14:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('887', '3', 'admin', '2017-02-22 21:21:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('888', '3', 'admin', '2017-02-22 21:22:43', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('889', '3', 'admin', '2017-02-22 21:30:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('890', '3', 'admin', '2017-02-22 21:39:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('891', '3', 'admin', '2017-02-22 21:53:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('892', '3', 'admin', '2017-02-22 22:02:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('893', '3', 'admin', '2017-02-22 22:10:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('894', '3', 'admin', '2017-02-22 22:10:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('895', '3', 'admin', '2017-02-22 23:06:17', '113.12.102.80');
INSERT INTO `mds_userlogin` VALUES ('896', '3', 'admin', '2017-02-24 20:51:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('897', '3', 'admin', '2017-02-24 20:53:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('898', '3', 'admin', '2017-02-24 21:21:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('899', '3', 'admin', '2017-02-24 22:46:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('900', '3', 'admin', '2017-02-24 22:46:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('901', '3', 'admin', '2017-02-24 22:47:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('902', '3', 'admin', '2017-02-24 22:48:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('903', '3', 'admin', '2017-02-24 22:51:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('904', '3', 'admin', '2017-02-24 22:51:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('905', '3', 'admin', '2017-02-24 22:51:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('906', '3', 'admin', '2017-02-24 22:54:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('907', '3', 'admin', '2017-02-24 22:55:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('908', '3', 'admin', '2017-02-24 23:03:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('909', '3', 'admin', '2017-02-24 23:20:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('910', '3', 'admin', '2017-02-24 23:22:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('911', '3', 'admin', '2017-02-24 23:23:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('912', '3', 'admin', '2017-02-24 23:31:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('913', '3', 'admin', '2017-02-24 23:34:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('914', '3', 'admin', '2017-02-24 23:35:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('915', '3', 'admin', '2017-02-24 23:35:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('916', '3', 'admin', '2017-02-24 23:37:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('917', '3', 'admin', '2017-02-24 23:48:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('918', '3', 'admin', '2017-02-24 23:48:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('919', '3', 'admin', '2017-02-24 23:48:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('920', '3', 'admin', '2017-02-25 00:27:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('921', '3', 'admin', '2017-02-25 09:12:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('922', '3', 'admin', '2017-02-25 09:12:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('923', '3', 'admin', '2017-02-25 09:14:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('924', '3', 'admin', '2017-02-25 09:37:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('925', '3', 'admin', '2017-02-25 09:47:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('926', '3', 'admin', '2017-02-25 09:47:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('927', '3', 'admin', '2017-02-25 09:47:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('928', '3', 'admin', '2017-02-25 09:55:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('929', '3', 'admin', '2017-02-25 10:23:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('930', '3', 'admin', '2017-02-25 10:27:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('931', '3', 'admin', '2017-02-25 11:43:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('932', '3', 'admin', '2017-02-27 14:17:18', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('933', '3', 'admin', '2017-02-27 14:21:36', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('934', '3', 'admin', '2017-02-27 21:23:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('935', '3', 'admin', '2017-02-27 23:01:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('936', '3', 'admin', '2017-02-27 23:09:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('937', '3', 'admin', '2017-02-27 23:14:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('938', '3', 'admin', '2017-02-27 23:15:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('939', '3', 'admin', '2017-02-27 23:38:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('940', '3', 'admin', '2017-02-27 23:38:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('941', '3', 'admin', '2017-02-27 23:41:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('942', '3', 'admin', '2017-02-28 00:05:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('943', '3', 'admin', '2017-02-28 00:05:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('944', '3', 'admin', '2017-02-28 00:06:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('945', '3', 'admin', '2017-02-28 00:10:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('946', '3', 'admin', '2017-02-28 00:12:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('947', '3', 'admin', '2017-02-28 00:18:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('948', '3', 'admin', '2017-02-28 00:21:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('949', '3', 'admin', '2017-02-28 00:21:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('950', '3', 'admin', '2017-02-28 00:23:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('951', '3', 'admin', '2017-02-28 00:25:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('952', '3', 'admin', '2017-02-28 00:30:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('953', '3', 'admin', '2017-02-28 00:31:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('954', '3', 'admin', '2017-02-28 00:37:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('955', '3', 'admin', '2017-02-28 00:39:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('956', '3', 'admin', '2017-02-28 00:42:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('957', '3', 'admin', '2017-02-28 18:05:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('958', '3', 'admin', '2017-02-28 18:16:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('959', '3', 'admin', '2017-02-28 18:25:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('960', '3', 'admin', '2017-02-28 18:37:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('961', '3', 'admin', '2017-02-28 18:46:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('962', '3', 'admin', '2017-02-28 18:48:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('963', '3', 'admin', '2017-02-28 18:54:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('964', '3', 'admin', '2017-02-28 19:11:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('965', '3', 'admin', '2017-02-28 19:11:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('966', '3', 'admin', '2017-02-28 19:14:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('967', '3', 'admin', '2017-02-28 19:18:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('968', '3', 'admin', '2017-02-28 20:49:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('969', '3', 'admin', '2017-02-28 20:52:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('970', '3', 'admin', '2017-02-28 21:08:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('971', '3', 'admin', '2017-02-28 21:24:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('972', '3', 'admin', '2017-02-28 21:27:51', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('973', '3', 'admin', '2017-02-28 21:30:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('974', '3', 'admin', '2017-02-28 21:37:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('975', '3', 'admin', '2017-02-28 21:42:34', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('976', '3', 'admin', '2017-02-28 23:44:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('977', '3', 'admin', '2017-03-01 17:12:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('978', '3', 'admin', '2017-03-01 20:49:31', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('979', '3', 'admin', '2017-03-01 20:57:59', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('980', '3', 'admin', '2017-03-01 21:03:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('981', '3', 'admin', '2017-03-01 21:19:59', '14.220.122.210');
INSERT INTO `mds_userlogin` VALUES ('982', '3', 'admin', '2017-03-01 21:27:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('983', '3', 'admin', '2017-03-01 21:27:33', '14.220.122.210');
INSERT INTO `mds_userlogin` VALUES ('984', '3', 'admin', '2017-03-01 21:37:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('985', '3', 'admin', '2017-03-01 21:44:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('986', '3', 'admin', '2017-03-01 21:50:07', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('987', '3', 'admin', '2017-03-01 21:52:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('988', '3', 'admin', '2017-03-01 21:55:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('989', '3', 'admin', '2017-03-01 21:56:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('990', '3', 'admin', '2017-03-01 21:57:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('991', '3', 'admin', '2017-03-01 21:58:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('992', '3', 'admin', '2017-03-01 22:00:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('993', '3', 'admin', '2017-03-01 22:03:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('994', '3', 'admin', '2017-03-01 22:04:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('995', '3', 'admin', '2017-03-01 22:06:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('996', '3', 'admin', '2017-03-01 22:07:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('997', '3', 'admin', '2017-03-01 22:10:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('998', '3', 'admin', '2017-03-01 22:13:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('999', '3', 'admin', '2017-03-01 22:18:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1000', '3', 'admin', '2017-03-01 22:27:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1001', '3', 'admin', '2017-03-01 22:27:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1002', '3', 'admin', '2017-03-01 22:28:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1003', '3', 'admin', '2017-03-01 22:31:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1004', '3', 'admin', '2017-03-01 22:39:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1005', '3', 'admin', '2017-03-01 22:40:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1006', '3', 'admin', '2017-03-01 22:43:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1007', '3', 'admin', '2017-03-01 22:43:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1008', '3', 'admin', '2017-03-01 22:44:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1009', '3', 'admin', '2017-03-01 22:46:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1010', '3', 'admin', '2017-03-01 22:51:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1011', '3', 'admin', '2017-03-01 22:53:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1012', '3', 'admin', '2017-03-01 23:10:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1013', '3', 'admin', '2017-03-01 23:17:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1014', '3', 'admin', '2017-03-01 23:18:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1015', '3', 'admin', '2017-03-01 23:23:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1016', '3', 'admin', '2017-03-01 23:23:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1017', '3', 'admin', '2017-03-01 23:24:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1018', '3', 'admin', '2017-03-01 23:25:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1019', '3', 'admin', '2017-03-01 23:25:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1020', '3', 'admin', '2017-03-01 23:32:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1021', '3', 'admin', '2017-03-01 23:34:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1022', '3', 'admin', '2017-03-01 23:54:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1023', '3', 'admin', '2017-03-01 23:55:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1024', '3', 'admin', '2017-03-01 23:57:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1025', '3', 'admin', '2017-03-01 23:57:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1026', '3', 'admin', '2017-03-01 23:58:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1027', '3', 'admin', '2017-03-02 00:00:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1028', '3', 'admin', '2017-03-02 00:00:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1029', '3', 'admin', '2017-03-02 00:03:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1030', '3', 'admin', '2017-03-02 00:03:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1031', '3', 'admin', '2017-03-02 00:03:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1032', '3', 'admin', '2017-03-02 00:05:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1033', '3', 'admin', '2017-03-02 00:07:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1034', '3', 'admin', '2017-03-02 00:09:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1035', '3', 'admin', '2017-03-02 00:20:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1036', '3', 'admin', '2017-03-02 00:58:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1037', '3', 'admin', '2017-03-02 00:59:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1038', '3', 'admin', '2017-03-02 01:00:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1039', '3', 'admin', '2017-03-02 01:04:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1040', '3', 'admin', '2017-03-02 01:06:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1041', '3', 'admin', '2017-03-02 01:10:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1042', '3', 'admin', '2017-03-02 01:10:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1043', '3', 'admin', '2017-03-02 01:12:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1044', '3', 'admin', '2017-03-02 08:45:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1045', '3', 'admin', '2017-03-02 09:28:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1046', '3', 'admin', '2017-03-02 15:42:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1047', '3', 'admin', '2017-03-02 18:42:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1048', '3', 'admin', '2017-03-02 18:56:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1049', '3', 'admin', '2017-03-02 19:05:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1050', '3', 'admin', '2017-03-02 19:08:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1051', '3', 'admin', '2017-03-02 19:09:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1052', '3', 'admin', '2017-03-02 19:10:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1053', '3', 'admin', '2017-03-02 19:13:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1054', '3', 'admin', '2017-03-02 19:17:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1055', '3', 'admin', '2017-03-02 19:30:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1056', '3', 'admin', '2017-03-02 19:53:51', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1057', '3', 'admin', '2017-03-02 19:55:19', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1058', '3', 'admin', '2017-03-02 20:09:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1059', '3', 'admin', '2017-03-02 20:28:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1060', '3', 'admin', '2017-03-02 20:30:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1061', '3', 'admin', '2017-03-02 20:32:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1062', '3', 'admin', '2017-03-02 20:33:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1063', '3', 'admin', '2017-03-02 20:50:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1064', '3', 'admin', '2017-03-02 21:02:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1065', '3', 'admin', '2017-03-02 21:10:35', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1066', '3', 'admin', '2017-03-02 21:13:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1067', '3', 'admin', '2017-03-02 21:15:46', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1068', '3', 'admin', '2017-03-02 21:18:16', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1069', '3', 'admin', '2017-03-02 21:21:34', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1070', '3', 'admin', '2017-03-02 21:23:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1071', '3', 'admin', '2017-03-02 21:23:44', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1072', '3', 'admin', '2017-03-02 21:39:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1073', '3', 'admin', '2017-03-02 21:40:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1074', '3', 'admin', '2017-03-02 21:42:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1075', '3', 'admin', '2017-03-02 21:42:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1076', '3', 'admin', '2017-03-02 21:50:26', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1077', '3', 'admin', '2017-03-02 21:52:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1078', '3', 'admin', '2017-03-02 21:54:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1079', '3', 'admin', '2017-03-02 21:56:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1080', '3', 'admin', '2017-03-02 21:59:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1081', '3', 'admin', '2017-03-02 21:59:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1082', '3', 'admin', '2017-03-02 22:04:40', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1083', '3', 'admin', '2017-03-02 22:11:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1084', '3', 'admin', '2017-03-02 22:16:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1085', '3', 'admin', '2017-03-02 22:26:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1086', '3', 'admin', '2017-03-02 22:39:22', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1087', '3', 'admin', '2017-03-02 23:22:33', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1088', '3', 'admin', '2017-03-02 23:26:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1089', '3', 'admin', '2017-03-02 23:29:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1090', '3', 'admin', '2017-03-02 23:43:48', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1091', '3', 'admin', '2017-03-02 23:53:49', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1092', '3', 'admin', '2017-03-02 23:59:12', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1093', '3', 'admin', '2017-03-03 00:02:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1094', '3', 'admin', '2017-03-03 00:09:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1095', '3', 'admin', '2017-03-03 00:16:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1096', '3', 'admin', '2017-03-03 00:17:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1097', '3', 'admin', '2017-03-03 00:25:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1098', '3', 'admin', '2017-03-03 00:26:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1099', '3', 'admin', '2017-03-03 10:16:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1100', '3', 'admin', '2017-03-03 10:47:38', '117.136.79.200');
INSERT INTO `mds_userlogin` VALUES ('1101', '3', 'admin', '2017-03-03 10:56:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1102', '3', 'admin', '2017-03-03 12:01:59', '117.136.79.200');
INSERT INTO `mds_userlogin` VALUES ('1103', '3', 'admin', '2017-03-03 12:40:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1104', '3', 'admin', '2017-03-04 09:41:00', '223.104.63.221');
INSERT INTO `mds_userlogin` VALUES ('1105', '3', 'admin', '2017-03-04 09:45:15', '223.104.63.221');
INSERT INTO `mds_userlogin` VALUES ('1106', '3', 'admin', '2017-03-04 09:46:20', '223.104.63.221');
INSERT INTO `mds_userlogin` VALUES ('1107', '3', 'admin', '2017-03-04 14:00:39', '223.104.1.158');
INSERT INTO `mds_userlogin` VALUES ('1108', '3', 'admin', '2017-03-04 14:56:04', '223.104.1.158');
INSERT INTO `mds_userlogin` VALUES ('1109', '3', 'admin', '2017-03-04 21:48:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1110', '3', 'admin', '2017-03-05 21:37:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1111', '3', 'admin', '2017-03-05 22:04:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1112', '3', 'admin', '2017-03-05 22:13:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1113', '3', 'admin', '2017-03-05 22:55:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1114', '3', 'admin', '2017-03-05 22:55:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1115', '3', 'admin', '2017-03-05 22:58:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1116', '3', 'admin', '2017-03-05 23:06:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1117', '3', 'admin', '2017-03-05 23:12:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1118', '3', 'admin', '2017-03-05 23:15:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1119', '3', 'admin', '2017-03-05 23:20:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1120', '3', 'admin', '2017-03-05 23:22:07', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1121', '3', 'admin', '2017-03-05 23:29:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1122', '3', 'admin', '2017-03-05 23:34:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1123', '3', 'admin', '2017-03-05 23:36:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1124', '3', 'admin', '2017-03-05 23:40:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1125', '3', 'admin', '2017-03-06 00:15:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1126', '3', 'admin', '2017-03-06 00:18:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1127', '3', 'admin', '2017-03-06 00:22:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1128', '3', 'admin', '2017-03-06 00:23:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1129', '3', 'admin', '2017-03-06 20:54:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1130', '3', 'admin', '2017-03-06 21:04:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1131', '3', 'admin', '2017-03-06 21:17:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1132', '3', 'admin', '2017-03-06 21:24:36', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1133', '3', 'admin', '2017-03-06 21:30:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1134', '3', 'admin', '2017-03-07 20:20:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1135', '3', 'admin', '2017-03-07 20:32:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1136', '3', 'admin', '2017-03-07 20:34:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1137', '3', 'admin', '2017-03-07 20:57:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1138', '3', 'admin', '2017-03-07 20:59:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1139', '3', 'admin', '2017-03-07 21:04:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1140', '3', 'admin', '2017-03-07 21:18:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1141', '3', 'admin', '2017-03-07 21:23:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1142', '3', 'admin', '2017-03-07 21:24:49', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1143', '3', 'admin', '2017-03-07 21:58:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1144', '3', 'admin', '2017-03-07 22:21:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1145', '3', 'admin', '2017-03-07 23:03:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1146', '3', 'admin', '2017-03-07 23:03:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1147', '3', 'admin', '2017-03-08 10:16:54', '183.12.243.6');
INSERT INTO `mds_userlogin` VALUES ('1148', '3', 'admin', '2017-03-08 15:42:04', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1149', '3', 'admin', '2017-03-08 18:41:14', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1150', '3', 'admin', '2017-03-08 20:40:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1151', '3', 'admin', '2017-03-08 21:23:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1152', '3', 'admin', '2017-03-08 21:57:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1153', '3', 'admin', '2017-03-08 22:08:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1154', '3', 'admin', '2017-03-08 22:38:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1155', '3', 'admin', '2017-03-08 22:41:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1156', '3', 'admin', '2017-03-08 22:46:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1157', '3', 'admin', '2017-03-08 22:47:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1158', '3', 'admin', '2017-03-08 22:47:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1159', '3', 'admin', '2017-03-09 00:05:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1160', '3', 'admin', '2017-03-09 00:28:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1161', '3', 'admin', '2017-03-09 01:15:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1162', '3', 'admin', '2017-03-09 19:01:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1163', '3', 'admin', '2017-03-09 19:01:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1164', '3', 'admin', '2017-03-09 19:02:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1165', '3', 'admin', '2017-03-09 19:04:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1166', '3', 'admin', '2017-03-09 19:07:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1167', '3', 'admin', '2017-03-09 19:13:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1168', '3', 'admin', '2017-03-09 19:14:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1169', '3', 'admin', '2017-03-09 19:18:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1170', '3', 'admin', '2017-03-09 19:22:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1171', '3', 'admin', '2017-03-09 19:29:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1172', '3', 'admin', '2017-03-09 19:32:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1173', '3', 'admin', '2017-03-09 19:33:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1174', '3', 'admin', '2017-03-09 19:34:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1175', '3', 'admin', '2017-03-09 19:51:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1176', '3', 'admin', '2017-03-09 19:52:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1177', '3', 'admin', '2017-03-09 20:04:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1178', '3', 'admin', '2017-03-09 20:07:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1179', '3', 'admin', '2017-03-09 21:41:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1180', '3', 'admin', '2017-03-09 23:08:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1181', '3', 'admin', '2017-03-10 00:49:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1182', '3', 'admin', '2017-03-10 00:53:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1183', '3', 'admin', '2017-03-10 08:37:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1184', '3', 'admin', '2017-03-10 09:04:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1185', '3', 'admin', '2017-03-10 10:52:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1186', '3', 'admin', '2017-03-10 12:27:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1187', '3', 'admin', '2017-03-10 14:08:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1188', '3', 'admin', '2017-03-10 14:38:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1189', '3', 'admin', '2017-03-10 14:56:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1190', '3', 'admin', '2017-03-10 15:05:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1191', '3', 'admin', '2017-03-10 15:14:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1192', '3', 'admin', '2017-03-10 16:12:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1193', '3', 'admin', '2017-03-10 16:21:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1194', '3', 'admin', '2017-03-10 16:23:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1195', '3', 'admin', '2017-03-10 16:32:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1196', '3', 'admin', '2017-03-10 17:38:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1197', '3', 'admin', '2017-03-10 18:22:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1198', '3', 'admin', '2017-03-10 19:56:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1199', '3', 'admin', '2017-03-10 19:57:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1200', '3', 'admin', '2017-03-10 22:19:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1201', '3', 'admin', '2017-03-10 22:20:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1202', '3', 'admin', '2017-03-10 22:46:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1203', '3', 'admin', '2017-03-10 23:20:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1204', '3', 'admin', '2017-03-10 23:37:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1205', '3', 'admin', '2017-03-11 00:11:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1206', '3', 'admin', '2017-03-11 11:32:59', '120.77.147.255');
INSERT INTO `mds_userlogin` VALUES ('1207', '3', 'admin', '2017-03-11 11:41:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1208', '3', 'admin', '2017-03-11 11:42:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1209', '3', 'admin', '2017-03-11 13:22:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1210', '3', 'admin', '2017-03-11 13:33:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1211', '3', 'admin', '2017-03-11 13:36:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1212', '3', 'admin', '2017-03-11 14:26:34', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1213', '3', 'admin', '2017-03-11 14:31:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1214', '3', 'admin', '2017-03-11 14:35:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1215', '3', 'admin', '2017-03-11 14:39:06', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1216', '3', 'admin', '2017-03-11 14:39:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1217', '3', 'admin', '2017-03-11 14:41:56', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1218', '3', 'admin', '2017-03-11 14:44:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1219', '3', 'admin', '2017-03-11 14:49:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1220', '3', 'admin', '2017-03-11 14:50:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1221', '3', 'admin', '2017-03-11 14:55:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1222', '3', 'admin', '2017-03-11 14:56:05', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1223', '3', 'admin', '2017-03-11 14:59:04', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1224', '3', 'admin', '2017-03-11 15:03:37', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1225', '3', 'admin', '2017-03-11 15:09:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1226', '3', 'admin', '2017-03-11 15:10:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1227', '3', 'admin', '2017-03-11 15:10:55', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1228', '3', 'admin', '2017-03-11 15:12:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1229', '3', 'admin', '2017-03-11 15:20:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1230', '3', 'admin', '2017-03-11 15:23:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1231', '3', 'admin', '2017-03-11 15:37:52', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1232', '3', 'admin', '2017-03-11 15:44:28', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1233', '3', 'admin', '2017-03-11 15:46:01', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1234', '3', 'admin', '2017-03-11 15:46:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1235', '3', 'admin', '2017-03-11 15:46:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1236', '3', 'admin', '2017-03-11 15:51:04', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1237', '3', 'admin', '2017-03-11 15:53:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1238', '3', 'admin', '2017-03-11 15:54:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1239', '3', 'admin', '2017-03-11 16:09:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1240', '3', 'admin', '2017-03-11 16:12:35', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1241', '3', 'admin', '2017-03-11 16:13:49', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1242', '3', 'admin', '2017-03-11 16:16:13', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1243', '3', 'admin', '2017-03-11 16:19:12', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1244', '3', 'admin', '2017-03-11 16:23:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1245', '3', 'admin', '2017-03-11 16:23:26', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1246', '3', 'admin', '2017-03-11 16:36:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1247', '3', 'admin', '2017-03-11 16:42:06', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1248', '3', 'admin', '2017-03-11 16:48:46', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1249', '3', 'admin', '2017-03-11 16:57:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1250', '3', 'admin', '2017-03-11 17:03:10', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1251', '3', 'admin', '2017-03-11 17:10:51', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1252', '3', 'admin', '2017-03-11 17:13:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1253', '3', 'admin', '2017-03-11 17:16:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1254', '3', 'admin', '2017-03-11 17:17:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1255', '3', 'admin', '2017-03-11 17:18:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1256', '3', 'admin', '2017-03-11 17:25:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1257', '3', 'admin', '2017-03-11 17:27:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1258', '3', 'admin', '2017-03-11 17:29:31', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1259', '3', 'admin', '2017-03-11 17:43:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1260', '3', 'admin', '2017-03-11 17:46:18', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1261', '3', 'admin', '2017-03-11 18:06:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1262', '3', 'admin', '2017-03-11 18:08:36', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1263', '3', 'admin', '2017-03-11 18:22:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1264', '3', 'admin', '2017-03-11 18:24:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1265', '3', 'admin', '2017-03-11 18:36:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1266', '3', 'admin', '2017-03-11 18:51:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1267', '3', 'admin', '2017-03-11 19:19:09', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1268', '3', 'admin', '2017-03-11 20:06:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1269', '3', 'admin', '2017-03-11 20:12:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1270', '3', 'admin', '2017-03-11 20:15:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1271', '3', 'admin', '2017-03-11 20:24:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1272', '3', 'admin', '2017-03-11 20:26:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1273', '3', 'admin', '2017-03-11 20:30:20', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1274', '3', 'admin', '2017-03-11 20:35:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1275', '3', 'admin', '2017-03-11 20:36:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1276', '3', 'admin', '2017-03-11 20:37:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1277', '3', 'admin', '2017-03-11 20:40:35', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1278', '3', 'admin', '2017-03-11 20:54:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1279', '3', 'admin', '2017-03-11 20:56:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1280', '3', 'admin', '2017-03-11 21:23:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1281', '3', 'admin', '2017-03-11 21:53:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1282', '3', 'admin', '2017-03-11 22:06:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1283', '3', 'admin', '2017-03-11 22:14:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1284', '3', 'admin', '2017-03-11 22:18:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1285', '3', 'admin', '2017-03-11 22:34:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1286', '3', 'admin', '2017-03-11 22:53:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1287', '3', 'admin', '2017-03-11 23:02:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1288', '3', 'admin', '2017-03-11 23:12:02', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1289', '3', 'admin', '2017-03-11 23:26:08', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1290', '3', 'admin', '2017-03-11 23:29:49', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1291', '3', 'admin', '2017-03-11 23:32:47', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1292', '3', 'admin', '2017-03-11 23:33:20', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1293', '3', 'admin', '2017-03-11 23:44:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1294', '3', 'admin', '2017-03-11 23:54:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1295', '3', 'admin', '2017-03-11 23:56:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1296', '3', 'admin', '2017-03-12 00:02:26', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1297', '3', 'admin', '2017-03-12 00:18:08', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1298', '3', 'admin', '2017-03-12 00:32:03', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1299', '3', 'admin', '2017-03-12 00:42:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1300', '3', 'admin', '2017-03-12 01:01:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1301', '3', 'admin', '2017-03-12 01:06:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1302', '3', 'admin', '2017-03-12 01:10:30', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1303', '3', 'admin', '2017-03-12 01:13:41', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1304', '3', 'admin', '2017-03-12 01:18:31', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1305', '3', 'admin', '2017-03-12 01:22:56', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1306', '3', 'admin', '2017-03-12 01:50:53', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1307', '3', 'admin', '2017-03-12 02:06:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1308', '3', 'admin', '2017-03-12 02:19:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1309', '3', 'admin', '2017-03-12 02:26:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1310', '3', 'admin', '2017-03-12 02:32:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1311', '3', 'admin', '2017-03-12 02:34:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1312', '3', 'admin', '2017-03-12 02:35:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1313', '3', 'admin', '2017-03-12 02:36:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1314', '3', 'admin', '2017-03-12 02:37:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1315', '3', 'admin', '2017-03-12 02:52:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1316', '3', 'admin', '2017-03-12 03:19:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1317', '3', 'admin', '2017-03-12 03:53:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1318', '3', 'admin', '2017-03-12 04:04:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1319', '3', 'admin', '2017-03-12 04:15:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1320', '3', 'admin', '2017-03-12 04:19:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1321', '3', 'admin', '2017-03-12 11:24:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1322', '3', 'admin', '2017-03-12 11:52:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1323', '3', 'admin', '2017-03-12 13:18:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1324', '3', 'admin', '2017-03-12 13:18:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1325', '3', 'admin', '2017-03-12 13:32:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1326', '3', 'admin', '2017-03-12 13:36:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1327', '3', 'admin', '2017-03-12 13:48:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1328', '3', 'admin', '2017-03-12 13:51:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1329', '3', 'admin', '2017-03-12 13:57:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1330', '3', 'admin', '2017-03-12 14:00:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1331', '3', 'admin', '2017-03-12 14:02:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1332', '3', 'admin', '2017-03-12 14:22:17', '120.77.147.255');
INSERT INTO `mds_userlogin` VALUES ('1333', '3', 'admin', '2017-03-12 14:34:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1334', '3', 'admin', '2017-03-12 14:39:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1335', '3', 'admin', '2017-03-12 14:56:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1336', '3', 'admin', '2017-03-12 15:06:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1337', '3', 'admin', '2017-03-12 16:09:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1338', '3', 'admin', '2017-03-12 16:10:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1339', '3', 'admin', '2017-03-12 16:36:06', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1340', '3', 'admin', '2017-03-12 16:38:56', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1341', '3', 'admin', '2017-03-12 16:48:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1342', '3', 'admin', '2017-03-12 16:53:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1343', '3', 'admin', '2017-03-12 17:14:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1344', '3', 'admin', '2017-03-12 17:25:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1345', '3', 'admin', '2017-03-12 17:30:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1346', '3', 'admin', '2017-03-12 17:32:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1347', '3', 'admin', '2017-03-12 17:34:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1348', '3', 'admin', '2017-03-12 17:58:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1349', '3', 'admin', '2017-03-12 18:16:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1350', '3', 'admin', '2017-03-12 18:32:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1351', '3', 'admin', '2017-03-12 19:02:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1352', '3', 'admin', '2017-03-12 19:31:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1353', '3', 'admin', '2017-03-12 19:35:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1354', '3', 'admin', '2017-03-12 21:36:33', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1355', '3', 'admin', '2017-03-12 21:43:06', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1356', '3', 'admin', '2017-03-13 21:03:32', '14.220.123.137');
INSERT INTO `mds_userlogin` VALUES ('1357', '3', 'admin', '2017-03-13 22:28:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1358', '3', 'admin', '2017-03-13 22:35:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1359', '3', 'admin', '2017-03-13 22:46:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1360', '3', 'admin', '2017-03-13 22:47:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1361', '3', 'admin', '2017-03-13 22:51:59', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1362', '3', 'admin', '2017-03-13 23:00:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1363', '3', 'admin', '2017-03-13 23:01:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1364', '3', 'admin', '2017-03-13 23:02:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1365', '3', 'admin', '2017-03-13 23:21:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1366', '3', 'admin', '2017-03-14 00:29:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1367', '3', 'admin', '2017-03-14 10:41:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1368', '3', 'admin', '2017-03-14 10:49:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1369', '3', 'admin', '2017-03-14 11:09:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1370', '3', 'admin', '2017-03-14 12:24:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1371', '3', 'admin', '2017-03-14 12:28:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1372', '3', 'admin', '2017-03-14 19:41:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1373', '3', 'admin', '2017-03-14 20:07:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1374', '3', 'admin', '2017-03-14 20:12:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1375', '3', 'admin', '2017-03-14 20:12:29', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1376', '3', 'admin', '2017-03-14 20:13:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1377', '3', 'admin', '2017-03-14 21:09:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1378', '3', 'admin', '2017-03-14 21:29:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1379', '3', 'admin', '2017-03-14 22:04:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1380', '3', 'admin', '2017-03-14 22:13:18', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1381', '3', 'admin', '2017-03-14 22:14:41', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1382', '3', 'admin', '2017-03-14 22:18:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1383', '3', 'admin', '2017-03-14 22:21:50', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1384', '3', 'admin', '2017-03-14 22:33:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1385', '3', 'admin', '2017-03-14 22:44:01', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1386', '3', 'admin', '2017-03-14 22:50:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1387', '3', 'admin', '2017-03-14 23:07:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1388', '3', 'admin', '2017-03-14 23:43:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1389', '3', 'admin', '2017-03-14 23:44:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1390', '3', 'admin', '2017-03-14 23:45:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1391', '3', 'admin', '2017-03-15 02:14:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1392', '3', 'admin', '2017-03-15 02:36:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1393', '3', 'admin', '2017-03-15 14:15:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1394', '3', 'admin', '2017-03-15 16:11:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1395', '3', 'admin', '2017-03-15 17:13:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1396', '3', 'admin', '2017-03-15 17:28:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1397', '3', 'admin', '2017-03-15 17:32:02', '183.12.236.83');
INSERT INTO `mds_userlogin` VALUES ('1398', '3', 'admin', '2017-03-15 18:00:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1399', '3', 'admin', '2017-03-15 18:00:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1400', '3', 'admin', '2017-03-15 20:16:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1401', '3', 'admin', '2017-03-15 20:21:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1402', '3', 'admin', '2017-03-15 20:40:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1403', '3', 'admin', '2017-03-15 20:47:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1404', '3', 'admin', '2017-03-15 20:48:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1405', '3', 'admin', '2017-03-15 20:50:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1406', '3', 'admin', '2017-03-15 21:06:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1407', '3', 'admin', '2017-03-15 21:09:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1408', '3', 'admin', '2017-03-15 21:11:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1409', '3', 'admin', '2017-03-15 21:12:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1410', '3', 'admin', '2017-03-15 21:15:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1411', '3', 'admin', '2017-03-15 21:30:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1412', '3', 'admin', '2017-03-15 21:32:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1413', '3', 'admin', '2017-03-15 21:43:36', '103.228.209.254');
INSERT INTO `mds_userlogin` VALUES ('1414', '3', 'admin', '2017-03-15 21:52:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1415', '3', 'admin', '2017-03-15 21:55:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1416', '3', 'admin', '2017-03-15 22:13:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1417', '3', 'admin', '2017-03-15 22:14:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1418', '3', 'admin', '2017-03-15 22:22:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1419', '3', 'admin', '2017-03-15 22:24:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1420', '3', 'admin', '2017-03-15 22:27:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1421', '3', 'admin', '2017-03-15 22:39:40', '183.12.236.83');
INSERT INTO `mds_userlogin` VALUES ('1422', '3', 'admin', '2017-03-15 22:41:19', '14.220.123.169');
INSERT INTO `mds_userlogin` VALUES ('1423', '3', 'admin', '2017-03-15 22:46:12', '14.220.123.169');
INSERT INTO `mds_userlogin` VALUES ('1424', '3', 'admin', '2017-03-15 22:53:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1425', '3', 'admin', '2017-03-15 22:57:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1426', '3', 'admin', '2017-03-15 23:00:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1427', '3', 'admin', '2017-03-15 23:04:30', '14.220.123.169');
INSERT INTO `mds_userlogin` VALUES ('1428', '3', 'admin', '2017-03-15 23:06:11', '103.228.209.254');
INSERT INTO `mds_userlogin` VALUES ('1429', '3', 'admin', '2017-03-15 23:07:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1430', '3', 'admin', '2017-03-15 23:20:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1431', '3', 'admin', '2017-03-15 23:30:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1432', '3', 'admin', '2017-03-15 23:48:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1433', '3', 'admin', '2017-03-15 23:55:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1434', '3', 'admin', '2017-03-15 23:57:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1435', '3', 'admin', '2017-03-16 00:01:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1436', '3', 'admin', '2017-03-16 00:03:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1437', '3', 'admin', '2017-03-16 00:05:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1438', '3', 'admin', '2017-03-16 00:24:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1439', '3', 'admin', '2017-03-16 07:56:19', '103.228.209.254');
INSERT INTO `mds_userlogin` VALUES ('1440', '3', 'admin', '2017-03-16 09:26:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1441', '3', 'admin', '2017-03-16 10:11:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1442', '3', 'admin', '2017-03-16 10:14:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1443', '3', 'admin', '2017-03-16 10:18:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1444', '3', 'admin', '2017-03-16 10:29:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1445', '3', 'admin', '2017-03-16 11:33:01', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1446', '3', 'admin', '2017-03-16 11:43:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1447', '3', 'admin', '2017-03-16 11:52:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1448', '3', 'admin', '2017-03-16 13:15:53', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1449', '3', 'admin', '2017-03-16 13:18:50', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1450', '3', 'admin', '2017-03-16 13:22:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1451', '3', 'admin', '2017-03-16 13:25:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1452', '3', 'admin', '2017-03-16 13:50:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1453', '3', 'admin', '2017-03-16 14:33:35', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1454', '3', 'admin', '2017-03-16 15:01:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1455', '3', 'admin', '2017-03-16 16:10:31', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1456', '3', 'admin', '2017-03-16 16:19:51', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1457', '3', 'admin', '2017-03-16 16:34:37', '117.136.97.8');
INSERT INTO `mds_userlogin` VALUES ('1458', '3', 'admin', '2017-03-16 16:45:13', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1459', '3', 'admin', '2017-03-16 17:19:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1460', '3', 'admin', '2017-03-16 17:24:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1461', '3', 'admin', '2017-03-16 17:30:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1462', '3', 'admin', '2017-03-16 17:38:29', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1463', '3', 'admin', '2017-03-16 18:27:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1464', '3', 'admin', '2017-03-16 18:30:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1465', '3', 'admin', '2017-03-16 20:08:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1466', '3', 'admin', '2017-03-16 20:33:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1467', '3', 'admin', '2017-03-16 20:37:57', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1468', '3', 'admin', '2017-03-16 21:03:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1469', '3', 'admin', '2017-03-16 21:22:13', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1470', '3', 'admin', '2017-03-16 21:39:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1471', '3', 'admin', '2017-03-16 21:44:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1472', '3', 'admin', '2017-03-16 21:58:01', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1473', '3', 'admin', '2017-03-16 22:26:06', '14.220.123.69');
INSERT INTO `mds_userlogin` VALUES ('1474', '3', 'admin', '2017-03-16 22:45:40', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1475', '3', 'admin', '2017-03-16 22:47:04', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1476', '3', 'admin', '2017-03-16 22:58:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1477', '3', 'admin', '2017-03-16 23:12:23', '14.220.123.69');
INSERT INTO `mds_userlogin` VALUES ('1478', '3', 'admin', '2017-03-16 23:12:25', '183.12.242.138');
INSERT INTO `mds_userlogin` VALUES ('1479', '3', 'admin', '2017-03-16 23:13:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1480', '3', 'admin', '2017-03-16 23:33:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1481', '3', 'admin', '2017-03-16 23:39:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1482', '3', 'admin', '2017-03-16 23:55:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1483', '3', 'admin', '2017-03-17 00:15:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1484', '3', 'admin', '2017-03-17 00:17:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1485', '3', 'admin', '2017-03-17 00:19:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1486', '3', 'admin', '2017-03-17 00:26:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1487', '3', 'admin', '2017-03-17 00:44:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1488', '3', 'admin', '2017-03-17 00:45:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1489', '3', 'admin', '2017-03-17 00:47:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1490', '3', 'admin', '2017-03-17 01:02:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1491', '3', 'admin', '2017-03-17 01:09:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1492', '3', 'admin', '2017-03-17 01:12:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1493', '3', 'admin', '2017-03-17 01:17:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1494', '3', 'admin', '2017-03-17 01:22:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1495', '3', 'admin', '2017-03-17 01:31:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1496', '3', 'admin', '2017-03-17 01:36:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1497', '3', 'admin', '2017-03-17 02:04:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1498', '3', 'admin', '2017-03-17 02:29:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1499', '3', 'admin', '2017-03-17 03:05:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1500', '3', 'admin', '2017-03-17 03:36:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1501', '3', 'admin', '2017-03-17 03:42:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1502', '3', 'admin', '2017-03-17 03:51:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1503', '3', 'admin', '2017-03-17 03:55:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1504', '3', 'admin', '2017-03-17 09:32:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1505', '3', 'admin', '2017-03-17 09:40:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1506', '3', 'admin', '2017-03-17 10:27:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1507', '3', 'admin', '2017-03-17 10:30:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1508', '3', 'admin', '2017-03-17 10:39:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1509', '3', 'admin', '2017-03-17 11:08:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1510', '3', 'admin', '2017-03-17 14:02:22', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1511', '3', 'admin', '2017-03-17 14:35:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1512', '3', 'admin', '2017-03-17 14:40:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1513', '3', 'admin', '2017-03-17 14:53:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1514', '3', 'admin', '2017-03-17 14:59:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1515', '3', 'admin', '2017-03-17 15:04:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1516', '3', 'admin', '2017-03-17 15:56:52', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1517', '3', 'admin', '2017-03-17 16:15:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1518', '3', 'admin', '2017-03-17 20:17:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1519', '3', 'admin', '2017-03-17 20:30:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1520', '3', 'admin', '2017-03-17 20:38:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1521', '3', 'admin', '2017-03-17 20:40:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1522', '3', 'admin', '2017-03-17 20:42:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1523', '3', 'admin', '2017-03-17 20:47:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1524', '3', 'admin', '2017-03-17 20:54:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1525', '3', 'admin', '2017-03-17 21:08:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1526', '3', 'admin', '2017-03-17 21:16:40', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1527', '3', 'admin', '2017-03-17 21:20:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1528', '3', 'admin', '2017-03-17 21:20:03', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1529', '3', 'admin', '2017-03-17 21:26:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1530', '3', 'admin', '2017-03-17 21:39:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1531', '3', 'admin', '2017-03-17 22:22:49', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1532', '3', 'admin', '2017-03-17 22:23:37', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1533', '3', 'admin', '2017-03-17 22:28:31', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1534', '3', 'admin', '2017-03-17 22:30:19', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1535', '3', 'admin', '2017-03-17 22:44:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1536', '3', 'admin', '2017-03-17 22:49:19', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1537', '3', 'admin', '2017-03-17 22:50:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1538', '3', 'admin', '2017-03-17 22:55:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1539', '3', 'admin', '2017-03-17 22:59:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1540', '3', 'admin', '2017-03-17 23:07:06', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1541', '3', 'admin', '2017-03-17 23:14:58', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1542', '3', 'admin', '2017-03-17 23:15:12', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1543', '3', 'admin', '2017-03-17 23:16:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1544', '3', 'admin', '2017-03-17 23:17:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1545', '3', 'admin', '2017-03-17 23:23:39', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1546', '3', 'admin', '2017-03-17 23:25:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1547', '3', 'admin', '2017-03-17 23:32:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1548', '3', 'admin', '2017-03-17 23:33:09', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1549', '3', 'admin', '2017-03-17 23:34:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1550', '3', 'admin', '2017-03-17 23:35:44', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1551', '3', 'admin', '2017-03-17 23:36:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1552', '3', 'admin', '2017-03-17 23:39:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1553', '3', 'admin', '2017-03-17 23:42:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1554', '3', 'admin', '2017-03-17 23:44:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1555', '3', 'admin', '2017-03-17 23:49:26', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1556', '3', 'admin', '2017-03-17 23:58:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1557', '3', 'admin', '2017-03-18 00:08:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1558', '3', 'admin', '2017-03-18 00:14:25', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1559', '3', 'admin', '2017-03-18 00:15:02', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1560', '3', 'admin', '2017-03-18 00:15:45', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1561', '3', 'admin', '2017-03-18 00:16:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1562', '3', 'admin', '2017-03-18 00:23:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1563', '3', 'admin', '2017-03-18 00:28:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1564', '3', 'admin', '2017-03-18 00:40:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1565', '3', 'admin', '2017-03-18 00:42:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1566', '3', 'admin', '2017-03-18 00:48:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1567', '3', 'admin', '2017-03-18 00:49:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1568', '3', 'admin', '2017-03-18 00:50:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1569', '3', 'admin', '2017-03-18 00:53:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1570', '3', 'admin', '2017-03-18 00:55:51', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1571', '3', 'admin', '2017-03-18 00:56:35', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1572', '3', 'admin', '2017-03-18 01:00:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1573', '3', 'admin', '2017-03-18 01:03:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1574', '3', 'admin', '2017-03-18 01:03:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1575', '3', 'admin', '2017-03-18 01:13:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1576', '3', 'admin', '2017-03-18 01:14:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1577', '3', 'admin', '2017-03-18 01:14:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1578', '3', 'admin', '2017-03-18 01:20:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1579', '3', 'admin', '2017-03-18 01:20:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1580', '3', 'admin', '2017-03-18 01:23:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1581', '3', 'admin', '2017-03-18 01:24:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1582', '3', 'admin', '2017-03-18 01:24:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1583', '3', 'admin', '2017-03-18 01:25:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1584', '3', 'admin', '2017-03-18 01:27:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1585', '3', 'admin', '2017-03-18 01:27:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1586', '3', 'admin', '2017-03-18 01:34:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1587', '3', 'admin', '2017-03-18 01:37:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1588', '3', 'admin', '2017-03-18 01:38:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1589', '3', 'admin', '2017-03-18 01:38:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1590', '3', 'admin', '2017-03-18 01:40:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1591', '3', 'admin', '2017-03-18 01:42:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1592', '3', 'admin', '2017-03-18 01:45:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1593', '3', 'admin', '2017-03-18 01:49:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1594', '3', 'admin', '2017-03-18 01:54:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1595', '3', 'admin', '2017-03-18 02:00:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1596', '3', 'admin', '2017-03-18 02:26:10', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1597', '3', 'admin', '2017-03-18 02:26:27', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1598', '3', 'admin', '2017-03-18 02:31:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1599', '3', 'admin', '2017-03-18 02:38:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1600', '3', 'admin', '2017-03-18 02:47:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1601', '3', 'admin', '2017-03-18 02:47:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1602', '3', 'admin', '2017-03-18 02:56:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1603', '3', 'admin', '2017-03-18 03:03:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1604', '3', 'admin', '2017-03-18 03:09:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1605', '3', 'admin', '2017-03-18 03:12:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1606', '3', 'admin', '2017-03-18 03:13:14', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1607', '3', 'admin', '2017-03-18 03:20:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1608', '3', 'admin', '2017-03-18 03:21:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1609', '3', 'admin', '2017-03-18 03:21:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1610', '3', 'admin', '2017-03-18 03:21:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1611', '3', 'admin', '2017-03-18 03:22:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1612', '3', 'admin', '2017-03-18 03:23:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1613', '3', 'admin', '2017-03-18 03:24:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1614', '3', 'admin', '2017-03-18 03:24:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1615', '3', 'admin', '2017-03-18 03:34:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1616', '3', 'admin', '2017-03-18 03:38:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1617', '3', 'admin', '2017-03-18 03:38:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1618', '3', 'admin', '2017-03-18 03:41:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1619', '3', 'admin', '2017-03-18 03:44:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1620', '3', 'admin', '2017-03-18 03:45:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1621', '3', 'admin', '2017-03-18 03:48:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1622', '3', 'admin', '2017-03-18 04:05:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1623', '3', 'admin', '2017-03-18 04:07:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1624', '3', 'admin', '2017-03-18 04:16:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1625', '3', 'admin', '2017-03-18 04:34:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1626', '3', 'admin', '2017-03-18 04:35:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1627', '3', 'admin', '2017-03-18 04:38:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1628', '3', 'admin', '2017-03-18 04:38:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1629', '3', 'admin', '2017-03-18 04:40:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1630', '3', 'admin', '2017-03-18 04:49:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1631', '3', 'admin', '2017-03-18 04:50:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1632', '3', 'admin', '2017-03-18 04:57:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1633', '3', 'admin', '2017-03-18 04:59:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1634', '3', 'admin', '2017-03-18 05:02:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1635', '3', 'admin', '2017-03-18 09:17:02', '103.228.209.149');
INSERT INTO `mds_userlogin` VALUES ('1636', '3', 'admin', '2017-03-18 09:40:19', '103.228.209.149');
INSERT INTO `mds_userlogin` VALUES ('1637', '3', 'admin', '2017-03-18 10:57:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1638', '3', 'admin', '2017-03-18 10:57:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1639', '3', 'admin', '2017-03-18 10:59:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1640', '3', 'admin', '2017-03-18 11:05:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1641', '3', 'admin', '2017-03-18 11:06:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1642', '3', 'admin', '2017-03-18 11:16:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1643', '3', 'admin', '2017-03-18 11:20:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1644', '3', 'admin', '2017-03-18 11:24:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1645', '3', 'admin', '2017-03-18 11:27:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1646', '3', 'admin', '2017-03-18 11:27:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1647', '3', 'admin', '2017-03-18 11:42:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1648', '3', 'admin', '2017-03-18 11:44:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1649', '3', 'admin', '2017-03-18 11:53:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1650', '3', 'admin', '2017-03-18 11:56:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1651', '3', 'admin', '2017-03-18 12:02:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1652', '3', 'admin', '2017-03-18 12:03:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1653', '3', 'admin', '2017-03-18 12:04:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1654', '3', 'admin', '2017-03-18 12:10:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1655', '3', 'admin', '2017-03-18 12:12:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1656', '3', 'admin', '2017-03-18 12:13:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1657', '3', 'admin', '2017-03-18 12:15:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1658', '3', 'admin', '2017-03-18 12:18:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1659', '3', 'admin', '2017-03-18 12:23:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1660', '3', 'admin', '2017-03-18 12:51:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1661', '3', 'admin', '2017-03-18 12:53:14', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1662', '3', 'admin', '2017-03-18 13:14:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1663', '3', 'admin', '2017-03-18 13:30:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1664', '3', 'admin', '2017-03-18 14:13:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1665', '3', 'admin', '2017-03-18 14:19:48', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1666', '3', 'admin', '2017-03-18 14:21:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1667', '3', 'admin', '2017-03-18 14:22:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1668', '3', 'admin', '2017-03-18 14:58:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1669', '3', 'admin', '2017-03-18 15:20:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1670', '3', 'admin', '2017-03-18 15:23:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1671', '3', 'admin', '2017-03-18 15:26:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1672', '3', 'admin', '2017-03-18 15:26:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1673', '3', 'admin', '2017-03-18 15:29:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1674', '3', 'admin', '2017-03-18 15:29:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1675', '3', 'admin', '2017-03-18 15:35:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1676', '3', 'admin', '2017-03-18 15:36:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1677', '3', 'admin', '2017-03-18 15:41:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1678', '3', 'admin', '2017-03-18 15:42:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1679', '3', 'admin', '2017-03-18 15:43:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1680', '3', 'admin', '2017-03-18 15:46:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1681', '3', 'admin', '2017-03-18 15:52:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1682', '3', 'admin', '2017-03-18 15:57:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1683', '3', 'admin', '2017-03-18 16:06:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1684', '3', 'admin', '2017-03-18 16:06:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1685', '3', 'admin', '2017-03-18 16:08:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1686', '3', 'admin', '2017-03-18 16:08:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1687', '3', 'admin', '2017-03-18 16:09:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1688', '3', 'admin', '2017-03-18 16:10:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1689', '3', 'admin', '2017-03-18 16:14:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1690', '3', 'admin', '2017-03-18 16:16:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1691', '3', 'admin', '2017-03-18 16:21:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1692', '3', 'admin', '2017-03-18 16:22:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1693', '3', 'admin', '2017-03-18 16:26:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1694', '3', 'admin', '2017-03-18 16:30:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1695', '3', 'admin', '2017-03-18 16:35:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1696', '3', 'admin', '2017-03-18 16:49:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1697', '3', 'admin', '2017-03-18 16:49:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1698', '3', 'admin', '2017-03-18 16:51:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1699', '3', 'admin', '2017-03-18 16:52:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1700', '3', 'admin', '2017-03-18 17:20:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1701', '3', 'admin', '2017-03-18 17:22:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1702', '3', 'admin', '2017-03-18 17:41:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1703', '3', 'admin', '2017-03-18 18:11:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1704', '3', 'admin', '2017-03-18 18:18:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1705', '3', 'admin', '2017-03-18 18:24:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1706', '3', 'admin', '2017-03-18 18:25:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1707', '3', 'admin', '2017-03-18 18:39:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1708', '3', 'admin', '2017-03-18 18:48:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1709', '3', 'admin', '2017-03-18 19:04:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1710', '3', 'admin', '2017-03-18 19:26:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1711', '3', 'admin', '2017-03-18 19:36:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1712', '3', 'admin', '2017-03-18 19:41:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1713', '3', 'admin', '2017-03-18 19:51:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1714', '3', 'admin', '2017-03-18 19:55:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1715', '3', 'admin', '2017-03-18 19:56:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1716', '3', 'admin', '2017-03-18 20:01:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1717', '3', 'admin', '2017-03-18 20:03:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1718', '3', 'admin', '2017-03-18 20:05:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1719', '3', 'admin', '2017-03-18 20:05:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1720', '3', 'admin', '2017-03-18 20:06:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1721', '3', 'admin', '2017-03-18 20:08:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1722', '3', 'admin', '2017-03-18 20:10:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1723', '3', 'admin', '2017-03-18 20:10:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1724', '3', 'admin', '2017-03-18 20:13:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1725', '3', 'admin', '2017-03-18 20:15:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1726', '3', 'admin', '2017-03-18 20:15:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1727', '3', 'admin', '2017-03-18 20:16:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1728', '3', 'admin', '2017-03-18 20:17:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1729', '3', 'admin', '2017-03-18 20:22:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1730', '3', 'admin', '2017-03-18 20:22:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1731', '3', 'admin', '2017-03-18 20:23:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1732', '3', 'admin', '2017-03-18 20:24:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1733', '3', 'admin', '2017-03-18 20:26:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1734', '3', 'admin', '2017-03-18 20:34:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1735', '3', 'admin', '2017-03-18 20:36:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1736', '3', 'admin', '2017-03-18 20:37:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1737', '3', 'admin', '2017-03-18 20:39:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1738', '3', 'admin', '2017-03-18 20:40:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1739', '3', 'admin', '2017-03-18 20:48:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1740', '3', 'admin', '2017-03-18 20:48:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1741', '3', 'admin', '2017-03-18 20:53:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1742', '3', 'admin', '2017-03-18 20:53:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1743', '3', 'admin', '2017-03-18 20:53:36', '183.12.240.203');
INSERT INTO `mds_userlogin` VALUES ('1744', '3', 'admin', '2017-03-18 20:57:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1745', '3', 'admin', '2017-03-18 21:11:00', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1746', '3', 'admin', '2017-03-18 21:12:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1747', '3', 'admin', '2017-03-18 21:12:53', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1748', '3', 'admin', '2017-03-18 21:46:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1749', '3', 'admin', '2017-03-18 21:47:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1750', '3', 'admin', '2017-03-18 21:59:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1751', '3', 'admin', '2017-03-18 22:00:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1752', '3', 'admin', '2017-03-18 22:16:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1753', '3', 'admin', '2017-03-18 22:17:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1754', '3', 'admin', '2017-03-18 22:19:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1755', '3', 'admin', '2017-03-18 22:22:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1756', '3', 'admin', '2017-03-18 22:25:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1757', '3', 'admin', '2017-03-18 22:25:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1758', '3', 'admin', '2017-03-18 22:31:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1759', '3', 'admin', '2017-03-18 22:40:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1760', '3', 'admin', '2017-03-18 22:49:22', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1761', '3', 'admin', '2017-03-18 22:50:06', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1762', '3', 'admin', '2017-03-18 23:09:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1763', '3', 'admin', '2017-03-18 23:09:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1764', '3', 'admin', '2017-03-18 23:13:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1765', '3', 'admin', '2017-03-18 23:17:37', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1766', '3', 'admin', '2017-03-18 23:22:01', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1767', '3', 'admin', '2017-03-18 23:22:08', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1768', '3', 'admin', '2017-03-18 23:32:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1769', '3', 'admin', '2017-03-18 23:33:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1770', '3', 'admin', '2017-03-18 23:33:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1771', '3', 'admin', '2017-03-18 23:45:46', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1772', '3', 'admin', '2017-03-18 23:59:22', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1773', '3', 'admin', '2017-03-19 00:01:18', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1774', '3', 'admin', '2017-03-19 00:02:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1775', '3', 'admin', '2017-03-19 00:07:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1776', '3', 'admin', '2017-03-19 00:08:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1777', '3', 'admin', '2017-03-19 00:17:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1778', '3', 'admin', '2017-03-19 00:21:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1779', '3', 'admin', '2017-03-19 00:21:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1780', '3', 'admin', '2017-03-19 00:23:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1781', '3', 'admin', '2017-03-19 00:28:17', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1782', '3', 'admin', '2017-03-19 00:46:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1783', '3', 'admin', '2017-03-19 00:48:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1784', '3', 'admin', '2017-03-19 01:47:19', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1785', '3', 'admin', '2017-03-19 01:54:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1786', '3', 'admin', '2017-03-19 02:06:28', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1787', '3', 'admin', '2017-03-19 02:06:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1788', '3', 'admin', '2017-03-19 02:23:37', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1789', '3', 'admin', '2017-03-19 02:24:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1790', '3', 'admin', '2017-03-19 02:24:51', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1791', '3', 'admin', '2017-03-19 02:26:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1792', '3', 'admin', '2017-03-19 02:27:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1793', '3', 'admin', '2017-03-19 02:37:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1794', '3', 'admin', '2017-03-19 02:37:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1795', '3', 'admin', '2017-03-19 02:44:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1796', '3', 'admin', '2017-03-19 02:44:19', '14.220.123.103');
INSERT INTO `mds_userlogin` VALUES ('1797', '3', 'admin', '2017-03-19 02:47:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1798', '3', 'admin', '2017-03-19 03:08:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1799', '3', 'admin', '2017-03-19 03:08:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1800', '3', 'admin', '2017-03-19 03:23:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1801', '3', 'admin', '2017-03-19 03:23:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1802', '3', 'admin', '2017-03-19 13:37:12', '14.220.121.109');
INSERT INTO `mds_userlogin` VALUES ('1803', '3', 'admin', '2017-03-19 13:47:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1804', '3', 'admin', '2017-03-19 13:47:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1805', '3', 'admin', '2017-03-19 14:14:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1806', '3', 'admin', '2017-03-19 14:15:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1807', '3', 'admin', '2017-03-19 14:16:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1808', '3', 'admin', '2017-03-19 14:33:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1809', '3', 'admin', '2017-03-19 14:58:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1810', '3', 'admin', '2017-03-19 14:59:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1811', '3', 'admin', '2017-03-19 15:00:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1812', '3', 'admin', '2017-03-19 15:01:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1813', '3', 'admin', '2017-03-19 15:24:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1814', '3', 'admin', '2017-03-19 15:33:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1815', '3', 'admin', '2017-03-19 15:43:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1816', '3', 'admin', '2017-03-19 15:51:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1817', '3', 'admin', '2017-03-19 16:03:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1818', '3', 'admin', '2017-03-19 22:29:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1819', '3', 'admin', '2017-03-19 23:06:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1820', '3', 'admin', '2017-03-19 23:14:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1821', '3', 'admin', '2017-03-19 23:20:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1822', '3', 'admin', '2017-03-19 23:48:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1823', '3', 'admin', '2017-03-20 00:02:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1824', '3', 'admin', '2017-03-20 00:08:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1825', '3', 'admin', '2017-03-20 00:09:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1826', '3', 'admin', '2017-03-20 00:18:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1827', '3', 'admin', '2017-03-20 00:23:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1828', '27', 'test00002', '2017-03-20 00:38:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1829', '3', 'admin', '2017-03-20 00:39:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1830', '3', 'admin', '2017-03-20 00:55:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1831', '3', 'admin', '2017-03-20 00:55:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1832', '3', 'admin', '2017-03-20 00:58:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1833', '3', 'admin', '2017-03-20 00:58:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1834', '3', 'admin', '2017-03-20 01:02:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1835', '3', 'admin', '2017-03-20 01:04:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1836', '3', 'admin', '2017-03-20 01:05:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1837', '3', 'admin', '2017-03-20 01:06:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1838', '3', 'admin', '2017-03-20 01:06:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1839', '3', 'admin', '2017-03-20 01:29:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1840', '3', 'admin', '2017-03-20 01:37:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1841', '27', 'test00002', '2017-03-20 01:39:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1842', '27', 'test00002', '2017-03-20 01:40:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1843', '3', 'admin', '2017-03-20 01:40:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1844', '27', 'test00002', '2017-03-20 01:41:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1845', '3', 'admin', '2017-03-20 01:43:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1846', '27', 'test00002', '2017-03-20 01:49:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1847', '3', 'admin', '2017-03-20 01:50:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1848', '3', 'admin', '2017-03-20 01:56:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1849', '28', 'jayce', '2017-03-20 01:56:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1850', '3', 'admin', '2017-03-20 01:56:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1851', '3', 'admin', '2017-03-20 02:01:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1852', '3', 'admin', '2017-03-20 02:03:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1853', '3', 'admin', '2017-03-20 02:07:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1854', '3', 'admin', '2017-03-20 02:11:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1855', '3', 'admin', '2017-03-20 02:28:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1856', '3', 'admin', '2017-03-20 02:59:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1857', '3', 'admin', '2017-03-20 09:04:59', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1858', '3', 'admin', '2017-03-20 09:16:44', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1859', '29', 'gd001', '2017-03-20 09:17:51', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1860', '3', 'admin', '2017-03-20 09:18:15', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1861', '29', 'gd001', '2017-03-20 09:20:01', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1862', '3', 'admin', '2017-03-20 09:20:12', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1863', '29', 'gd001', '2017-03-20 09:22:07', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1864', '29', 'gd001', '2017-03-20 09:23:38', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1865', '3', 'admin', '2017-03-20 09:23:47', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1866', '3', 'admin', '2017-03-20 09:24:00', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1867', '29', 'gd001', '2017-03-20 09:24:52', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1868', '3', 'admin', '2017-03-20 09:26:39', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1869', '30', 'gn001', '2017-03-20 09:30:24', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1870', '3', 'admin', '2017-03-20 09:30:41', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1871', '3', 'admin', '2017-03-20 09:31:21', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1872', '30', 'gn001', '2017-03-20 09:31:57', '112.97.51.26');
INSERT INTO `mds_userlogin` VALUES ('1873', '3', 'admin', '2017-03-20 09:47:48', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1874', '3', 'admin', '2017-03-20 09:51:15', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1875', '3', 'admin', '2017-03-20 10:02:35', '116.7.20.102');
INSERT INTO `mds_userlogin` VALUES ('1876', '3', 'admin', '2017-03-20 10:11:35', '116.7.20.102');
INSERT INTO `mds_userlogin` VALUES ('1877', '3', 'admin', '2017-03-20 10:21:28', '116.7.20.102');
INSERT INTO `mds_userlogin` VALUES ('1878', '3', 'admin', '2017-03-20 10:34:13', '116.7.20.102');
INSERT INTO `mds_userlogin` VALUES ('1879', '3', 'admin', '2017-03-20 11:11:00', '14.220.122.71');
INSERT INTO `mds_userlogin` VALUES ('1880', '3', 'admin', '2017-03-20 11:12:03', '14.220.122.71');
INSERT INTO `mds_userlogin` VALUES ('1881', '3', 'admin', '2017-03-20 11:56:55', '113.16.142.72');
INSERT INTO `mds_userlogin` VALUES ('1882', '3', 'admin', '2017-03-20 11:58:06', '14.220.122.71');
INSERT INTO `mds_userlogin` VALUES ('1883', '3', 'admin', '2017-03-20 14:59:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1884', '3', 'admin', '2017-03-20 15:28:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1885', '29', 'gd001', '2017-03-20 22:24:01', '113.16.140.208');
INSERT INTO `mds_userlogin` VALUES ('1886', '29', 'gd001', '2017-03-20 22:25:27', '113.16.140.208');
INSERT INTO `mds_userlogin` VALUES ('1887', '29', 'gd001', '2017-03-20 23:31:00', '113.16.142.116');
INSERT INTO `mds_userlogin` VALUES ('1888', '3', 'admin', '2017-03-20 23:51:06', '14.220.120.91');
INSERT INTO `mds_userlogin` VALUES ('1889', '3', 'admin', '2017-03-20 23:55:56', '113.16.140.208');
INSERT INTO `mds_userlogin` VALUES ('1890', '3', 'admin', '2017-03-21 00:04:39', '14.220.120.91');
INSERT INTO `mds_userlogin` VALUES ('1891', '3', 'admin', '2017-03-21 00:22:23', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1892', '3', 'admin', '2017-03-21 00:36:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1893', '29', 'gd001', '2017-03-21 00:45:56', '113.16.142.116');
INSERT INTO `mds_userlogin` VALUES ('1894', '29', 'gd001', '2017-03-21 00:46:55', '113.16.142.116');
INSERT INTO `mds_userlogin` VALUES ('1895', '29', 'gd001', '2017-03-21 00:48:32', '113.16.142.116');
INSERT INTO `mds_userlogin` VALUES ('1896', '29', 'gd001', '2017-03-21 00:49:48', '113.16.142.116');
INSERT INTO `mds_userlogin` VALUES ('1897', '3', 'admin', '2017-03-21 00:50:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1898', '3', 'admin', '2017-03-21 01:00:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1899', '3', 'admin', '2017-03-21 01:08:02', '14.220.120.91');
INSERT INTO `mds_userlogin` VALUES ('1900', '29', 'gd001', '2017-03-21 01:08:16', '223.104.22.195');
INSERT INTO `mds_userlogin` VALUES ('1901', '3', 'admin', '2017-03-21 01:18:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1902', '3', 'admin', '2017-03-21 01:19:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1903', '3', 'admin', '2017-03-21 01:20:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1904', '3', 'admin', '2017-03-21 01:24:44', '14.220.120.91');
INSERT INTO `mds_userlogin` VALUES ('1905', '3', 'admin', '2017-03-21 01:37:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1906', '3', 'admin', '2017-03-21 01:37:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1907', '3', 'admin', '2017-03-21 01:41:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1908', '3', 'admin', '2017-03-21 01:43:10', '14.220.120.91');
INSERT INTO `mds_userlogin` VALUES ('1909', '3', 'admin', '2017-03-21 01:45:23', '14.220.120.91');
INSERT INTO `mds_userlogin` VALUES ('1910', '3', 'admin', '2017-03-21 08:23:28', '113.16.142.116');
INSERT INTO `mds_userlogin` VALUES ('1911', '3', 'admin', '2017-03-21 08:25:27', '113.16.142.116');
INSERT INTO `mds_userlogin` VALUES ('1912', '3', 'admin', '2017-03-21 08:59:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1913', '3', 'admin', '2017-03-21 09:38:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1914', '3', 'admin', '2017-03-21 10:24:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1915', '3', 'admin', '2017-03-21 10:34:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1916', '3', 'admin', '2017-03-21 10:53:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1917', '3', 'admin', '2017-03-21 12:57:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1918', '3', 'admin', '2017-03-21 12:59:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1919', '3', 'admin', '2017-03-21 13:01:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1920', '3', 'admin', '2017-03-21 13:16:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1921', '3', 'admin', '2017-03-21 13:22:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1922', '3', 'admin', '2017-03-21 13:24:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1923', '3', 'admin', '2017-03-21 13:27:20', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1924', '29', 'gd001', '2017-03-21 13:45:43', '113.12.102.197');
INSERT INTO `mds_userlogin` VALUES ('1925', '29', 'gd001', '2017-03-21 14:30:42', '113.12.102.197');
INSERT INTO `mds_userlogin` VALUES ('1926', '3', 'admin', '2017-03-21 15:14:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1927', '3', 'admin', '2017-03-21 15:39:11', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1928', '30', 'gn001', '2017-03-21 15:53:09', '223.104.91.98');
INSERT INTO `mds_userlogin` VALUES ('1929', '30', 'gn001', '2017-03-21 16:12:10', '219.159.159.146');
INSERT INTO `mds_userlogin` VALUES ('1930', '30', 'gn001', '2017-03-21 16:17:17', '223.104.91.98');
INSERT INTO `mds_userlogin` VALUES ('1931', '3', 'admin', '2017-03-22 15:28:37', '219.159.159.146');
INSERT INTO `mds_userlogin` VALUES ('1932', '3', 'admin', '2017-03-22 15:31:33', '219.159.159.146');
INSERT INTO `mds_userlogin` VALUES ('1933', '3', 'admin', '2017-03-22 20:32:01', '113.16.143.131');
INSERT INTO `mds_userlogin` VALUES ('1934', '3', 'admin', '2017-03-22 21:19:49', '113.16.143.131');
INSERT INTO `mds_userlogin` VALUES ('1935', '29', 'gd001', '2017-03-22 23:48:46', '113.16.142.72');
INSERT INTO `mds_userlogin` VALUES ('1936', '3', 'admin', '2017-03-23 20:57:36', '113.16.143.131');
INSERT INTO `mds_userlogin` VALUES ('1937', '3', 'admin', '2017-03-23 21:19:00', '113.16.143.131');
INSERT INTO `mds_userlogin` VALUES ('1938', '3', 'admin', '2017-03-23 21:22:38', '113.16.143.131');
INSERT INTO `mds_userlogin` VALUES ('1939', '3', 'admin', '2017-03-23 21:23:38', '113.16.143.131');
INSERT INTO `mds_userlogin` VALUES ('1940', '3', 'admin', '2017-03-23 21:25:15', '113.16.143.131');
INSERT INTO `mds_userlogin` VALUES ('1941', '3', 'admin', '2017-03-25 15:34:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1942', '3', 'admin', '2017-03-26 15:33:54', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1943', '3', 'admin', '2017-03-26 15:35:07', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1944', '3', 'admin', '2017-03-26 15:36:33', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1945', '3', 'admin', '2017-03-26 15:36:45', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1946', '3', 'admin', '2017-03-26 15:36:55', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1947', '3', 'admin', '2017-03-26 16:06:24', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1948', '3', 'admin', '2017-03-26 16:08:00', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1949', '3', 'admin', '2017-03-26 16:19:50', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1950', '3', 'admin', '2017-03-26 16:20:48', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1951', '32', 'wangjiwen', '2017-03-26 16:22:59', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1952', '3', 'admin', '2017-03-26 16:23:39', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1953', '32', 'wangjiwen', '2017-03-26 16:30:07', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1954', '3', 'admin', '2017-03-26 16:30:22', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1955', '32', 'wangjiwen', '2017-03-26 16:31:13', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1956', '3', 'admin', '2017-03-26 16:32:04', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1957', '33', 'wangjiwen1', '2017-03-26 16:35:44', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1958', '3', 'admin', '2017-03-26 16:36:09', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1959', '3', 'admin', '2017-03-26 16:47:28', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1960', '30', 'gn001', '2017-03-26 16:56:04', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1961', '29', 'gd001', '2017-03-26 16:57:10', '183.12.242.107');
INSERT INTO `mds_userlogin` VALUES ('1962', '3', 'admin', '2017-03-27 08:47:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1963', '3', 'admin', '2017-03-27 08:47:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1964', '3', 'admin', '2017-03-27 15:37:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1965', '3', 'admin', '2017-03-27 16:06:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1966', '3', 'admin', '2017-03-27 20:42:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1967', '3', 'admin', '2017-03-27 20:59:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1968', '3', 'admin', '2017-03-27 21:01:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1969', '3', 'admin', '2017-03-27 21:01:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1970', '3', 'admin', '2017-03-27 21:02:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1971', '3', 'admin', '2017-03-27 21:15:23', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1972', '3', 'admin', '2017-03-27 21:16:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('1973', '3', 'admin', '2017-03-28 11:39:31', '117.136.98.244');
INSERT INTO `mds_userlogin` VALUES ('1974', '3', 'admin', '2017-03-28 11:40:14', '183.12.237.10');
INSERT INTO `mds_userlogin` VALUES ('1975', '3', 'admin', '2017-03-28 11:40:23', '183.12.237.10');
INSERT INTO `mds_userlogin` VALUES ('1976', '3', 'admin', '2017-03-29 12:04:02', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1977', '3', 'admin', '2017-03-30 22:53:32', '14.220.122.199');
INSERT INTO `mds_userlogin` VALUES ('1978', '3', 'admin', '2017-03-30 22:53:53', '14.220.122.199');
INSERT INTO `mds_userlogin` VALUES ('1979', '3', 'admin', '2017-03-31 01:28:03', '14.220.122.199');
INSERT INTO `mds_userlogin` VALUES ('1980', '3', 'admin', '2017-04-06 10:44:09', '117.136.97.46');
INSERT INTO `mds_userlogin` VALUES ('1981', '3', 'admin', '2017-04-06 15:40:08', '117.136.97.62');
INSERT INTO `mds_userlogin` VALUES ('1982', '3', 'admin', '2017-04-06 15:42:24', '117.136.97.62');
INSERT INTO `mds_userlogin` VALUES ('1983', '3', 'admin', '2017-04-07 09:59:11', '117.136.97.49');
INSERT INTO `mds_userlogin` VALUES ('1984', '3', 'admin', '2017-04-07 21:04:20', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1985', '3', 'admin', '2017-04-07 21:04:34', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1986', '3', 'admin', '2017-04-07 22:22:33', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1987', '3', 'admin', '2017-04-07 22:23:00', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1988', '3', 'admin', '2017-04-07 22:23:16', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1989', '3', 'admin', '2017-04-07 22:23:23', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1990', '3', 'admin', '2017-04-07 22:25:19', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1991', '3', 'admin', '2017-04-07 22:27:11', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1992', '3', 'admin', '2017-04-07 22:27:48', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1993', '3', 'admin', '2017-04-07 22:27:54', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1994', '3', 'admin', '2017-04-07 22:30:46', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1995', '3', 'admin', '2017-04-08 11:42:38', '14.220.123.159');
INSERT INTO `mds_userlogin` VALUES ('1996', '3', 'admin', '2017-04-09 22:20:33', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('1997', '3', 'admin', '2017-04-09 22:21:08', '14.220.121.1');
INSERT INTO `mds_userlogin` VALUES ('1998', '3', 'admin', '2017-04-11 14:18:08', '183.12.240.170');
INSERT INTO `mds_userlogin` VALUES ('1999', '3', 'admin', '2017-04-15 14:05:30', '14.220.122.197');
INSERT INTO `mds_userlogin` VALUES ('2000', '3', 'admin', '2017-04-15 14:53:55', '183.12.243.104');
INSERT INTO `mds_userlogin` VALUES ('2001', '3', 'admin', '2017-04-15 14:54:11', '183.12.243.104');
INSERT INTO `mds_userlogin` VALUES ('2002', '3', 'admin', '2017-04-15 14:55:00', '183.12.243.104');
INSERT INTO `mds_userlogin` VALUES ('2003', '3', 'admin', '2017-04-15 14:55:09', '183.12.243.104');
INSERT INTO `mds_userlogin` VALUES ('2004', '3', 'admin', '2017-04-16 22:26:29', '183.12.243.104');
INSERT INTO `mds_userlogin` VALUES ('2005', '3', 'admin', '2017-04-16 22:28:18', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('2006', '3', 'admin', '2017-04-16 22:36:00', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('2007', '3', 'admin', '2017-04-16 22:37:08', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('2008', '3', 'admin', '2017-04-19 21:20:41', '183.12.243.132');
INSERT INTO `mds_userlogin` VALUES ('2009', '3', 'admin', '2017-04-19 21:31:13', '14.220.120.8');
INSERT INTO `mds_userlogin` VALUES ('2010', '3', 'admin', '2017-04-21 21:39:12', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2011', '36', 'wj123456', '2017-04-21 21:40:20', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2012', '3', 'admin', '2017-04-21 21:42:00', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2013', '3', 'admin', '2017-04-21 21:53:23', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2014', '3', 'admin', '2017-04-22 01:16:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2015', '36', 'wj123456', '2017-04-22 01:18:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2016', '36', 'wj123456', '2017-04-22 01:26:30', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2017', '36', 'wj123456', '2017-04-22 01:28:03', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2018', '3', 'admin', '2017-04-22 02:15:40', '183.61.51.226');
INSERT INTO `mds_userlogin` VALUES ('2019', '3', 'admin', '2017-04-22 02:16:13', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2020', '3', 'admin', '2017-04-22 16:54:02', '27.46.4.254');
INSERT INTO `mds_userlogin` VALUES ('2021', '3', 'admin', '2017-04-22 16:54:09', '183.61.51.179');
INSERT INTO `mds_userlogin` VALUES ('2022', '3', 'admin', '2017-04-22 19:00:08', '61.141.73.53');
INSERT INTO `mds_userlogin` VALUES ('2023', '37', 'sukeyu', '2017-04-22 19:07:08', '61.141.73.53');
INSERT INTO `mds_userlogin` VALUES ('2024', '3', 'admin', '2017-04-22 19:08:17', '61.141.73.53');
INSERT INTO `mds_userlogin` VALUES ('2025', '3', 'admin', '2017-04-22 21:15:52', '27.46.4.232');
INSERT INTO `mds_userlogin` VALUES ('2026', '3', 'admin', '2017-04-22 23:49:50', '61.141.73.53');
INSERT INTO `mds_userlogin` VALUES ('2027', '37', 'sukeyu', '2017-04-22 23:50:05', '61.141.73.53');
INSERT INTO `mds_userlogin` VALUES ('2028', '3', 'admin', '2017-04-22 23:51:07', '61.141.73.53');
INSERT INTO `mds_userlogin` VALUES ('2029', '37', 'sukeyu', '2017-04-22 23:51:42', '61.141.73.53');
INSERT INTO `mds_userlogin` VALUES ('2030', '3', 'admin', '2017-04-23 00:06:12', '14.220.121.190');
INSERT INTO `mds_userlogin` VALUES ('2031', '3', 'admin', '2017-04-23 15:50:56', '163.125.98.25');
INSERT INTO `mds_userlogin` VALUES ('2032', '3', 'admin', '2017-04-23 22:15:47', '14.220.123.68');
INSERT INTO `mds_userlogin` VALUES ('2033', '3', 'admin', '2017-04-23 22:18:04', '14.220.123.68');
INSERT INTO `mds_userlogin` VALUES ('2034', '3', 'admin', '2017-04-23 23:08:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2035', '3', 'admin', '2017-04-23 23:28:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2036', '3', 'admin', '2017-04-23 23:48:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2037', '3', 'admin', '2017-04-23 23:57:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2038', '3', 'admin', '2017-04-24 00:14:15', '14.220.123.68');
INSERT INTO `mds_userlogin` VALUES ('2039', '3', 'admin', '2017-04-24 00:30:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2040', '3', 'admin', '2017-04-24 00:40:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2041', '3', 'admin', '2017-04-24 00:52:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2042', '3', 'admin', '2017-04-24 01:29:40', '14.220.123.68');
INSERT INTO `mds_userlogin` VALUES ('2043', '3', 'admin', '2017-04-24 01:38:59', '14.220.123.68');
INSERT INTO `mds_userlogin` VALUES ('2044', '3', 'admin', '2017-04-24 01:42:21', '14.220.123.68');
INSERT INTO `mds_userlogin` VALUES ('2045', '3', 'admin', '2017-04-24 22:35:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2046', '3', 'admin', '2017-04-24 22:49:42', '14.220.120.187');
INSERT INTO `mds_userlogin` VALUES ('2047', '3', 'admin', '2017-04-24 23:11:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2048', '3', 'admin', '2017-04-24 23:19:36', '14.220.120.187');
INSERT INTO `mds_userlogin` VALUES ('2049', '3', 'admin', '2017-04-24 23:31:25', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2050', '3', 'admin', '2017-04-25 00:09:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2051', '3', 'admin', '2017-04-25 00:10:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2052', '3', 'admin', '2017-04-25 00:15:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2053', '3', 'admin', '2017-04-25 00:17:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2054', '3', 'admin', '2017-04-25 00:24:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2055', '3', 'admin', '2017-05-01 17:39:15', '163.125.98.153');
INSERT INTO `mds_userlogin` VALUES ('2056', '3', 'admin', '2017-05-03 20:31:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2057', '3', 'admin', '2017-05-03 20:56:10', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2058', '3', 'admin', '2017-05-03 21:44:12', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2059', '3', 'admin', '2017-05-03 21:57:30', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2060', '3', 'admin', '2017-05-03 21:57:55', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2061', '3', 'admin', '2017-05-03 21:59:18', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2062', '3', 'admin', '2017-05-03 21:59:38', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2063', '3', 'admin', '2017-05-03 22:00:40', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2064', '3', 'admin', '2017-05-04 00:12:05', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2065', '3', 'admin', '2017-05-04 00:16:29', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2066', '3', 'admin', '2017-05-04 00:17:14', '14.220.123.1');
INSERT INTO `mds_userlogin` VALUES ('2067', '3', 'admin', '2017-05-04 21:08:22', '14.220.121.183');
INSERT INTO `mds_userlogin` VALUES ('2068', '3', 'admin', '2017-05-04 22:19:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2069', '3', 'admin', '2017-05-04 22:21:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2070', '3', 'admin', '2017-05-04 22:34:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2071', '3', 'admin', '2017-05-04 22:37:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2072', '3', 'admin', '2017-05-04 22:45:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2073', '3', 'admin', '2017-05-04 22:47:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2074', '3', 'admin', '2017-05-04 22:52:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2075', '3', 'admin', '2017-05-04 22:56:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2076', '3', 'admin', '2017-05-05 00:10:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2077', '3', 'admin', '2017-05-05 00:13:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2078', '3', 'admin', '2017-05-05 00:30:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2079', '3', 'admin', '2017-05-05 00:41:05', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2080', '3', 'admin', '2017-05-05 00:50:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2081', '3', 'admin', '2017-05-05 00:52:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2082', '3', 'admin', '2017-05-05 00:57:55', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2083', '3', 'admin', '2017-05-05 00:59:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2084', '3', 'admin', '2017-05-05 01:02:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2085', '3', 'admin', '2017-05-05 01:04:48', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2086', '3', 'admin', '2017-05-05 01:07:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2087', '3', 'admin', '2017-05-05 01:12:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2088', '3', 'admin', '2017-05-05 01:14:52', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2089', '3', 'admin', '2017-05-05 01:19:53', '14.220.121.183');
INSERT INTO `mds_userlogin` VALUES ('2090', '3', 'admin', '2017-05-05 10:41:27', '116.7.20.174');
INSERT INTO `mds_userlogin` VALUES ('2091', '3', 'admin', '2017-05-06 21:28:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2092', '3', 'admin', '2017-05-06 21:40:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2093', '3', 'admin', '2017-05-06 21:44:00', '14.220.123.92');
INSERT INTO `mds_userlogin` VALUES ('2094', '3', 'admin', '2017-05-08 14:36:31', '116.7.20.149');
INSERT INTO `mds_userlogin` VALUES ('2095', '3', 'admin', '2017-05-11 19:03:40', '116.7.20.170');
INSERT INTO `mds_userlogin` VALUES ('2096', '3', 'admin', '2017-05-12 20:24:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2097', '3', 'admin', '2017-05-12 22:11:01', '14.220.121.37');
INSERT INTO `mds_userlogin` VALUES ('2098', '3', 'admin', '2017-05-12 22:43:40', '14.220.121.37');
INSERT INTO `mds_userlogin` VALUES ('2099', '3', 'admin', '2017-05-12 22:44:20', '14.220.121.37');
INSERT INTO `mds_userlogin` VALUES ('2100', '3', 'admin', '2017-05-12 22:44:57', '14.220.121.37');
INSERT INTO `mds_userlogin` VALUES ('2101', '3', 'admin', '2017-05-14 12:26:07', '14.220.120.99');
INSERT INTO `mds_userlogin` VALUES ('2102', '3', 'admin', '2017-05-14 21:59:45', '116.7.22.103');
INSERT INTO `mds_userlogin` VALUES ('2103', '3', 'admin', '2017-05-14 22:05:57', '116.7.22.103');
INSERT INTO `mds_userlogin` VALUES ('2104', '3', 'admin', '2017-05-14 22:36:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2105', '3', 'admin', '2017-05-14 23:15:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2106', '3', 'admin', '2017-05-14 23:25:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2107', '3', 'admin', '2017-05-14 23:25:33', '14.220.120.99');
INSERT INTO `mds_userlogin` VALUES ('2108', '3', 'admin', '2017-05-14 23:29:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2109', '3', 'admin', '2017-05-15 00:02:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2110', '3', 'admin', '2017-05-15 00:17:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2111', '3', 'admin', '2017-05-15 00:28:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2112', '3', 'admin', '2017-05-16 20:24:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2113', '3', 'admin', '2017-05-16 20:45:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2114', '3', 'admin', '2017-05-16 20:46:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2115', '3', 'admin', '2017-05-16 20:48:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2116', '3', 'admin', '2017-05-16 20:49:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2117', '3', 'admin', '2017-05-16 20:50:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2118', '3', 'admin', '2017-05-16 20:53:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2119', '3', 'admin', '2017-05-16 20:54:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2120', '3', 'admin', '2017-05-16 20:55:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2121', '3', 'admin', '2017-05-16 20:57:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2122', '3', 'admin', '2017-05-16 21:00:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2123', '3', 'admin', '2017-05-16 21:00:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2124', '3', 'admin', '2017-05-16 21:01:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2125', '3', 'admin', '2017-05-16 21:02:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2126', '3', 'admin', '2017-05-16 21:02:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2127', '3', 'admin', '2017-05-16 21:02:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2128', '3', 'admin', '2017-05-16 21:38:01', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2129', '3', 'admin', '2017-05-16 21:39:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2130', '3', 'admin', '2017-05-16 21:49:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2131', '3', 'admin', '2017-05-16 21:49:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2132', '3', 'admin', '2017-05-16 21:51:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2133', '3', 'admin', '2017-05-16 21:52:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2134', '3', 'admin', '2017-05-16 21:55:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2135', '3', 'admin', '2017-05-16 21:56:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2136', '3', 'admin', '2017-05-16 22:08:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2137', '3', 'admin', '2017-05-16 22:17:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2138', '3', 'admin', '2017-05-16 22:22:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2139', '3', 'admin', '2017-05-16 22:36:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2140', '3', 'admin', '2017-05-16 22:49:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2141', '3', 'admin', '2017-05-16 22:52:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2142', '3', 'admin', '2017-05-16 23:00:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2143', '3', 'admin', '2017-05-17 21:36:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2144', '3', 'admin', '2017-05-17 22:13:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2145', '3', 'admin', '2017-05-17 22:18:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2146', '3', 'admin', '2017-05-17 22:21:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2147', '3', 'admin', '2017-05-17 22:22:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2148', '3', 'admin', '2017-05-17 22:28:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2149', '3', 'admin', '2017-05-17 22:38:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2150', '3', 'admin', '2017-05-17 22:42:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2151', '3', 'admin', '2017-05-17 22:48:47', '14.220.121.85');
INSERT INTO `mds_userlogin` VALUES ('2152', '3', 'admin', '2017-05-17 23:02:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2153', '3', 'admin', '2017-05-17 23:42:05', '14.220.121.85');
INSERT INTO `mds_userlogin` VALUES ('2154', '3', 'admin', '2017-05-17 23:44:38', '14.220.121.85');
INSERT INTO `mds_userlogin` VALUES ('2155', '3', 'admin', '2017-05-18 00:47:02', '112.97.54.161');
INSERT INTO `mds_userlogin` VALUES ('2156', '3', 'admin', '2017-05-18 00:47:25', '112.97.54.161');
INSERT INTO `mds_userlogin` VALUES ('2157', '3', 'admin', '2017-05-19 21:24:16', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2158', '3', 'admin', '2017-05-19 21:27:39', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2159', '3', 'admin', '2017-05-19 21:46:02', '58.101.6.174');
INSERT INTO `mds_userlogin` VALUES ('2160', '3', 'admin', '2017-05-19 21:46:59', '58.101.6.174');
INSERT INTO `mds_userlogin` VALUES ('2161', '3', 'admin', '2017-05-20 15:55:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2162', '3', 'admin', '2017-05-20 15:56:16', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2163', '3', 'admin', '2017-05-20 17:11:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2164', '3', 'admin', '2017-05-20 17:11:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2165', '3', 'admin', '2017-05-20 17:11:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2166', '3', 'admin', '2017-05-20 17:16:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2167', '3', 'admin', '2017-05-20 17:21:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2168', '3', 'admin', '2017-05-20 17:23:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2169', '3', 'admin', '2017-05-20 17:27:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2170', '3', 'admin', '2017-05-20 17:50:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2171', '3', 'admin', '2017-05-20 17:53:29', '112.97.227.80');
INSERT INTO `mds_userlogin` VALUES ('2172', '3', 'admin', '2017-05-20 18:05:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2173', '3', 'admin', '2017-05-20 18:24:21', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2174', '3', 'admin', '2017-05-20 18:25:14', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2175', '3', 'admin', '2017-05-20 18:25:22', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2176', '3', 'admin', '2017-05-20 18:26:16', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2177', '3', 'admin', '2017-05-20 18:27:19', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2178', '3', 'admin', '2017-05-20 18:28:45', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2179', '3', 'admin', '2017-05-20 18:29:38', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2180', '3', 'admin', '2017-05-20 18:31:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2181', '3', 'admin', '2017-05-20 18:31:49', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2182', '3', 'admin', '2017-05-20 18:31:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2183', '3', 'admin', '2017-05-20 18:32:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2184', '3', 'admin', '2017-05-20 18:36:25', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2185', '3', 'admin', '2017-05-20 18:36:37', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2186', '3', 'admin', '2017-05-21 12:17:54', '14.220.122.112');
INSERT INTO `mds_userlogin` VALUES ('2187', '3', 'admin', '2017-05-21 12:37:14', '222.52.151.86');
INSERT INTO `mds_userlogin` VALUES ('2188', '3', 'admin', '2017-05-21 12:43:49', '222.52.151.86');
INSERT INTO `mds_userlogin` VALUES ('2189', '3', 'admin', '2017-05-21 12:45:46', '222.52.151.86');
INSERT INTO `mds_userlogin` VALUES ('2190', '3', 'admin', '2017-05-21 13:05:31', '222.52.151.86');
INSERT INTO `mds_userlogin` VALUES ('2191', '3', 'admin', '2017-05-22 09:25:56', '112.97.55.36');
INSERT INTO `mds_userlogin` VALUES ('2192', '3', 'admin', '2017-05-24 13:30:28', '223.104.22.80');
INSERT INTO `mds_userlogin` VALUES ('2193', '3', 'admin', '2017-05-24 14:44:28', '112.97.63.75');
INSERT INTO `mds_userlogin` VALUES ('2194', '3', 'admin', '2017-05-24 14:44:50', '112.97.63.75');
INSERT INTO `mds_userlogin` VALUES ('2195', '3', 'admin', '2017-05-29 23:17:24', '14.220.121.13');
INSERT INTO `mds_userlogin` VALUES ('2196', '3', 'admin', '2017-05-29 23:18:25', '14.220.121.13');
INSERT INTO `mds_userlogin` VALUES ('2197', '3', 'admin', '2017-05-30 14:42:31', '14.220.123.59');
INSERT INTO `mds_userlogin` VALUES ('2198', '3', 'admin', '2017-05-30 14:50:49', '14.220.123.59');
INSERT INTO `mds_userlogin` VALUES ('2199', '3', 'admin', '2017-05-30 14:50:55', '14.220.123.59');
INSERT INTO `mds_userlogin` VALUES ('2200', '3', 'admin', '2017-06-04 18:48:28', '113.91.32.191');
INSERT INTO `mds_userlogin` VALUES ('2201', '3', 'admin', '2017-06-06 21:08:01', '14.220.120.60');
INSERT INTO `mds_userlogin` VALUES ('2202', '3', 'admin', '2017-06-07 00:57:03', '14.220.120.60');
INSERT INTO `mds_userlogin` VALUES ('2203', '3', 'admin', '2017-06-07 01:11:57', '14.220.120.60');
INSERT INTO `mds_userlogin` VALUES ('2204', '3', 'admin', '2017-06-07 11:57:08', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2205', '3', 'admin', '2017-06-07 22:21:25', '14.220.121.50');
INSERT INTO `mds_userlogin` VALUES ('2206', '3', 'admin', '2017-06-18 23:24:24', '14.220.120.87');
INSERT INTO `mds_userlogin` VALUES ('2207', '3', 'admin', '2017-06-19 01:19:23', '14.220.120.87');
INSERT INTO `mds_userlogin` VALUES ('2208', '3', 'admin', '2017-06-23 10:32:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2209', '3', 'admin', '2017-06-23 10:40:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2210', '3', 'admin', '2017-06-23 11:09:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2211', '3', 'admin', '2017-06-23 11:22:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2212', '3', 'admin', '2017-06-23 11:31:49', '183.12.238.242');
INSERT INTO `mds_userlogin` VALUES ('2213', '3', 'admin', '2017-06-24 16:51:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2214', '3', 'admin', '2017-06-24 17:03:52', '14.220.120.49');
INSERT INTO `mds_userlogin` VALUES ('2215', '3', 'admin', '2017-06-24 17:05:30', '14.220.120.49');
INSERT INTO `mds_userlogin` VALUES ('2216', '3', 'admin', '2017-06-25 13:31:17', '14.220.122.116');
INSERT INTO `mds_userlogin` VALUES ('2217', '3', 'admin', '2017-06-27 11:03:33', '14.220.123.142');
INSERT INTO `mds_userlogin` VALUES ('2218', '3', 'admin', '2017-06-27 11:13:46', '119.103.189.217');
INSERT INTO `mds_userlogin` VALUES ('2219', '3', 'admin', '2017-06-27 11:22:51', '119.103.189.217');
INSERT INTO `mds_userlogin` VALUES ('2220', '3', 'admin', '2017-06-28 01:22:45', '14.220.120.165');
INSERT INTO `mds_userlogin` VALUES ('2221', '3', 'admin', '2017-07-05 00:54:26', '14.220.123.15');
INSERT INTO `mds_userlogin` VALUES ('2222', '3', 'admin', '2017-07-05 00:56:31', '14.220.123.15');
INSERT INTO `mds_userlogin` VALUES ('2223', '3', 'admin', '2017-07-05 00:58:14', '14.220.123.15');
INSERT INTO `mds_userlogin` VALUES ('2224', '3', 'admin', '2017-07-05 08:58:30', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('2225', '3', 'admin', '2017-07-05 11:00:48', '14.220.123.15');
INSERT INTO `mds_userlogin` VALUES ('2226', '3', 'admin', '2017-07-06 16:50:57', '14.220.123.167');
INSERT INTO `mds_userlogin` VALUES ('2227', '3', 'admin', '2017-07-06 19:55:13', '14.220.123.167');
INSERT INTO `mds_userlogin` VALUES ('2228', '3', 'admin', '2017-07-11 22:16:47', '14.220.121.246');
INSERT INTO `mds_userlogin` VALUES ('2229', '3', 'admin', '2017-07-15 21:11:22', '117.136.40.226');
INSERT INTO `mds_userlogin` VALUES ('2230', '3', 'admin', '2017-07-15 21:12:02', '117.136.40.226');
INSERT INTO `mds_userlogin` VALUES ('2231', '3', 'admin', '2017-07-15 21:13:01', '117.136.40.226');
INSERT INTO `mds_userlogin` VALUES ('2232', '3', 'admin', '2017-07-15 21:13:44', '117.136.40.226');
INSERT INTO `mds_userlogin` VALUES ('2233', '3', 'admin', '2017-07-15 22:04:38', '117.136.40.226');
INSERT INTO `mds_userlogin` VALUES ('2234', '3', 'admin', '2017-07-16 16:31:45', '220.112.14.231');
INSERT INTO `mds_userlogin` VALUES ('2235', '3', 'admin', '2017-07-16 16:37:14', '220.112.14.231');
INSERT INTO `mds_userlogin` VALUES ('2236', '3', 'admin', '2017-07-16 16:41:17', '220.112.14.231');
INSERT INTO `mds_userlogin` VALUES ('2237', '3', 'admin', '2017-07-17 11:27:40', '58.49.42.155');
INSERT INTO `mds_userlogin` VALUES ('2238', '3', 'admin', '2017-07-17 11:28:23', '58.49.42.155');
INSERT INTO `mds_userlogin` VALUES ('2239', '3', 'admin', '2017-07-17 11:46:33', '14.152.69.140');
INSERT INTO `mds_userlogin` VALUES ('2240', '3', 'admin', '2017-07-17 12:25:55', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('2241', '3', 'admin', '2017-07-17 12:37:52', '183.12.237.83');
INSERT INTO `mds_userlogin` VALUES ('2242', '3', 'admin', '2017-07-17 12:38:09', '183.12.237.83');
INSERT INTO `mds_userlogin` VALUES ('2243', '3', 'admin', '2017-07-18 20:24:55', '223.104.1.186');
INSERT INTO `mds_userlogin` VALUES ('2244', '3', 'admin', '2017-07-19 18:53:50', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2245', '3', 'admin', '2017-07-23 12:42:10', '113.91.37.135');
INSERT INTO `mds_userlogin` VALUES ('2246', '3', 'admin', '2017-07-24 10:12:58', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2247', '3', 'admin', '2017-07-24 12:34:46', '113.91.36.164');
INSERT INTO `mds_userlogin` VALUES ('2248', '3', 'admin', '2017-07-24 12:36:23', '113.91.36.164');
INSERT INTO `mds_userlogin` VALUES ('2249', '3', 'admin', '2017-07-24 12:37:19', '113.91.36.164');
INSERT INTO `mds_userlogin` VALUES ('2250', '3', 'admin', '2017-07-24 12:40:00', '113.91.36.164');
INSERT INTO `mds_userlogin` VALUES ('2251', '3', 'admin', '2017-07-28 19:48:36', '14.220.120.204');
INSERT INTO `mds_userlogin` VALUES ('2252', '3', 'admin', '2017-08-02 11:30:53', '14.220.123.65');
INSERT INTO `mds_userlogin` VALUES ('2253', '38', 'guest', '2017-08-02 11:34:09', '14.220.123.65');
INSERT INTO `mds_userlogin` VALUES ('2254', '38', 'guest', '2017-08-02 11:45:42', '111.221.201.46');
INSERT INTO `mds_userlogin` VALUES ('2255', '38', 'guest', '2017-08-02 13:25:32', '14.220.123.65');
INSERT INTO `mds_userlogin` VALUES ('2256', '38', 'guest', '2017-08-02 16:57:56', '111.221.201.46');
INSERT INTO `mds_userlogin` VALUES ('2257', '38', 'guest', '2017-08-02 21:59:48', '14.220.123.65');
INSERT INTO `mds_userlogin` VALUES ('2258', '3', 'admin', '2017-08-08 14:13:46', '14.220.122.246');
INSERT INTO `mds_userlogin` VALUES ('2259', '3', 'admin', '2017-08-13 21:26:23', '183.12.238.17');
INSERT INTO `mds_userlogin` VALUES ('2260', '3', 'admin', '2017-08-13 23:44:38', '183.12.238.17');
INSERT INTO `mds_userlogin` VALUES ('2261', '3', 'admin', '2017-08-16 15:02:07', '14.220.123.27');
INSERT INTO `mds_userlogin` VALUES ('2262', '3', 'admin', '2017-08-16 15:25:45', '14.220.123.27');
INSERT INTO `mds_userlogin` VALUES ('2263', '3', 'admin', '2017-08-16 15:48:59', '14.220.123.27');
INSERT INTO `mds_userlogin` VALUES ('2264', '3', 'admin', '2017-08-17 10:01:28', '14.220.123.27');
INSERT INTO `mds_userlogin` VALUES ('2265', '3', 'admin', '2017-08-17 18:14:33', '14.220.123.27');
INSERT INTO `mds_userlogin` VALUES ('2266', '3', 'admin', '2017-08-18 09:56:58', '14.220.122.148');
INSERT INTO `mds_userlogin` VALUES ('2267', '3', 'admin', '2017-08-18 09:59:27', '14.220.122.148');
INSERT INTO `mds_userlogin` VALUES ('2268', '3', 'admin', '2017-08-18 14:40:49', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2269', '3', 'admin', '2017-08-21 22:05:48', '121.15.41.199');
INSERT INTO `mds_userlogin` VALUES ('2270', '3', 'admin', '2017-08-21 22:13:06', '14.17.37.43');
INSERT INTO `mds_userlogin` VALUES ('2271', '3', 'admin', '2017-08-22 09:13:34', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2272', '3', 'admin', '2017-08-22 09:19:02', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2273', '3', 'admin', '2017-08-22 09:20:30', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2274', '3', 'admin', '2017-08-25 11:26:16', '14.220.123.196');
INSERT INTO `mds_userlogin` VALUES ('2275', '3', 'admin', '2017-08-25 13:44:18', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2276', '3', 'admin', '2017-08-25 17:39:06', '14.220.123.196');
INSERT INTO `mds_userlogin` VALUES ('2277', '3', 'admin', '2017-08-25 21:02:37', '14.220.123.196');
INSERT INTO `mds_userlogin` VALUES ('2278', '3', 'admin', '2017-08-25 21:33:57', '14.220.123.196');
INSERT INTO `mds_userlogin` VALUES ('2279', '3', 'admin', '2017-08-25 21:34:16', '14.220.123.196');
INSERT INTO `mds_userlogin` VALUES ('2280', '3', 'admin', '2017-08-25 22:02:06', '14.220.123.196');
INSERT INTO `mds_userlogin` VALUES ('2281', '3', 'admin', '2017-08-25 22:05:35', '14.220.123.196');
INSERT INTO `mds_userlogin` VALUES ('2282', '3', 'admin', '2017-08-27 17:03:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2283', '3', 'admin', '2017-08-27 17:30:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2284', '3', 'admin', '2017-08-27 18:35:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2285', '3', 'admin', '2017-08-27 20:49:48', '14.220.121.226');
INSERT INTO `mds_userlogin` VALUES ('2286', '3', 'admin', '2017-08-28 09:44:00', '14.220.123.17');
INSERT INTO `mds_userlogin` VALUES ('2287', '3', 'admin', '2017-08-29 09:06:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2288', '3', 'admin', '2017-08-29 15:55:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2289', '3', 'admin', '2017-08-29 19:52:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2290', '3', 'admin', '2017-08-29 19:56:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2291', '3', 'admin', '2017-08-30 09:14:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2292', '3', 'admin', '2017-08-30 11:33:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2293', '3', 'admin', '2017-08-30 19:44:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2294', '3', 'admin', '2017-08-31 09:51:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2295', '3', 'admin', '2017-08-31 09:54:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2296', '3', 'admin', '2017-08-31 09:54:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2297', '3', 'admin', '2017-08-31 11:02:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2298', '3', 'admin', '2017-08-31 11:19:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2299', '3', 'admin', '2017-08-31 14:01:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2300', '3', 'admin', '2017-08-31 14:08:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2301', '3', 'admin', '2017-08-31 16:40:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2302', '3', 'admin', '2017-08-31 17:09:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2303', '3', 'admin', '2017-08-31 18:26:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2304', '3', 'admin', '2017-08-31 18:30:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2305', '3', 'admin', '2017-08-31 20:29:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2306', '3', 'admin', '2017-08-31 21:12:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2307', '3', 'admin', '2017-08-31 21:12:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2308', '3', 'admin', '2017-09-01 08:40:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2309', '3', 'admin', '2017-09-01 09:12:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2310', '3', 'admin', '2017-09-01 09:44:33', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2311', '3', 'admin', '2017-09-01 09:46:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2312', '3', 'admin', '2017-09-01 09:49:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2313', '3', 'admin', '2017-09-01 09:50:06', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2314', '3', 'admin', '2017-09-01 09:56:54', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2315', '3', 'admin', '2017-09-01 09:59:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2316', '3', 'admin', '2017-09-01 10:05:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2317', '3', 'admin', '2017-09-01 10:08:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2318', '3', 'admin', '2017-09-01 10:13:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2319', '3', 'admin', '2017-09-01 10:18:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2320', '3', 'admin', '2017-09-01 10:21:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2321', '3', 'admin', '2017-09-01 10:25:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2322', '3', 'admin', '2017-09-01 10:26:47', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2323', '3', 'admin', '2017-09-01 10:30:39', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2324', '3', 'admin', '2017-09-01 10:32:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2325', '3', 'admin', '2017-09-01 10:33:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2326', '3', 'admin', '2017-09-01 10:38:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2327', '3', 'admin', '2017-09-01 10:52:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2328', '3', 'admin', '2017-09-01 10:52:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2329', '3', 'admin', '2017-09-01 11:21:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2330', '3', 'admin', '2017-09-01 11:31:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2331', '3', 'admin', '2017-09-01 11:35:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2332', '3', 'admin', '2017-09-01 11:38:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2333', '3', 'admin', '2017-09-01 11:43:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2334', '3', 'admin', '2017-09-01 11:43:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2335', '3', 'admin', '2017-09-01 12:43:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2336', '3', 'admin', '2017-09-01 13:06:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2337', '3', 'admin', '2017-09-01 14:15:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2338', '3', 'admin', '2017-09-01 14:19:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2339', '3', 'admin', '2017-09-01 14:29:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2340', '3', 'admin', '2017-09-01 14:31:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2341', '3', 'admin', '2017-09-01 14:33:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2342', '3', 'admin', '2017-09-01 14:36:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2343', '3', 'admin', '2017-09-01 14:40:58', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2344', '3', 'admin', '2017-09-02 10:07:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2345', '3', 'admin', '2017-09-02 10:19:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2346', '3', 'admin', '2017-09-02 10:32:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2347', '3', 'admin', '2017-09-02 10:32:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2348', '3', 'admin', '2017-09-02 10:36:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2349', '3', 'admin', '2017-09-02 10:43:29', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2350', '3', 'admin', '2017-09-02 11:26:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2351', '3', 'admin', '2017-09-02 15:24:24', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2352', '3', 'admin', '2017-09-02 16:01:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2353', '3', 'admin', '2017-09-02 16:03:00', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2354', '3', 'admin', '2017-09-02 17:35:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2355', '3', 'admin', '2017-09-02 17:37:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2356', '3', 'admin', '2017-09-02 17:39:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2357', '3', 'admin', '2017-09-02 17:40:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2358', '3', 'admin', '2017-09-02 17:41:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2359', '3', 'admin', '2017-09-02 17:42:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2360', '3', 'admin', '2017-09-02 17:46:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2361', '3', 'admin', '2017-09-02 17:57:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2362', '3', 'admin', '2017-09-02 18:00:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2363', '3', 'admin', '2017-09-02 18:08:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2364', '3', 'admin', '2017-09-02 18:36:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2365', '3', 'admin', '2017-09-02 18:44:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2366', '3', 'admin', '2017-09-02 18:51:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2367', '3', 'admin', '2017-09-02 18:56:45', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2368', '3', 'admin', '2017-09-02 19:06:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2369', '3', 'admin', '2017-09-02 19:08:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2370', '3', 'admin', '2017-09-02 21:07:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2371', '3', 'admin', '2017-09-02 21:25:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2372', '3', 'admin', '2017-09-02 21:26:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2373', '3', 'admin', '2017-09-02 21:41:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2374', '3', 'admin', '2017-09-02 22:07:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2375', '3', 'admin', '2017-09-02 22:22:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2376', '3', 'admin', '2017-09-02 22:36:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2377', '3', 'admin', '2017-09-02 22:38:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2378', '3', 'admin', '2017-09-02 22:40:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2379', '3', 'admin', '2017-09-02 22:43:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2380', '3', 'admin', '2017-09-02 22:46:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2381', '3', 'admin', '2017-09-02 22:47:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2382', '3', 'admin', '2017-09-02 22:49:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2383', '3', 'admin', '2017-09-02 22:49:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2384', '3', 'admin', '2017-09-02 22:49:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2385', '3', 'admin', '2017-09-02 22:54:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2386', '3', 'admin', '2017-09-02 23:00:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2387', '3', 'admin', '2017-09-02 23:01:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2388', '3', 'admin', '2017-09-02 23:03:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2389', '3', 'admin', '2017-09-02 23:04:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2390', '3', 'admin', '2017-09-02 23:06:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2391', '3', 'admin', '2017-09-02 23:17:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2392', '3', 'admin', '2017-09-02 23:21:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2393', '3', 'admin', '2017-09-02 23:22:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2394', '3', 'admin', '2017-09-02 23:22:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2395', '3', 'admin', '2017-09-02 23:22:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2396', '3', 'admin', '2017-09-02 23:32:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2397', '3', 'admin', '2017-09-02 23:48:56', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2398', '3', 'admin', '2017-09-03 16:41:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2399', '3', 'admin', '2017-09-03 16:52:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2400', '3', 'admin', '2017-09-04 09:12:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2401', '3', 'admin', '2017-09-04 09:13:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2402', '3', 'admin', '2017-09-04 09:15:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2403', '3', 'admin', '2017-09-04 09:27:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2404', '3', 'admin', '2017-09-04 09:29:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2405', '3', 'admin', '2017-09-04 09:31:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2406', '3', 'admin', '2017-09-04 09:36:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2407', '3', 'admin', '2017-09-04 09:36:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2408', '3', 'admin', '2017-09-04 10:09:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2409', '3', 'admin', '2017-09-04 11:03:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2410', '3', 'admin', '2017-09-04 11:26:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2411', '3', 'admin', '2017-09-04 11:36:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2412', '3', 'admin', '2017-09-04 11:46:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2413', '3', 'admin', '2017-09-04 11:50:34', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2414', '3', 'admin', '2017-09-04 11:50:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2415', '3', 'admin', '2017-09-04 12:00:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2416', '3', 'admin', '2017-09-04 12:00:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2417', '3', 'admin', '2017-09-04 12:01:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2418', '3', 'admin', '2017-09-04 13:58:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2419', '3', 'admin', '2017-09-04 14:00:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2420', '3', 'admin', '2017-09-04 14:05:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2421', '3', 'admin', '2017-09-04 14:10:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2422', '3', 'admin', '2017-09-04 14:15:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2423', '3', 'admin', '2017-09-04 14:19:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2424', '3', 'admin', '2017-09-04 14:26:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2425', '3', 'admin', '2017-09-04 14:37:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2426', '3', 'admin', '2017-09-04 14:48:36', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2427', '3', 'admin', '2017-09-04 14:49:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2428', '3', 'admin', '2017-09-04 15:14:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2429', '3', 'admin', '2017-09-04 15:19:57', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2430', '3', 'admin', '2017-09-04 15:25:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2431', '3', 'admin', '2017-09-04 15:31:59', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2432', '3', 'admin', '2017-09-04 15:37:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2433', '3', 'admin', '2017-09-04 15:40:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2434', '3', 'admin', '2017-09-04 15:41:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2435', '3', 'admin', '2017-09-04 15:50:32', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2436', '3', 'admin', '2017-09-04 15:54:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2437', '3', 'admin', '2017-09-04 16:14:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2438', '3', 'admin', '2017-09-04 16:16:38', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2439', '3', 'admin', '2017-09-04 16:19:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2440', '3', 'admin', '2017-09-04 16:20:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2441', '3', 'admin', '2017-09-04 16:26:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2442', '3', 'admin', '2017-09-04 16:32:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2443', '3', 'admin', '2017-09-04 16:35:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2444', '3', 'admin', '2017-09-04 16:46:31', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2445', '3', 'admin', '2017-09-04 17:10:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2446', '3', 'admin', '2017-09-04 17:15:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2447', '3', 'admin', '2017-09-04 17:18:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2448', '3', 'admin', '2017-09-04 17:20:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2449', '3', 'admin', '2017-09-04 17:22:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2450', '3', 'admin', '2017-09-04 17:23:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2451', '3', 'admin', '2017-09-04 17:30:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2452', '3', 'admin', '2017-09-04 17:42:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2453', '3', 'admin', '2017-09-04 17:47:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2454', '3', 'admin', '2017-09-04 17:49:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2455', '3', 'admin', '2017-09-04 17:52:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2456', '3', 'admin', '2017-09-04 18:02:43', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2457', '3', 'admin', '2017-09-04 18:06:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2458', '3', 'admin', '2017-09-04 18:13:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2459', '3', 'admin', '2017-09-04 18:16:02', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2460', '3', 'admin', '2017-09-04 18:18:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2461', '3', 'admin', '2017-09-04 18:20:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2462', '3', 'admin', '2017-09-04 18:22:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2463', '3', 'admin', '2017-09-04 18:31:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2464', '3', 'admin', '2017-09-04 18:33:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2465', '3', 'admin', '2017-09-04 18:36:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2466', '3', 'admin', '2017-09-04 18:37:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2467', '3', 'admin', '2017-09-04 19:45:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2468', '3', 'admin', '2017-09-04 19:49:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2469', '3', 'admin', '2017-09-04 20:01:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2470', '3', 'admin', '2017-09-04 20:05:05', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2471', '3', 'admin', '2017-09-04 20:08:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2472', '3', 'admin', '2017-09-04 20:10:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2473', '3', 'admin', '2017-09-04 20:14:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2474', '3', 'admin', '2017-09-04 20:19:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2475', '3', 'admin', '2017-09-04 20:21:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2476', '3', 'admin', '2017-09-04 20:25:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2477', '3', 'admin', '2017-09-04 20:32:18', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2478', '3', 'admin', '2017-09-04 20:33:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2479', '3', 'admin', '2017-09-04 20:41:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2480', '3', 'admin', '2017-09-04 20:45:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2481', '3', 'admin', '2017-09-04 20:58:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2482', '3', 'admin', '2017-09-04 21:04:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2483', '3', 'admin', '2017-09-04 21:07:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2484', '3', 'admin', '2017-09-04 21:07:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2485', '3', 'admin', '2017-09-04 21:11:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2486', '3', 'admin', '2017-09-04 21:15:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2487', '3', 'admin', '2017-09-04 21:17:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2488', '3', 'admin', '2017-09-04 21:19:18', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2489', '3', 'admin', '2017-09-04 21:21:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2490', '3', 'admin', '2017-09-04 21:22:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2491', '3', 'admin', '2017-09-04 21:23:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2492', '3', 'admin', '2017-09-04 21:24:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2493', '3', 'admin', '2017-09-04 21:27:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2494', '3', 'admin', '2017-09-04 21:28:20', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2495', '3', 'admin', '2017-09-04 21:30:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2496', '3', 'admin', '2017-09-04 21:30:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2497', '3', 'admin', '2017-09-04 21:36:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2498', '3', 'admin', '2017-09-04 21:36:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2499', '3', 'admin', '2017-09-04 21:45:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2500', '3', 'admin', '2017-09-04 21:53:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2501', '3', 'admin', '2017-09-04 22:53:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2502', '3', 'admin', '2017-09-04 22:56:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2503', '3', 'admin', '2017-09-04 22:56:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2504', '3', 'admin', '2017-09-04 22:56:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2505', '3', 'admin', '2017-09-04 22:56:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2506', '3', 'admin', '2017-09-04 23:02:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2507', '3', 'admin', '2017-09-04 23:05:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2508', '3', 'admin', '2017-09-04 23:22:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2509', '3', 'admin', '2017-09-04 23:36:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2510', '3', 'admin', '2017-09-04 23:39:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2511', '3', 'admin', '2017-09-04 23:42:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2512', '3', 'admin', '2017-09-04 23:44:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2513', '3', 'admin', '2017-09-04 23:55:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2514', '3', 'admin', '2017-09-05 00:11:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2515', '3', 'admin', '2017-09-05 00:12:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2516', '3', 'admin', '2017-09-05 00:15:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2517', '3', 'admin', '2017-09-05 00:39:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2518', '3', 'admin', '2017-09-05 00:43:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2519', '3', 'admin', '2017-09-05 00:44:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2520', '3', 'admin', '2017-09-05 00:45:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2521', '3', 'admin', '2017-09-05 00:48:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2522', '3', 'admin', '2017-09-05 00:50:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2523', '3', 'admin', '2017-09-05 00:51:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2524', '3', 'admin', '2017-09-05 00:59:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2525', '3', 'admin', '2017-09-05 01:07:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2526', '3', 'admin', '2017-09-05 01:09:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2527', '3', 'admin', '2017-09-05 09:09:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2528', '3', 'admin', '2017-09-05 09:16:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2529', '3', 'admin', '2017-09-05 10:26:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2530', '3', 'admin', '2017-09-05 10:28:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2531', '3', 'admin', '2017-09-05 10:33:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2532', '3', 'admin', '2017-09-05 10:36:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2533', '3', 'admin', '2017-09-05 10:37:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2534', '3', 'admin', '2017-09-05 11:09:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2535', '3', 'admin', '2017-09-05 11:11:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2536', '3', 'admin', '2017-09-05 11:16:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2537', '3', 'admin', '2017-09-05 11:22:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2538', '3', 'admin', '2017-09-05 11:23:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2539', '3', 'admin', '2017-09-05 11:26:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2540', '3', 'admin', '2017-09-05 11:38:09', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2541', '3', 'admin', '2017-09-05 11:40:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2542', '3', 'admin', '2017-09-05 11:44:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2543', '3', 'admin', '2017-09-05 11:48:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2544', '3', 'admin', '2017-09-05 11:54:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2545', '3', 'admin', '2017-09-05 11:55:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2546', '3', 'admin', '2017-09-05 12:00:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2547', '3', 'admin', '2017-09-05 12:02:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2548', '3', 'admin', '2017-09-05 12:05:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2549', '3', 'admin', '2017-09-05 12:07:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2550', '3', 'admin', '2017-09-05 12:11:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2551', '3', 'admin', '2017-09-05 12:14:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2552', '3', 'admin', '2017-09-05 13:58:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2553', '3', 'admin', '2017-09-05 14:08:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2554', '3', 'admin', '2017-09-05 14:10:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2555', '3', 'admin', '2017-09-05 14:23:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2556', '3', 'admin', '2017-09-05 14:30:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2557', '3', 'admin', '2017-09-05 14:36:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2558', '3', 'admin', '2017-09-05 14:38:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2559', '3', 'admin', '2017-09-05 14:40:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2560', '3', 'admin', '2017-09-05 14:41:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2561', '3', 'admin', '2017-09-05 14:44:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2562', '3', 'admin', '2017-09-05 14:46:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2563', '3', 'admin', '2017-09-05 14:47:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2564', '3', 'admin', '2017-09-05 14:49:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2565', '3', 'admin', '2017-09-05 14:51:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2566', '3', 'admin', '2017-09-05 14:53:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2567', '3', 'admin', '2017-09-05 14:57:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2568', '3', 'admin', '2017-09-05 15:02:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2569', '3', 'admin', '2017-09-05 15:07:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2570', '3', 'admin', '2017-09-05 15:09:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2571', '3', 'admin', '2017-09-05 15:13:19', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2572', '3', 'admin', '2017-09-05 15:13:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2573', '3', 'admin', '2017-09-05 15:17:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2574', '3', 'admin', '2017-09-05 15:21:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2575', '3', 'admin', '2017-09-05 15:24:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2576', '3', 'admin', '2017-09-05 15:26:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2577', '3', 'admin', '2017-09-05 15:34:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2578', '3', 'admin', '2017-09-05 15:42:44', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2579', '3', 'admin', '2017-09-05 15:43:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2580', '3', 'admin', '2017-09-05 15:45:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2581', '3', 'admin', '2017-09-05 15:45:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2582', '3', 'admin', '2017-09-05 15:50:52', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2583', '3', 'admin', '2017-09-05 15:53:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2584', '3', 'admin', '2017-09-05 15:55:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2585', '3', 'admin', '2017-09-05 15:57:21', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2586', '3', 'admin', '2017-09-05 15:58:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2587', '3', 'admin', '2017-09-05 16:00:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2588', '3', 'admin', '2017-09-05 16:02:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2589', '3', 'admin', '2017-09-05 16:02:07', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2590', '3', 'admin', '2017-09-05 16:04:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2591', '3', 'admin', '2017-09-05 16:05:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2592', '3', 'admin', '2017-09-05 16:08:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2593', '3', 'admin', '2017-09-05 16:11:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2594', '3', 'admin', '2017-09-05 16:35:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2595', '3', 'admin', '2017-09-05 16:39:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2596', '3', 'admin', '2017-09-05 16:42:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2597', '3', 'admin', '2017-09-05 16:49:06', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2598', '3', 'admin', '2017-09-05 16:55:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2599', '3', 'admin', '2017-09-05 16:56:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2600', '3', 'admin', '2017-09-05 16:58:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2601', '3', 'admin', '2017-09-05 17:00:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2602', '3', 'admin', '2017-09-05 17:02:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2603', '3', 'admin', '2017-09-05 17:06:31', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2604', '3', 'admin', '2017-09-05 17:08:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2605', '3', 'admin', '2017-09-05 17:08:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2606', '3', 'admin', '2017-09-05 17:11:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2607', '3', 'admin', '2017-09-05 17:11:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2608', '3', 'admin', '2017-09-05 17:13:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2609', '3', 'admin', '2017-09-05 17:17:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2610', '3', 'admin', '2017-09-05 17:34:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2611', '3', 'admin', '2017-09-05 17:36:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2612', '3', 'admin', '2017-09-05 17:56:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2613', '3', 'admin', '2017-09-05 19:20:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2614', '3', 'admin', '2017-09-05 19:22:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2615', '3', 'admin', '2017-09-05 19:30:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2616', '3', 'admin', '2017-09-05 19:32:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2617', '3', 'admin', '2017-09-05 19:37:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2618', '3', 'admin', '2017-09-05 19:44:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2619', '3', 'admin', '2017-09-05 19:48:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2620', '3', 'admin', '2017-09-05 20:00:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2621', '3', 'admin', '2017-09-05 20:01:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2622', '3', 'admin', '2017-09-05 20:01:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2623', '3', 'admin', '2017-09-05 20:04:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2624', '3', 'admin', '2017-09-05 20:09:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2625', '3', 'admin', '2017-09-05 20:11:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2626', '3', 'admin', '2017-09-05 20:12:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2627', '3', 'admin', '2017-09-05 20:14:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2628', '3', 'admin', '2017-09-05 20:15:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2629', '3', 'admin', '2017-09-05 20:18:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2630', '3', 'admin', '2017-09-05 20:25:04', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2631', '3', 'admin', '2017-09-05 20:28:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2632', '3', 'admin', '2017-09-05 20:31:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2633', '3', 'admin', '2017-09-05 20:32:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2634', '3', 'admin', '2017-09-05 20:52:36', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2635', '3', 'admin', '2017-09-05 20:53:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2636', '3', 'admin', '2017-09-05 20:58:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2637', '3', 'admin', '2017-09-05 21:07:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2638', '3', 'admin', '2017-09-05 21:32:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2639', '3', 'admin', '2017-09-05 21:42:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2640', '3', 'admin', '2017-09-05 22:34:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2641', '3', 'admin', '2017-09-05 22:50:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2642', '3', 'admin', '2017-09-06 08:57:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2643', '3', 'admin', '2017-09-06 09:25:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2644', '3', 'admin', '2017-09-06 09:29:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2645', '3', 'admin', '2017-09-06 09:37:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2646', '3', 'admin', '2017-09-06 09:37:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2647', '3', 'admin', '2017-09-06 09:39:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2648', '3', 'admin', '2017-09-06 09:40:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2649', '3', 'admin', '2017-09-06 09:52:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2650', '3', 'admin', '2017-09-06 10:37:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2651', '3', 'admin', '2017-09-06 11:37:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2652', '3', 'admin', '2017-09-06 11:44:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2653', '3', 'admin', '2017-09-06 15:39:33', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2654', '3', 'admin', '2017-09-06 15:44:27', '14.220.122.34');
INSERT INTO `mds_userlogin` VALUES ('2655', '3', 'admin', '2017-09-06 15:46:53', '14.220.122.34');
INSERT INTO `mds_userlogin` VALUES ('2656', '3', 'admin', '2017-09-06 15:47:14', '14.220.122.34');
INSERT INTO `mds_userlogin` VALUES ('2657', '3', 'admin', '2017-09-06 15:50:13', '14.220.122.34');
INSERT INTO `mds_userlogin` VALUES ('2658', '3', 'admin', '2017-09-06 15:51:26', '14.220.122.34');
INSERT INTO `mds_userlogin` VALUES ('2659', '3', 'admin', '2017-09-06 15:53:28', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('2660', '3', 'admin', '2017-09-06 16:05:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2661', '3', 'admin', '2017-09-06 16:05:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2662', '3', 'admin', '2017-09-06 16:07:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2663', '3', 'admin', '2017-09-06 16:07:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2664', '3', 'admin', '2017-09-06 16:11:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2665', '3', 'admin', '2017-09-06 16:11:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2666', '3', 'admin', '2017-09-06 16:15:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2667', '3', 'admin', '2017-09-06 16:15:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2668', '3', 'admin', '2017-09-06 16:16:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2669', '3', 'admin', '2017-09-06 16:28:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2670', '3', 'admin', '2017-09-06 16:28:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2671', '3', 'admin', '2017-09-06 16:29:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2672', '3', 'admin', '2017-09-06 16:47:10', '14.220.122.34');
INSERT INTO `mds_userlogin` VALUES ('2673', '3', 'admin', '2017-09-06 19:36:32', '14.220.122.34');
INSERT INTO `mds_userlogin` VALUES ('2674', '3', 'admin', '2017-09-06 19:55:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2675', '3', 'admin', '2017-09-06 19:59:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2676', '3', 'admin', '2017-09-06 20:37:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2677', '3', 'admin', '2017-09-06 20:38:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2678', '3', 'admin', '2017-09-07 09:02:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2679', '3', 'admin', '2017-09-07 09:21:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2680', '3', 'admin', '2017-09-07 09:22:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2681', '3', 'admin', '2017-09-07 09:33:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2682', '3', 'admin', '2017-09-07 09:38:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2683', '3', 'admin', '2017-09-07 10:00:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2684', '3', 'admin', '2017-09-07 10:36:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2685', '3', 'admin', '2017-09-07 11:11:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2686', '3', 'admin', '2017-09-07 11:12:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2687', '3', 'admin', '2017-09-07 11:15:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2688', '3', 'admin', '2017-09-07 11:30:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2689', '3', 'admin', '2017-09-07 11:33:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2690', '3', 'admin', '2017-09-07 16:02:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2691', '3', 'admin', '2017-09-07 16:21:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2692', '3', 'admin', '2017-09-07 17:57:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2693', '3', 'admin', '2017-09-07 17:57:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2694', '3', 'admin', '2017-09-07 18:59:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2695', '3', 'admin', '2017-09-07 19:22:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2696', '3', 'admin', '2017-09-07 19:39:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2697', '3', 'admin', '2017-09-07 19:49:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2698', '3', 'admin', '2017-09-07 19:58:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2699', '3', 'admin', '2017-09-07 20:10:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2700', '3', 'admin', '2017-09-07 20:55:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2701', '3', 'admin', '2017-09-07 21:22:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2702', '3', 'admin', '2017-09-08 08:33:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2703', '3', 'admin', '2017-09-08 08:38:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2704', '3', 'admin', '2017-09-08 11:28:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2705', '3', 'admin', '2017-09-08 12:02:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2706', '3', 'admin', '2017-09-08 13:43:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2707', '3', 'admin', '2017-09-08 14:36:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2708', '3', 'admin', '2017-09-08 14:39:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2709', '3', 'admin', '2017-09-08 15:26:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2710', '3', 'admin', '2017-09-08 15:29:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2711', '3', 'admin', '2017-09-08 16:17:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2712', '3', 'admin', '2017-09-08 16:20:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2713', '3', 'admin', '2017-09-08 17:34:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2714', '3', 'admin', '2017-09-08 18:15:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2715', '3', 'admin', '2017-09-08 18:20:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2716', '3', 'admin', '2017-09-08 18:23:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2717', '3', 'admin', '2017-09-08 18:27:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2718', '3', 'admin', '2017-09-08 18:28:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2719', '3', 'admin', '2017-09-08 18:34:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2720', '3', 'admin', '2017-09-08 19:15:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2721', '3', 'admin', '2017-09-08 19:16:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2722', '3', 'admin', '2017-09-08 19:19:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2723', '3', 'admin', '2017-09-08 20:26:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2724', '3', 'admin', '2017-09-09 09:24:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2725', '3', 'admin', '2017-09-09 10:56:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2726', '3', 'admin', '2017-09-09 11:04:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2727', '3', 'admin', '2017-09-09 11:07:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2728', '3', 'admin', '2017-09-09 12:16:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2729', '3', 'admin', '2017-09-09 13:46:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2730', '3', 'admin', '2017-09-09 15:34:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2731', '3', 'admin', '2017-09-09 15:59:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2732', '3', 'admin', '2017-09-09 16:00:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2733', '3', 'admin', '2017-09-09 16:02:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2734', '3', 'admin', '2017-09-09 19:07:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2735', '3', 'admin', '2017-09-09 23:37:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2736', '3', 'admin', '2017-09-10 18:41:31', '183.12.241.83');
INSERT INTO `mds_userlogin` VALUES ('2737', '3', 'admin', '2017-09-10 18:41:58', '183.12.241.83');
INSERT INTO `mds_userlogin` VALUES ('2738', '3', 'admin', '2017-09-11 09:18:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2739', '3', 'admin', '2017-09-11 11:09:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2740', '3', 'admin', '2017-09-11 11:26:56', '14.220.120.160');
INSERT INTO `mds_userlogin` VALUES ('2741', '3', 'admin', '2017-09-11 14:05:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2742', '3', 'admin', '2017-09-11 18:19:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2743', '3', 'admin', '2017-09-11 18:36:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2744', '3', 'admin', '2017-09-11 19:53:11', '192.168.1.100');
INSERT INTO `mds_userlogin` VALUES ('2745', '3', 'admin', '2017-09-11 19:54:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2746', '3', 'admin', '2017-09-11 21:04:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2747', '3', 'admin', '2017-09-11 21:24:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2748', '3', 'admin', '2017-09-11 21:36:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2749', '3', 'admin', '2017-09-11 23:21:38', '14.220.120.160');
INSERT INTO `mds_userlogin` VALUES ('2750', '3', 'admin', '2017-09-11 23:28:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2751', '3', 'admin', '2017-09-11 23:30:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2752', '3', 'admin', '2017-09-11 23:31:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2753', '3', 'admin', '2017-09-12 09:20:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2754', '3', 'admin', '2017-09-12 10:19:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2755', '3', 'admin', '2017-09-12 12:03:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2756', '3', 'admin', '2017-09-12 14:06:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2757', '3', 'admin', '2017-09-12 14:45:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2758', '3', 'admin', '2017-09-12 15:24:10', '192.168.1.105');
INSERT INTO `mds_userlogin` VALUES ('2759', '3', 'admin', '2017-09-12 16:50:48', '192.168.1.105');
INSERT INTO `mds_userlogin` VALUES ('2760', '3', 'admin', '2017-09-12 17:20:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2761', '3', 'admin', '2017-09-12 17:22:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2762', '3', 'admin', '2017-09-12 19:22:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2763', '3', 'admin', '2017-09-12 19:41:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2764', '3', 'admin', '2017-09-12 19:43:33', '192.168.1.105');
INSERT INTO `mds_userlogin` VALUES ('2765', '3', 'admin', '2017-09-12 19:44:20', '192.168.1.105');
INSERT INTO `mds_userlogin` VALUES ('2766', '3', 'admin', '2017-09-12 19:44:27', '192.168.1.105');
INSERT INTO `mds_userlogin` VALUES ('2767', '3', 'admin', '2017-09-12 19:44:44', '192.168.1.105');
INSERT INTO `mds_userlogin` VALUES ('2768', '3', 'admin', '2017-09-12 20:07:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2769', '3', 'admin', '2017-09-12 20:18:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2770', '3', 'admin', '2017-09-12 20:30:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2771', '3', 'admin', '2017-09-12 21:31:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2772', '3', 'admin', '2017-09-13 09:27:16', '14.220.120.6');
INSERT INTO `mds_userlogin` VALUES ('2773', '3', 'admin', '2017-09-13 09:27:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2774', '3', 'admin', '2017-09-13 10:07:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2775', '3', 'admin', '2017-09-13 10:59:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2776', '3', 'admin', '2017-09-13 14:02:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2777', '3', 'admin', '2017-09-13 15:13:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2778', '3', 'admin', '2017-09-13 16:38:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2779', '3', 'admin', '2017-09-13 18:00:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2780', '3', 'admin', '2017-09-13 18:04:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2781', '3', 'admin', '2017-09-13 19:05:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2782', '3', 'admin', '2017-09-13 19:47:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2783', '3', 'admin', '2017-09-13 20:14:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2784', '3', 'admin', '2017-09-13 20:17:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2785', '3', 'admin', '2017-09-13 22:42:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2786', '3', 'admin', '2017-09-13 22:51:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2787', '3', 'admin', '2017-09-13 23:08:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2788', '3', 'admin', '2017-09-13 23:13:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2789', '3', 'admin', '2017-09-13 23:36:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2790', '3', 'admin', '2017-09-13 23:39:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2791', '3', 'admin', '2017-09-13 23:52:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2792', '3', 'admin', '2017-09-14 09:44:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2793', '3', 'admin', '2017-09-14 10:46:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2794', '3', 'admin', '2017-09-14 11:21:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2795', '3', 'admin', '2017-09-14 12:00:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2796', '3', 'admin', '2017-09-14 14:02:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2797', '3', 'admin', '2017-09-14 15:40:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2798', '3', 'admin', '2017-09-14 17:09:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2799', '3', 'admin', '2017-09-14 19:11:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2800', '3', 'admin', '2017-09-14 19:15:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2801', '3', 'admin', '2017-09-14 19:20:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2802', '3', 'admin', '2017-09-14 19:32:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2803', '3', 'admin', '2017-09-14 19:39:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2804', '3', 'admin', '2017-09-14 21:56:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2805', '3', 'admin', '2017-09-15 09:28:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2806', '3', 'admin', '2017-09-15 10:00:46', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2807', '3', 'admin', '2017-09-15 10:11:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2808', '3', 'admin', '2017-09-15 10:26:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2809', '3', 'admin', '2017-09-15 10:57:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2810', '3', 'admin', '2017-09-15 11:45:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2811', '3', 'admin', '2017-09-15 14:17:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2812', '3', 'admin', '2017-09-15 15:29:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2813', '3', 'admin', '2017-09-15 16:09:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2814', '3', 'admin', '2017-09-15 17:09:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2815', '3', 'admin', '2017-09-15 17:39:20', '192.168.1.101');
INSERT INTO `mds_userlogin` VALUES ('2816', '3', 'admin', '2017-09-15 18:03:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2817', '3', 'admin', '2017-09-15 19:31:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2818', '3', 'admin', '2017-09-15 19:34:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2819', '3', 'admin', '2017-09-15 19:50:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2820', '3', 'admin', '2017-09-15 21:16:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2821', '3', 'admin', '2017-09-16 09:37:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2822', '3', 'admin', '2017-09-16 09:58:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2823', '3', 'admin', '2017-09-16 10:14:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2824', '3', 'admin', '2017-09-16 10:22:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2825', '3', 'admin', '2017-09-16 10:35:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2826', '3', 'admin', '2017-09-16 10:44:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2827', '3', 'admin', '2017-09-16 11:11:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2828', '3', 'admin', '2017-09-16 11:38:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2829', '3', 'admin', '2017-09-16 12:15:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2830', '3', 'admin', '2017-09-16 14:04:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2831', '3', 'admin', '2017-09-16 14:05:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2832', '3', 'admin', '2017-09-16 15:19:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2833', '3', 'admin', '2017-09-16 15:23:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2834', '3', 'admin', '2017-09-16 15:24:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2835', '3', 'admin', '2017-09-16 15:26:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2836', '3', 'admin', '2017-09-16 15:31:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2837', '3', 'admin', '2017-09-16 15:38:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2838', '3', 'admin', '2017-09-16 16:15:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2839', '3', 'admin', '2017-09-16 16:16:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2840', '3', 'admin', '2017-09-16 16:28:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2841', '3', 'admin', '2017-09-16 16:30:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2842', '3', 'admin', '2017-09-16 16:35:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2843', '3', 'admin', '2017-09-16 16:36:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2844', '3', 'admin', '2017-09-16 16:41:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2845', '3', 'admin', '2017-09-16 16:47:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2846', '3', 'admin', '2017-09-16 16:51:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2847', '3', 'admin', '2017-09-16 16:52:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2848', '3', 'admin', '2017-09-16 16:59:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2849', '3', 'admin', '2017-09-16 17:02:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2850', '3', 'admin', '2017-09-16 17:10:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2851', '3', 'admin', '2017-09-16 17:13:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2852', '3', 'admin', '2017-09-16 17:46:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2853', '3', 'admin', '2017-09-16 17:56:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2854', '3', 'admin', '2017-09-16 17:58:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2855', '3', 'admin', '2017-09-16 17:58:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2856', '3', 'admin', '2017-09-16 18:01:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2857', '3', 'admin', '2017-09-16 18:01:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2858', '3', 'admin', '2017-09-16 18:03:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2859', '3', 'admin', '2017-09-16 18:03:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2860', '3', 'admin', '2017-09-16 18:04:26', '192.168.1.100');
INSERT INTO `mds_userlogin` VALUES ('2861', '3', 'admin', '2017-09-16 18:05:42', '192.168.1.100');
INSERT INTO `mds_userlogin` VALUES ('2862', '3', 'admin', '2017-09-16 18:07:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2863', '3', 'admin', '2017-09-16 18:17:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2864', '3', 'admin', '2017-09-17 12:43:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2865', '3', 'admin', '2017-09-18 09:08:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2866', '3', 'admin', '2017-09-18 09:12:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2867', '3', 'admin', '2017-09-18 09:59:02', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2868', '3', 'admin', '2017-09-18 10:28:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2869', '3', 'admin', '2017-09-18 11:09:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2870', '3', 'admin', '2017-09-18 14:17:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2871', '3', 'admin', '2017-09-18 14:18:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2872', '3', 'admin', '2017-09-18 14:30:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2873', '3', 'admin', '2017-09-18 14:38:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2874', '3', 'admin', '2017-09-18 14:42:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2875', '3', 'admin', '2017-09-18 14:51:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2876', '3', 'admin', '2017-09-18 14:57:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2877', '3', 'admin', '2017-09-18 15:32:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2878', '3', 'admin', '2017-09-18 15:37:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2879', '3', 'admin', '2017-09-18 15:51:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2880', '3', 'admin', '2017-09-18 16:15:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2881', '3', 'admin', '2017-09-18 16:16:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2882', '3', 'admin', '2017-09-18 16:51:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2883', '3', 'admin', '2017-09-18 17:15:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2884', '1', 'admin', '2017-09-18 17:26:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2885', '1', 'admin', '2017-09-18 17:27:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2886', '1', 'admin', '2017-09-18 17:52:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2887', '1', 'admin', '2017-09-18 20:21:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2888', '1', 'admin', '2017-09-18 20:35:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2889', '1', 'admin', '2017-09-18 20:44:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2890', '1', 'admin', '2017-09-18 20:50:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2891', '1', 'admin', '2017-09-18 21:05:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2892', '1', 'admin', '2017-09-18 21:17:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2893', '1', 'admin', '2017-09-18 21:24:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2894', '1', 'admin', '2017-09-18 21:34:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2895', '1', 'admin', '2017-09-18 21:41:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2896', '1', 'admin', '2017-09-18 21:45:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2897', '1', 'admin', '2017-09-18 21:49:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2898', '1', 'admin', '2017-09-18 22:00:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2899', '1', 'admin', '2017-09-18 22:01:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2900', '1', 'admin', '2017-09-18 22:03:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2901', '1', 'admin', '2017-09-18 22:03:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2902', '1', 'admin', '2017-09-18 22:05:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2903', '1', 'admin', '2017-09-18 22:07:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2904', '1', 'admin', '2017-09-18 22:08:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2905', '1', 'admin', '2017-09-18 22:08:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2906', '1', 'admin', '2017-09-18 22:10:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2907', '1', 'admin', '2017-09-18 22:10:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2908', '1', 'admin', '2017-09-18 22:12:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2909', '1', 'admin', '2017-09-18 22:14:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2910', '1', 'admin', '2017-09-18 22:15:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2911', '1', 'admin', '2017-09-18 22:17:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2912', '1', 'admin', '2017-09-18 22:19:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2913', '1', 'admin', '2017-09-18 22:20:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2914', '1', 'admin', '2017-09-18 22:25:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2915', '1', 'admin', '2017-09-18 22:26:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2916', '1', 'admin', '2017-09-18 22:29:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2917', '1', 'admin', '2017-09-18 23:08:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2918', '1', 'admin', '2017-09-18 23:10:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2919', '1', 'admin', '2017-09-18 23:14:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2920', '1', 'admin', '2017-09-18 23:23:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2921', '1', 'admin', '2017-09-18 23:47:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2922', '1', 'admin', '2017-09-18 23:52:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2923', '1', 'admin', '2017-09-19 00:04:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2924', '1', 'admin', '2017-09-19 00:13:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2925', '1', 'admin', '2017-09-19 00:20:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2926', '1', 'admin', '2017-09-19 00:30:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2927', '1', 'admin', '2017-09-19 00:36:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2928', '1', 'admin', '2017-09-19 00:39:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2929', '1', 'admin', '2017-09-19 00:42:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2930', '1', 'admin', '2017-09-19 09:06:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2931', '1', 'admin', '2017-09-19 09:13:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2932', '1', 'admin', '2017-09-19 09:18:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2933', '1', 'admin', '2017-09-19 09:20:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2934', '1', 'admin', '2017-09-19 09:24:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2935', '1', 'admin', '2017-09-19 09:33:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2936', '1', 'admin', '2017-09-19 09:45:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2937', '1', 'admin', '2017-09-19 09:49:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2938', '1', 'admin', '2017-09-19 09:59:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2939', '1', 'admin', '2017-09-19 10:01:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2940', '1', 'admin', '2017-09-19 10:16:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2941', '1', 'admin', '2017-09-19 10:21:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2942', '1', 'admin', '2017-09-19 10:27:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2943', '1', 'admin', '2017-09-19 10:32:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2944', '1', 'admin', '2017-09-19 10:38:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2945', '1', 'admin', '2017-09-19 10:38:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2946', '1', 'admin', '2017-09-19 10:41:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2947', '1', 'admin', '2017-09-19 10:41:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2948', '1', 'admin', '2017-09-19 10:43:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2949', '1', 'admin', '2017-09-19 10:43:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2950', '1', 'admin', '2017-09-19 10:46:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2951', '1', 'admin', '2017-09-19 10:47:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2952', '1', 'admin', '2017-09-19 10:49:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2953', '1', 'admin', '2017-09-19 10:50:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2954', '1', 'admin', '2017-09-19 10:51:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2955', '1', 'admin', '2017-09-19 10:54:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2956', '1', 'admin', '2017-09-19 10:58:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2957', '1', 'admin', '2017-09-19 11:00:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2958', '1', 'admin', '2017-09-19 11:09:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2959', '1', 'admin', '2017-09-19 11:15:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2960', '1', 'admin', '2017-09-19 11:21:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2961', '1', 'admin', '2017-09-19 11:29:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2962', '1', 'admin', '2017-09-19 11:30:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2963', '1', 'admin', '2017-09-19 11:46:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2964', '1', 'admin', '2017-09-19 12:05:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2965', '1', 'admin', '2017-09-19 12:07:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2966', '1', 'admin', '2017-09-19 12:24:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2967', '1', 'admin', '2017-09-19 12:27:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2968', '1', 'admin', '2017-09-19 12:31:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2969', '1', 'admin', '2017-09-19 12:32:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2970', '1', 'admin', '2017-09-19 13:14:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2971', '1', 'admin', '2017-09-19 13:15:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2972', '1', 'admin', '2017-09-19 13:22:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2973', '1', 'admin', '2017-09-19 14:02:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2974', '1', 'admin', '2017-09-19 14:02:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2975', '1', 'admin', '2017-09-19 14:08:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2976', '1', 'admin', '2017-09-19 14:12:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2977', '1', 'admin', '2017-09-19 14:17:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2978', '1', 'admin', '2017-09-19 14:18:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2979', '1', 'admin', '2017-09-19 14:24:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2980', '1', 'admin', '2017-09-19 14:32:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2981', '1', 'admin', '2017-09-19 14:33:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2982', '1', 'admin', '2017-09-19 14:37:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2983', '1', 'admin', '2017-09-19 14:43:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2984', '1', 'admin', '2017-09-19 14:47:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2985', '1', 'admin', '2017-09-19 14:47:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2986', '1', 'admin', '2017-09-19 14:49:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2987', '1', 'admin', '2017-09-19 14:53:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2988', '1', 'admin', '2017-09-19 15:03:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2989', '1', 'admin', '2017-09-19 15:17:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2990', '1', 'admin', '2017-09-19 16:05:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2991', '1', 'admin', '2017-09-19 16:09:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2992', '1', 'admin', '2017-09-19 16:17:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2993', '1', 'admin', '2017-09-19 16:18:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2994', '1', 'admin', '2017-09-19 16:18:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2995', '1', 'admin', '2017-09-19 16:19:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2996', '1', 'admin', '2017-09-19 16:19:26', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('2997', '1', 'admin', '2017-09-19 16:24:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2998', '1', 'admin', '2017-09-19 16:27:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('2999', '1', 'admin', '2017-09-19 16:30:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3000', '1', 'admin', '2017-09-19 16:35:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3001', '1', 'admin', '2017-09-19 16:54:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3002', '1', 'admin', '2017-09-19 17:05:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3003', '1', 'admin', '2017-09-19 17:13:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3004', '1', 'admin', '2017-09-19 17:17:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3005', '1', 'admin', '2017-09-19 19:01:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3006', '1', 'admin', '2017-09-19 19:11:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3007', '1', 'admin', '2017-09-19 20:00:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3008', '1', 'admin', '2017-09-19 20:18:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3009', '1', 'admin', '2017-09-19 20:28:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3010', '1', 'admin', '2017-09-19 20:38:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3011', '1', 'admin', '2017-09-19 20:40:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3012', '1', 'admin', '2017-09-19 20:41:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3013', '1', 'admin', '2017-09-19 20:42:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3014', '1', 'admin', '2017-09-19 20:45:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3015', '1', 'admin', '2017-09-19 20:47:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3016', '1', 'admin', '2017-09-19 21:03:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3017', '1', 'admin', '2017-09-19 21:05:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3018', '1', 'admin', '2017-09-19 21:11:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3019', '1', 'admin', '2017-09-19 21:20:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3020', '1', 'admin', '2017-09-19 21:29:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3021', '1', 'admin', '2017-09-19 21:37:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3022', '1', 'admin', '2017-09-19 21:41:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3023', '1', 'admin', '2017-09-19 21:44:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3024', '1', 'admin', '2017-09-19 21:50:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3025', '1', 'admin', '2017-09-19 22:05:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3026', '1', 'admin', '2017-09-19 22:07:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3027', '1', 'admin', '2017-09-19 22:10:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3028', '1', 'admin', '2017-09-19 22:42:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3029', '1', 'admin', '2017-09-19 22:47:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3030', '1', 'admin', '2017-09-19 22:49:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3031', '1', 'admin', '2017-09-19 22:51:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3032', '1', 'admin', '2017-09-20 08:57:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3033', '1', 'admin', '2017-09-20 09:07:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3034', '1', 'admin', '2017-09-20 09:46:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3035', '1', 'admin', '2017-09-20 09:49:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3036', '1', 'admin', '2017-09-20 09:53:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3037', '1', 'admin', '2017-09-20 09:54:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3038', '1', 'admin', '2017-09-20 09:59:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3039', '1', 'admin', '2017-09-20 10:05:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3040', '1', 'admin', '2017-09-20 10:06:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3041', '1', 'admin', '2017-09-20 10:08:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3042', '1', 'admin', '2017-09-20 10:11:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3043', '1', 'admin', '2017-09-20 10:19:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3044', '1', 'admin', '2017-09-20 10:21:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3045', '1', 'admin', '2017-09-20 10:24:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3046', '1', 'admin', '2017-09-20 10:34:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3047', '1', 'admin', '2017-09-20 10:36:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3048', '1', 'admin', '2017-09-20 10:45:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3049', '1', 'admin', '2017-09-20 11:05:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3050', '1', 'admin', '2017-09-20 11:23:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3051', '1', 'admin', '2017-09-20 11:25:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3052', '1', 'admin', '2017-09-20 11:41:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3053', '1', 'admin', '2017-09-20 11:45:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3054', '1', 'admin', '2017-09-20 12:00:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3055', '1', 'admin', '2017-09-20 14:14:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3056', '1', 'admin', '2017-09-20 14:15:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3057', '1', 'admin', '2017-09-20 15:14:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3058', '1', 'admin', '2017-09-20 17:13:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3059', '1', 'admin', '2017-09-20 17:16:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3060', '1', 'admin', '2017-09-20 17:17:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3061', '1', 'admin', '2017-09-20 17:34:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3062', '1', 'admin', '2017-09-20 17:35:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3063', '1', 'admin', '2017-09-20 17:36:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3064', '1', 'admin', '2017-09-20 17:37:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3065', '1', 'admin', '2017-09-20 17:37:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3066', '1', 'admin', '2017-09-20 17:49:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3067', '1', 'admin', '2017-09-20 18:04:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3068', '1', 'admin', '2017-09-20 18:06:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3069', '1', 'admin', '2017-09-20 19:01:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3070', '1', 'admin', '2017-09-20 19:13:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3071', '1', 'admin', '2017-09-20 19:19:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3072', '1', 'admin', '2017-09-20 19:22:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3073', '1', 'admin', '2017-09-20 19:38:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3074', '1', 'admin', '2017-09-20 20:02:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3075', '1', 'admin', '2017-09-20 20:22:52', '192.168.1.102');
INSERT INTO `mds_userlogin` VALUES ('3076', '1', 'admin', '2017-09-20 20:23:15', '192.168.1.102');
INSERT INTO `mds_userlogin` VALUES ('3077', '1', 'admin', '2017-09-20 20:50:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3078', '1', 'admin', '2017-09-20 21:40:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3079', '1', 'admin', '2017-09-20 21:54:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3080', '1', 'admin', '2017-09-20 22:43:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3081', '1', 'admin', '2017-09-20 22:47:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3082', '1', 'admin', '2017-09-20 22:48:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3083', '1', 'admin', '2017-09-20 22:49:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3084', '1', 'admin', '2017-09-21 09:26:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3085', '1', 'admin', '2017-09-21 09:33:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3086', '0', 'admin', '2017-09-21 10:18:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3087', '0', 'admin', '2017-09-21 10:21:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3088', '0', 'admin', '2017-09-21 10:22:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3089', '0', 'admin', '2017-09-21 10:24:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3090', '0', 'admin', '2017-09-21 10:33:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3091', '0', 'admin', '2017-09-21 10:38:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3092', '0', 'admin', '2017-09-21 10:38:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3093', '0', 'admin', '2017-09-21 10:44:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3094', '0', 'admin', '2017-09-21 10:45:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3095', '0', 'admin', '2017-09-21 10:47:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3096', '0', 'admin', '2017-09-21 10:48:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3097', '0', 'admin', '2017-09-21 10:48:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3098', '0', 'admin', '2017-09-21 10:49:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3099', 'e180b10a87a24acc8d84c92b57f0fae5', 'test', '2017-09-21 10:49:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3100', '0', 'admin', '2017-09-21 10:49:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3101', 'e180b10a87a24acc8d84c92b57f0fae5', 'test', '2017-09-21 10:49:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3102', '0', 'admin', '2017-09-21 10:49:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3103', '0', 'admin', '2017-09-21 10:51:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3104', 'e180b10a87a24acc8d84c92b57f0fae5', 'test', '2017-09-21 10:52:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3105', '1711dd472326474daaa0bed1469b8ae9', 'huahao', '2017-09-21 10:52:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3106', '0', 'admin', '2017-09-21 10:52:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3107', 'e180b10a87a24acc8d84c92b57f0fae5', 'test', '2017-09-21 10:54:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3108', '0', 'admin', '2017-09-21 10:57:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3109', '0', 'admin', '2017-09-21 10:58:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3110', 'e180b10a87a24acc8d84c92b57f0fae5', 'test', '2017-09-21 11:00:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3111', 'e180b10a87a24acc8d84c92b57f0fae5', 'test', '2017-09-21 11:03:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3112', '1711dd472326474daaa0bed1469b8ae9', 'huahao', '2017-09-21 11:03:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3113', '0', 'admin', '2017-09-21 11:03:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3114', '0', 'admin', '2017-09-21 11:12:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3115', '0', 'admin', '2017-09-21 11:33:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3116', '0', 'admin', '2017-09-21 13:58:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3117', '0', 'admin', '2017-09-21 14:18:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3118', '0', 'admin', '2017-09-21 14:33:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3119', '0', 'admin', '2017-09-21 14:38:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3120', '0', 'admin', '2017-09-21 14:40:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3121', '0', 'admin', '2017-09-21 14:42:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3122', '0', 'admin', '2017-09-21 14:52:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3123', '0', 'admin', '2017-09-21 14:54:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3124', '0', 'admin', '2017-09-21 14:57:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3125', '0', 'admin', '2017-09-21 14:59:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3126', '0', 'admin', '2017-09-21 15:04:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3127', '0', 'admin', '2017-09-21 15:05:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3128', '0', 'admin', '2017-09-21 15:07:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3129', '0', 'admin', '2017-09-21 15:11:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3130', '0', 'admin', '2017-09-21 15:17:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3131', '1711dd472326474daaa0bed1469b8ae9', 'huahao', '2017-09-21 15:17:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3132', '0', 'admin', '2017-09-21 15:17:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3133', '0', 'admin', '2017-09-21 15:22:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3134', '0', 'admin', '2017-09-21 16:26:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3135', '0', 'admin', '2017-09-21 16:48:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3136', '0', 'admin', '2017-09-21 16:50:59', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('3137', '0', 'admin', '2017-09-21 16:55:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3138', '0', 'admin', '2017-09-21 16:59:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3139', '0', 'admin', '2017-09-21 17:02:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3140', '0', 'admin', '2017-09-21 17:10:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3141', '0', 'admin', '2017-09-21 17:15:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3142', '0', 'admin', '2017-09-21 17:16:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3143', '0', 'admin', '2017-09-21 18:20:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3144', '0', 'admin', '2017-09-21 18:25:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3145', '0', 'admin', '2017-09-21 19:19:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3146', '0', 'admin', '2017-09-21 19:43:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3147', '0', 'admin', '2017-09-21 20:08:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3148', '0', 'admin', '2017-09-21 20:09:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3149', '0', 'admin', '2017-09-21 20:48:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3150', '0', 'admin', '2017-09-21 20:53:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3151', '0', 'admin', '2017-09-21 20:59:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3152', '0', 'admin', '2017-09-21 21:01:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3153', '0', 'admin', '2017-09-21 21:37:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3154', '0', 'admin', '2017-09-21 21:37:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3155', '0', 'admin', '2017-09-21 21:45:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3156', '0', 'admin', '2017-09-21 22:49:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3157', '0', 'admin', '2017-09-22 09:15:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3158', '0', 'admin', '2017-09-22 09:16:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3159', '0', 'admin', '2017-09-22 09:23:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3160', '0', 'admin', '2017-09-22 09:58:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3161', '0', 'admin', '2017-09-22 10:02:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3162', '0', 'admin', '2017-09-22 10:26:18', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('3163', '0', 'admin', '2017-09-22 10:27:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3164', '0', 'admin', '2017-09-22 10:30:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3165', '0', 'admin', '2017-09-22 10:31:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3166', '0', 'admin', '2017-09-22 10:34:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3167', '0', 'admin', '2017-09-22 10:37:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3168', '0', 'admin', '2017-09-22 10:42:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3169', '0', 'admin', '2017-09-22 10:43:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3170', '0', 'admin', '2017-09-22 10:46:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3171', '0', 'admin', '2017-09-22 10:46:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3172', '0', 'admin', '2017-09-22 11:16:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3173', '0', 'admin', '2017-09-22 11:19:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3174', '0', 'admin', '2017-09-22 11:20:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3175', '0', 'admin', '2017-09-22 11:22:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3176', '0', 'admin', '2017-09-22 11:47:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3177', '0', 'admin', '2017-09-22 14:09:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3178', '0', 'admin', '2017-09-22 14:30:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3179', '0', 'admin', '2017-09-22 14:36:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3180', '0', 'admin', '2017-09-22 14:48:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3181', '0', 'admin', '2017-09-22 14:51:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3182', '0', 'admin', '2017-09-22 15:11:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3183', '0', 'admin', '2017-09-22 15:25:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3184', '0', 'admin', '2017-09-22 17:00:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3185', '0', 'admin', '2017-09-22 17:01:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3186', '0', 'admin', '2017-09-22 17:05:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3187', '0', 'admin', '2017-09-22 17:11:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3188', '0', 'admin', '2017-09-22 17:23:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3189', '0', 'admin', '2017-09-22 17:33:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3190', '0', 'admin', '2017-09-22 17:36:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3191', '0', 'admin', '2017-09-22 19:13:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3192', '0', 'admin', '2017-09-22 19:30:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3193', '0', 'admin', '2017-09-22 19:32:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3194', '0', 'admin', '2017-09-22 19:40:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3195', '0', 'admin', '2017-09-22 19:46:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3196', '0', 'admin', '2017-09-22 20:03:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3197', '0', 'admin', '2017-09-22 20:03:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3198', '0', 'admin', '2017-09-22 20:39:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3199', '0', 'admin', '2017-09-22 20:43:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3200', '0', 'admin', '2017-09-22 20:55:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3201', '0', 'admin', '2017-09-22 20:59:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3202', '0', 'admin', '2017-09-22 21:12:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3203', '0', 'admin', '2017-09-22 21:12:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3204', '0', 'admin', '2017-09-22 21:14:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3205', '0', 'admin', '2017-09-22 21:16:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3206', '0', 'admin', '2017-09-22 21:24:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3207', '0', 'admin', '2017-09-22 21:26:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3208', '0', 'admin', '2017-09-22 21:28:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3209', '0', 'admin', '2017-09-22 21:32:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3210', '0', 'admin', '2017-09-22 21:38:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3211', '0', 'admin', '2017-09-22 21:42:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3212', '0', 'admin', '2017-09-22 21:43:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3213', '0', 'admin', '2017-09-22 21:46:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3214', '0', 'admin', '2017-09-22 21:48:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3215', '0', 'admin', '2017-09-22 21:52:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3216', '0', 'admin', '2017-09-22 22:45:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3217', '0', 'admin', '2017-09-22 22:49:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3218', '0', 'admin', '2017-09-22 23:03:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3219', '0', 'admin', '2017-09-22 23:06:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3220', '0', 'admin', '2017-09-22 23:08:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3221', '0', 'admin', '2017-09-22 23:17:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3222', '0', 'admin', '2017-09-22 23:29:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3223', '0', 'admin', '2017-09-23 09:09:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3224', '0', 'admin', '2017-09-23 09:31:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3225', '0', 'admin', '2017-09-23 09:33:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3226', '0', 'admin', '2017-09-23 09:35:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3227', '0', 'admin', '2017-09-23 10:04:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3228', '0', 'admin', '2017-09-23 10:06:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3229', '0', 'admin', '2017-09-23 10:14:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3230', '0', 'admin', '2017-09-23 10:21:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3231', '0', 'admin', '2017-09-23 10:28:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3232', '0', 'admin', '2017-09-23 10:36:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3233', '0', 'admin', '2017-09-23 10:38:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3234', '0', 'admin', '2017-09-23 10:52:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3235', '0', 'admin', '2017-09-23 11:27:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3236', '0', 'admin', '2017-09-23 11:30:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3237', '0', 'admin', '2017-09-23 11:38:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3238', '0', 'admin', '2017-09-23 11:39:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3239', '0', 'admin', '2017-09-23 11:54:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3240', '0', 'admin', '2017-09-23 12:31:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3241', '0', 'admin', '2017-09-23 12:33:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3242', '0', 'admin', '2017-09-23 13:27:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3243', '0', 'admin', '2017-09-23 14:02:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3244', '0', 'admin', '2017-09-23 14:06:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3245', '0', 'admin', '2017-09-23 14:25:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3246', '0', 'admin', '2017-09-23 14:28:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3247', '0', 'admin', '2017-09-23 14:34:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3248', '0', 'admin', '2017-09-23 15:12:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3249', '0', 'admin', '2017-09-23 15:35:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3250', '0', 'admin', '2017-09-23 15:43:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3251', '0', 'admin', '2017-09-23 15:49:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3252', '0', 'admin', '2017-09-23 16:19:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3253', '0', 'admin', '2017-09-23 16:46:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3254', '0', 'admin', '2017-09-23 16:59:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3255', '0', 'admin', '2017-09-23 17:04:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3256', '0', 'admin', '2017-09-23 17:06:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3257', '0', 'admin', '2017-09-23 17:08:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3258', '0', 'admin', '2017-09-23 17:09:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3259', '0', 'admin', '2017-09-23 17:11:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3260', '0', 'admin', '2017-09-23 17:15:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3261', '0', 'admin', '2017-09-23 17:16:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3262', '0', 'admin', '2017-09-23 17:19:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3263', '0', 'admin', '2017-09-23 17:26:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3264', '0', 'admin', '2017-09-23 18:02:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3265', '0', 'admin', '2017-09-23 18:08:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3266', '0', 'admin', '2017-09-23 18:09:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3267', '0', 'admin', '2017-09-23 19:42:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3268', '0', 'admin', '2017-09-23 20:44:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3269', '0', 'admin', '2017-09-23 20:58:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3270', '0', 'admin', '2017-09-23 22:09:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3271', '0', 'admin', '2017-09-23 22:09:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3272', '0', 'admin', '2017-09-25 08:59:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3273', '0', 'admin', '2017-09-25 09:09:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3274', '0', 'admin', '2017-09-25 10:24:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3275', '0', 'admin', '2017-09-25 10:24:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3276', '0', 'admin', '2017-09-25 10:40:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3277', '0', 'admin', '2017-09-25 11:46:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3278', '0', 'admin', '2017-09-25 12:04:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3279', '0', 'admin', '2017-09-25 14:13:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3280', '0', 'admin', '2017-09-25 14:15:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3281', '0', 'admin', '2017-09-25 14:17:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3282', '0', 'admin', '2017-09-25 14:23:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3283', '0', 'admin', '2017-09-25 14:26:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3284', '0', 'admin', '2017-09-25 14:30:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3285', '0', 'admin', '2017-09-25 16:08:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3286', '0', 'admin', '2017-09-25 16:14:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3287', '0', 'admin', '2017-09-25 16:19:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3288', '0', 'admin', '2017-09-25 17:25:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3289', '0', 'admin', '2017-09-25 17:28:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3290', '0', 'admin', '2017-09-25 19:04:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3291', '0', 'admin', '2017-09-25 19:08:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3292', '0', 'admin', '2017-09-25 19:15:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3293', '0', 'admin', '2017-09-25 19:20:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3294', '0', 'admin', '2017-09-25 19:51:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3295', '0', 'admin', '2017-09-25 20:15:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3296', '0', 'admin', '2017-09-25 20:26:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3297', '0', 'admin', '2017-09-25 21:06:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3298', '0', 'admin', '2017-09-25 23:03:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3299', '0', 'admin', '2017-09-25 23:06:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3300', '0', 'admin', '2017-09-25 23:17:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3301', '0', 'admin', '2017-09-26 09:17:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3302', '0', 'admin', '2017-09-26 09:24:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3303', '0', 'admin', '2017-09-26 09:25:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3304', '0', 'admin', '2017-09-26 09:56:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3305', '0', 'admin', '2017-09-26 10:03:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3306', '0', 'admin', '2017-09-26 10:05:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3307', '0', 'admin', '2017-09-26 10:18:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3308', '0', 'admin', '2017-09-26 10:20:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3309', '0', 'admin', '2017-09-26 10:26:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3310', '0', 'admin', '2017-09-26 10:27:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3311', '0', 'admin', '2017-09-26 10:29:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3312', '0', 'admin', '2017-09-26 10:32:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3313', '0', 'admin', '2017-09-26 11:07:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3314', '0', 'admin', '2017-09-26 11:09:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3315', '0', 'admin', '2017-09-26 11:19:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3316', '0', 'admin', '2017-09-26 14:05:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3317', '0', 'admin', '2017-09-26 14:06:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3318', '0', 'admin', '2017-09-26 14:09:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3319', '0', 'admin', '2017-09-26 14:15:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3320', '0', 'admin', '2017-09-26 14:26:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3321', '0', 'admin', '2017-09-26 14:47:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3322', '0', 'admin', '2017-09-26 14:57:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3323', '0', 'admin', '2017-09-26 14:59:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3324', '0', 'admin', '2017-09-26 15:02:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3325', '0', 'admin', '2017-09-26 15:09:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3326', '0', 'admin', '2017-09-26 15:36:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3327', '0', 'admin', '2017-09-26 15:50:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3328', '0', 'admin', '2017-09-26 15:54:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3329', '0', 'admin', '2017-09-26 16:08:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3330', '0', 'admin', '2017-09-26 16:29:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3331', '0', 'admin', '2017-09-26 16:33:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3332', '0', 'admin', '2017-09-26 16:45:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3333', '0', 'admin', '2017-09-26 16:50:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3334', '0', 'admin', '2017-09-26 17:08:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3335', '0', 'admin', '2017-09-26 17:54:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3336', '0', 'admin', '2017-09-26 18:55:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3337', '0', 'admin', '2017-09-26 19:03:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3338', '0', 'admin', '2017-09-26 19:39:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3339', '0', 'admin', '2017-09-26 19:40:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3340', '0', 'admin', '2017-09-26 20:34:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3341', '0', 'admin', '2017-09-27 09:17:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3342', '0', 'admin', '2017-09-27 10:00:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3343', '0', 'admin', '2017-09-27 11:01:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3344', '0', 'admin', '2017-09-27 11:07:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3345', '0', 'admin', '2017-09-27 11:09:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3346', '0', 'admin', '2017-09-27 11:50:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3347', '0', 'admin', '2017-09-27 11:53:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3348', '0', 'admin', '2017-09-27 11:59:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3349', '0', 'admin', '2017-09-27 12:06:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3350', '0', 'admin', '2017-09-27 12:12:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3351', '0', 'admin', '2017-09-27 12:17:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3352', '0', 'admin', '2017-09-27 12:34:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3353', '0', 'admin', '2017-09-27 14:19:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3354', '0', 'admin', '2017-10-03 15:46:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3355', '0', 'admin', '2017-10-03 15:47:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3356', '0', 'admin', '2017-10-05 09:36:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3357', '0', 'admin', '2017-10-05 09:36:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3358', '0', 'admin', '2017-10-09 14:35:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3359', '0', 'admin', '2017-10-09 16:22:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3360', '0', 'admin', '2017-10-09 17:51:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3361', '0', 'admin', '2017-10-09 17:56:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3362', '0', 'admin', '2017-10-09 18:00:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3363', '0', 'admin', '2017-10-10 11:46:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3364', '0', 'admin', '2017-10-10 11:48:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3365', '0', 'admin', '2017-10-10 14:14:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3366', '0', 'admin', '2017-10-10 14:30:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3367', '0', 'admin', '2017-10-10 14:40:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3368', '0', 'admin', '2017-10-10 14:43:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3369', '0', 'admin', '2017-10-10 14:47:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3370', '0', 'admin', '2017-10-10 15:37:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3371', '0', 'admin', '2017-10-10 16:04:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3372', '0', 'admin', '2017-10-10 16:22:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3373', '0', 'admin', '2017-10-10 17:05:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3374', '0', 'admin', '2017-10-10 17:52:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3375', '0', 'admin', '2017-10-10 18:17:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3376', '0', 'admin', '2017-10-11 09:02:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3377', '0', 'admin', '2017-10-11 09:48:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3378', '0', 'admin', '2017-10-11 09:53:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3379', '0', 'admin', '2017-10-11 09:53:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3380', '0', 'admin', '2017-10-11 10:25:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3381', '0', 'admin', '2017-10-11 10:30:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3382', '0', 'admin', '2017-10-11 10:59:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3383', '0', 'admin', '2017-10-11 11:16:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3384', '0', 'admin', '2017-10-11 11:57:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3385', '0', 'admin', '2017-10-11 11:58:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3386', '0', 'admin', '2017-10-11 14:06:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3387', '0', 'admin', '2017-10-11 14:17:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3388', '0', 'admin', '2017-10-11 14:34:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3389', '0', 'admin', '2017-10-11 14:52:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3390', '0', 'admin', '2017-10-11 15:07:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3391', '0', 'admin', '2017-10-11 15:13:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3392', '0', 'admin', '2017-10-11 15:15:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3393', '0', 'admin', '2017-10-11 15:20:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3394', '0', 'admin', '2017-10-11 15:42:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3395', '0', 'admin', '2017-10-11 16:05:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3396', '0', 'admin', '2017-10-11 17:23:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3397', '0', 'admin', '2017-10-11 17:24:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3398', '0', 'admin', '2017-10-11 18:02:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3399', '0', 'admin', '2017-10-12 09:00:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3400', '0', 'admin', '2017-10-12 09:12:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3401', '0', 'admin', '2017-10-12 09:46:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3402', '0', 'admin', '2017-10-12 09:53:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3403', '0', 'admin', '2017-10-12 10:09:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3404', '0', 'admin', '2017-10-12 10:12:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3405', '0', 'admin', '2017-10-12 10:34:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3406', '0', 'admin', '2017-10-12 10:56:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3407', '0', 'admin', '2017-10-12 10:58:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3408', '0', 'admin', '2017-10-12 11:05:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3409', '0', 'admin', '2017-10-12 11:13:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3410', '0', 'admin', '2017-10-12 11:22:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3411', '0', 'admin', '2017-10-12 11:24:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3412', '0', 'admin', '2017-10-12 11:33:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3413', '0', 'admin', '2017-10-12 11:47:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3414', '0', 'admin', '2017-10-12 14:07:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3415', '0', 'admin', '2017-10-12 14:07:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3416', '0', 'admin', '2017-10-12 14:15:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3417', '0', 'admin', '2017-10-12 14:27:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3418', '0', 'admin', '2017-10-12 15:02:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3419', '0', 'admin', '2017-10-12 15:02:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3420', '0', 'admin', '2017-10-12 15:25:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3421', '0', 'admin', '2017-10-12 15:28:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3422', '0', 'admin', '2017-10-12 15:29:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3423', '0', 'admin', '2017-10-12 16:06:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3424', '0', 'admin', '2017-10-12 16:53:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3425', '0', 'admin', '2017-10-12 16:56:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3426', '0', 'admin', '2017-10-12 17:07:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3427', '0', 'admin', '2017-10-12 17:14:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3428', '0', 'admin', '2017-10-12 17:35:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3429', '0', 'admin', '2017-10-12 17:50:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3430', '0', 'admin', '2017-10-12 18:02:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3431', '0', 'admin', '2017-10-12 18:13:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3432', '0', 'admin', '2017-10-12 18:13:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3433', '0', 'admin', '2017-10-12 18:14:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3434', '0', 'admin', '2017-10-12 18:14:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3435', '0', 'admin', '2017-10-12 18:14:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3436', '0', 'admin', '2017-10-12 18:16:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3437', '0', 'admin', '2017-10-12 18:18:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3438', '0', 'admin', '2017-10-12 18:30:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3439', '0', 'admin', '2017-10-12 18:38:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3440', '0', 'admin', '2017-10-12 18:42:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3441', '0', 'admin', '2017-10-12 18:48:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3442', '0', 'admin', '2017-10-12 18:51:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3443', '0', 'admin', '2017-10-12 20:15:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3444', '0', 'admin', '2017-10-12 20:48:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3445', '0', 'admin', '2017-10-13 09:04:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3446', '0', 'admin', '2017-10-13 09:15:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3447', '0', 'admin', '2017-10-13 09:16:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3448', '0', 'admin', '2017-10-13 09:21:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3449', '0', 'admin', '2017-10-13 09:59:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3450', '0', 'admin', '2017-10-13 10:20:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3451', '0', 'admin', '2017-10-13 10:36:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3452', '0', 'admin', '2017-10-13 10:38:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3453', '0', 'admin', '2017-10-13 10:57:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3454', '0', 'admin', '2017-10-13 11:29:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3455', '0', 'admin', '2017-10-13 11:35:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3456', '0', 'admin', '2017-10-13 11:47:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3457', '0', 'admin', '2017-10-13 11:50:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3458', '0', 'admin', '2017-10-13 14:03:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3459', '0', 'admin', '2017-10-13 14:04:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3460', '0', 'admin', '2017-10-13 14:04:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3461', '0', 'admin', '2017-10-13 14:18:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3462', '0', 'admin', '2017-10-13 15:15:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3463', '0', 'admin', '2017-10-13 16:11:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3464', '0', 'admin', '2017-10-13 17:05:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3465', '0', 'admin', '2017-10-13 17:20:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3466', '0', 'admin', '2017-10-13 17:41:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3467', '0', 'admin', '2017-10-13 18:04:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3468', '0', 'admin', '2017-10-13 18:08:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3469', '0', 'admin', '2017-10-13 19:55:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3470', '0', 'admin', '2017-10-13 20:18:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3471', '0', 'admin', '2017-10-13 20:35:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3472', '0', 'admin', '2017-10-14 09:10:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3473', '0', 'admin', '2017-10-14 10:41:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3474', '0', 'admin', '2017-10-14 10:45:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3475', '0', 'admin', '2017-10-14 10:47:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3476', '0', 'admin', '2017-10-14 10:57:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3477', '0', 'admin', '2017-10-14 11:31:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3478', '0', 'admin', '2017-10-14 11:47:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3479', '0', 'admin', '2017-10-14 11:48:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3480', '0', 'admin', '2017-10-14 11:54:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3481', '0', 'admin', '2017-10-14 14:54:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3482', '0', 'admin', '2017-10-14 15:05:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3483', '0', 'admin', '2017-10-14 15:09:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3484', '0', 'admin', '2017-10-14 15:11:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3485', '0', 'admin', '2017-10-14 15:29:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3486', '0', 'admin', '2017-10-14 15:34:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3487', '0', 'admin', '2017-10-14 20:24:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3488', '0', 'admin', '2017-10-14 20:32:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3489', '0', 'admin', '2017-10-14 20:46:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3490', '0', 'admin', '2017-10-14 20:55:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3491', '0', 'admin', '2017-10-14 21:10:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3492', '0', 'admin', '2017-10-14 21:16:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3493', '0', 'admin', '2017-10-14 21:23:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3494', '0', 'admin', '2017-10-14 21:31:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3495', '0', 'admin', '2017-10-14 21:35:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3496', '0', 'admin', '2017-10-14 21:44:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3497', '0', 'admin', '2017-10-14 22:03:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3498', '0', 'admin', '2017-10-14 22:03:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3499', '0', 'admin', '2017-10-14 22:09:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3500', '0', 'admin', '2017-10-15 00:07:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3501', '0', 'admin', '2017-10-15 00:12:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3502', '0', 'admin', '2017-10-15 00:15:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3503', '0', 'admin', '2017-10-15 00:20:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3504', '0', 'admin', '2017-10-16 09:14:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3505', '0', 'admin', '2017-10-16 09:25:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3506', '0', 'admin', '2017-10-16 09:32:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3507', '0', 'admin', '2017-10-16 09:52:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3508', '0', 'admin', '2017-10-16 09:59:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3509', '0', 'admin', '2017-10-16 10:52:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3510', '0', 'admin', '2017-10-16 11:03:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3511', '0', 'admin', '2017-10-16 11:09:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3512', '0', 'admin', '2017-10-16 11:09:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3513', '0', 'admin', '2017-10-16 11:38:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3514', '0', 'admin', '2017-10-16 14:04:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3515', '0', 'admin', '2017-10-16 14:25:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3516', '0', 'admin', '2017-10-16 14:31:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3517', '0', 'admin', '2017-10-16 14:45:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3518', '0', 'admin', '2017-10-16 15:46:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3519', '0', 'admin', '2017-10-16 16:30:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3520', '0', 'admin', '2017-10-16 16:39:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3521', '0', 'admin', '2017-10-16 16:46:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3522', '0', 'admin', '2017-10-16 16:52:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3523', '0', 'admin', '2017-10-16 16:52:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3524', '0', 'admin', '2017-10-16 16:55:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3525', '0', 'admin', '2017-10-16 16:58:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3526', '0', 'admin', '2017-10-16 17:06:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3527', '0', 'admin', '2017-10-16 18:18:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3528', '0', 'admin', '2017-10-16 18:22:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3529', '0', 'admin', '2017-10-16 18:36:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3530', '0', 'admin', '2017-10-16 18:43:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3531', '0', 'admin', '2017-10-16 18:45:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3532', '0', 'admin', '2017-10-16 18:54:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3533', '0', 'admin', '2017-10-16 18:56:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3534', '0', 'admin', '2017-10-16 19:43:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3535', '0', 'admin', '2017-10-16 19:52:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3536', '0', 'admin', '2017-10-16 20:21:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3537', '0', 'admin', '2017-10-17 09:11:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3538', '0', 'admin', '2017-10-17 09:15:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3539', '0', 'admin', '2017-10-17 10:15:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3540', '0', 'admin', '2017-10-17 10:54:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3541', '0', 'admin', '2017-10-17 11:06:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3542', '0', 'admin', '2017-10-17 11:30:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3543', '0', 'admin', '2017-10-17 11:55:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3544', '0', 'admin', '2017-10-17 14:06:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3545', '0', 'admin', '2017-10-17 15:03:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3546', '0', 'admin', '2017-10-17 16:47:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3547', '0', 'admin', '2017-10-17 17:15:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3548', '0', 'admin', '2017-10-17 17:19:42', '127.0.0.1');
INSERT INTO `mds_userlogin` VALUES ('3549', '0', 'admin', '2017-10-17 17:22:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3550', '0', 'admin', '2017-10-17 17:31:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3551', '0', 'admin', '2017-10-17 20:14:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3552', '0', 'admin', '2017-10-17 20:22:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3553', '0', 'admin', '2017-10-17 20:43:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3554', '0', 'admin', '2017-10-17 20:52:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3555', '0', 'admin', '2017-10-17 21:33:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3556', '0', 'admin', '2017-10-17 23:25:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3557', '0', 'admin', '2017-10-18 09:05:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3558', '0', 'admin', '2017-10-18 09:10:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3559', '0', 'admin', '2017-10-18 09:12:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3560', '0', 'admin', '2017-10-18 09:35:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3561', '0', 'admin', '2017-10-18 09:42:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3562', '0', 'admin', '2017-10-18 09:48:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3563', '0', 'admin', '2017-10-18 10:35:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3564', '0', 'admin', '2017-10-18 11:06:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3565', '0', 'admin', '2017-10-18 11:35:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3566', '0', 'admin', '2017-10-18 11:43:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3567', '0', 'admin', '2017-10-18 12:05:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3568', '0', 'admin', '2017-10-18 14:05:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3569', '0', 'admin', '2017-10-18 14:11:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3570', '0', 'admin', '2017-10-18 14:14:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3571', '0', 'admin', '2017-10-18 14:17:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3572', '0', 'admin', '2017-10-18 15:07:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3573', '0', 'admin', '2017-10-18 15:12:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3574', '0', 'admin', '2017-10-18 15:16:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3575', '0', 'admin', '2017-10-18 15:36:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3576', '0', 'admin', '2017-10-18 15:41:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3577', '0', 'admin', '2017-10-18 15:48:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3578', '0', 'admin', '2017-10-18 15:49:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3579', '0', 'admin', '2017-10-18 15:51:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3580', '0', 'admin', '2017-10-18 15:55:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3581', '0', 'admin', '2017-10-18 16:13:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3582', '0', 'admin', '2017-10-18 16:20:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3583', '0', 'admin', '2017-10-18 16:22:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3584', '0', 'admin', '2017-10-18 16:23:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3585', '0', 'admin', '2017-10-18 16:45:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3586', '0', 'admin', '2017-10-18 16:58:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3587', '0', 'admin', '2017-10-18 17:02:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3588', '0', 'admin', '2017-10-18 17:12:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3589', '0', 'admin', '2017-10-18 17:14:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3590', '0', 'admin', '2017-10-18 17:18:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3591', '0', 'admin', '2017-10-18 17:23:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3592', '0', 'admin', '2017-10-18 17:44:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3593', '0', 'admin', '2017-10-18 17:47:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3594', '0', 'admin', '2017-10-18 17:54:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3595', '0', 'admin', '2017-10-18 17:55:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3596', '0', 'admin', '2017-10-18 18:00:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3597', '0', 'admin', '2017-10-18 20:14:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3598', '0', 'admin', '2017-10-18 20:34:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3599', '0', 'admin', '2017-10-18 20:34:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3600', '0', 'admin', '2017-10-18 21:00:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3601', '0', 'admin', '2017-10-18 21:16:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3602', '0', 'admin', '2017-10-18 22:50:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3603', '0', 'admin', '2017-10-19 09:05:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3604', '0', 'admin', '2017-10-19 09:17:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3605', '0', 'admin', '2017-10-19 09:18:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3606', '0', 'admin', '2017-10-19 09:31:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3607', '0', 'admin', '2017-10-19 09:32:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3608', '0', 'admin', '2017-10-19 09:45:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3609', '0', 'admin', '2017-10-19 09:45:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3610', '0', 'admin', '2017-10-19 09:47:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3611', '0', 'admin', '2017-10-19 09:52:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3612', '0', 'admin', '2017-10-19 10:05:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3613', '0', 'admin', '2017-10-19 10:08:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3614', '0', 'admin', '2017-10-19 10:08:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3615', '0', 'admin', '2017-10-19 10:12:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3616', '0', 'admin', '2017-10-19 10:13:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3617', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:15:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3618', '0', 'admin', '2017-10-19 10:16:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3619', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:17:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3620', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:22:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3621', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:22:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3622', '0', 'admin', '2017-10-19 10:25:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3623', '0', 'admin', '2017-10-19 10:26:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3624', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:28:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3625', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:29:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3626', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:31:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3627', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 10:36:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3628', '0', 'admin', '2017-10-19 10:38:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3629', '0', 'admin', '2017-10-19 10:54:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3630', '0', 'admin', '2017-10-19 10:54:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3631', '0', 'admin', '2017-10-19 10:57:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3632', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 11:24:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3633', '0', 'admin', '2017-10-19 11:28:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3634', '0', 'admin', '2017-10-19 11:29:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3635', '0', 'admin', '2017-10-19 11:47:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3636', '0', 'admin', '2017-10-19 11:48:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3637', '0', 'admin', '2017-10-19 12:34:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3638', '0', 'admin', '2017-10-19 12:34:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3639', '0', 'admin', '2017-10-19 12:38:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3640', '0', 'admin', '2017-10-19 12:38:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3641', '0', 'admin', '2017-10-19 14:05:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3642', '0', 'admin', '2017-10-19 14:05:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3643', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 14:07:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3644', '0', 'admin', '2017-10-19 14:21:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3645', '0', 'admin', '2017-10-19 14:21:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3646', '0', 'admin', '2017-10-19 14:30:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3647', '0', 'admin', '2017-10-19 14:43:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3648', '0', 'admin', '2017-10-19 14:43:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3649', '0', 'admin', '2017-10-19 14:44:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3650', '0', 'admin', '2017-10-19 14:48:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3651', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 14:48:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3652', '0', 'admin', '2017-10-19 14:59:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3653', '0', 'admin', '2017-10-19 15:06:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3654', '0', 'admin', '2017-10-19 15:09:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3655', '0', 'admin', '2017-10-19 15:30:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3656', '0', 'admin', '2017-10-19 15:34:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3657', '0', 'admin', '2017-10-19 15:37:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3658', '0', 'admin', '2017-10-19 15:38:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3659', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 15:55:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3660', '0', 'admin', '2017-10-19 16:36:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3661', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 16:37:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3662', '0', 'admin', '2017-10-19 17:16:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3663', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 17:36:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3664', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 17:37:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3665', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 17:38:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3666', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 17:40:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3667', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 17:57:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3668', '0', 'admin', '2017-10-19 17:58:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3669', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 18:06:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3670', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 18:35:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3671', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 19:27:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3672', '0', 'admin', '2017-10-19 19:30:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3673', '0', 'admin', '2017-10-19 19:31:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3674', '0', 'admin', '2017-10-19 19:35:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3675', '0', 'admin', '2017-10-19 19:38:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3676', '0', 'admin', '2017-10-19 20:13:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3677', '0', 'admin', '2017-10-19 20:17:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3678', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 20:17:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3679', '0', 'admin', '2017-10-19 20:24:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3680', '0', 'admin', '2017-10-19 20:26:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3681', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 20:26:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3682', '0', 'admin', '2017-10-19 20:36:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3683', '372153feac434058ac43499929482a5e', 'zs', '2017-10-19 20:51:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3684', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 09:32:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3685', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 10:07:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3686', '0', 'admin', '2017-10-20 10:11:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3687', '0', 'admin', '2017-10-20 10:21:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3688', '0', 'admin', '2017-10-20 10:55:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3689', '0', 'admin', '2017-10-20 11:16:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3690', '0', 'admin', '2017-10-20 11:17:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3691', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 14:21:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3692', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 14:44:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3693', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 15:26:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3694', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 15:32:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3695', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 16:16:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3696', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 16:29:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3697', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 17:37:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3698', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 17:47:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3699', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 19:13:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3700', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 20:24:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3701', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 20:30:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3702', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 20:45:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3703', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 21:04:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3704', '372153feac434058ac43499929482a5e', 'zs', '2017-10-20 21:17:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3705', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 09:34:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3706', '0', 'admin', '2017-10-21 09:48:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3707', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 09:56:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3708', '0', 'admin', '2017-10-21 10:26:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3709', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 10:28:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3710', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 10:34:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3711', '0', 'admin', '2017-10-21 10:42:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3712', '0', 'admin', '2017-10-21 10:51:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3713', '0', 'admin', '2017-10-21 10:54:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3714', '0', 'admin', '2017-10-21 11:27:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3715', '0', 'admin', '2017-10-21 11:48:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3716', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 11:56:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3717', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 11:56:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3718', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 14:24:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3719', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 14:43:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3720', '0', 'admin', '2017-10-21 15:14:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3721', '0', 'admin', '2017-10-21 15:40:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3722', '0', 'admin', '2017-10-21 15:49:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3723', '0', 'admin', '2017-10-21 16:16:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3724', '0', 'admin', '2017-10-21 16:20:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3725', '0', 'admin', '2017-10-21 16:23:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3726', '0', 'admin', '2017-10-21 16:35:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3727', '0', 'admin', '2017-10-21 16:58:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3728', '0', 'admin', '2017-10-21 16:58:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3729', '0', 'admin', '2017-10-21 17:10:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3730', '0', 'admin', '2017-10-21 17:34:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3731', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 17:35:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3732', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 17:45:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3733', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 17:46:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3734', '0', 'admin', '2017-10-21 17:58:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3735', '0', 'admin', '2017-10-21 18:00:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3736', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 18:04:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3737', '0', 'admin', '2017-10-21 18:06:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3738', '0', 'admin', '2017-10-21 18:10:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3739', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 18:11:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3740', '0', 'admin', '2017-10-21 18:17:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3741', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 19:37:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3742', '0', 'admin', '2017-10-21 19:39:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3743', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 20:10:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3744', '0', 'admin', '2017-10-21 20:24:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3745', '0', 'admin', '2017-10-21 20:32:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3746', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 20:39:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3747', '0', 'admin', '2017-10-21 20:47:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3748', '0', 'admin', '2017-10-21 20:49:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3749', '0', 'admin', '2017-10-21 20:50:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3750', '0', 'admin', '2017-10-21 20:59:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3751', '0', 'admin', '2017-10-21 21:01:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3752', '0', 'admin', '2017-10-21 21:07:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3753', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 21:07:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3754', '0', 'admin', '2017-10-21 21:15:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3755', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 21:21:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3756', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 21:22:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3757', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 21:28:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3758', '0', 'admin', '2017-10-21 21:39:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3759', '0', 'admin', '2017-10-21 21:59:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3760', '0', 'admin', '2017-10-21 22:05:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3761', '0', 'admin', '2017-10-21 22:15:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3762', '0', 'admin', '2017-10-21 22:17:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3763', '0', 'admin', '2017-10-21 22:19:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3764', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 22:21:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3765', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 22:22:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3766', '0', 'admin', '2017-10-21 22:26:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3767', '0', 'admin', '2017-10-21 22:27:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3768', '0', 'admin', '2017-10-21 22:42:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3769', '0', 'admin', '2017-10-21 22:50:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3770', '0', 'admin', '2017-10-21 22:51:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3771', '0', 'admin', '2017-10-21 22:56:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3772', '0', 'admin', '2017-10-21 22:56:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3773', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 23:13:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3774', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 23:15:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3775', '0', 'admin', '2017-10-21 23:19:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3776', '0', 'admin', '2017-10-21 23:19:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3777', '0', 'admin', '2017-10-21 23:22:47', '183.12.242.176');
INSERT INTO `mds_userlogin` VALUES ('3778', '0', 'admin', '2017-10-21 23:24:33', '183.12.242.176');
INSERT INTO `mds_userlogin` VALUES ('3779', '0', 'admin', '2017-10-21 23:28:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3780', '0', 'admin', '2017-10-21 23:29:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3781', '0', 'admin', '2017-10-21 23:31:00', '183.12.242.176');
INSERT INTO `mds_userlogin` VALUES ('3782', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 23:34:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3783', '0', 'admin', '2017-10-21 23:36:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3784', '0', 'admin', '2017-10-21 23:38:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3785', '0', 'admin', '2017-10-21 23:39:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3786', '0', 'admin', '2017-10-21 23:42:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3787', '0', 'admin', '2017-10-21 23:54:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3788', '372153feac434058ac43499929482a5e', 'zs', '2017-10-21 23:56:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3789', '0', 'admin', '2017-10-22 00:16:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3790', '0', 'admin', '2017-10-22 00:18:28', '14.220.120.108');
INSERT INTO `mds_userlogin` VALUES ('3791', '372153feac434058ac43499929482a5e', 'zs', '2017-10-22 00:19:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3792', '0', 'admin', '2017-10-22 00:24:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3793', '372153feac434058ac43499929482a5e', 'zs', '2017-10-22 00:50:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3794', '0', 'admin', '2017-10-22 01:04:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3795', '372153feac434058ac43499929482a5e', 'zs', '2017-10-22 01:04:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3796', '0', 'admin', '2017-10-22 09:33:39', '14.220.120.108');
INSERT INTO `mds_userlogin` VALUES ('3797', '0', 'admin', '2017-10-22 09:46:36', '183.12.242.180');
INSERT INTO `mds_userlogin` VALUES ('3798', '0', 'admin', '2017-10-22 09:47:33', '183.12.242.180');
INSERT INTO `mds_userlogin` VALUES ('3799', '0', 'admin', '2017-10-22 09:50:59', '14.220.120.108');
INSERT INTO `mds_userlogin` VALUES ('3800', '0', 'admin', '2017-10-22 09:51:55', '183.12.242.180');
INSERT INTO `mds_userlogin` VALUES ('3801', '372153feac434058ac43499929482a5e', 'zs', '2017-10-22 09:55:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3802', '0', 'admin', '2017-10-22 09:56:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3803', '0', 'admin', '2017-10-22 10:02:14', '183.12.242.180');
INSERT INTO `mds_userlogin` VALUES ('3804', '0', 'admin', '2017-10-22 10:03:23', '183.12.242.180');
INSERT INTO `mds_userlogin` VALUES ('3805', '0', 'admin', '2017-10-22 10:04:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3806', '372153feac434058ac43499929482a5e', 'zs', '2017-10-22 10:06:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3807', '372153feac434058ac43499929482a5e', 'zs', '2017-10-22 10:31:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3808', '0', 'admin', '2017-10-22 10:32:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3809', '0', 'admin', '2017-10-22 10:38:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3810', '0', 'admin', '2017-10-22 10:50:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3811', '0', 'admin', '2017-10-22 10:50:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3812', '0', 'admin', '2017-10-22 10:53:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3813', '0', 'admin', '2017-10-22 11:00:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3814', '0', 'admin', '2017-10-22 13:45:11', '14.220.120.108');
INSERT INTO `mds_userlogin` VALUES ('3815', '0', 'admin', '2017-10-22 13:46:00', '14.220.120.108');
INSERT INTO `mds_userlogin` VALUES ('3816', '0', 'admin', '2017-10-22 13:50:55', '14.220.120.108');
INSERT INTO `mds_userlogin` VALUES ('3817', '0', 'admin', '2017-10-22 13:54:15', '14.220.120.108');
INSERT INTO `mds_userlogin` VALUES ('3818', '0', 'admin', '2017-10-22 13:59:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3819', '0', 'admin', '2017-10-22 14:00:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3820', '0', 'admin', '2017-10-22 14:00:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3821', '0', 'admin', '2017-10-22 15:16:58', '14.220.123.40');
INSERT INTO `mds_userlogin` VALUES ('3822', '0', 'admin', '2017-10-22 20:54:10', '183.12.242.180');
INSERT INTO `mds_userlogin` VALUES ('3823', '0', 'admin', '2017-10-23 08:56:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3824', '0', 'admin', '2017-10-23 09:07:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3825', '0', 'admin', '2017-10-23 09:09:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3826', '0', 'admin', '2017-10-23 09:11:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3827', '0', 'admin', '2017-10-23 09:15:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3828', '0', 'admin', '2017-10-23 09:19:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3829', '0', 'admin', '2017-10-23 10:27:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3830', '0', 'admin', '2017-10-23 10:37:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3831', '0', 'admin', '2017-10-23 10:40:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3832', '0', 'admin', '2017-10-23 14:22:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3833', '0', 'admin', '2017-10-23 14:36:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3834', '0', 'admin', '2017-10-23 14:44:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3835', '0', 'admin', '2017-10-23 14:50:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3836', '0', 'admin', '2017-10-23 14:56:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3837', '0', 'admin', '2017-10-23 15:34:01', '14.220.123.40');
INSERT INTO `mds_userlogin` VALUES ('3838', '0', 'admin', '2017-10-23 15:34:31', '14.220.123.40');
INSERT INTO `mds_userlogin` VALUES ('3839', '0', 'admin', '2017-10-23 15:54:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3840', '0', 'admin', '2017-10-23 16:09:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3841', '0', 'admin', '2017-10-23 16:25:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3842', '0', 'admin', '2017-10-23 16:46:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3843', '0', 'admin', '2017-10-23 17:06:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3844', '0', 'admin', '2017-10-23 17:11:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3845', '0', 'admin', '2017-10-23 17:14:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3846', '0', 'admin', '2017-10-23 17:27:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3847', '0', 'admin', '2017-10-23 17:46:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3848', '0', 'admin', '2017-10-23 17:46:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3849', '0', 'admin', '2017-10-23 18:54:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3850', '0', 'admin', '2017-10-23 19:18:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3851', '0', 'admin', '2017-10-23 19:28:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3852', '0', 'admin', '2017-10-23 19:34:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3853', '0', 'admin', '2017-10-23 19:39:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3854', '0', 'admin', '2017-10-23 19:46:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3855', '0', 'admin', '2017-10-23 19:48:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3856', '0', 'admin', '2017-10-23 20:06:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3857', '0', 'admin', '2017-10-23 20:12:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3858', '0', 'admin', '2017-10-23 20:24:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3859', '0', 'admin', '2017-10-24 08:59:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3860', '0', 'admin', '2017-10-24 09:14:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3861', '0', 'admin', '2017-10-24 10:13:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3862', '0', 'admin', '2017-10-24 10:17:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3863', '0', 'admin', '2017-10-24 10:19:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3864', '0', 'admin', '2017-10-24 10:31:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3865', '0', 'admin', '2017-10-24 10:59:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3866', '0', 'admin', '2017-10-24 10:59:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3867', '0', 'admin', '2017-10-24 11:00:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3868', '0', 'admin', '2017-10-24 11:00:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3869', '0', 'admin', '2017-10-24 11:04:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3870', '0', 'admin', '2017-10-24 11:10:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3871', '0', 'admin', '2017-10-24 12:29:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3872', '0', 'admin', '2017-10-24 12:30:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3873', '0', 'admin', '2017-10-24 12:37:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3874', '0', 'admin', '2017-10-24 14:05:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3875', '0', 'admin', '2017-10-24 14:11:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3876', '0', 'admin', '2017-10-24 14:31:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3877', '0', 'admin', '2017-10-24 14:54:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3878', '0', 'admin', '2017-10-24 15:09:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3879', '0', 'admin', '2017-10-24 15:11:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3880', '0', 'admin', '2017-10-24 15:22:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3881', '0', 'admin', '2017-10-24 15:25:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3882', '0', 'admin', '2017-10-24 15:25:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3883', '0', 'admin', '2017-10-24 15:47:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3884', '0', 'admin', '2017-10-24 15:48:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3885', '0', 'admin', '2017-10-24 15:56:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3886', '0', 'admin', '2017-10-24 16:04:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3887', '0', 'admin', '2017-10-24 16:53:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3888', '0', 'admin', '2017-10-24 17:16:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3889', '0', 'admin', '2017-10-24 17:17:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3890', '0', 'admin', '2017-10-24 18:10:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3891', '0', 'admin', '2017-10-24 18:41:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3892', '0', 'admin', '2017-10-24 18:44:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3893', '0', 'admin', '2017-10-24 20:24:28', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('3894', '0', 'admin', '2017-10-24 20:24:36', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('3895', '0', 'admin', '2017-10-24 20:26:45', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('3896', '0', 'admin', '2017-10-24 20:32:48', '113.91.34.225');
INSERT INTO `mds_userlogin` VALUES ('3897', '0', 'admin', '2017-10-24 20:33:53', '113.91.34.225');
INSERT INTO `mds_userlogin` VALUES ('3898', '372153feac434058ac43499929482a5e', 'zs', '2017-10-24 20:34:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3899', '0', 'admin', '2017-10-24 20:35:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3900', '0', 'admin', '2017-10-24 20:35:31', '113.91.34.225');
INSERT INTO `mds_userlogin` VALUES ('3901', '372153feac434058ac43499929482a5e', 'zs', '2017-10-24 20:43:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3902', '0', 'admin', '2017-10-24 20:43:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3903', '0', 'admin', '2017-10-24 20:45:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3904', '0', 'admin', '2017-10-24 20:48:26', '113.91.34.225');
INSERT INTO `mds_userlogin` VALUES ('3905', '0', 'admin', '2017-10-25 09:05:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3906', '0', 'admin', '2017-10-25 09:29:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3907', '0', 'admin', '2017-10-25 09:30:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3908', '0', 'admin', '2017-10-25 09:31:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3909', '0', 'admin', '2017-10-25 09:54:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3910', '0', 'admin', '2017-10-25 10:08:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3911', '0', 'admin', '2017-10-25 10:34:06', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('3912', '0', 'admin', '2017-10-25 10:36:43', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('3913', '0', 'admin', '2017-10-25 10:49:41', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('3914', '0', 'admin', '2017-10-25 11:14:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3915', '0', 'admin', '2017-10-25 11:25:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3916', '0', 'admin', '2017-10-25 11:28:28', '14.220.123.163');
INSERT INTO `mds_userlogin` VALUES ('3917', '0', 'admin', '2017-10-25 11:57:20', '192.168.1.102');
INSERT INTO `mds_userlogin` VALUES ('3918', '0', 'admin', '2017-10-25 11:58:13', '192.168.1.101');
INSERT INTO `mds_userlogin` VALUES ('3919', '0', 'admin', '2017-10-25 12:50:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3920', '0', 'admin', '2017-10-25 14:00:23', '192.168.1.102');
INSERT INTO `mds_userlogin` VALUES ('3921', '0', 'admin', '2017-10-25 14:01:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3922', '0', 'admin', '2017-10-25 14:03:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3923', '0', 'admin', '2017-10-25 14:57:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3924', '0', 'admin', '2017-10-25 16:11:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3925', '0', 'admin', '2017-10-25 16:12:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3926', '0', 'admin', '2017-10-25 16:17:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3927', '0', 'admin', '2017-10-25 16:31:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3928', '0', 'admin', '2017-10-25 16:35:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3929', '0', 'admin', '2017-10-25 16:45:01', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('3930', '0', 'admin', '2017-10-25 16:45:01', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('3931', '0', 'admin', '2017-10-25 17:10:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3932', '0', 'admin', '2017-10-25 17:12:34', '223.104.63.196');
INSERT INTO `mds_userlogin` VALUES ('3933', '0', 'admin', '2017-10-25 17:12:49', '223.104.63.196');
INSERT INTO `mds_userlogin` VALUES ('3934', '0', 'admin', '2017-10-25 17:12:55', '223.104.63.196');
INSERT INTO `mds_userlogin` VALUES ('3935', '0', 'admin', '2017-10-25 17:24:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3936', '0', 'admin', '2017-10-25 17:35:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3937', '0', 'admin', '2017-10-25 17:35:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3938', '0', 'admin', '2017-10-25 17:42:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3939', '0', 'admin', '2017-10-25 18:13:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3940', '0', 'admin', '2017-10-25 19:11:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3941', '0', 'admin', '2017-10-25 19:11:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3942', '0', 'admin', '2017-10-25 19:11:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3943', '0', 'admin', '2017-10-25 19:11:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3944', '0', 'admin', '2017-10-25 19:30:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3945', '0', 'admin', '2017-10-25 19:37:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3946', '0', 'admin', '2017-10-25 20:48:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3947', '0', 'admin', '2017-10-25 20:57:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3948', '0', 'admin', '2017-10-25 20:57:59', '14.220.123.163');
INSERT INTO `mds_userlogin` VALUES ('3949', '0', 'admin', '2017-10-25 21:05:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3950', '0', 'admin', '2017-10-25 21:05:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3951', '0', 'admin', '2017-10-25 21:11:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3952', '0', 'admin', '2017-10-26 09:54:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3953', '0', 'admin', '2017-10-26 10:45:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3954', '0', 'admin', '2017-10-26 11:03:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3955', '0', 'admin', '2017-10-26 11:23:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3956', '0', 'admin', '2017-10-26 11:27:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3957', '0', 'admin', '2017-10-26 11:52:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3958', '0', 'admin', '2017-10-26 14:13:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3959', '372153feac434058ac43499929482a5e', 'zs', '2017-10-26 14:33:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3960', '372153feac434058ac43499929482a5e', 'zs', '2017-10-26 14:45:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3961', '372153feac434058ac43499929482a5e', 'zs', '2017-10-26 15:04:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3962', '0', 'admin', '2017-10-26 15:49:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3963', '372153feac434058ac43499929482a5e', 'zs', '2017-10-26 15:50:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3964', '0', 'admin', '2017-10-26 15:53:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3965', '0', 'admin', '2017-10-26 15:53:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3966', '372153feac434058ac43499929482a5e', 'zs', '2017-10-26 16:21:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3967', '0', 'admin', '2017-10-26 16:23:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3968', '0', 'admin', '2017-10-26 16:31:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3969', '0', 'admin', '2017-10-26 16:40:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3970', '0', 'admin', '2017-10-26 16:47:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3971', '0', 'admin', '2017-10-26 17:52:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3972', '0', 'admin', '2017-10-26 18:40:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3973', '0', 'admin', '2017-10-26 19:23:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3974', '0', 'admin', '2017-10-26 19:23:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3975', '0', 'admin', '2017-10-26 19:30:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3976', '0', 'admin', '2017-10-26 20:03:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3977', '0', 'admin', '2017-10-26 20:35:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3978', '0', 'admin', '2017-10-26 20:37:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3979', '0', 'admin', '2017-10-26 20:39:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3980', '0', 'admin', '2017-10-26 20:41:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3981', '0', 'admin', '2017-10-26 21:17:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3982', '0', 'admin', '2017-10-26 22:08:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3983', '0', 'admin', '2017-10-26 22:14:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3984', '0', 'admin', '2017-10-26 23:39:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3985', '0', 'admin', '2017-10-26 23:46:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3986', '0', 'admin', '2017-10-27 00:30:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3987', '0', 'admin', '2017-10-27 09:33:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3988', '0', 'admin', '2017-10-27 10:10:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3989', '0', 'admin', '2017-10-27 11:25:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3990', '0', 'admin', '2017-10-27 13:27:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3991', '0', 'admin', '2017-10-27 15:35:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3992', '0', 'admin', '2017-10-27 16:29:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3993', '0', 'admin', '2017-10-27 17:17:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3994', '0', 'admin', '2017-10-27 17:26:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3995', '0', 'admin', '2017-10-27 18:00:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3996', '0', 'admin', '2017-10-27 18:44:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3997', '0', 'admin', '2017-10-27 18:55:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3998', '0', 'admin', '2017-10-27 19:02:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('3999', '0', 'admin', '2017-10-27 19:03:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4000', '0', 'admin', '2017-10-27 19:22:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4001', '0', 'admin', '2017-10-27 19:25:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4002', '0', 'admin', '2017-10-27 19:31:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4003', '0', 'admin', '2017-10-27 19:31:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4004', '0', 'admin', '2017-10-27 19:34:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4005', '0', 'admin', '2017-10-27 20:00:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4006', '0', 'admin', '2017-10-27 20:05:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4007', '0', 'admin', '2017-10-27 20:07:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4008', '0', 'admin', '2017-10-27 20:09:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4009', '0', 'admin', '2017-10-27 20:11:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4010', '0', 'admin', '2017-10-27 20:15:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4011', '0', 'admin', '2017-10-27 20:17:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4012', '0', 'admin', '2017-10-27 20:20:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4013', '0', 'admin', '2017-10-27 20:23:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4014', '0', 'admin', '2017-10-27 20:24:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4015', '0', 'admin', '2017-10-27 22:12:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4016', '0', 'admin', '2017-10-27 22:14:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4017', '0', 'admin', '2017-10-27 22:17:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4018', '0', 'admin', '2017-10-27 22:40:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4019', '0', 'admin', '2017-10-27 23:19:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4020', '0', 'admin', '2017-10-27 23:26:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4021', '0', 'admin', '2017-10-27 23:28:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4022', '0', 'admin', '2017-10-27 23:30:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4023', '0', 'admin', '2017-10-27 23:32:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4024', '0', 'admin', '2017-10-28 16:30:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4025', '0', 'admin', '2017-10-28 16:50:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4026', '0', 'admin', '2017-10-28 18:12:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4027', '0', 'admin', '2017-10-28 18:36:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4028', '0', 'admin', '2017-10-28 18:43:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4029', '0', 'admin', '2017-10-28 18:49:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4030', '372153feac434058ac43499929482a5e', 'zs', '2017-10-29 15:00:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4031', '372153feac434058ac43499929482a5e', 'zs', '2017-10-29 15:04:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4032', '372153feac434058ac43499929482a5e', 'zs', '2017-10-29 15:21:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4033', '372153feac434058ac43499929482a5e', 'zs', '2017-10-29 15:23:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4034', '372153feac434058ac43499929482a5e', 'zs', '2017-10-29 15:33:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4035', '372153feac434058ac43499929482a5e', 'zs', '2017-10-29 15:36:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4036', '0', 'admin', '2017-10-29 15:55:55', '113.91.36.52');
INSERT INTO `mds_userlogin` VALUES ('4037', '0', 'admin', '2017-10-29 15:56:27', '113.91.36.52');
INSERT INTO `mds_userlogin` VALUES ('4038', '0', 'admin', '2017-10-29 18:37:22', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4039', '0', 'admin', '2017-10-29 18:38:05', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4040', '0', 'admin', '2017-10-29 18:41:06', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4041', '0', 'admin', '2017-10-29 18:45:13', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4042', '0', 'admin', '2017-10-29 18:57:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4043', '0', 'admin', '2017-10-29 19:02:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4044', '0', 'admin', '2017-10-29 19:05:55', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4045', '0', 'admin', '2017-10-29 20:05:48', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4046', '0', 'admin', '2017-10-29 20:06:47', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4047', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-29 20:09:21', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4048', '0', 'admin', '2017-10-29 20:10:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4049', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-29 20:37:20', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4050', '0', 'admin', '2017-10-29 22:40:48', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4051', '0', 'admin', '2017-10-29 22:42:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4052', '0', 'admin', '2017-10-29 22:47:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4053', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-29 22:53:24', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4054', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-29 22:57:08', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4055', '0', 'admin', '2017-10-30 09:02:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4056', '0', 'admin', '2017-10-30 09:20:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4057', '0', 'admin', '2017-10-30 09:22:51', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4058', '0', 'admin', '2017-10-30 09:29:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4059', '0', 'admin', '2017-10-30 09:29:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4060', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 09:59:14', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('4061', '0', 'admin', '2017-10-30 10:40:29', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4062', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 10:40:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4063', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 10:42:08', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4064', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 10:51:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4065', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 10:53:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4066', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 11:00:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4067', '0', 'admin', '2017-10-30 11:33:32', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4068', '0', 'admin', '2017-10-30 11:36:02', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4069', '0', 'admin', '2017-10-30 11:36:26', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4070', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 11:43:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4071', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 11:51:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4072', '0', 'admin', '2017-10-30 14:02:50', '14.220.120.173');
INSERT INTO `mds_userlogin` VALUES ('4073', '0', 'admin', '2017-10-30 14:03:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4074', '0', 'admin', '2017-10-30 14:03:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4075', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 14:23:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4076', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 14:28:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4077', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 14:46:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4078', '0', 'admin', '2017-10-30 14:57:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4079', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 15:01:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4080', '0', 'admin', '2017-10-30 15:40:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4081', '0', 'admin', '2017-10-30 15:43:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4082', '0', 'admin', '2017-10-30 15:45:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4083', '0', 'admin', '2017-10-30 15:45:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4084', '0', 'admin', '2017-10-30 15:47:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4085', '0', 'admin', '2017-10-30 15:47:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4086', '0', 'admin', '2017-10-30 15:47:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4087', '0', 'admin', '2017-10-30 15:49:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4088', '0', 'admin', '2017-10-30 15:49:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4089', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 16:37:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4090', '0', 'admin', '2017-10-30 17:01:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4091', '0', 'admin', '2017-10-30 17:06:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4092', '0', 'admin', '2017-10-30 17:26:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4093', '0', 'admin', '2017-10-30 17:26:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4094', '0', 'admin', '2017-10-30 17:51:08', '192.168.1.102');
INSERT INTO `mds_userlogin` VALUES ('4095', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 17:53:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4096', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 17:58:10', '192.168.1.101');
INSERT INTO `mds_userlogin` VALUES ('4097', '0', 'admin', '2017-10-30 17:59:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4098', '0', 'admin', '2017-10-30 18:00:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4099', '0', 'admin', '2017-10-30 18:00:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4100', '0', 'admin', '2017-10-30 18:01:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4101', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 18:05:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4102', '0', 'admin', '2017-10-30 18:07:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4103', '0', 'admin', '2017-10-30 18:37:52', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('4104', '0', 'admin', '2017-10-30 18:38:53', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('4105', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 19:53:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4106', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 20:13:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4107', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 20:46:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4108', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 21:51:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4109', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 21:55:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4110', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:08:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4111', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:15:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4112', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:18:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4113', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:21:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4114', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:28:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4115', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:31:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4116', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:37:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4117', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-30 23:42:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4118', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-31 09:21:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4119', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-31 10:31:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4120', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-31 11:28:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4121', '2ad2d59e3c3440598e34b1866ec63df7', '123', '2017-10-31 14:36:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4122', '0', 'admin', '2017-10-31 14:40:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4123', '0', 'admin', '2017-10-31 14:43:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4124', '0', 'admin', '2017-10-31 14:43:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4125', '0', 'admin', '2017-10-31 14:53:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4126', '0', 'admin', '2017-10-31 14:53:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4127', '0', 'admin', '2017-10-31 14:57:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4128', '0', 'admin', '2017-10-31 14:59:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4129', '0', 'admin', '2017-10-31 15:20:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4130', '0', 'admin', '2017-10-31 15:29:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4131', '0', 'admin', '2017-10-31 16:05:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4132', '0', 'admin', '2017-10-31 16:06:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4133', '0', 'admin', '2017-10-31 16:06:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4134', '0', 'admin', '2017-10-31 16:59:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4135', '0', 'admin', '2017-10-31 17:20:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4136', '0', 'admin', '2017-10-31 17:25:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4137', '0', 'admin', '2017-10-31 17:27:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4138', '0', 'admin', '2017-10-31 17:28:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4139', '0', 'admin', '2017-10-31 17:35:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4140', '0', 'admin', '2017-10-31 17:49:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4141', '0', 'admin', '2017-10-31 17:56:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4142', '0', 'admin', '2017-10-31 17:56:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4143', '0', 'admin', '2017-10-31 17:57:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4144', '0', 'admin', '2017-10-31 17:57:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4145', '0', 'admin', '2017-10-31 18:33:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4146', '0', 'admin', '2017-10-31 19:28:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4147', '0', 'admin', '2017-10-31 21:28:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4148', '0', 'admin', '2017-11-01 09:39:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4149', '0', 'admin', '2017-11-01 09:39:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4150', '0', 'admin', '2017-11-01 09:48:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4151', '0', 'admin', '2017-11-01 10:26:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4152', '0', 'admin', '2017-11-01 10:28:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4153', '0', 'admin', '2017-11-01 10:31:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4154', '0', 'admin', '2017-11-01 11:38:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4155', '0', 'admin', '2017-11-01 11:42:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4156', '0', 'admin', '2017-11-01 11:44:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4157', '0', 'admin', '2017-11-01 14:14:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4158', '0', 'admin', '2017-11-01 14:14:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4159', '0', 'admin', '2017-11-01 14:33:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4160', '0', 'admin', '2017-11-01 14:49:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4161', '0', 'admin', '2017-11-01 14:55:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4162', '0', 'admin', '2017-11-01 15:22:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4163', '0', 'admin', '2017-11-01 15:36:09', '14.220.120.106');
INSERT INTO `mds_userlogin` VALUES ('4164', '0', 'admin', '2017-11-01 15:58:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4165', '0', 'admin', '2017-11-01 16:04:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4166', '0', 'admin', '2017-11-01 16:33:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4167', '0', 'admin', '2017-11-01 16:33:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4168', '0', 'admin', '2017-11-01 17:14:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4169', '0', 'admin', '2017-11-01 18:46:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4170', '0', 'admin', '2017-11-01 18:50:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4171', '0', 'admin', '2017-11-01 19:12:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4172', '0', 'admin', '2017-11-01 19:51:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4173', '0', 'admin', '2017-11-01 19:54:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4174', '0', 'admin', '2017-11-01 20:24:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4175', '0', 'admin', '2017-11-01 20:37:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4176', '0', 'admin', '2017-11-01 21:11:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4177', '0', 'admin', '2017-11-01 21:13:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4178', '0', 'admin', '2017-11-01 21:17:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4179', '0', 'admin', '2017-11-02 11:11:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4180', '0', 'admin', '2017-11-02 11:30:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4181', '0', 'admin', '2017-11-02 14:04:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4182', '0', 'admin', '2017-11-02 15:02:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4183', '0', 'admin', '2017-11-02 15:35:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4184', '0', 'admin', '2017-11-02 15:55:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4185', '0', 'admin', '2017-11-02 15:57:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4186', '0', 'admin', '2017-11-02 16:10:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4187', '0', 'admin', '2017-11-02 16:13:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4188', '0', 'admin', '2017-11-02 16:25:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4189', '0', 'admin', '2017-11-02 16:35:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4190', '0', 'admin', '2017-11-02 16:44:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4191', '0', 'admin', '2017-11-02 17:00:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4192', '0', 'admin', '2017-11-02 17:14:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4193', '0', 'admin', '2017-11-02 17:25:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4194', '0', 'admin', '2017-11-02 17:34:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4195', '0', 'admin', '2017-11-02 17:44:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4196', '0', 'admin', '2017-11-02 18:13:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4197', '0', 'admin', '2017-11-02 19:17:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4198', '0', 'admin', '2017-11-02 19:20:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4199', '0', 'admin', '2017-11-02 19:20:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4200', '0', 'admin', '2017-11-02 21:02:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4201', '0', 'admin', '2017-11-02 22:46:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4202', '0', 'admin', '2017-11-02 22:51:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4203', '0', 'admin', '2017-11-02 22:53:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4204', '0', 'admin', '2017-11-03 00:01:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4205', '0', 'admin', '2017-11-03 00:10:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4206', '0', 'admin', '2017-11-03 00:14:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4207', '0', 'admin', '2017-11-03 09:19:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4208', '0', 'admin', '2017-11-03 09:24:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4209', '0', 'admin', '2017-11-03 09:53:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4210', '0', 'admin', '2017-11-03 10:00:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4211', '0', 'admin', '2017-11-03 10:07:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4212', '0', 'admin', '2017-11-03 10:16:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4213', '0', 'admin', '2017-11-03 10:21:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4214', '0', 'admin', '2017-11-03 10:22:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4215', '0', 'admin', '2017-11-03 10:39:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4216', '0', 'admin', '2017-11-03 10:41:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4217', '0', 'admin', '2017-11-03 10:49:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4218', '0', 'admin', '2017-11-03 10:56:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4219', '0', 'admin', '2017-11-03 11:04:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4220', '0', 'admin', '2017-11-03 11:15:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4221', '0', 'admin', '2017-11-03 11:34:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4222', '0', 'admin', '2017-11-03 14:01:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4223', '0', 'admin', '2017-11-03 14:08:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4224', '0', 'admin', '2017-11-03 14:26:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4225', '0', 'admin', '2017-11-03 14:44:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4226', '0', 'admin', '2017-11-03 15:11:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4227', '0', 'admin', '2017-11-03 15:11:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4228', '0', 'admin', '2017-11-03 15:34:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4229', '0', 'admin', '2017-11-03 16:16:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4230', '0', 'admin', '2017-11-03 16:32:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4231', '0', 'admin', '2017-11-03 16:33:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4232', '0', 'admin', '2017-11-03 16:46:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4233', '0', 'admin', '2017-11-03 17:14:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4234', '0', 'admin', '2017-11-03 17:36:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4235', '0', 'admin', '2017-11-03 17:36:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4236', '0', 'admin', '2017-11-03 17:55:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4237', '0', 'admin', '2017-11-03 18:20:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4238', '0', 'admin', '2017-11-03 19:06:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4239', '0', 'admin', '2017-11-03 19:33:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4240', '0', 'admin', '2017-11-03 19:47:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4241', '0', 'admin', '2017-11-03 19:51:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4242', '0', 'admin', '2017-11-03 19:57:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4243', '0', 'admin', '2017-11-03 20:01:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4244', '0', 'admin', '2017-11-03 20:01:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4245', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 20:02:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4246', '0', 'admin', '2017-11-03 20:17:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4247', '0', 'admin', '2017-11-03 20:21:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4248', '0', 'admin', '2017-11-03 20:25:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4249', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 20:26:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4250', '0', 'admin', '2017-11-03 20:27:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4251', '0', 'admin', '2017-11-03 20:28:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4252', '0', 'admin', '2017-11-03 20:29:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4253', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 20:30:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4254', '0', 'admin', '2017-11-03 20:31:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4255', '0', 'admin', '2017-11-03 20:31:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4256', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 20:31:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4257', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 20:48:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4258', '0', 'admin', '2017-11-03 20:54:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4259', '0', 'admin', '2017-11-03 20:55:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4260', '0', 'admin', '2017-11-03 20:56:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4261', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 20:56:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4262', '0', 'admin', '2017-11-03 20:59:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4263', '0', 'admin', '2017-11-03 21:34:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4264', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 21:43:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4265', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 21:47:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4266', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 21:55:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4267', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 22:02:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4268', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 22:04:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4269', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 22:07:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4270', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 22:33:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4271', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 22:56:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4272', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 23:05:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4273', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-03 23:10:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4274', '0', 'admin', '2017-11-03 23:11:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4275', '0', 'admin', '2017-11-03 23:14:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4276', '0', 'admin', '2017-11-04 09:08:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4277', '0', 'admin', '2017-11-04 09:14:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4278', '0', 'admin', '2017-11-04 10:18:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4279', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 10:18:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4280', '0', 'admin', '2017-11-04 10:18:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4281', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 10:20:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4282', '0', 'admin', '2017-11-04 10:36:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4283', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 10:37:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4284', '0', 'admin', '2017-11-04 10:44:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4285', '0', 'admin', '2017-11-04 10:50:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4286', '0', 'admin', '2017-11-04 10:56:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4287', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 10:56:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4288', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 11:02:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4289', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 11:08:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4290', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 11:18:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4291', '0', 'admin', '2017-11-04 11:20:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4292', '0', 'admin', '2017-11-04 11:26:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4293', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 11:29:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4294', '0', 'admin', '2017-11-04 11:32:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4295', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 13:16:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4296', '0', 'admin', '2017-11-04 13:16:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4297', '0', 'admin', '2017-11-04 14:04:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4298', '0', 'admin', '2017-11-04 14:05:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4299', '0', 'admin', '2017-11-04 14:10:55', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4300', '0', 'admin', '2017-11-04 14:11:10', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4301', '0', 'admin', '2017-11-04 14:38:32', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4302', '0', 'admin', '2017-11-04 14:38:45', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4303', '0', 'admin', '2017-11-04 14:44:14', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4304', '0', 'admin', '2017-11-04 14:45:21', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4305', '0', 'admin', '2017-11-04 14:46:09', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4306', '0', 'admin', '2017-11-04 14:47:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4307', '0', 'admin', '2017-11-04 15:36:19', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4308', '0', 'admin', '2017-11-04 15:47:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4309', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 15:47:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4310', '0', 'admin', '2017-11-04 16:56:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4311', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 17:00:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4312', '0', 'admin', '2017-11-04 17:02:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4313', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 17:05:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4314', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 17:11:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4315', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 17:13:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4316', '0', 'admin', '2017-11-04 17:18:31', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4317', '0', 'admin', '2017-11-04 17:18:45', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4318', '0', 'admin', '2017-11-04 17:19:12', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4319', '0', 'admin', '2017-11-04 17:19:40', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4320', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-04 17:20:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4321', '0', 'admin', '2017-11-04 17:26:49', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4322', '0', 'admin', '2017-11-04 17:29:08', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4323', '0', 'admin', '2017-11-04 21:55:29', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4324', '0', 'admin', '2017-11-04 22:19:59', '183.38.57.132');
INSERT INTO `mds_userlogin` VALUES ('4325', '0', 'admin', '2017-11-04 22:20:02', '14.220.122.254');
INSERT INTO `mds_userlogin` VALUES ('4326', '0', 'admin', '2017-11-05 22:55:32', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4327', '0', 'admin', '2017-11-05 22:57:46', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4328', '0', 'admin', '2017-11-05 22:58:04', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4329', '0', 'admin', '2017-11-05 23:38:07', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4330', '0', 'admin', '2017-11-05 23:39:52', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4331', '0', 'admin', '2017-11-06 09:08:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4332', '0', 'admin', '2017-11-06 09:08:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4333', '0', 'admin', '2017-11-06 09:09:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4334', '0', 'admin', '2017-11-06 09:09:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4335', '0', 'admin', '2017-11-06 09:10:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4336', '0', 'admin', '2017-11-06 09:17:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4337', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 09:21:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4338', '0', 'admin', '2017-11-06 09:21:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4339', '0', 'admin', '2017-11-06 09:24:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4340', '0', 'admin', '2017-11-06 09:25:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4341', '0', 'admin', '2017-11-06 09:28:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4342', '0', 'admin', '2017-11-06 09:53:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4343', '0', 'admin', '2017-11-06 09:55:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4344', '0', 'admin', '2017-11-06 09:55:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4345', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 09:58:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4346', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 09:59:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4347', '0', 'admin', '2017-11-06 10:30:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4348', '0', 'admin', '2017-11-06 10:31:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4349', '0', 'admin', '2017-11-06 10:49:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4350', '0', 'admin', '2017-11-06 11:03:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4351', '0', 'admin', '2017-11-06 11:04:47', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4352', '0', 'admin', '2017-11-06 11:06:33', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4353', '0', 'admin', '2017-11-06 11:08:19', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4354', '0', 'admin', '2017-11-06 11:09:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4355', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 11:27:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4356', '0', 'admin', '2017-11-06 11:27:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4357', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 11:31:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4358', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 11:32:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4359', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 11:34:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4360', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 11:41:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4361', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 11:51:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4362', '0', 'admin', '2017-11-06 11:55:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4363', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:06:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4364', '0', 'admin', '2017-11-06 14:22:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4365', '0', 'admin', '2017-11-06 14:27:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4366', '0', 'admin', '2017-11-06 14:29:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4367', '0', 'admin', '2017-11-06 14:30:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4368', '0', 'admin', '2017-11-06 14:32:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4369', '0', 'admin', '2017-11-06 14:32:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4370', '0', 'admin', '2017-11-06 14:34:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4371', '0', 'admin', '2017-11-06 14:37:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4372', '0', 'admin', '2017-11-06 14:37:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4373', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:40:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4374', '0', 'admin', '2017-11-06 14:41:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4375', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:42:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4376', '0', 'admin', '2017-11-06 14:42:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4377', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:42:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4378', '0', 'admin', '2017-11-06 14:43:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4379', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:44:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4380', '0', 'admin', '2017-11-06 14:45:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4381', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:46:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4382', '0', 'admin', '2017-11-06 14:46:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4383', '0', 'admin', '2017-11-06 14:48:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4384', '0', 'admin', '2017-11-06 14:48:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4385', '0', 'admin', '2017-11-06 14:49:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4386', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:49:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4387', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:54:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4388', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 14:57:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4389', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 15:09:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4390', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 15:10:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4391', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 15:13:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4392', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 15:13:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4393', '0', 'admin', '2017-11-06 15:15:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4394', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 16:05:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4395', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 16:06:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4396', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 16:07:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4397', '0', 'admin', '2017-11-06 16:07:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4398', '0', 'admin', '2017-11-06 16:10:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4399', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 16:10:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4400', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 16:12:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4401', '0', 'admin', '2017-11-06 16:14:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4402', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 16:14:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4403', '0', 'admin', '2017-11-06 16:15:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4404', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 16:18:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4405', '0', 'admin', '2017-11-06 16:52:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4406', '0', 'admin', '2017-11-06 17:05:30', '14.220.122.222');
INSERT INTO `mds_userlogin` VALUES ('4407', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 17:22:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4408', '0', 'admin', '2017-11-06 17:23:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4409', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 17:29:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4410', '0', 'admin', '2017-11-06 17:31:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4411', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 17:31:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4412', '0', 'admin', '2017-11-06 17:45:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4413', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 17:49:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4414', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:11:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4415', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:14:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4416', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:30:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4417', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:32:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4418', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:34:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4419', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:38:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4420', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:55:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4421', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 18:58:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4422', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-06 19:04:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4423', '0', 'admin', '2017-11-07 09:15:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4424', '0', 'admin', '2017-11-07 09:21:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4425', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 09:25:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4426', '0', 'admin', '2017-11-07 09:46:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4427', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 09:46:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4428', '0', 'admin', '2017-11-07 09:46:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4429', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 09:47:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4430', '0', 'admin', '2017-11-07 09:47:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4431', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 09:48:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4432', '0', 'admin', '2017-11-07 09:50:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4433', '0', 'admin', '2017-11-07 10:14:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4434', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 10:20:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4435', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 10:54:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4436', '0', 'admin', '2017-11-07 10:54:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4437', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 10:56:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4438', '0', 'admin', '2017-11-07 10:57:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4439', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 11:01:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4440', '0', 'admin', '2017-11-07 11:06:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4441', '0', 'admin', '2017-11-07 11:11:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4442', '0', 'admin', '2017-11-07 11:18:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4443', '0', 'admin', '2017-11-07 11:28:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4444', '0', 'admin', '2017-11-07 11:28:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4445', '0', 'admin', '2017-11-07 14:04:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4446', '0', 'admin', '2017-11-07 14:12:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4447', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 14:29:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4448', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 14:30:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4449', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 14:32:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4450', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 16:26:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4451', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 16:26:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4452', '0', 'admin', '2017-11-07 16:28:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4453', '0', 'admin', '2017-11-07 16:28:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4454', '0', 'admin', '2017-11-07 17:01:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4455', '0', 'admin', '2017-11-07 17:04:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4456', '0', 'admin', '2017-11-07 17:11:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4457', '0', 'admin', '2017-11-07 17:21:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4458', '0', 'admin', '2017-11-07 17:21:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4459', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 17:21:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4460', '0', 'admin', '2017-11-07 17:24:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4461', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-07 17:43:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4462', '0', 'admin', '2017-11-07 17:52:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4463', '0', 'admin', '2017-11-07 17:52:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4464', '0', 'admin', '2017-11-07 18:04:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4465', '0', 'admin', '2017-11-07 18:04:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4466', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-08 09:16:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4467', '0', 'admin', '2017-11-08 09:24:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4468', '0', 'admin', '2017-11-08 09:24:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4469', '0', 'admin', '2017-11-08 09:27:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4470', '0', 'admin', '2017-11-08 09:28:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4471', '0', 'admin', '2017-11-08 09:34:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4472', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-08 09:35:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4473', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-08 09:35:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4474', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-08 09:36:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4475', '0', 'admin', '2017-11-08 10:06:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4476', '0', 'admin', '2017-11-08 10:06:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4477', '0', 'admin', '2017-11-08 10:14:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4478', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 10:17:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4479', '0', 'admin', '2017-11-08 10:21:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4480', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 10:27:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4481', '0', 'admin', '2017-11-08 10:37:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4482', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 11:01:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4483', '0', 'admin', '2017-11-08 11:05:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4484', '0', 'admin', '2017-11-08 11:11:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4485', '0', 'admin', '2017-11-08 11:11:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4486', '0', 'admin', '2017-11-08 11:20:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4487', '0', 'admin', '2017-11-08 11:43:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4488', '0', 'admin', '2017-11-08 11:52:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4489', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 14:01:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4490', '0', 'admin', '2017-11-08 14:06:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4491', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 14:22:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4492', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 14:23:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4493', '0', 'admin', '2017-11-08 14:27:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4494', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 14:29:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4495', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 14:30:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4496', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 14:31:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4497', '0', 'admin', '2017-11-08 14:33:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4498', '0', 'admin', '2017-11-08 14:34:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4499', '0', 'admin', '2017-11-08 14:35:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4500', '0', 'admin', '2017-11-08 14:46:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4501', '0', 'admin', '2017-11-08 14:59:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4502', '0', 'admin', '2017-11-08 15:15:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4503', '0', 'admin', '2017-11-08 15:25:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4504', '0', 'admin', '2017-11-08 15:27:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4505', '0', 'admin', '2017-11-08 15:30:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4506', '0', 'admin', '2017-11-08 15:40:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4507', '0', 'admin', '2017-11-08 16:02:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4508', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 16:08:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4509', '0', 'admin', '2017-11-08 16:18:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4510', '0', 'admin', '2017-11-08 16:38:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4511', '0', 'admin', '2017-11-08 16:50:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4512', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 16:54:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4513', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 16:58:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4514', '0', 'admin', '2017-11-08 17:09:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4515', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 17:15:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4516', '0', 'admin', '2017-11-08 17:18:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4517', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 17:32:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4518', '0', 'admin', '2017-11-08 17:37:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4519', '0', 'admin', '2017-11-08 17:46:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4520', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 17:53:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4521', '0', 'admin', '2017-11-08 17:55:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4522', '0', 'admin', '2017-11-08 17:56:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4523', '0', 'admin', '2017-11-08 18:07:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4524', '0', 'admin', '2017-11-08 18:08:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4525', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 18:09:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4526', '0', 'admin', '2017-11-08 18:13:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4527', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 18:19:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4528', '0', 'admin', '2017-11-08 18:21:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4529', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-08 18:22:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4530', '0', 'admin', '2017-11-08 18:22:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4531', '0', 'admin', '2017-11-08 20:51:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4532', '0', 'admin', '2017-11-08 20:57:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4533', '0', 'admin', '2017-11-08 22:58:09', '14.220.121.106');
INSERT INTO `mds_userlogin` VALUES ('4534', '0', 'admin', '2017-11-08 22:59:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4535', '0', 'admin', '2017-11-09 09:07:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4536', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-09 09:20:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4537', '0', 'admin', '2017-11-09 09:23:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4538', '0', 'admin', '2017-11-09 09:32:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4539', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-09 09:41:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4540', '0', 'admin', '2017-11-09 09:41:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4541', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-09 09:44:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4542', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-09 09:45:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4543', '0', 'admin', '2017-11-09 09:46:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4544', '0', 'admin', '2017-11-09 09:49:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4545', '0', 'admin', '2017-11-09 10:20:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4546', '0', 'admin', '2017-11-09 10:23:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4547', '0', 'admin', '2017-11-09 10:54:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4548', '0', 'admin', '2017-11-09 10:55:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4549', '0', 'admin', '2017-11-09 10:56:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4550', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-09 11:21:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4551', '0', 'admin', '2017-11-09 13:14:10', '14.220.121.106');
INSERT INTO `mds_userlogin` VALUES ('4552', 'f09bab99843a4d7ab91081ab91f3eb72', 'zoufa', '2017-11-09 14:09:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4553', '0', 'admin', '2017-11-09 14:47:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4554', '0', 'admin', '2017-11-09 15:48:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4555', '0', 'admin', '2017-11-09 16:01:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4556', '0', 'admin', '2017-11-09 16:17:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4557', '0', 'admin', '2017-11-09 16:35:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4558', '0', 'admin', '2017-11-09 16:52:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4559', '0', 'admin', '2017-11-09 17:00:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4560', '0', 'admin', '2017-11-09 17:02:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4561', '0', 'admin', '2017-11-09 17:10:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4562', '0', 'admin', '2017-11-09 17:15:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4563', '0', 'admin', '2017-11-09 17:24:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4564', '0', 'admin', '2017-11-09 17:38:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4565', '0', 'admin', '2017-11-09 17:45:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4566', '0', 'admin', '2017-11-10 09:18:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4567', '0', 'admin', '2017-11-10 10:22:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4568', '0', 'admin', '2017-11-10 10:27:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4569', '0', 'admin', '2017-11-10 10:27:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4570', '0', 'admin', '2017-11-10 10:30:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4571', '0', 'admin', '2017-11-10 10:47:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4572', '0', 'admin', '2017-11-10 10:51:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4573', '0', 'admin', '2017-11-10 11:27:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4574', '0', 'admin', '2017-11-10 11:50:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4575', '0', 'admin', '2017-11-10 15:03:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4576', '0', 'admin', '2017-11-10 15:12:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4577', '0', 'admin', '2017-11-10 15:27:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4578', '0', 'admin', '2017-11-10 15:40:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4579', '0', 'admin', '2017-11-10 16:49:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4580', '0', 'admin', '2017-11-10 17:14:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4581', '0', 'admin', '2017-11-10 17:34:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4582', '0', 'admin', '2017-11-10 17:37:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4583', '0', 'admin', '2017-11-10 19:03:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4584', '0', 'admin', '2017-11-10 19:13:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4585', '0', 'admin', '2017-11-10 19:19:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4586', '0', 'admin', '2017-11-10 20:03:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4587', '0', 'admin', '2017-11-10 20:11:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4588', '0', 'admin', '2017-11-10 20:12:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4589', '0', 'admin', '2017-11-10 20:12:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4590', '0', 'admin', '2017-11-10 20:13:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4591', '0', 'admin', '2017-11-10 20:13:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4592', '0', 'admin', '2017-11-10 20:13:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4593', '0', 'admin', '2017-11-10 20:13:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4594', '0', 'admin', '2017-11-10 20:15:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4595', '0', 'admin', '2017-11-10 20:16:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4596', '0', 'admin', '2017-11-10 20:21:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4597', '0', 'admin', '2017-11-10 20:21:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4598', '0', 'admin', '2017-11-10 20:22:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4599', '0', 'admin', '2017-11-11 09:13:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4600', '0', 'admin', '2017-11-11 09:46:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4601', '0', 'admin', '2017-11-11 09:56:31', '117.136.76.225');
INSERT INTO `mds_userlogin` VALUES ('4602', '0', 'admin', '2017-11-11 10:25:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4603', '0', 'admin', '2017-11-11 11:41:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4604', '0', 'admin', '2017-11-11 11:48:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4605', '0', 'admin', '2017-11-11 14:02:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4606', '0', 'admin', '2017-11-11 14:54:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4607', '0', 'admin', '2017-11-11 15:05:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4608', '0', 'admin', '2017-11-11 15:06:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4609', '0', 'admin', '2017-11-11 15:10:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4610', '0', 'admin', '2017-11-11 15:10:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4611', '0', 'admin', '2017-11-11 15:22:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4612', '0', 'admin', '2017-11-11 15:33:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4613', '0', 'admin', '2017-11-11 15:37:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4614', '0', 'admin', '2017-11-11 15:41:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4615', '0', 'admin', '2017-11-11 16:22:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4616', '0', 'admin', '2017-11-11 16:55:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4617', '0', 'admin', '2017-11-11 17:06:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4618', '0', 'admin', '2017-11-12 13:10:51', '14.220.122.178');
INSERT INTO `mds_userlogin` VALUES ('4619', '0', 'admin', '2017-11-12 13:13:15', '14.220.122.178');
INSERT INTO `mds_userlogin` VALUES ('4620', '0', 'admin', '2017-11-12 16:00:31', '14.220.122.178');
INSERT INTO `mds_userlogin` VALUES ('4621', '0', 'admin', '2017-11-12 17:14:58', '14.220.122.178');
INSERT INTO `mds_userlogin` VALUES ('4622', '0', 'admin', '2017-11-13 16:34:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4623', '0', 'admin', '2017-11-13 16:40:40', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4624', '0', 'admin', '2017-11-13 16:48:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4625', '0', 'admin', '2017-11-13 16:48:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4626', '0', 'admin', '2017-11-13 16:52:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4627', '0', 'admin', '2017-11-13 16:54:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4628', '0', 'admin', '2017-11-13 16:55:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4629', '0', 'admin', '2017-11-13 16:58:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4630', '0', 'admin', '2017-11-13 17:01:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4631', '0', 'admin', '2017-11-13 17:05:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4632', '0', 'admin', '2017-11-13 17:08:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4633', '0', 'admin', '2017-11-13 17:09:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4634', '0', 'admin', '2017-11-13 17:16:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4635', 'daa00cd9a9ce48e7aea8682efcbe0762', 'z0001', '2017-11-13 17:16:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4636', '0', 'admin', '2017-11-13 17:17:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4637', '0', 'admin', '2017-11-13 17:18:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4638', '0', 'admin', '2017-11-13 17:19:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4639', '0', 'admin', '2017-11-13 17:19:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4640', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:20:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4641', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:21:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4642', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:31:50', '14.220.123.240');
INSERT INTO `mds_userlogin` VALUES ('4643', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:32:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4644', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:36:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4645', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:44:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4646', '0', 'admin', '2017-11-13 17:44:47', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4647', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:53:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4648', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:56:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4649', '0', 'admin', '2017-11-13 17:58:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4650', '7e888a1bb0604a4682ca34a2bc62ef64', 'z0002', '2017-11-13 17:59:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4651', '0', 'admin', '2017-11-13 18:00:30', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4652', '0', 'admin', '2017-11-14 14:46:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4653', '0', 'admin', '2017-11-14 14:56:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4654', '0', 'admin', '2017-11-14 15:28:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4655', '0', 'admin', '2017-11-14 16:20:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4656', '0', 'admin', '2017-11-14 16:48:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4657', '0', 'admin', '2017-11-14 16:49:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4658', '0', 'admin', '2017-11-15 09:35:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4659', '0', 'admin', '2017-11-15 09:39:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4660', '0', 'admin', '2017-11-15 09:41:24', '14.220.123.240');
INSERT INTO `mds_userlogin` VALUES ('4661', '0', 'admin', '2017-11-15 09:42:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4662', '0', 'admin', '2017-11-15 09:42:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4663', '0', 'admin', '2017-11-15 10:08:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4664', '0', 'admin', '2017-11-15 11:41:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4665', '0', 'admin', '2017-11-15 14:28:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4666', '0', 'admin', '2017-11-15 14:48:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4667', '0', 'admin', '2017-11-15 16:08:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4668', '0', 'admin', '2017-11-15 16:38:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4669', '0', 'admin', '2017-11-15 16:53:43', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4670', '0', 'admin', '2017-11-15 16:56:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4671', '0', 'admin', '2017-11-15 17:21:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4672', '0', 'admin', '2017-11-15 17:23:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4673', '0', 'admin', '2017-11-15 17:23:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4674', '0', 'admin', '2017-11-15 17:26:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4675', '0', 'admin', '2017-11-15 20:47:09', '112.97.192.188');
INSERT INTO `mds_userlogin` VALUES ('4676', '0', 'admin', '2017-11-15 20:47:26', '112.97.192.188');
INSERT INTO `mds_userlogin` VALUES ('4677', '0', 'admin', '2017-11-15 20:47:31', '112.97.192.188');
INSERT INTO `mds_userlogin` VALUES ('4678', '0', 'admin', '2017-11-16 09:11:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4679', '0', 'admin', '2017-11-16 16:38:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4680', '0', 'admin', '2017-11-16 16:38:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4681', '0', 'admin', '2017-11-16 17:23:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4682', '0', 'admin', '2017-11-17 09:10:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4683', '0', 'admin', '2017-11-17 10:02:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4684', '0', 'admin', '2017-11-17 10:04:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4685', '0', 'admin', '2017-11-17 10:21:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4686', '0', 'admin', '2017-11-17 11:34:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4687', '0', 'admin', '2017-11-17 11:47:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4688', '0', 'admin', '2017-11-17 14:35:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4689', '0', 'admin', '2017-11-17 15:36:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4690', '0', 'admin', '2017-11-17 15:45:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4691', '0', 'admin', '2017-11-17 15:48:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4692', '0', 'admin', '2017-11-17 16:24:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4693', '0', 'admin', '2017-11-17 16:24:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4694', '0', 'admin', '2017-11-17 16:26:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4695', '0', 'admin', '2017-11-17 16:49:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4696', '0', 'admin', '2017-11-17 17:54:02', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4697', '0', 'admin', '2017-11-18 14:05:32', '14.220.123.201');
INSERT INTO `mds_userlogin` VALUES ('4698', '0', 'admin', '2017-11-20 12:41:23', '117.136.39.208');
INSERT INTO `mds_userlogin` VALUES ('4699', '0', 'admin', '2017-11-20 12:43:24', '14.152.68.88');
INSERT INTO `mds_userlogin` VALUES ('4700', '0', 'admin', '2017-11-20 15:06:52', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4701', '0', 'admin', '2017-11-20 15:10:06', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4702', '0', 'admin', '2017-11-20 15:13:53', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4703', '0', 'admin', '2017-11-20 15:16:29', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4704', '0', 'admin', '2017-11-20 15:17:08', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4705', '0', 'admin', '2017-11-20 15:54:04', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4706', '0', 'admin', '2017-11-20 16:18:00', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4707', '0', 'admin', '2017-11-20 16:18:14', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4708', '0', 'admin', '2017-11-20 16:28:08', '14.220.120.27');
INSERT INTO `mds_userlogin` VALUES ('4709', '0', 'admin', '2017-11-22 11:55:25', '14.220.122.83');
INSERT INTO `mds_userlogin` VALUES ('4710', '0', 'admin', '2017-12-04 10:59:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4711', '0', 'admin', '2017-12-04 11:30:40', '14.220.121.139');
INSERT INTO `mds_userlogin` VALUES ('4712', '0', 'admin', '2017-12-04 11:34:00', '211.161.245.149');
INSERT INTO `mds_userlogin` VALUES ('4713', '0', 'admin', '2017-12-04 11:37:35', '14.220.121.139');
INSERT INTO `mds_userlogin` VALUES ('4714', '0', 'admin', '2017-12-04 11:40:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4715', '0', 'admin', '2017-12-04 11:43:37', '14.220.121.139');
INSERT INTO `mds_userlogin` VALUES ('4716', '0', 'admin', '2017-12-04 14:18:29', '211.161.240.157');
INSERT INTO `mds_userlogin` VALUES ('4717', '0', 'admin', '2017-12-04 15:23:03', '14.220.121.139');
INSERT INTO `mds_userlogin` VALUES ('4718', '0', 'admin', '2017-12-04 17:48:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4719', '0', 'admin', '2017-12-04 17:54:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4720', '0', 'admin', '2017-12-04 17:54:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4721', '0', 'admin', '2017-12-05 11:43:43', '211.161.248.143');
INSERT INTO `mds_userlogin` VALUES ('4722', '0', 'admin', '2017-12-05 11:44:56', '211.161.248.143');
INSERT INTO `mds_userlogin` VALUES ('4723', '0', 'admin', '2017-12-05 21:58:04', '113.77.230.27');
INSERT INTO `mds_userlogin` VALUES ('4724', '0', 'admin', '2017-12-06 17:09:49', '113.77.230.27');
INSERT INTO `mds_userlogin` VALUES ('4725', '0', 'admin', '2017-12-07 10:32:58', '113.77.230.27');
INSERT INTO `mds_userlogin` VALUES ('4726', '0', 'admin', '2017-12-07 12:12:21', '113.77.230.27');
INSERT INTO `mds_userlogin` VALUES ('4727', '0', 'admin', '2017-12-07 14:30:55', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4728', '0', 'admin', '2017-12-07 14:33:29', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4729', '0', 'admin', '2017-12-08 10:03:21', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4730', '0', 'admin', '2017-12-08 16:06:27', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4731', '0', 'admin', '2017-12-08 17:06:28', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4732', '0', 'admin', '2017-12-08 18:56:23', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4733', '0', 'admin', '2017-12-08 19:02:04', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4734', '0', 'admin', '2017-12-08 19:07:05', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4735', '0', 'admin', '2017-12-08 19:07:34', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4736', '0', 'admin', '2017-12-08 19:08:07', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4737', '0', 'admin', '2017-12-08 20:44:05', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4738', '0', 'admin', '2017-12-08 20:44:59', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4739', '0', 'admin', '2017-12-09 10:07:44', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4740', '0', 'admin', '2017-12-09 10:38:50', '14.220.123.217');
INSERT INTO `mds_userlogin` VALUES ('4741', '0', 'admin', '2017-12-11 10:07:34', '113.77.231.164');
INSERT INTO `mds_userlogin` VALUES ('4742', '0', 'admin', '2017-12-11 15:54:57', '14.220.121.219');
INSERT INTO `mds_userlogin` VALUES ('4743', '0', 'admin', '2017-12-11 19:52:00', '14.220.121.219');
INSERT INTO `mds_userlogin` VALUES ('4744', '0', 'admin', '2017-12-12 14:20:23', '14.220.121.219');
INSERT INTO `mds_userlogin` VALUES ('4745', '0', 'admin', '2017-12-12 20:17:41', '14.220.121.219');
INSERT INTO `mds_userlogin` VALUES ('4746', '0', 'admin', '2017-12-13 14:16:58', '113.77.229.196');
INSERT INTO `mds_userlogin` VALUES ('4747', '0', 'admin', '2017-12-13 18:01:53', '113.77.229.196');
INSERT INTO `mds_userlogin` VALUES ('4748', '0', 'admin', '2017-12-14 11:35:46', '113.77.229.196');
INSERT INTO `mds_userlogin` VALUES ('4749', '0', 'admin', '2017-12-16 16:29:24', '14.220.123.107');
INSERT INTO `mds_userlogin` VALUES ('4750', '0', 'admin', '2017-12-18 09:47:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4751', '0', 'admin', '2017-12-18 09:58:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4752', '0', 'admin', '2017-12-18 10:00:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4753', '0', 'admin', '2017-12-18 10:01:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4754', '0', 'admin', '2017-12-18 10:03:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4755', '0', 'admin', '2017-12-18 10:07:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4756', '0', 'admin', '2017-12-18 10:08:32', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4757', '0', 'admin', '2017-12-18 10:09:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4758', '0', 'admin', '2017-12-18 10:09:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4759', '0', 'admin', '2017-12-18 10:10:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4760', '0', 'admin', '2017-12-18 10:19:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4761', '0', 'admin', '2017-12-18 10:20:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4762', '0', 'admin', '2017-12-18 11:52:17', '14.220.120.14');
INSERT INTO `mds_userlogin` VALUES ('4763', '0', 'admin', '2017-12-18 14:40:34', '14.220.120.14');
INSERT INTO `mds_userlogin` VALUES ('4764', '0', 'admin', '2017-12-18 14:42:17', '14.220.120.14');
INSERT INTO `mds_userlogin` VALUES ('4765', '0', 'admin', '2017-12-18 14:47:53', '14.220.120.14');
INSERT INTO `mds_userlogin` VALUES ('4766', '0', 'admin', '2017-12-18 14:48:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4767', '0', 'admin', '2017-12-18 14:49:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4768', '0', 'admin', '2017-12-18 17:36:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4769', '0', 'admin', '2017-12-19 16:21:21', '14.220.120.14');
INSERT INTO `mds_userlogin` VALUES ('4770', '0', 'admin', '2017-12-19 16:22:39', '14.220.120.14');
INSERT INTO `mds_userlogin` VALUES ('4771', '0', 'admin', '2017-12-19 16:29:40', '14.220.120.14');
INSERT INTO `mds_userlogin` VALUES ('4772', '0', 'admin', '2017-12-22 13:05:16', '14.220.121.191');
INSERT INTO `mds_userlogin` VALUES ('4773', '0', 'admin', '2017-12-22 18:08:07', '14.220.121.191');
INSERT INTO `mds_userlogin` VALUES ('4774', '0', 'admin', '2017-12-25 18:46:02', '14.220.123.128');
INSERT INTO `mds_userlogin` VALUES ('4775', '0', 'admin', '2017-12-29 00:09:42', '113.77.228.246');
INSERT INTO `mds_userlogin` VALUES ('4776', '0', 'admin', '2018-01-02 09:57:52', '14.220.123.50');
INSERT INTO `mds_userlogin` VALUES ('4777', '0', 'admin', '2018-01-02 10:02:29', '14.220.123.50');
INSERT INTO `mds_userlogin` VALUES ('4778', '0', 'admin', '2018-01-06 10:02:30', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4779', '0', 'admin', '2018-01-06 10:56:47', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4780', '0', 'admin', '2018-01-06 11:19:46', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4781', '0', 'admin', '2018-01-06 11:24:49', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4782', '0', 'admin', '2018-01-06 11:25:05', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4783', '0', 'admin', '2018-01-06 11:26:25', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4784', '0', 'admin', '2018-01-06 11:26:55', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4785', '0', 'admin', '2018-01-06 11:27:08', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4786', '0', 'admin', '2018-01-06 11:28:39', '222.95.83.233');
INSERT INTO `mds_userlogin` VALUES ('4787', '0', 'admin', '2018-01-06 11:35:59', '14.220.123.212');
INSERT INTO `mds_userlogin` VALUES ('4788', '0', 'admin', '2018-01-06 11:56:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4789', '0', 'admin', '2018-01-06 14:30:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4790', '0', 'admin', '2018-01-06 14:38:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4791', '0', 'admin', '2018-01-06 15:26:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4792', '0', 'admin', '2018-01-06 16:24:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4793', '0', 'admin', '2018-01-08 08:53:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4794', '0', 'admin', '2018-01-08 09:22:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4795', '0', 'admin', '2018-01-08 09:26:06', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4796', '0', 'admin', '2018-01-08 09:53:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4797', '0', 'admin', '2018-01-08 09:58:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4798', '0', 'admin', '2018-01-08 10:12:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4799', '0', 'admin', '2018-01-08 10:37:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4800', '0', 'admin', '2018-01-08 10:38:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4801', '0', 'admin', '2018-01-08 11:22:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4802', '0', 'admin', '2018-01-08 11:27:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4803', '0', 'admin', '2018-01-08 11:30:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4804', '0', 'admin', '2018-01-08 14:00:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4805', '0', 'admin', '2018-01-08 14:03:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4806', '0', 'admin', '2018-01-08 14:18:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4807', '0', 'admin', '2018-01-08 14:50:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4808', '0', 'admin', '2018-01-08 15:01:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4809', '0', 'admin', '2018-01-08 15:15:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4810', '0', 'admin', '2018-01-08 15:27:52', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4811', '0', 'admin', '2018-01-08 15:54:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4812', '0', 'admin', '2018-01-08 15:54:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4813', '0', 'admin', '2018-01-08 16:10:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4814', '0', 'admin', '2018-01-08 16:10:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4815', '0', 'admin', '2018-01-08 16:12:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4816', '0', 'admin', '2018-01-08 16:22:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4817', '0', 'admin', '2018-01-09 09:50:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4818', '0', 'admin', '2018-01-09 11:06:20', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4819', '0', 'admin', '2018-01-11 10:40:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4820', '0', 'admin', '2018-01-11 14:32:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4821', '0', 'admin', '2018-01-11 14:33:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4822', '0', 'admin', '2018-01-11 16:26:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4823', '0', 'admin', '2018-01-11 16:58:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4824', '0', 'admin', '2018-01-12 19:59:44', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4825', '0', 'admin', '2018-01-12 20:00:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4826', '0', 'admin', '2018-01-12 20:23:38', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4827', '0', 'admin', '2018-01-12 20:34:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4828', '0', 'admin', '2018-01-12 20:37:29', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4829', '0', 'admin', '2018-01-12 21:04:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4830', '0', 'admin', '2018-01-16 15:07:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4831', '0', 'admin', '2018-01-16 15:07:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4832', '0', 'admin', '2018-01-16 15:07:33', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4833', '0', 'admin', '2018-01-16 15:07:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4834', '0', 'admin', '2018-01-16 15:11:16', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4835', '0', 'admin', '2018-02-24 16:01:24', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4836', '0', 'admin', '2018-02-24 16:05:58', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4837', '0', 'admin', '2018-02-24 16:20:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4838', '0', 'admin', '2018-02-24 16:30:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4839', '0', 'admin', '2018-02-24 16:37:08', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4840', '0', 'admin', '2018-02-26 10:32:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4841', '0', 'admin', '2018-02-26 16:16:15', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4842', '0', 'admin', '2018-03-29 16:14:55', '113.91.33.222');
INSERT INTO `mds_userlogin` VALUES ('4843', '0', 'admin', '2018-03-29 16:18:13', '113.91.33.222');
INSERT INTO `mds_userlogin` VALUES ('4844', '0', 'admin', '2018-03-29 16:27:59', '221.224.35.90');
INSERT INTO `mds_userlogin` VALUES ('4845', '0', 'admin', '2018-03-30 09:33:34', '221.224.35.90');
INSERT INTO `mds_userlogin` VALUES ('4846', '0', 'admin', '2018-03-30 15:41:30', '113.91.33.222');
INSERT INTO `mds_userlogin` VALUES ('4847', '0', 'admin', '2018-04-03 17:41:48', '183.12.243.254');
INSERT INTO `mds_userlogin` VALUES ('4848', '0', 'admin', '2018-04-03 17:43:21', '49.90.134.115');
INSERT INTO `mds_userlogin` VALUES ('4849', '0', 'admin', '2018-04-03 18:43:28', '221.226.111.50');
INSERT INTO `mds_userlogin` VALUES ('4850', '0', 'admin', '2018-04-04 09:50:18', '221.224.35.90');
INSERT INTO `mds_userlogin` VALUES ('4851', '0', 'admin', '2018-04-04 10:14:30', '49.90.134.115');
INSERT INTO `mds_userlogin` VALUES ('4852', '0', 'admin', '2018-04-08 18:39:46', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4853', '0', 'admin', '2018-04-08 18:46:47', '113.91.38.78');
INSERT INTO `mds_userlogin` VALUES ('4854', '0', 'admin', '2018-04-21 20:04:01', '14.220.122.120');
INSERT INTO `mds_userlogin` VALUES ('4855', '0', 'admin', '2018-04-26 18:26:47', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('4856', '0', 'admin', '2018-05-08 14:42:25', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('4857', '0', 'admin', '2018-05-17 16:35:25', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('4858', '0', 'admin', '2018-05-17 21:08:33', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('4859', '0', 'admin', '2018-05-17 21:14:02', '114.228.40.42');
INSERT INTO `mds_userlogin` VALUES ('4860', '0', 'admin', '2018-05-17 21:15:36', '114.228.40.42');
INSERT INTO `mds_userlogin` VALUES ('4861', '0', 'admin', '2018-05-17 21:16:15', '114.228.40.42');
INSERT INTO `mds_userlogin` VALUES ('4862', '0', 'admin', '2018-05-18 08:27:12', '58.216.168.131');
INSERT INTO `mds_userlogin` VALUES ('4863', '0', 'admin', '2018-05-18 08:28:30', '58.216.168.131');
INSERT INTO `mds_userlogin` VALUES ('4864', '0', 'admin', '2018-05-18 08:29:43', '58.216.168.131');
INSERT INTO `mds_userlogin` VALUES ('4865', '0', 'admin', '2018-05-18 08:39:15', '58.216.168.131');
INSERT INTO `mds_userlogin` VALUES ('4866', '0', 'admin', '2018-05-18 08:39:53', '58.216.168.131');
INSERT INTO `mds_userlogin` VALUES ('4867', '0', 'admin', '2018-05-18 08:42:22', '58.216.168.131');
INSERT INTO `mds_userlogin` VALUES ('4868', '0', 'admin', '2018-05-18 09:02:31', '58.216.168.131');
INSERT INTO `mds_userlogin` VALUES ('4869', '0', 'admin', '2018-05-18 18:31:14', '112.10.243.44');
INSERT INTO `mds_userlogin` VALUES ('4870', '0', 'admin', '2018-05-18 18:34:25', '112.10.243.44');
INSERT INTO `mds_userlogin` VALUES ('4871', '0', 'admin', '2018-05-31 15:51:03', '183.62.230.174');
INSERT INTO `mds_userlogin` VALUES ('4872', '0', 'admin', '2018-05-31 16:15:15', '165.225.96.89');
INSERT INTO `mds_userlogin` VALUES ('4873', '0', 'admin', '2018-05-31 16:16:50', '165.225.96.89');
INSERT INTO `mds_userlogin` VALUES ('4874', '0', 'admin', '2018-05-31 16:25:55', '165.225.96.89');
INSERT INTO `mds_userlogin` VALUES ('4875', '0', 'admin', '2018-05-31 16:30:17', '165.225.96.89');
INSERT INTO `mds_userlogin` VALUES ('4876', '0', 'admin', '2018-06-06 12:49:48', '223.104.30.174');
INSERT INTO `mds_userlogin` VALUES ('4877', '0', 'admin', '2018-06-06 13:05:51', '124.117.210.93');
INSERT INTO `mds_userlogin` VALUES ('4878', '0', 'admin', '2018-06-10 17:50:43', '183.12.237.242');
INSERT INTO `mds_userlogin` VALUES ('4879', '0', 'admin', '2018-06-10 17:51:51', '183.12.237.242');
INSERT INTO `mds_userlogin` VALUES ('4880', '0', 'admin', '2018-06-10 22:01:07', '14.220.122.8');
INSERT INTO `mds_userlogin` VALUES ('4881', '0', 'admin', '2018-06-10 22:08:08', '111.30.245.254');
INSERT INTO `mds_userlogin` VALUES ('4882', '0', 'admin', '2018-06-11 09:41:57', '111.163.123.221');
INSERT INTO `mds_userlogin` VALUES ('4883', '0', 'admin', '2018-06-11 10:55:44', '111.163.123.221');
INSERT INTO `mds_userlogin` VALUES ('4884', '0', 'admin', '2018-06-11 14:12:32', '117.136.54.24');
INSERT INTO `mds_userlogin` VALUES ('4885', '0', 'admin', '2018-06-11 14:13:59', '117.136.54.24');
INSERT INTO `mds_userlogin` VALUES ('4886', '0', 'admin', '2018-06-11 15:18:28', '117.136.54.24');
INSERT INTO `mds_userlogin` VALUES ('4887', '0', 'admin', '2018-06-11 20:09:59', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('4888', '0', 'admin', '2018-06-11 21:58:10', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4889', '0', 'admin', '2018-06-11 22:04:08', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4890', '0', 'admin', '2018-06-11 22:33:07', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4891', '0', 'admin', '2018-06-11 22:34:15', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4892', '0', 'admin', '2018-06-11 22:34:53', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4893', '0', 'admin', '2018-06-11 22:35:34', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4894', '0', 'admin', '2018-06-11 22:43:46', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4895', '0', 'admin', '2018-06-11 22:45:03', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4896', '0', 'admin', '2018-06-11 22:51:00', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4897', '0', 'admin', '2018-06-11 22:59:53', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4898', '0', 'admin', '2018-06-11 23:00:05', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4899', '0', 'admin', '2018-06-11 23:03:12', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4900', '0', 'admin', '2018-06-11 23:04:11', '119.123.193.172');
INSERT INTO `mds_userlogin` VALUES ('4901', '0', 'admin', '2018-06-19 09:22:58', '111.163.120.215');
INSERT INTO `mds_userlogin` VALUES ('4902', '0', 'admin', '2018-06-19 17:28:52', '125.36.116.163');
INSERT INTO `mds_userlogin` VALUES ('4903', '0', 'admin', '2018-06-19 18:29:59', '223.104.63.212');
INSERT INTO `mds_userlogin` VALUES ('4904', '0', 'admin', '2018-06-20 13:03:45', '125.36.116.163');
INSERT INTO `mds_userlogin` VALUES ('4905', '0', 'admin', '2018-06-30 19:12:51', '113.77.231.92');
INSERT INTO `mds_userlogin` VALUES ('4906', '0', 'admin', '2018-06-30 19:17:55', '113.77.231.92');
INSERT INTO `mds_userlogin` VALUES ('4907', '0', 'admin', '2018-06-30 19:20:05', '113.77.231.92');
INSERT INTO `mds_userlogin` VALUES ('4908', '0', 'admin', '2018-07-02 11:44:21', '183.12.236.48');
INSERT INTO `mds_userlogin` VALUES ('4909', 'b59c30100aba4836baad49621e9678a6', 'test', '2018-07-02 11:45:38', '183.12.236.48');
INSERT INTO `mds_userlogin` VALUES ('4910', 'b59c30100aba4836baad49621e9678a6', 'test', '2018-07-02 11:49:23', '183.12.236.48');
INSERT INTO `mds_userlogin` VALUES ('4911', 'b59c30100aba4836baad49621e9678a6', 'test', '2018-07-02 11:55:23', '183.12.236.48');
INSERT INTO `mds_userlogin` VALUES ('4912', '0', 'admin', '2018-07-02 11:57:38', '183.12.236.48');
INSERT INTO `mds_userlogin` VALUES ('4913', '0', 'admin', '2018-07-02 16:36:58', '223.104.1.154');
INSERT INTO `mds_userlogin` VALUES ('4914', '0', 'admin', '2018-07-02 20:59:32', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4915', '0', 'admin', '2018-07-02 21:32:40', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4916', '0', 'admin', '2018-07-04 13:55:34', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4917', '0', 'admin', '2018-07-04 14:09:12', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4918', '0', 'admin', '2018-07-04 15:27:13', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4919', '0', 'admin', '2018-07-04 15:48:33', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4920', '0', 'admin', '2018-07-04 22:02:32', '223.104.63.183');
INSERT INTO `mds_userlogin` VALUES ('4921', '0', 'admin', '2018-07-04 23:26:57', '119.123.195.135');
INSERT INTO `mds_userlogin` VALUES ('4922', '0', 'admin', '2018-07-04 23:52:57', '119.123.195.135');
INSERT INTO `mds_userlogin` VALUES ('4923', '0', 'admin', '2018-07-04 23:53:15', '119.123.195.135');
INSERT INTO `mds_userlogin` VALUES ('4924', '0', 'admin', '2018-07-05 18:23:23', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4925', '0', 'admin', '2018-07-05 22:32:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4926', '0', 'admin', '2018-07-05 22:34:32', '49.92.209.72');
INSERT INTO `mds_userlogin` VALUES ('4927', '0', 'admin', '2018-07-05 22:58:57', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4928', '0', 'admin', '2018-07-05 23:35:17', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4929', '0', 'admin', '2018-07-05 23:41:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4930', '0', 'admin', '2018-07-05 23:47:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4931', '0', 'admin', '2018-07-06 08:33:14', '183.62.230.175');
INSERT INTO `mds_userlogin` VALUES ('4932', '0', 'admin', '2018-07-06 16:09:20', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4933', '0', 'admin', '2018-07-06 17:06:40', '122.114.15.30');
INSERT INTO `mds_userlogin` VALUES ('4934', '0', 'admin', '2018-07-06 22:19:05', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4935', '0', 'admin', '2018-07-06 22:37:37', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4936', '0', 'admin', '2018-07-06 22:39:12', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4937', '0', 'admin', '2018-07-06 23:22:03', '14.220.123.251');
INSERT INTO `mds_userlogin` VALUES ('4938', '0', 'admin', '2018-07-07 09:35:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4939', '0', 'admin', '2018-07-07 09:36:54', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4940', '0', 'admin', '2018-07-07 09:37:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4941', '0', 'admin', '2018-07-07 09:47:32', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4942', '0', 'admin', '2018-07-07 09:48:05', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4943', '0', 'admin', '2018-07-07 09:49:35', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4944', '0', 'admin', '2018-07-07 10:02:26', '14.220.123.251');
INSERT INTO `mds_userlogin` VALUES ('4945', '0', 'admin', '2018-07-07 10:29:51', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4946', '0', 'admin', '2018-07-07 10:50:56', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4947', '0', 'admin', '2018-07-07 11:15:50', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4948', '0', 'admin', '2018-07-07 11:24:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4949', '0', 'admin', '2018-07-07 11:28:22', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4950', '0', 'admin', '2018-07-07 11:32:28', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4951', '0', 'admin', '2018-07-07 11:39:25', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4952', '0', 'admin', '2018-07-07 11:54:42', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4953', '0', 'admin', '2018-07-07 11:55:07', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4954', '0', 'admin', '2018-07-07 12:05:18', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4955', '0', 'admin', '2018-07-07 14:01:37', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4956', '0', 'admin', '2018-07-07 14:02:00', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4957', '0', 'admin', '2018-07-07 14:05:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4958', '0', 'admin', '2018-07-07 14:07:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4959', '0', 'admin', '2018-07-07 14:17:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4960', '0', 'admin', '2018-07-07 14:20:49', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4961', '0', 'admin', '2018-07-07 14:28:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4962', '0', 'admin', '2018-07-07 16:15:41', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4963', '0', 'admin', '2018-07-07 16:17:42', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4964', '0', 'admin', '2018-07-07 16:28:32', '14.220.123.251');
INSERT INTO `mds_userlogin` VALUES ('4965', '0', 'admin', '2018-07-07 16:33:00', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4966', '0', 'admin', '2018-07-07 16:39:19', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4967', '0', 'admin', '2018-07-07 16:46:01', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4968', '0', 'admin', '2018-07-07 16:47:14', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4969', '0', 'admin', '2018-07-07 16:47:28', '14.220.123.251');
INSERT INTO `mds_userlogin` VALUES ('4970', '0', 'admin', '2018-07-07 16:52:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4971', '0', 'admin', '2018-07-07 16:57:19', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4972', '0', 'admin', '2018-07-07 17:00:18', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4973', '0', 'admin', '2018-07-07 17:00:28', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4974', '0', 'admin', '2018-07-07 17:08:09', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4975', '0', 'admin', '2018-07-07 17:10:07', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4976', '0', 'admin', '2018-07-07 17:12:57', '113.91.37.125');
INSERT INTO `mds_userlogin` VALUES ('4977', '0', 'admin', '2018-07-07 17:41:13', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4978', '0', 'admin', '2018-07-07 17:48:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4979', '0', 'admin', '2018-07-07 18:11:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4980', '0', 'admin', '2018-07-07 22:13:53', '14.220.123.251');
INSERT INTO `mds_userlogin` VALUES ('4981', '0', 'admin', '2018-07-07 22:15:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4982', '0', 'admin', '2018-07-07 22:21:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4983', '0', 'admin', '2018-07-07 23:18:45', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4984', '0', 'admin', '2018-07-07 23:49:26', '14.220.123.251');
INSERT INTO `mds_userlogin` VALUES ('4985', '0', 'admin', '2018-07-08 09:35:27', '49.90.147.210');
INSERT INTO `mds_userlogin` VALUES ('4986', '0', 'admin', '2018-07-08 09:41:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4987', '0', 'admin', '2018-07-08 10:13:03', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4988', '0', 'admin', '2018-07-08 10:16:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4989', '0', 'admin', '2018-07-08 11:13:58', '14.220.123.251');
INSERT INTO `mds_userlogin` VALUES ('4990', '0', 'admin', '2018-07-08 11:18:59', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4991', '0', 'admin', '2018-07-08 15:37:51', '223.104.63.218');
INSERT INTO `mds_userlogin` VALUES ('4992', '0', 'admin', '2018-07-08 18:11:07', '113.77.230.219');
INSERT INTO `mds_userlogin` VALUES ('4993', '0', 'admin', '2018-07-08 18:12:34', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4994', '0', 'admin', '2018-07-08 18:15:10', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4995', '0', 'admin', '2018-07-08 18:15:21', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4996', '0', 'admin', '2018-07-08 18:16:39', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4997', '0', 'admin', '2018-07-08 18:21:55', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4998', '0', 'admin', '2018-07-08 18:25:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('4999', '0', 'admin', '2018-07-08 18:25:36', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5000', '0', 'admin', '2018-07-08 18:34:31', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5001', '0', 'admin', '2018-07-08 18:41:48', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5002', '0', 'admin', '2018-07-08 19:26:11', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5003', '0', 'admin', '2018-07-08 20:05:26', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5004', '0', 'admin', '2018-07-08 20:10:27', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5005', '0', 'admin', '2018-07-08 20:25:23', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5006', '0', 'admin', '2018-07-08 20:25:53', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5007', '0', 'admin', '2018-07-08 20:28:04', '0:0:0:0:0:0:0:1');
INSERT INTO `mds_userlogin` VALUES ('5008', '0', 'admin', '2018-07-09 21:53:44', '0:0:0:0:0:0:0:1');

-- ----------------------------
-- Table structure for mds_user_role
-- ----------------------------
DROP TABLE IF EXISTS `mds_user_role`;
CREATE TABLE `mds_user_role` (
  `user_id` varchar(64) NOT NULL,
  `role_id` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_user_role
-- ----------------------------
INSERT INTO `mds_user_role` VALUES ('2ad2d59e3c3440598e34b1866ec63df7', '6bc48f44b596403891135e9d482eb4f6');
INSERT INTO `mds_user_role` VALUES ('7e888a1bb0604a4682ca34a2bc62ef64', '6907f56e454345b1bde76f9281175fd5');
INSERT INTO `mds_user_role` VALUES ('b59c30100aba4836baad49621e9678a6', 'e28f46fa492a4fa2a080ea1914ee71ca');
INSERT INTO `mds_user_role` VALUES ('daa00cd9a9ce48e7aea8682efcbe0762', '6907f56e454345b1bde76f9281175fd5');

-- ----------------------------
-- Table structure for mds_user_shift
-- ----------------------------
DROP TABLE IF EXISTS `mds_user_shift`;
CREATE TABLE `mds_user_shift` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `site_id` varchar(64) NOT NULL COMMENT '站点PK_id',
  `user_id` varchar(64) NOT NULL COMMENT '用户PK_id',
  `shift_id` varchar(64) NOT NULL COMMENT '班次PK_id',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_user_shift
-- ----------------------------

-- ----------------------------
-- Table structure for mds_workcenter
-- ----------------------------
DROP TABLE IF EXISTS `mds_workcenter`;
CREATE TABLE `mds_workcenter` (
  `id` varchar(64) NOT NULL COMMENT 'PK',
  `workcenter_no` varchar(255) NOT NULL COMMENT '工作中心编号',
  `workcenter_name` varchar(255) NOT NULL COMMENT '工作中心名称',
  `workcenter_description` varchar(255) DEFAULT NULL COMMENT '工作中心描述',
  `site_id` varchar(255) NOT NULL COMMENT '站点id',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `status` int(1) NOT NULL COMMENT '状态：1、可用；-1、不可用；',
  `workcenter_level` varchar(255) NOT NULL COMMENT '工作中心层级（车间：workcenter、生产线：workline）',
  `workcenter_type` varchar(255) NOT NULL COMMENT '工作中心类型（装配：assemble、调度：control、装运：shipment、转移：shift）',
  `workcenter_version` varchar(255) NOT NULL COMMENT '版本',
  `workcenter_parent_id` varchar(64) DEFAULT NULL COMMENT '父级工作中心',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_workcenter
-- ----------------------------
INSERT INTO `mds_workcenter` VALUES ('7220466201db48f3b5f21672f44ee8fd', 'WK001', 'WK001', '组装车间', '0', 'admin', '2018-07-08 19:41:13', '1', 'workcenter', 'assemble', 'A', null);
INSERT INTO `mds_workcenter` VALUES ('b6f24eb126274a69830f544e9d97556b', 'LINE001', 'LINE001', '组装流水线01', '0', 'admin', '2018-07-08 19:55:03', '1', 'workline', 'assemble', 'A', '7220466201db48f3b5f21672f44ee8fd');

-- ----------------------------
-- Table structure for mds_workshop_inventory
-- ----------------------------
DROP TABLE IF EXISTS `mds_workshop_inventory`;
CREATE TABLE `mds_workshop_inventory` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `item_id` varchar(64) NOT NULL COMMENT '物料PK_id',
  `item_sfc` varchar(255) DEFAULT NULL COMMENT '物料的SFC',
  `item_inner_batch` varchar(255) DEFAULT NULL COMMENT '物料批次',
  `receive_number` int(11) NOT NULL COMMENT '接收数量',
  `workshop_id` varchar(64) NOT NULL COMMENT '工作中心PK_id',
  `site_id` varchar(64) NOT NULL COMMENT '站点PK_id',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_workshop_inventory
-- ----------------------------
INSERT INTO `mds_workshop_inventory` VALUES ('cb08e93dd86e48689ee759f9db6d44d2', '9bdd7c4632244d58af0b651a421a99b7', 'BATCH001', null, '1', '7220466201db48f3b5f21672f44ee8fd', '0', 'admin', '2018-07-08 20:40:59');

-- ----------------------------
-- Table structure for mds_work_resource
-- ----------------------------
DROP TABLE IF EXISTS `mds_work_resource`;
CREATE TABLE `mds_work_resource` (
  `id` varchar(64) NOT NULL,
  `resource_no` varchar(255) NOT NULL COMMENT '资源名称',
  `resource_desc` varchar(255) DEFAULT NULL COMMENT '资源描述',
  `resource_type_id` varchar(255) DEFAULT NULL COMMENT '资源类型PK_id',
  `status` int(1) NOT NULL COMMENT '状态 -1未启用；1已启用',
  `site_id` varchar(255) NOT NULL COMMENT '站点PK_id',
  `create_user` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `workline_id` varchar(255) DEFAULT NULL COMMENT '所属工作中心（产线）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_work_resource
-- ----------------------------
INSERT INTO `mds_work_resource` VALUES ('597175d91a6949ec9db7d68a9a99add6', 'R001', 'R001', 'fec5d0edb98648b891d644569f99dedc', '1', '0', 'admin', '2018-07-08 19:55:18', 'b6f24eb126274a69830f544e9d97556b');

-- ----------------------------
-- Table structure for mds_work_resource_type
-- ----------------------------
DROP TABLE IF EXISTS `mds_work_resource_type`;
CREATE TABLE `mds_work_resource_type` (
  `id` varchar(64) NOT NULL,
  `resource_type` varchar(255) NOT NULL COMMENT '资源类型',
  `resource_type_des` varchar(255) DEFAULT NULL COMMENT '资源类型描述',
  `site_id` varchar(255) NOT NULL COMMENT '站点PK_id',
  `create_user` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mds_work_resource_type
-- ----------------------------
INSERT INTO `mds_work_resource_type` VALUES ('fec5d0edb98648b891d644569f99dedc', 'R', 'R', '0', 'admin', '2018-07-08 19:54:02');
DROP TRIGGER IF EXISTS `tri_before_insert_on_mds_sfc_step`;
DELIMITER ;;
CREATE TRIGGER `tri_before_insert_on_mds_sfc_step` BEFORE INSERT ON `mds_sfc_step` FOR EACH ROW BEGIN
     SET new.seq = null;
END
;;
DELIMITER ;
