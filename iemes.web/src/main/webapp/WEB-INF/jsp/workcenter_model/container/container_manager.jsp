<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>容器类型管理</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/workcenter_model/container/container_maintenance.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</div>

		<div class="hidden">
			<input type="hidden" value="${page_res_id}" id="page_res_id">
			<input type="hidden" name="containerTypeFormMap.id" value="${containerTypeFormMap.id}" id="container_type_id">
			<input type="hidden" value="${errorMessage}" id="errorMessage">
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site }</span>
			</div>
			<div class="formField_query">
				<label class="must">容器类型编号:</label> 
				<input type="text" class="formText_query majuscule"
					dataValue="container_type_no" 
					viewTitle="容器类型"
					data-url="/popup/queryAllContainer.shtml"
					id="tbx_container_type_no" name="containerTypeFormMap.container_type_no" value="${containerTypeFormMap.container_type_no }"
					validate_allowedempty="N" validate_errormsg="请输入容器类型编号！" />
				<input type="button" submit="N" value="检索"  onclick="operationBrowse(this)"  textFieldId="tbx_container_type_no">
			</div>
		</div>


		<!--  Tab -->
		<ul id="tabs" class="tabs_li_3tabs">
			<li><a href="#" tabid="tab1">基础信息</a></li>
			<li><a href="#" tabid="tab2">尺寸规格</a></li>
			<li><a href="#" tabid="tab3">自定义数据</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="inputForm_content">
					<div class="formField_content">
						<label class="must">编号规则:</label>
						<input type="text"
						    dataValue="number_rule"
							viewTitle="编号规则"
							relationId="tbx_number_rule_id"
							data-url="/popup/queryAllNumberRule.shtml" 
							class="formText_query majuscule" id="tbx_number_rule"
							value="${numberRuleFormMap.number_rule}"
							validate_allowedempty="N" validate_errormsg="请输入编号规则！" />
						<input type="hidden" dataValue="id" id="tbx_number_rule_id" value="${numberRuleFormMap.id}" name="containerTypeFormMap.number_rule_id" >	
					    <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_number_rule">		
					</div>
					<div class="formField_content">
						<label>描述:</label> <input type="text" class="formText_content"  name="containerTypeFormMap.container_type_desc" value="${containerTypeFormMap.container_type_desc}"
							id="tbx_container_des" />
					</div>
					<div class="formField_content">
						<label class="must">类型:</label> <select class="formText_content" name="containerTypeFormMap.container_level"
							id="select_isusable">
							<option value="box" <c:if test="${ containerTypeFormMap.container_level=='box'}"> selected="selected"</c:if>>盒子</option>
							<option value="carton" <c:if test="${ containerTypeFormMap.container_level=='carton'}"> selected="selected"</c:if>>中箱</option>
							<option value="pallet" <c:if test="${ containerTypeFormMap.container_level=='pallet'}"> selected="selected"</c:if>>栈板</option>
						</select>
					</div>
					<div class="formField_content">
						<label class="must">是否可用:</label> <select class="formText_content"  name="containerTypeFormMap.status"
							id="select_isusable">
							<c:if test="${containerTypeFormMap.status==-1 }">
								<option value="1">是</option>
								<option value="-1" selected="selected">否</option>
							</c:if>
							<c:if test="${containerTypeFormMap.status!=-1 }">
								<option value="1" selected="selected">是</option>
								<option value="-1">否</option>
							</c:if>
						</select>
					</div>
					<div class="formField_content">
						<label class="must">最小总数量:</label> <input type="Number" class="formText_content" name="containerTypeFormMap.min_number"  value="${containerTypeFormMap.min_number }"
							id="tbx_min_number" />
					</div>
					<div class="formField_content">
						<label class="must">最大总数量:</label> <input type="Number" class="formText_content" name="containerTypeFormMap.max_number" value="${containerTypeFormMap.max_number }"
							id="tbx_max_number" />
					</div>
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${ containerTypeFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${containerTypeFormMap.create_time }</span>
					</div>
				</div>
			</div>
			<div id="tab2">
				<div class="inputForm_content">
					<div class="formField_content">
						<label>长度:</label> <input type="text" class="formText_content" name="containerTypeFormMap.container_length" value="${containerTypeFormMap.container_length }"
							id="tbx_container_length" />
					</div>
					<div class="formField_content">
						<label>宽度:</label> <input type="text" class="formText_content" name="containerTypeFormMap.container_width" value="${containerTypeFormMap.container_width }"
							id="tbx_container_width" />
					</div>
					<div class="formField_content">
						<label>高度:</label> <input type="text" class="formText_content" name="containerTypeFormMap.container_height" value="${containerTypeFormMap.container_height }"
							id="tbx_container_height" />
					</div>
					<div class="formField_content">
						<label>最大填充重量:</label> <input type="text" class="formText_content" name="containerTypeFormMap.max_weight" value="${containerTypeFormMap.max_weight }"
							id="tbx_container_max_fillweight" />
					</div>
					<div class="formField_content">
						<label>容器重量:</label> <input type="text" class="formText_content" name="containerTypeFormMap.container_weight" value="${containerTypeFormMap.container_weight }"
							id="tbx_container_weight" />
					</div>
				</div>
			</div>
			<div id="tab3">
				<table class="mtable" id="container_type_table">
					<tr>
						<th data-value="key">数据字段</th>
						<th data-value="value">字段值</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td data-value="${key.id}">${key.data_label}</td>
							<td data-value="${key.value}">
							   <input type='text' value="${key.value}">
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>

		<!--  Tab -->
	</form>
</body>
</html>