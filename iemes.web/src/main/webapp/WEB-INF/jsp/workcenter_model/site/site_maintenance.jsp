<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>站点维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

<script type="text/javascript" src="js/workcenter_model/site/site_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<%-- <input type="hidden" name="siteFormMap.id" value="${siteFormMap.id}" id="site_id"> --%>
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="siteFormMap.id" value="${siteFormMap.id}" id="site_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点编号:</label> 
			<input type="text"
				dataValue="site"
				viewTitle="站点"
				data-url="/popup/queryAllSite.shtml"
				class="formText_query majuscule" id="tbx_site_code" name="siteFormMap.site" value="${siteFormMap.site}" validate_allowedempty="N" validate_errormsg="请输入站点编号！" />
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_site_code">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_1tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label class="must">站点名称:</label> <input type="text"
						class="formText_content" id="tbx_site_name" name="siteFormMap.site_name" value="${siteFormMap.site_name}" validate_allowedempty="N" validate_errormsg="请输入站点名称！"/>
				</div>
				<div class="formField_content">
					<label>描述:</label> <input type="text" value="${siteFormMap.site_description}"
						class="formText_content" id="tbx_site_des" name="siteFormMap.site_description" />
				</div>
				<div class="formField_content">
					<label>状态:</label>
					<select class="formText_content"  id="select_status" name="siteFormMap.status">
					<c:if test="${siteFormMap==null}">
						<option value="1" selected="selected">可用</option>
						<option value="-1">不可用</option>
					</c:if>
					<c:if test="${siteFormMap.status==1}">
						<option value="1" selected="selected">可用</option>
						<option value="-1">不可用</option>
					</c:if>
                    <c:if test="${siteFormMap.status==-1}">
						<option value="1">可用</option>
						<option value="-1" selected="selected">不可用</option>
					</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${siteFormMap.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${siteFormMap.create_time} </span>
				</div>	
				
			</div>
		</div>

	</div>

	<!--  Tab -->
	</form>
</body>
</html>