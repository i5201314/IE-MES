<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>物料清单维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript" src="js/workcenter_model/item_bom/item_bom_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="itemBomFormMap.id" value="${itemBomFormMap.id}" id="item_bom_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">物料BOM编号:</label> 
			<input type="text"
			    dataValue="item_bom_no"
			    relationId= "tbx_version"
				viewTitle="物料清单"
				data-url="/popup/queryAllItemBom.shtml" 
				class="formText_query majuscule" id="tbx_item_bom_no" name="itemBomFormMap.item_bom_no" value="${itemBomFormMap.item_bom_no}"
				validate_allowedempty="N" validate_errormsg="请输入物料BOM编号！" />
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_item_bom_no">			
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_3tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">子物料</a></li>
		<li><a href="#" tabid="tab3">自定义数据</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">		    
				<div class="formField_content">
					<label>物料BOM名称:</label> <input type="text" name="itemBomFormMap.item_bom_name" value="${itemBomFormMap.item_bom_name}"
						class="formText_content" id="tbx_item_bom_name" />
				</div>
				<div class="formField_content">
					<label>物料BOM描述:</label> <input type="text" name="itemBomFormMap.item_bom_desc" value="${itemBomFormMap.item_bom_desc}"
						class="formText_content" id="tbx_itemBOM_des" />
				</div>
				 <div class="formField_content">
					<label class="must">版本:</label> 
					<input type="text" name="itemBomFormMap.item_bom_version" value="${itemBomFormMap.item_bom_version}"
					    dataValue="item_bom_version"
					    validate_allowedempty="N" validate_errormsg="物料清单版本号必须输入一位字符！" maxlength="1"
						class="formText_content majuscule" id="tbx_version" />
				</div>
				<div class="formField_content">
					<label>状态:</label> 
					<select class="formText_content"  id="select_item_bom_status"  name="itemBomFormMap.status" value="${itemBomFormMap.status}">
					<c:if test="${itemBomFormMap==null || itemBomFormMap.status==1}">
                    <option value=1 selected="selected">可用</option>
                    <option value=-1>不可用</option>
                    </c:if>
                    <c:if test="${itemBomFormMap.status==-1}">
                    <option value=1 >可用</option>
                    <option value=-1 selected="selected">不可用</option>
                    </c:if>
                    </select>
				</div>	
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${itemBomFormMap.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${itemBomFormMap.create_time}</span>
				</div>			
			</div>
		</div>
		<div id="tab2">
			    <div class="bt_div">
					<a class="bt_a" id="tb_add">插入新行</a>
					<a class="bt_a" id="tb_del">删除选定行</a>
					<a class="bt_a" id="tb_delAll">删除全部</a>
				</div>
				<table class="mtable" id="childItemList">
					<tr>
					    <th class="must" data-value="item_seq">顺序</th>
						<th class="must" data-value="item_id">物料编号</th>
						<th class="must" data-value="item_version">物料版本</th>
						<th class="must" data-value="use_num">使用数量</th>
						<!-- <th class="must" data-value="balance_up">结存上限</th>
						<th class="must" data-value="balance_down">结存下限</th> -->
					</tr>
					<c:forEach var="item" items="${dataBomItems}">
						<tr>
							<td data-value="${item.item_seq}">
							     <input type='text' validate_datatype='number_int'  readonly='readonly'  validate_errormsg='顺序必须为数字' value="${item.item_seq}">						     
							</td>
							<td>
							    <input type="text"
							       class="formText_query majuscule"
						           dataValue="item_no" 
						           relationId= "${item.itemCellId2}"
						           viewTitle="物料"
						           data-url="/popup/queryAllItem.shtml" 
						           validate_allowedempty='N' validate_errormsg='物料编号不能为空！'
						           id="${item.itemCellId1}" value="${item.item_no }"/>
					            <input type="hidden" id="${item.itemCellId2}" relationId= "${item.itemVersionCellId}" value="${item.item_id }" />
					            <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="${item.itemCellId1}" />
							</td>
							<td>
							     <input type='text' readonly='readonly' validate_allowedempty='N' id="${item.itemVersionCellId}"  
							     value="${item.item_version}" dataValue='item_version' validate_errormsg='版本不能为空！' >
							</td>
							<td>
							    <input type='text'  validate_datatype='number_int' validate_errormsg='使用数量必须为数字！' value="${item.use_number}" />
							</td>
							<%-- <td>
							    <input type='text'  validate_datatype='number_int' validate_errormsg='结存上限必须为数字！' value="${item.balance_up}" />
							</td>
							<td>
							    <input type='text'  validate_datatype='number_int' validate_errormsg='结存下限必须为数字！' value="${item.balance_down}" />
							</td> --%>
						</tr>
					</c:forEach>
				</table>
		</div>
		<div id="tab3">
			<table class="mtable" id="uDData">
					<tr>
						<th data-value="key">数据字段</th>
						<th data-value="value">值</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td data-value="${key.id}">${key.data_label}</td>
							<td data-value="${key.value}">
							   <input type='text' value="${key.value}">
							</td>
						</tr>
					</c:forEach>
			</table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>