<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>班次维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript" src="js/workcenter_model/shift/shift_maintenance.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</div>
	    
	    <div class="hidden">
	    <input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="shiftFormMap.id" value="${shiftFormMap.id}" id="shift_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	    </div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site}</span>
			</div>
			<div class="formField_query">
				<label class="must">班次:</label>
			    <input type="text"
			    dataValue="shift_no"
				viewTitle="班次"
				data-url="/popup/queryAllShift.shtml" 
				class="formText_query majuscule" id="tbx_shift_no" name="shiftFormMap.shift_no" value="${shiftFormMap.shift_no}"
				validate_allowedempty="N" validate_errormsg="请输入班次！" />
			    <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_shift_no">		
			</div>
		</div>


		<!--  Tab -->
		<ul id="tabs" class="tabs_li_3tabs">
			<li><a href="#" tabid="tab1">基础信息</a></li>
			<li><a href="#" tabid="tab2">人员分配</a></li>
			<li><a href="#" tabid="tab3">自定义数据</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="inputForm_content">
					<div class="formField_content">
						<label>描述:</label> <input type="text" class="formText_content"
						 name="shiftFormMap.shift_desc" value="${shiftFormMap.shift_desc}"
							id="tbx_shift_des" />
					</div>
					<div class="formField_content">
						<label class="must">班次开始(HH:mm:ss):</label> <input type="text" class="formText_content"
						name="shiftFormMap.shift_start_time" value="${shiftFormMap.shift_start_time}"
						datetime_format_type="time"
						validate_allowedempty="N" validate_datatype = "time" validate_errormsg="请输入班次开始时间点(格式举例:08:00:00)！"
							id="tbx_shift_start_time" />
					</div>
					<div class="formField_content">
						<label class="must">班次结束(HH:mm:ss):</label>
						 <input type="text" class="formText_content"
						 datetime_format_type="time"
						validate_allowedempty="N" validate_datatype = "time" validate_errormsg="请输入班次结束时间点(格式举例:18:00:00)！"
						name="shiftFormMap.shift_end_time" value="${shiftFormMap.shift_end_time}"
							id="tbx_shift_end_time" />
					</div>
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${shiftFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${shiftFormMap.create_time}</span>
					</div>
				</div>
			</div>
			<div id="tab2">
				<div class="inputForm_content">
					<table name="groups_tweenbox" class="groups_tweenTable">
						<tbody>
							<tr>
								<td>已分配用户</td>
								<td></td>
								<td>可分配用户</td>
							</tr>
							<tr>
								<td><select id="selectGroups" multiple="multiple"
									name="selectGroups">
										<c:forEach var="user" items="${selectedUserList}">
											<option value="${user.id}">${user.user_name}</option>
										</c:forEach>
								</select></td>
								<td>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selectedAll()" type="button" submit="N" title="全选">&lt;&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selected()" type="button" submit="N" title="选择">&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselected()" type="button" submit="N" title="取消">&gt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselectedAll()" type="button"  submit="N" title="全取消">&gt;&gt;</button>
									</div>
								</td>
								<td><select id="groupsForSelect" multiple="multiple">
										<c:forEach var="user" items="${unSelectedUserList}">
											<option value="${user.id}">${user.user_name}</option>
										</c:forEach>
								</select></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div id="tab3">
				<table class="mtable" id="uDData">
					<tr>
						<th data-value="key">数据字段</th>
						<th data-value="value">值</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td data-value="${key.id}">${key.data_label}</td>
							<td data-value="${key.value}">
							   <input type='text' value="${key.value}">
							</td>
						</tr>
					</c:forEach>
			    </table>
			</div>
		</div>

		<!--  Tab -->
	</form>
</body>
</html>