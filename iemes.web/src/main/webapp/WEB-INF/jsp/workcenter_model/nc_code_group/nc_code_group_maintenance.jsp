<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>不良代码组维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

<script type="text/javascript" src="js/workcenter_model/nc_code_group/nc_code_group_maintenance.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</div>
		
		<div class="hidden">
			<input type="hidden" value="${page_res_id}" id="page_res_id">
			<input type="hidden" name="NcCodeGroupFormMap.id" value="${ncCodeGroupFormMap.id}" id="nc_code_group_id">
			<input type="hidden" value="${errorMessage}" id="errorMessage">
			<input type="hidden" value="${successMessage}" id="successMessage">
	    </div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site}</span>
			</div>
			<div class="formField_query">
				<label class="must">不良代码组编号:</label>
				<input type="text"
			    dataValue="nc_code_group_no"
				viewTitle="不良代码组"
				data-url="/popup/queryAllNcCodeGroup.shtml" 
				class="formText_query majuscule" id="tbx_nc_code_group_no" name="ncCodeGroupFormMap.nc_code_group_no" value="${ncCodeGroupFormMap.nc_code_group_no}"
				validate_allowedempty="N" validate_errormsg="请输入不良代码组编号！" />
			    <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_nc_code_group_no">	
			</div>
		</div>


		<!--  Tab -->
		<ul id="tabs" class="tabs_li_2tabs">
			<li><a href="#" tabid="tab1">基础信息</a></li>
			<li><a href="#" tabid="tab2">不良代码分配</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="inputForm_content">
					<div class="formField_content">
						<label>描述:</label> <input type="text" class="formText_content"
						    name="ncCodeGroupFormMap.nc_code_group_desc" value="${ncCodeGroupFormMap.nc_code_group_desc}"
							id="tbx_nc_code_group_desc" />
					</div>                   
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${ncCodeGroupFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${ncCodeGroupFormMap.create_time}</span>
					</div>
				</div>
			</div>
			<div id="tab2">
				<div class="inputForm_content">
					<table name="groups_tweenbox" class="groups_tweenTable">
						<tbody>
							<tr>
								<td>已分配的不良代码</td>
								<td></td>
								<td>可分配的不良代码</td>
							</tr>
							<tr>
								<td><select id="selectGroups" multiple="multiple"
									name="selectGroups">
										<c:forEach var="ncCode" items="${selectedNcCodeList}">
											<option value="${ncCode.id}">${ncCode.nc_code}</option>
										</c:forEach>
								</select></td>
								<td>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selectedAll()" type="button" submit="N" title="全选">&lt;&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="selected()" type="button" submit="N" title="选择">&lt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselected()" type="button" submit="N" title="取消">&gt;</button>
									</div>
									<div class="groups_tweenTable_centerDiv">
										<button onclick="unselectedAll()" type="button"  submit="N" title="全取消">&gt;&gt;</button>
									</div>
								</td>
								<td><select id="groupsForSelect" multiple="multiple">
										<c:forEach var="ncCode" items="${unSelectedNcCodeList}">
											<option value="${ncCode.id}">${ncCode.nc_code}</option>
										</c:forEach>
								</select></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!--  Tab -->
	</form>
</body>
</html>