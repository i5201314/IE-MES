<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>生产操作员-按钮维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/work_process/pod_button/pod_button_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<input type="button" id="btnQuery" value="检索"> 
		<input type="submit" id="btnSave"  value="保存"> 
		<input type="button" id="btnClear" value="清除"> 
		<input type="button" id="btnDelete" value="删除">
	</div>
	
	<div class="hidden">
		<input type="hidden" name="podButtonFormMap.id" value="${podButtonFormMap.id}" id="pod_button_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site }</span>
		</div>
		<div class="formField_query">
			<label class="must">POD按钮编号:</label> 
			<input type="text"
				dataValue="pod_button_no" 
				viewTitle="POD按钮"
				data-url="/popup/queryAllPodButton.shtml" 
				class="formText_query majuscule" id="tbx_pod_button_no" validate_allowedempty="N" validate_errormsg="请输入POD按钮编号！" 
				name="podButtonFormMap.pod_button_no" value="${podButtonFormMap.pod_button_no }"/>
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_pod_button_no">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_1tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label>POD按钮名称:</label> <input type="text"
						class="formText_content" id="tbx_pod_button_name" name="podButtonFormMap.pod_button_name" value="${podButtonFormMap.pod_button_name }"/>
				</div>
				<div class="formField_content">
					<label>POD按钮触发器:</label> 
					<input type="text"
						dataValue="pod_function_no" 
						viewTitle="POD功能"
						relationId="input_pod_function_id"
						data-url="/popup/queryAllPodFunction.shtml" 
						class="formText_query majuscule" id="tbx_pod_button_trigger" validate_allowedempty="N" validate_errormsg="请输入POD功能编号！" 
						value="${podFunctionFormMap.pod_function_no }"/>
					<input type="hidden" id="input_pod_function_id" dataValue="id" name="podButtonFormMap.pod_function" value="${podButtonFormMap.pod_function }">	
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_pod_button_trigger">
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${podButtonFormMap.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${podButtonFormMap.create_time}</span>
				</div>	
			</div>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>