<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>记录不良</title>
<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/work_process/pod_operation/pod_record_nocode_maintenance.js"></script>
</head>
<body>
	<div id="mds_form_content">
		<form id="record_nccode_form" >
			<div class="hidden_div">
				<input type="hidden" name="sfcNcFormMap.sfc" value="${ncMap.sfc }" id="sfc">
				<input type="hidden" name="sfcNcFormMap.process_workflow_id" value="${ncMap.process_workflow_id }" id="process_workflow_id">
				<input type="hidden" name="sfcNcFormMap.sfc_step_id" value="${ncMap.id }" id="sfc_step_id">
				<input type="hidden" name="sfcNcFormMap.item_id" value="${ncMap.item_sfc_id }" id="item_sfc_id">
				<input type="hidden" name="sfcNcFormMap.main_item_id" value="${ncMap.main_item_id }" id="main_item_id">
				<input type="hidden" name="sfcNcFormMap.workshop_id" value="${ncMap.workshop_id }" id="workshop_id">
				<input type="hidden" name="sfcNcFormMap.workline_id" value="${ncMap.workline_id }" id="workline_id">
				<input type="hidden" name="sfcNcFormMap.operation_id" value="${ncMap.operation_id }" id="operation_id">
				<input type="hidden" name="sfcNcFormMap.work_resource_id" value="${ncMap.work_resource_id }" id="work_resource_id">
				<input type="hidden" name="sfcNcFormMap.shoporder_id" value="${ncMap.shoporder_id }" id="shoporder_id">
				<input type="hidden" name="sfcNcFormMap.use_num" value="${ncMap.use_num }" id="use_num">
			</div>

			<div class="mds_container">
				<div class="mds_row">
					<div class="col-md-5 col-sm-5">
						<label class="assemble_form_label">不良代码组：</label> <input
							type="text" class="assemble_form_input"
							dataValue="nc_code_group_no" viewTitle="不良代码组"
							relationId="nc_code_group_id"
							data-url="/popup/queryAllNcCodeGroup.shtml"
							id="nc_code_group_input"> <input type="hidden"
							id="nc_code_group_id" dataValue="id"
							validate_errormsg="不良代码组不能为空！"
							name="sfcNcFormMap.nc_code_group_id"> <input
							type="button" submit="N" value="检索"
							onclick="operationBrowse(this)" textFieldId="nc_code_group_input">
					</div>
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-4 col-sm-4">
						<label class="assemble_form_label">不良代码：</label> <input
							type="text" class="assemble_form_input" dataValue="nc_code"
							viewTitle="不合格代码" data-url="/popup/queryNcCodeByGroup.shtml"
							filterId="nc_code_group_id" relationId="nc_code_id"
							id="nc_code_input" name="sfcNcFormMap.item_sfc"> <input
							type="hidden" id="nc_code_id" dataValue="id"
							validate_errormsg="不良代码不能为空！" name="sfcNcFormMap.nc_code_id">
						<input type="button" submit="N" value="检索"
							onclick="operationBrowse(this)" textFieldId="nc_code_input">
					</div>
				</div>
				<div class="mds_row-10"></div>
				<div class="mds_row">
					<div class="col-md-5 col-sm-5">
						<label class="assemble_form_label">备注：</label>
						<textarea class="assemble_form_textarea_ext" name="sfcNcFormMap.nc_desc" style="margin-right: 45px; padding-right: 20px;"></textarea>
					</div>
				</div>
				<div class="mds_row">
					<div class="assemble_from_tr_ext">
						<input type="button" value="记录不合格" class="nccode_button" id="bt_record_nccode">
						<input type="reset" value="清空" class="nccode_button">
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>