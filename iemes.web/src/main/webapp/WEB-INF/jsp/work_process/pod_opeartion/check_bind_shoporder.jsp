<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/js/common/common.jspf"%>
<script type="text/javascript" src="${ctx}/js/common/mtable.js"></script>
<script type="text/javascript" src="${ctx}/js/work_process/pod_operation/check_bind_shoporder.js"></script>
<title>MDS IE-MES</title>
</head>
<body class="openPopup">
	<div class="openPopup_title">选择要绑定的工单</div>
	<form class="mds_tab_form">
		<table class="mtable" id="check_bind_shoporder">
			<tr>
				<th class="mtable_head_hide" data-value="shoporder_id">shoporder_id</th>
				<th data-value="shoporder_no">工单</th>
			</tr>
			<c:forEach var="data" items="${dataList}">
				<tr>
					<td class="mtable_head_hide" >${data.id}</td>
					<td>${data.shoporder_no}</td>
				</tr>
			</c:forEach>
		</table>
	</form>
</body>
</html>