<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<%@include file="/js/common/common.jspf"%>
<title>车间物料消耗看板</title>
    <script type="text/javascript" src= "${ctx}/js/common/mtable.js"></script>
	<script type="text/javascript" src="${ctx}/js/report/workshop_item_consumption_kanban.js"></script>	
</head>

<body class="report_body" >	
<form class="mds_tab_form">
	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div> 
	<div class="hidden">
		<input type="hidden" value="${workshop_id}" id="input_workshop_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
    </div>
	<div id="container" class="report_container">
			<div>
			    <label class="report_kanban_title_label">物料消耗看板</label>
				<label class="report_kanban_date_label" id="kanban_date"></label>
			</div>
			<table class="mtable" id="mtable_item_list">
					<tr>
						<th>物料编号</th>
						<th>物料名称</th>
						<th>库存总数</th>
						<th>消耗数量</th>
						<th>库存数量</th>	
						<!-- <th>库存上限</th>	
						<th>安全库存数</th>	 -->					
					</tr>
			 </table>
	</div>
</form>
</body>
</html>
