<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>生产进度报表</title>
<script type="text/javascript" src="js/report/work_schedule_report.js"></script>
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
</head>
<body class="report_body">	
<form class="mds_tab_form">
        <div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<input type="button" id="btnQuery" value="检索"> 
			<input type="button" id="btnClear" value="清除"> 
		</div>	
		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
		</div>
		<!-- 报表div -->
		<div class="report_header">
			<div class="formField_query">
				<label class="must">站点:</label> <span>${site}</span>
			</div>
			<div class="formField_query">
			<label class="must">工单编号:</label>
			<input type="text"
			    dataValue="shoporder_no"
				viewTitle="工单"
				relationId="tbx_input_shoporder_id"
				data-url="/popup/queryAllShoporder.shtml" 
				class="formText_query majuscule" id="tbx_shoporder_no""
				validate_allowedempty="N" validate_errormsg="请输入工单编号！" />
			 <input type="hidden" id="tbx_input_shoporder_id" dataValue="id" >	
		     <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_shoporder_no">		
			</div>
   		</div> 
		<div id="container" class="report_container"></div>
		
		<!-- 报表div -->
</form>
</body>
</html>