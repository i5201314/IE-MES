<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>不合格品处置</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<input type="button" id="btnQuery" value="检索"> 
		<input type="button" id="btnSave"  value="保存"> 
		<input type="button" id="btnClear" value="清除"> 
		<input type="button" id="btnDelete" value="删除">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">*</span>
		</div>
		<div class="formField_query">
			<label class="must">车间作业控制:</label> <input type="text"
				class="formText_query" id="tbx_sfc_no" validate_allowedempty="N" validate_errormsg="请输入车间作业控制！" />
			<input type="button" submit="N" value="检索">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_2tabs">
		<li><a href="#" tabid="tab1">产品信息</a></li>
		<li><a href="#" tabid="tab2">处置</a></li>
	</ul>
	<div id="content">
	     <div id="tab1">
	 		<div class="inputForm_content">
				<div class="formField_content">
					<label>产品名称:</label> <span class="formField_content">测试产品1</span>
				</div>
				<div class="formField_content">
					<label>产品描述:</label><span class="formField_content">用于测试</span>
				</div>
				<div class="formField_content">
					<label>所属工单:</label> <span class="formField_content">so001</span>
				</div>
				<div class="formField_content">
					<label>异常工位:</label>  <span class="formField_content">aoi</span>
				</div>
				<div class="formField_content">
					<label>不良代码组:</label>  <span class="formField_content">ncg01</span>
				</div>
				<div class="formField_content">
					<label>不良代码:</label>  <span class="formField_content">nc01</span>
				</div>
				<div class="formField_content">
					<label>不良代码描述:</label>  <span class="formField_content">nc01des</span>
				</div>
				<div class="formField_content">
					<label>操作人:</label> <span class="formText_content">Test</span>
				</div>
				<div class="formField_content">
					<label>操作时间:</label> <span class="formText_content">1990-01-01 10:10</span>
				</div>	
			</div>
		  </div>
		  <div id="tab2">
			<div class="inputForm_content">
					<div class="formField_content">
						<label>处置方式:</label> <select class="formText_content"
							id="select_handle_type">
							<option>维修</option>
							<option>报废</option>
						</select>
					</div>
					<div class="formField_content">
						<label>指定新的步骤:</label> <input type="text" class="formText_content"
							id="tbx_new_opeartionstep" /> <input type="button" submit="N"
							value="检索">
					</div>
					<div class="formField_content">
						<label>备注:</label> <input type="text" class="formText_content"
							id="tbx_remark" />
					</div>
			</div>
		  </div>

		</div>

	<!--  Tab -->
	</form>
</body>
</html>