$(document).ready(function (e){
	$('table.mtable tr:gt(0)').bind('click',function (e){
		$(this).addClass("tr_select").siblings().removeClass("tr_select"); 
	});
	
	$.MTable = {
		getTableData : function (id){
			var rows = $('#'+id+' tr');
			var tableData = [];
			for (var j=1; j<rows.length; j++) {
				var row = rows[j].children;
				var headers = rows[0].children;
				
				var rowData = {};
				for (var i=0; i<row.length; i++) {
					headerKeys = $(headers).eq(i).attr("data-value");
					var cell = {};
					if (row[i].children!=undefined && row[i].children[0]!=undefined) {
						if (row[i].children[0].tagName=="INPUT") {
							var val = $(row[i].children[0]).eq(0).val();
							cell.viewValue = val;
							cell.value = val;
							rowData[headerKeys] = cell;
						}
					}else {
						cell.viewValue = row[i].innerText;
						cell.value = $(row[i]).eq(i).attr("data-value");
						rowData[headerKeys] = cell;
					}
				}
				tableData.push(rowData);
			}
			console.log(tableData);
			return tableData;
		},
		getTableDataWithMultiInputsInOneCell : function (id){
			var rows = $('#'+id+' tr');
			var tableData = [];
			for (var j=1; j<rows.length; j++) {
				var row = rows[j].children;
				var headers = rows[0].children;
				
				var rowData = {};
				for (var i=0; i<row.length; i++) {
					headerKeys = $(headers).eq(i).attr("data-value");
					var cell = {};
					if (row[i].children!=undefined && row[i].children[0]!=undefined) {
						var cellInputs = [];
						var cellControls = row[i].children;
						for(var k=0;k<cellControls.length;k++){
							if (row[i].children[k].tagName=="INPUT") {
								var val = $(row[i].children[k]).eq(0).val();
								cellInputs.push(val);
							}
						}						
						cell["viewValue"] = cellInputs;
						cell["value"] = cellInputs;
						
						rowData[headerKeys] = cell;
					}else {
						var cellValue = [];
						var cellView = [];
						cellValue.push($(row[i]).eq(i).attr("data-value"));
						cellView.push(row[i].innerText);
						
						cell["value"] = cellValue;
						cell["viewValue"] = cellView;
						
						rowData[headerKeys] = cell;
					}
				}
				tableData.push(rowData);
			}
			console.log(tableData);
			return tableData;
		},
		getCheckRowDataByRowCol : function (id,headLine, dataCol){		//可以指定头的行，和从第几列开始读取
			if ($('#'+id+' .tr_select').length==0) {
				$.MsgBox.Alert("系统提示", "未选中任何行！");
			}else {
				var rowData = {};
				var row = $('#'+id+' .tr_select td');
				var headers = $('#'+id+' tbody tr')[headLine].children;
				for (var i=dataCol; i<row.length; i++) {
					headerKeys = $(headers).eq(i).attr("data-value")
					rowData[headerKeys] = row[i].innerText;
				}
				return rowData;
			}
		},
		getCheckRowData : function (id){
			return $.MTable.getCheckRowDataByRowCol(id,0,0);
		},
		getClickRowData : function (id,obj){
			var rowTds = $(obj).parent().parent().find("td");
			var rowData = {};
			$(rowTds).each(function (index,element) {
				var head = $('#'+id+' th:eq('+index+')').data("value");
				rowData[head] = $(element).text();
			});
			return rowData;
		},
		delCheckRow : function (id){
			if ($('#'+id+' .tr_select').length==0) {
				$.MsgBox.Alert("系统提示", "未选中任何行！");
			}else {
				$('#'+id+' .tr_select').remove();
			}
		},
		addRow : function (id,newRow){
			$('#'+id+" tr:last").after(newRow);
			$('#'+id+' tr:gt(0)').bind('click',function (e){
				$(this).addClass("tr_select").siblings().removeClass("tr_select"); 
			});
		},
		delAll : function(id){
			$('#'+id+" tr:not(:first)").remove();
		},
		clickFun : function (id,callback){
			$('#'+id).dblclick(function (){
				if (typeof callback === "function"){
					var row = $.MTable.getCheckRowDataByRowCol(id,1,0);
					callback(row);
		        }
			})
		},clickFun2 : function (id,callback,headLine,dataCol){
			$('#'+id).dblclick(function (){
				if (typeof callback === "function"){
					var row = $.MTable.getCheckRowDataByRowCol(id,headLine,dataCol);
					callback(row);
		        }
			})
		}
	}
})