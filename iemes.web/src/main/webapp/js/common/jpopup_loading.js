(function () {  
    $.MsgLoading = {   
        Loading: function (title, msg) {  
        	GenerateLoadingHtml("confirm", title, msg);  
        },
        UnLoading: function () { 
        	$("#mb_box,#mb_con").remove();  
    } 
    }  
    // 生成Html
    var GenerateLoadingHtml = function (type, title, msg) {
    	if (title==undefined || title == "" || title == null) {
    		title = "系统提示";
    	}
        var _html = "";  
        _html += '<div id="mb_box"></div><div id="mb_con"><span id="mb_tit">' + title + '</span>';  
        //_html += '<div id="mb_con"><span id="mb_tit">' + title + '</span>';  
        _html += '<div id="mb_msg"><span>' + msg + '</div></div>';   
        _html += '</div>';  
        // 必须先将_html添加到body，再设置Css样式
        $("body").append(_html);   
        // 生成Css
        GenerateLoadingCss();  
    }  
  
    // 生成Css
    var GenerateLoadingCss = function () {  
        /*$("#mb_box").css({ width: '100%', height: '100%', zIndex: '99999', position: 'fixed',  
            filter: 'Alpha(opacity=60)', backgroundColor: 'black', top: '0', left: '0', opacity: '0.6'  
        });*/  
    	$("#mb_box").css({ width: '100%', height: '100%', zIndex: '99999', position: 'fixed',  
            filter: 'Alpha(opacity=60)', top: '0', left: '0', opacity: '0.6'  
        });
        $("#mb_con").css({ zIndex: '999999', width: '400px', position: 'fixed',  
            backgroundColor: 'White', borderRadius: '15px'  
        });  
        $("#mb_tit").css({ display: 'block', fontSize: '14px', color: '#444', padding: '10px 15px',  
            backgroundColor: '#DDD', borderRadius: '15px 15px 0 0',  
            borderBottom: '3px solid #009BFE', fontWeight: 'bold'  
        });  
        $("#mb_msg").css({ padding: '20px', lineHeight: '20px', fontSize: '13px',
        	  "background-image": "url(images/loading.gif)",
              "background-size": "10% 50%",
              "background-repeat": "no-repeat",
              "background-position": "10px 15px",
              "margin-left": "10px"   
        	});  
        $("#mb_msg span").css({ padding: '18%'
      	}); 
        
        var _widht = document.documentElement.clientWidth;  // 屏幕宽
        var _height = document.documentElement.clientHeight; // 屏幕高
        var boxWidth = $("#mb_con").width();  
        var boxHeight = $("#mb_con").height();  
        // 让提示框居中
        $("#mb_con").css({ top: (_height - boxHeight) / 2 + "px", left: (_widht - boxWidth) / 2 + "px" });  
    }   
})();