$(document).ready(function() {		
		if (!stringIsNull($('#errorMessage').val())) {
			showErrorNoticeMessage($('#errorMessage').val());
		}
		
		if (!stringIsNull($('#successMessage').val())) {
			showSuccessNoticeMessage($('#successMessage').val());
		}
		var page_res_id = $('#page_res_id').val();
		//保存
		$("form").submit(function() {
			var data = $("form").serialize();
			$.post(rootPath + "/workcenter_model/site/saveSite.shtml", data, function(e, status, xhr){
				var data= JSON.parse(e);
				if (data.status) {
					showSuccessNoticeMessage("站点信息保存成功");
				}else {
					//showErrorNoticeMessage("站点信息保存失败："+data.message);
					showErrorNoticeMessage(data.message);
				}
			},"json")
			return false;
		});
		
		//检索
		$('#btnQuery').click(function (){
			var site = $('#tbx_site_code').val();
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/site/querySiteDataBySiteCode.shtml?site="+site+"&page_res_id="+page_res_id);

		})
		
		//清除
		$('#btnClear').click(function (){
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/site/site_maintenance.shtml?page_res_id="+page_res_id);
		})
		
		$('#btnDelete').click(function (){
			$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
				$.MsgBox.Confirm("","确定要删除站点吗？站点删除操作会导致该站点所有功能无法使用，请慎重！",function (){
					$.MsgBox.Confirm("","点击确认，系统会执行删除站点的动作，请慎重！",function (){
						var site_id = $('#site_id').val();
						
						var tb = $(".index_centent");
						tb.empty();
						tb.load(rootPath + "/workcenter_model/site/delSite.shtml?site_id="+site_id+"&page_res_id="+page_res_id);
					});
				});
			});
		})
	})