$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {	
		var data = $("form").serialize();
		var udefine_data = $.MTable.getTableData("uDData");
		/*udefine_data = JSON.stringify(udefine_data).replace(/"/g,"'");*/
		udefine_data = JSON.stringify(udefine_data);
		
		data = data+"&ncCodeFormMap.udefined_data="+udefine_data;
		
		$.post(rootPath + "/workcenter_model/nc_code/saveNcCode.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("不良代码信息保存成功");
			}else {
				//showErrorNoticeMessage("不良代码信息保存失败："+data.message);
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var nc_code = $('#tbx_nc_code').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/nc_code/queryNcCode.shtml?nc_code="+nc_code+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/nc_code/nc_code_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var nc_code_id = $('#nc_code_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/nc_code/delNcCode.shtml?nc_code_id="+nc_code_id+"&page_res_id="+page_res_id);
		});
	})
	
	//----------------动态表格-----------------------------
	//插入新行
	$('#tb_add').click(function (){
		var num = 0;
		var itemCellId1= "";
		var itemCellId2= "";
		var itemVersionCellId= "";
		var trs = $('#childItemList tr');
		for (var i=1;i<trs.length;i++) {
			var td = trs.eq(i).find("input:first");
			if (td[0]!=undefined) {
				if (td[0].value!=undefined && parseInt(td[0].value)>=num) {
					num = parseInt(td[0].value);		
				}
			}
		}
		num += 10;
		itemCellId1 = "itemCellId1_" + num.toString();
		itemCellId2 = "itemCellId2_" + num.toString();
		itemVersionCellId = "versionCellId_" + num.toString();
		var newRow = "<tr>" +
				"<td><input type='text' validate_datatype='number_int' readonly='readonly' validate_errormsg='顺序必须为数字' value="+num+"></td>" +
				/*"<td data-value='"+ num +"'"+">"+ num +"</td>" +*/
				"<td>" +
				       "<input type='text' "+ 
				       "class='formText_query majuscule' " +
			           "dataValue='item_no' "+ 
			           "relationId='" + itemCellId2 +"' " +
			           "viewTitle='物料' "+ 
			           "data-url='/popup/queryAllItem.shtml'  "+ 
			           "validate_allowedempty='N' validate_errormsg='物料编号不能为空！' "+ 
			           "id='"+ itemCellId1 + "' /> "+ 
		               "<input type='hidden' id='"+ itemCellId2 +"' dataValue='id' relationId='" + itemVersionCellId +"'> "+ 
		               "<input type='button' submit='N' value='检索' onclick='operationBrowse(this)' textFieldId='"+ itemCellId1 +"' > "+ 
				"</td>" +
				"<td><input type='text' readonly='readonly' validate_allowedempty='N' id='"+itemVersionCellId+"' dataValue='item_version' validate_errormsg='版本不能为空！' ></td>" +
				"<td><input type='text'  validate_datatype='number_int' validate_errormsg='使用数量必须为数字！'></td>" +
				"<td><input type='text'  validate_datatype='number_int' validate_errormsg='结存上限必须为数字！'></td>" +
				"<td><input type='text'  validate_datatype='number_int' validate_errormsg='结存下限必须为数字！'></td>" +
			    "</tr>";
		$.MTable.addRow("childItemList",newRow);
	})
	
	//删除选定行
	$('#tb_del').click(function (){
		$.MTable.delCheckRow("childItemList");
	})
	
	//删除全部行
	$('#tb_delAll').click(function (){
		$.MsgBox.Confirm("系统提示","该操作会清空下列所有行，确定继续吗？",function (){
			$.MTable.delAll("childItemList");
		});
	})
	
})

