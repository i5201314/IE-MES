$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		var udefine_data = $.MTable.getTableData("uDData");
		udefine_data = JSON.stringify(udefine_data);
		data = data+"&workCenterFormMap.udefined_data="+udefine_data;
		
		$.post(rootPath + "/workcenter_model/workcenter/saveWorkCenter.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("工作中心保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var version = $('#tbx_version').val();
		var workcenter_no = $('#tbx_workcenter_code').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/workcenter/queryWorkCenter.shtml?version="+version +"&workcenter_no="+workcenter_no+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/workcenter/workcenter_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var workcenter_id = $('#workcenter_id').val();
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/workcenter/delWorkCenter.shtml?workcenter_id="+workcenter_id+"&page_res_id="+page_res_id);
		});
	})
})