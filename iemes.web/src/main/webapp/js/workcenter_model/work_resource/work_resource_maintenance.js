$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {	
		var data = $("form").serialize();
		var udefine_data = $.MTable.getTableData("work_resource_table");
		
		/*udefine_data = JSON.stringify(udefine_data).replace(/"/g,"'");*/
		udefine_data = JSON.stringify(udefine_data);
		
		data = data+"&workResourceFormMap.udefined_data="+udefine_data;
		
		$.post(rootPath + "/workcenter_model/work_resource/saveWorkResource.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("资源信息保存成功");
			}else {
				//showErrorNoticeMessage("资源信息保存失败："+data.message);
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var work_resource_no = $('#tbx_work_resource_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/work_resource/queryWorkResource.shtml?work_resource_no="+work_resource_no+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/work_resource/work_resource_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var work_resource_id = $('#work_resource_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/work_resource/delWorkResource.shtml?work_resource_id="+work_resource_id+"&page_res_id="+page_res_id);
		});
	})
	
	//绑定资源类型列表
	var url = rootPath + '/workcenter_model/work_resource/work_resource_type_list.shtml';
	var dataWorkResourceTypeList = "";
	var data = "";
	$.ajax({
        url:url,
        dataType: 'json',
        async: false,//默认异步调用 false：同步
        data:data,
        type:'POST',
        success:function(data){
        	dataWorkResourceTypeList = data;
        }
    });
	if (dataWorkResourceTypeList != null) {
		var currWorkResourceTypeId = $("#work_resource_type_id").val();
		var isBindWorkResourceType = "N";
		var h = "";
		for ( var i = 0; i < dataWorkResourceTypeList.length; i++) {
			if($.trim(currWorkResourceTypeId) == $.trim(dataWorkResourceTypeList[i].id)){
				isBindWorkResourceType = "Y";
				h+="<option selected=\"selected\" value='" + dataWorkResourceTypeList[i].id + "'>"+ dataWorkResourceTypeList[i].resource_type + "</option>";
			}else{
				h+="<option value='" + dataWorkResourceTypeList[i].id + "'>"+ dataWorkResourceTypeList[i].resource_type + "</option>";
			}			
		}
		if(isBindWorkResourceType=="N"){
		    h+="<option selected='selected' value=''>无</option>";
		}else{
			h+="<option value=''>无</option>";
		}
		$("#select_work_resource_type_id").html(h);
	} else {
		showErrorNoticeMessage("获取资源类型信息错误，请联系管理员！");
	}	
})

function extraction_mTableData(){
	//动态表格提交数据生成
	var d = []
	var trs = $('table.mtable tr');
	for (var i=1;i<trs.length;i++) {
		var ipts = trs.eq(i).find("input");
		var dd = [];
		for (var j=0;j<ipts.length;j++) {
			if (ipts[j]!=undefined) {
				if (ipts[j].value!=undefined) {
					dd.push(ipts[j].value);
				}
			}
		}
		d.push(dd);
	}
	
	d = JSON.stringify(d).replace(/"/g,"'");
	return d;
}

