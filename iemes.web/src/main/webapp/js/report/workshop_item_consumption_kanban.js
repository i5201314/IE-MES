$(document).ready(function() {
	updateTime();
	getReport();
	setInterval("updateTime()",1000);
	setInterval("getReport()",10000);
})

function p(s) {
    return s < 10 ? '0' + s: s;
}

function updateTime(){
	var myDate = new Date();
	//获取当前年
	var year=myDate.getFullYear();
	//获取当前月
	var month=myDate.getMonth()+1;
	//获取当前日
	var date=myDate.getDate(); 
	var h=myDate.getHours();       //获取当前小时数(0-23)
	var m=myDate.getMinutes();     //获取当前分钟数(0-59)
	var s=myDate.getSeconds();  

	var now = year+'-'+p(month)+"-"+p(date)+" "+p(h)+':'+p(m)+":"+p(s);
	
	//设置 kanban_date 的值
	var kanban_date = $("#kanban_date").html(now); 
}

function getReport(){
	var workshop_id = $("#input_workshop_id").val();
	if(workshop_id==null || workshop_id==""){
		showErrorNoticeMessage('车间不能为空');
		return;
	}
	$("#container").show();

    var data = 'formMap.reportMethod=getInventoryData&formMap.workshop_id='+ workshop_id;
	$.ajax({
		type: 'POST', 
		data: data, 
		url: '/IE-MES/report/getReportData.shtml',
        success: function (e,status) {
        	$.MTable.delAll("mtable_item_list");
        	 var rowCount = 0;
        	 var data= JSON.parse(e);
        	 data = JSON.parse(data);
        	 if (data.result) {
        		 /* <th>物料编号</th>
					<th>物料名称</th>
					<th>库存总数</th>
					<th>消耗数量</th>
					<th>结存数量</th>	
					<th>库存上限</th>	
					<th>安全库存数</th>*/	
        		for (var d in data.result){
        			var newRow = "<tr>" +
    				"<td>"+data.result[d].item_no+"</td>" +
    				"<td>"+data.result[d].item_name+"</td>" +
    				"<td>"+data.result[d].zs+"</td>" +
    				"<td>"+data.result[d].us+"</td>" +
    				"<td>"+data.result[d].kc+"</td>" +
    				/*"<td>"+data.result[d].balance_up+"</td>" +
    				"<td>"+data.result[d].balance_down+"</td>" +*/
    			    "</tr>";
    		        $.MTable.addRow("mtable_item_list",newRow);
    		        
    		        rowCount += 1;
        		}
        		
        		var minRowCount = 15;
        		if(rowCount < minRowCount){
        			for(var i = rowCount;i<=minRowCount;i++){
        				var newRow = "<tr>" +
        				"<td></td>" +
        				"<td></td>" +
        				"<td></td>" +
        				"<td></td>" +
        				"<td></td>" +
        				/*"<td></td>" +
        				"<td></td>" +*/
        			    "</tr>";
        				$.MTable.addRow("mtable_item_list",newRow);
        			}
        		}
        		
        		$("#mtable_item_list tr th").css({"color":"white","background":"red","height":"40px","font-size":"25px"});
        		$("#mtable_item_list tr td").css({"color":"white","background":"black","font-size":"25px"});
        		//showSuccessNoticeMessage("检索成功");

        	}else {
        		showErrorNoticeMessage("检索失败："+data.message);
        	}
        },
        error: function (xhr,e) {
        	showErrorNoticeMessage('请求失败,请联系管理员');
        } 
    });
}


