$(document).ready(function() {
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	// 检索
	$('#btnQuery').click(function() {
		var sfc = $("#input_sfc").val();
		var tb = $(".index_centent");
		tb.empty();
		var data_url = rootPath + "/report/query_sfc_assembly_report.shtml?sfc="+sfc;
		tb.load(data_url);
	})

	// 清除
	$('#btnClear').click(function() {
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/report/sfc_assembly_report.shtml");
	})
})