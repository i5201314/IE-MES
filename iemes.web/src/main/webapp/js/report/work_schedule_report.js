$(document).ready(function() {
	// 检索
	$('#btnQuery').click(function() {
		getReport();
	})

	// 清除
	$('#btnClear').click(function() {
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/report/workshop_inventory_report.shtml");
	})
})

function getReport() {
	var shoporder_id = $("#tbx_input_shoporder_id").val();
	if (shoporder_id == null || shoporder_id == "") {
		showErrorNoticeMessage("工单不能为空");
		return;
	}

	$("#container").show();
	var dom = document.getElementById("container");
	var myChart = echarts.init(dom);
	myChart.showLoading();
	var app = {};
	option = null;

	app.title = '嵌套环形图';

	option = {
		color : [ '#A2CD5A', '#8B8970', '#696969' ],
		backgroundColor : {
			type : 'linear',
			x : 0,
			y : 0,
			x2 : 0,
			y2 : 1,
			colorStops : [ {
				offset : 0,
				color : '#F0F0F0' // 0% 处的颜色
			}, {
				offset : 1,
				color : '#B0C4DE' // 100% 处的颜色
			} ],
			globalCoord : false
		// 缺省为 false
		},
		title : [ {
			text : '生产进度报表',
			x : '45%',
		}, {
			text : '当天生产情况',
			x : '25%',
			y : '12%',
			textAlign : 'center'
		}, {
			text : '生产进度',
			x : '75%',
			y : '12%',
			textAlign : 'center'
		} ],
		tooltip : {},
		grid : [ {
			top : 200,
			width : '50%',
			bottom : '25%',
			left : 35,
			containLabel : true
		} ],
		xAxis : [ {
			type : 'category',
			data : [],
			axisTick : {
				alignWithLabel : true
			}
		} ],
		yAxis : [ {
			type : 'value'
		} ],
		series : [ {
			gridIndex : 1,
			name : '生产进度',
			type : 'pie',
			radius : '45%',
			center : [ '75%', '45%' ],
			data : [],
			itemStyle : {
				emphasis : {
					shadowBlur : 10,
					shadowOffsetX : 0,
					shadowColor : 'rgba(0, 0, 0, 0.5)'
				}

			}
		}, {
			color : [ '#7EC0EE' ],
			name : '已生产',
			type : 'bar',
			barWidth : '60%',
			data : []
		} ]
	};

	myChart.hideLoading();

	if (option && typeof option === "object") {
		myChart.setOption(option, true);
	}

	myChart.showLoading();
	var data = 'formMap.reportMethod=getScheduleReportData&formMap.shoporder_id='
			+ shoporder_id;
	$.ajax({
		type : 'POST',
		data : data,
		url : '/IE-MES/report/getReportData.shtml',
		success : function(e,status) {
			var data = JSON.parse(e);
			data = JSON.parse(data);
			if (data.status) {
				var newoption = myChart.getOption();
				var xAxisData = [];
				var yAxisData = [];
				if (data.result.length == 0) {
					layer.msg("未查询到该工单下的数据");
					myChart.hideLoading();
					newoption.series[0].data = [];
					newoption.xAxis[0].data = [];
					newoption.series[1].data = [];
					myChart.setOption(newoption);
					return;
				}
				var total = data.result[0].all;
				var done = data.result[0].value;
				var scrapValue = data.result[0].scrapvalue;
				var todayDoneNum = 0;

				for (var i = 0; i < data.result.length; i++) {
					if (data.result[i].name != null) {
						xAxisData.push(data.result[i].name);
						yAxisData.push(data.result[i].rangeDoneNum);

						todayDoneNum += data.result[i].rangeDoneNum;
					}
				}

				newoption.title[1].subtext = "工单总计:" + data.result[0].all
						+ ";当天生产：" + todayDoneNum;
				var jsondata = [
						{
							value : done,
							name : '已生产('
									+ (done / total * 100)
											.toPrecision(3) + '%)'
						},
						{
							value : (total - done - scrapValue),
							name : '未生产('
									+ ((total - done - scrapValue)
											/ total * 100)
											.toPrecision(3) + '%)'
						},
						{
							value : scrapValue,
							name : '报废('
									+ (scrapValue / total * 100)
											.toPrecision(3) + '%)'
						} ];
				newoption.series[0].data = jsondata;
				newoption.xAxis[0].data = xAxisData;
				newoption.series[1].data = yAxisData;
				myChart.hideLoading();
				myChart.setOption(newoption);
				
				showSuccessNoticeMessage("检索成功");

			} else {
				showErrorNoticeMessage("检索失败："+data.message);
			}
		},
		error : function(xhr, e) {
			showErrorNoticeMessage('请求失败,请联系管理员');
		}
	});
}
