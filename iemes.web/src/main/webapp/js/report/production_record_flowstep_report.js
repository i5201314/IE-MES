$(document).ready(function() {
	
	//装配信息
	$('.flow_assemble_info').click(function (){
		var rowData = $.MTable.getClickRowData("production_record_flowstep_table",this);
		var operation_no = rowData.operation_no;
		var sfc = rowData.sfc;
		
		var data_url = rootPath + "/report/production_record_assemblelist_report.shtml?formMap.sfc="+sfc+"&formMap.operation_no="+operation_no;			
		window.open(data_url, "_blank");
	});
	
	//不良信息
	$('.flow_nc_info').click(function (){
		var rowData = $.MTable.getClickRowData("production_record_flowstep_table",this);
		var operation_no = rowData.operation_no;
		var sfc = rowData.sfc;
		
		var data_url = rootPath + "/report/production_record_ncinfo_report.shtml?formMap.sfc="+sfc+"&formMap.operation_no="+operation_no;			
		window.open(data_url, "_blank");
	});
	
	//装配信息
	$('.step_assemble_info').click(function (){
		var rowData = $.MTable.getClickRowData("production_record_sfcstep_table",this);
		var operation_no = rowData.operation_no;
		var sfc = rowData.sfc;
		
		var data_url = rootPath + "/report/production_record_assemblelist_report.shtml?formMap.sfc="+sfc+"&formMap.operation_no="+operation_no;			
		window.open(data_url, "_blank");
	});
	
	//不良信息
	$('.step_nc_info').click(function (){
		var rowData = $.MTable.getClickRowData("production_record_sfcstep_table",this);
		var operation_no = rowData.operation_no;
		var sfc = rowData.sfc;
		
		var data_url = rootPath + "/report/production_record_ncinfo_report.shtml?formMap.sfc="+sfc+"&formMap.operation_no="+operation_no;			
		window.open(data_url, "_blank");
	});
	
})

