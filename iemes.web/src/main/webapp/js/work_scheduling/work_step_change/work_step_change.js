$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//保存
	$("form").submit(function() {	
		var data = $("form").serialize();		

		$.post(rootPath + "/work_scheduling/work_step_change/changeWorkStep.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("步骤调整成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var sfc_no = $('#tbx_sfc_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_scheduling/work_step_change/queryWorkStep.shtml?sfc_no="+sfc_no);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_scheduling/work_step_change/work_step_change.shtml");
	})
	
})

