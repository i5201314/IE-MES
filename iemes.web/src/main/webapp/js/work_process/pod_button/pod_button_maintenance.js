$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		$.post(rootPath + "/work_process/pod_button/savePodButton.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("POD功能保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var pod_button_no = $('#tbx_pod_button_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_button/queryPodButton.shtml?pod_button_no="+pod_button_no);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_button/pod_button_maintenance.shtml");
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("系统提示","该条数据将会被删除，确定继续吗？",function (){
			var pod_button_id = $('#pod_button_id').val();
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/work_process/pod_button/delPodButton.shtml?pod_button_id="+pod_button_id);
		});
	})
})