package com.iemes.controller.popup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.entity.ContainerTypeFormMap;
import com.iemes.entity.ItemBomFormMap;
import com.iemes.entity.ItemFormMap;
import com.iemes.entity.NcCodeFormMap;
import com.iemes.entity.NcCodeGroupFormMap;
import com.iemes.entity.NumberRuleFormMap;
import com.iemes.entity.OperationFormMap;
import com.iemes.entity.PageSaveFormMap;
import com.iemes.entity.PodButtonFormMap;
import com.iemes.entity.PodFunctionFormMap;
import com.iemes.entity.PodPanelFormMap;
import com.iemes.entity.ProcessWorkFlowFormMap;
import com.iemes.entity.ResFormMap;
import com.iemes.entity.RoleFormMap;
import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.ShiftFormMap;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.entity.SiteFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.entity.WorkCenterFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.entity.WorkResourceTypeFormMap;
import com.iemes.entity.WorkShopInventoryFormMap;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.mapper.packing.PackingMapper;
import com.iemes.service.work_process.WorkPodPanelService;
import com.iemes.util.Common;
import com.iemes.util.ListUtils;
import com.iemes.util.MTable;
import com.iemes.util.MTableCol;
import com.iemes.util.ShiroSecurityHelper;

import net.sf.json.JSONObject;

/**
 * 公共弹出窗控制器
 * 用于存放系统中所有弹出框的后台地址
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/popup/")
public class PublicPopupController {
	
	@Inject
	private BaseExtMapper baseMapper;
	
	@Inject
	private PackingMapper packingMapper;
	
	@Inject
	private WorkPodPanelService workPodPanelService;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	private final String publicPopupUrl =  Common.BACKGROUND_PATH + "/popup/public_popup";
	
	/**
	 * 站点检索窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllSite")
	public String queryAllSite(Model model, HttpServletRequest request){
		log.debug(">>> 进入站点弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		SiteFormMap siteFormMap = new SiteFormMap();
		siteFormMap.put("site", "%"+inputValue+"%");			//模糊查询
		List<SiteFormMap> listSiteFormMap = baseMapper.findByNames(siteFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "站点id", -1));
		mTable.addHead(new MTableCol("site", "站点编号"));
		mTable.addHead(new MTableCol("site_name", "站点名称"));
		mTable.addHead(new MTableCol("site_description", "站点描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(listSiteFormMap);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}

	/**
	 * 物料检索窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllItem")
	public String queryAllItem(Model model, HttpServletRequest request){
		log.debug(">>> 进入物料弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ItemFormMap itemFormMap = new ItemFormMap();
		itemFormMap.put("item_no", "%"+inputValue+"%");			//模糊查询
		itemFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ItemFormMap> listItemFormMap = baseMapper.findByNames(itemFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "物料id", -1));
		mTable.addHead(new MTableCol("item_no", "物料编号"));
		mTable.addHead(new MTableCol("item_name", "物料名称"));
		mTable.addHead(new MTableCol("item_desc", "物料描述"));
		mTable.addHead(new MTableCol("item_save_type", "存储类型"));
		mTable.addHead(new MTableCol("item_version", "物料版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(listItemFormMap);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 资源类型检索窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllResourceType")
	public String queryAllResourceType(Model model, HttpServletRequest request){
		log.debug(">>> 进入资源类型弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		WorkResourceTypeFormMap workResourceTypeFormMap = new WorkResourceTypeFormMap();
		workResourceTypeFormMap.put("resource_type", "%"+inputValue+"%");			//模糊查询
		workResourceTypeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<WorkResourceTypeFormMap> listWorkResourceTypeFormMap = baseMapper.findByNames(workResourceTypeFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "资源类型id", -1));
		mTable.addHead(new MTableCol("resource_type", "资源类型编号"));
		mTable.addHead(new MTableCol("resource_type_des", "资源类型描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(listWorkResourceTypeFormMap);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 用户弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllUser")
	public String queryAllUser(Model model, HttpServletRequest request){
		log.debug(">>> 进入用户弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		UserFormMap userFormMap = new UserFormMap();
		userFormMap.put("account_name", "%"+inputValue+"%");			//模糊查询
		userFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<UserFormMap> list = baseMapper.findByNames(userFormMap);
		
		JSONObject head = new JSONObject();
		head.put("account_name", "用户编号");
		head.put("user_name", "用户名称");
		head.put("description", "用户描述");
		head.put("create_user", "创建人");
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "用户id", -1));
		mTable.addHead(new MTableCol("account_name", "用户编号"));
		mTable.addHead(new MTableCol("user_name", "用户名称"));
		mTable.addHead(new MTableCol("description", "用户描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 角色弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllRole")
	public String queryAllRole(Model model, HttpServletRequest request){
		log.debug(">>> 进入角色弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		RoleFormMap roleFormMap = new RoleFormMap();
		roleFormMap.put("role_key", "%"+inputValue+"%");			//模糊查询
		roleFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<RoleFormMap> list = baseMapper.findByNames(roleFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "角色id", -1));
		mTable.addHead(new MTableCol("role_key", "角色编号"));
		mTable.addHead(new MTableCol("role_name", "角色名称"));
		mTable.addHead(new MTableCol("description", "角色描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 物料清单弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllItemBom")
	public String queryAllItemBom(Model model, HttpServletRequest request){
		log.debug(">>> 进入物料清单弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ItemBomFormMap itemBomFormMap = new ItemBomFormMap();
		itemBomFormMap.put("item_bom_no", "%"+inputValue+"%");			//模糊查询
		itemBomFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ItemBomFormMap> list = baseMapper.findByNames(itemBomFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "物料清单id", -1));
		mTable.addHead(new MTableCol("item_bom_no", "物料清单编号"));
		mTable.addHead(new MTableCol("item_bom_name", "物料清单名称"));
		mTable.addHead(new MTableCol("item_bom_desc", "物料清单描述"));
		mTable.addHead(new MTableCol("item_bom_version", "物料清单版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 菜单弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllMenu")
	public String queryAllMenu(Model model, HttpServletRequest request){
		log.debug(">>> 进入菜单弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ResFormMap resFormMap = new ResFormMap();
		resFormMap.put("res_key", "%"+inputValue+"%");			//模糊查询
		resFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ResFormMap> list = baseMapper.findByNames(resFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "菜单id", -1));
		mTable.addHead(new MTableCol("res_key", "菜单编号"));
		mTable.addHead(new MTableCol("res_name", "菜单名称"));
		mTable.addHead(new MTableCol("description", "菜单描述"));
		mTable.addHead(new MTableCol("res_url", "菜单地址"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 操作弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllOperation")
	public String queryAllOperation(Model model, HttpServletRequest request){
		log.debug(">>> 进入操作弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		OperationFormMap operationFormMap = new OperationFormMap();
		operationFormMap.put("operation_no", "%"+inputValue+"%");			//模糊查询
		operationFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<OperationFormMap> list = baseMapper.findByNames(operationFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "操作id", -1));
		mTable.addHead(new MTableCol("operation_no", "操作编号"));
		mTable.addHead(new MTableCol("operation_desc", "操作描述"));
		mTable.addHead(new MTableCol("operation_type", "操作类型"));
		mTable.addHead(new MTableCol("operation_version", "操作版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 资源弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllResource")
	public String queryAllResource(Model model, HttpServletRequest request){
		log.debug(">>> 进入资源弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		WorkResourceFormMap workResourceFormMap = new WorkResourceFormMap();
		workResourceFormMap.put("resource_no", "%"+inputValue+"%");			//模糊查询
		workResourceFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<WorkResourceFormMap> list = baseMapper.findByNames(workResourceFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "操作id", -1));
		mTable.addHead(new MTableCol("resource_no", "资源编号"));
		mTable.addHead(new MTableCol("resource_desc", "资源描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 不良代码弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllNcCode")
	public String queryAllNcCode(Model model, HttpServletRequest request){
		log.debug(">>> 进入不良代码弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		NcCodeFormMap ncCodeFormMap = new NcCodeFormMap();
		ncCodeFormMap.put("nc_code", "%"+inputValue+"%");			//模糊查询
		ncCodeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<NcCodeFormMap> list = baseMapper.findByNames(ncCodeFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "不良代码id", -1));
		mTable.addHead(new MTableCol("nc_code", "不良代码"));
		mTable.addHead(new MTableCol("nc_code_desc", "不良代码描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 不良代码弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryNcCodeByGroup")
	public String queryNcCodeByGroup(Model model, HttpServletRequest request){
		log.debug(">>> 进入不良代码弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		String ncGroup = request.getParameter("nc_code_group_id");
		
		StringBuffer param = new StringBuffer();
		param.append(" where nc_code like '%"+inputValue+"%' ");
		param.append(" and site_id = '"+ShiroSecurityHelper.getSiteId() + "'");
		param.append(" and id in (");
		param.append(" select nc_code_id from mds_nc_code_relation ");
		param.append(" where nc_code_group_id = '"+ncGroup+"')");
		
		NcCodeFormMap ncCodeFormMap = new NcCodeFormMap();
		ncCodeFormMap.put("where", param.toString());
		List<NcCodeFormMap> list = baseMapper.findByWhere(ncCodeFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "不良代码id", -1));
		mTable.addHead(new MTableCol("nc_code", "不良代码"));
		mTable.addHead(new MTableCol("nc_code_desc", "不良代码描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 不良代码组弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllNcCodeGroup")
	public String queryAllNcCodeGroup(Model model, HttpServletRequest request){
		log.debug(">>> 进入不良代码组弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		NcCodeGroupFormMap ncCodeGroupFormMap = new NcCodeGroupFormMap();
		ncCodeGroupFormMap.put("nc_code_group_no", "%"+inputValue+"%");			//模糊查询
		ncCodeGroupFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<NcCodeGroupFormMap> list = baseMapper.findByNames(ncCodeGroupFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "不良代码组id", -1));
		mTable.addHead(new MTableCol("nc_code_group_no", "不良代码组编号"));
		mTable.addHead(new MTableCol("nc_code_group_desc", "不良代码组描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 容器弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllContainer")
	public String queryAllContainer(Model model, HttpServletRequest request){
		log.debug(">>> 进入容器弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ContainerTypeFormMap containerTypeFormMap = new ContainerTypeFormMap();
		containerTypeFormMap.put("container_type_no", "%"+inputValue+"%");			//模糊查询
		containerTypeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ContainerTypeFormMap> list = baseMapper.findByNames(containerTypeFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("container_type_no", "容器编号"));
		mTable.addHead(new MTableCol("container_type_desc", "容器描述"));
		mTable.addHead(new MTableCol("container_level", "容器类型"));
		mTable.addHead(new MTableCol("min_number", "最小总数量"));
		mTable.addHead(new MTableCol("max_number", "最大总数量"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 班次弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllShift")
	public String queryAllShifts(Model model, HttpServletRequest request){
		log.debug(">>> 进入班次弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ShiftFormMap shiftFormMap = new ShiftFormMap();
		shiftFormMap.put("shift_no", "%"+inputValue+"%");			//模糊查询
		shiftFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ShiftFormMap> list = baseMapper.findByNames(shiftFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "班次id", -1));
		mTable.addHead(new MTableCol("shift_no", "班次"));
		mTable.addHead(new MTableCol("shift_desc", "班次描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 工艺路线弹出窗(全部)
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllProcessWorkflow")
	public String queryAllProcessWorkflows(Model model, HttpServletRequest request){
		log.debug(">>> 进入工艺路线弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ProcessWorkFlowFormMap processWorkFlowFormMap = new ProcessWorkFlowFormMap();
		processWorkFlowFormMap.put("process_workflow", "%"+inputValue+"%");			//模糊查询
		processWorkFlowFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ProcessWorkFlowFormMap> list = baseMapper.findByNames(processWorkFlowFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "工艺路线id", -1));
		mTable.addHead(new MTableCol("process_workflow", "工艺路线"));
		mTable.addHead(new MTableCol("process_workflow_desc", "工艺路线描述"));
		mTable.addHead(new MTableCol("process_workflow_type", "工艺路线类型"));
		mTable.addHead(new MTableCol("process_workflow_version", "工艺路线版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	
	/**
	 * 工艺路线弹出窗(正常/返工)
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryProcessWorkflowByType")
	public String queryProcessWorkflowByType(Model model, HttpServletRequest request){
		log.debug(">>> 进入工艺路线弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		String process_workflow_type = request.getParameter("process_flow_type");
		
		ProcessWorkFlowFormMap processWorkFlowFormMap = new ProcessWorkFlowFormMap();
		processWorkFlowFormMap.put("process_workflow", "%"+inputValue+"%");			//模糊查询
		processWorkFlowFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		processWorkFlowFormMap.put("process_workflow_type", process_workflow_type);
		List<ProcessWorkFlowFormMap> list = baseMapper.findByNames(processWorkFlowFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "工艺路线id", -1));
		mTable.addHead(new MTableCol("process_workflow", "工艺路线"));
		mTable.addHead(new MTableCol("process_workflow_desc", "工艺路线描述"));
		mTable.addHead(new MTableCol("process_workflow_type", "工艺路线类型"));
		mTable.addHead(new MTableCol("process_workflow_version", "工艺路线版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	
	/**
	 * 工单弹出窗（全部）
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllShoporder")
	public String queryAllShoporder(Model model, HttpServletRequest request){
		log.debug(">>> 进入工单弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ShoporderFormMap shoporderFormMap = new ShoporderFormMap();
		shoporderFormMap.put("shoporder_no", "%"+inputValue+"%");			//模糊查询
		shoporderFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ShoporderFormMap> list = baseMapper.findByNames(shoporderFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "工单id", -1));
		mTable.addHead(new MTableCol("shoporder_no", "工单编号"));
		mTable.addHead(new MTableCol("shoporder_number", "工单数量"));
		mTable.addHead(new MTableCol("shoporder_issued_number", "工单已下达数量"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 工单弹出窗（根据类型进行检索）
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryShoporderByType")
	public String queryShoporderByType(Model model, HttpServletRequest request){
		log.debug(">>> 进入工单弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		String shoporder_type = request.getParameter("shoporder_type");
		
		ShoporderFormMap shoporderFormMap = new ShoporderFormMap();
		shoporderFormMap.put("shoporder_no", "%"+inputValue+"%");			//模糊查询
		shoporderFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		shoporderFormMap.put("shoporder_type", shoporder_type);
		List<ShoporderFormMap> list = baseMapper.findByNames(shoporderFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "工单id", -1));
		mTable.addHead(new MTableCol("shoporder_no", "工单编号"));
		mTable.addHead(new MTableCol("shoporder_type", "工单类型"));
		mTable.addHead(new MTableCol("shoporder_number", "工单数量"));
		mTable.addHead(new MTableCol("shoporder_issued_number", "工单已下达数量"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 工作中心弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllWorkCenter")
	public String queryAllWorkCenter(Model model, HttpServletRequest request){
		log.debug(">>> 进入工作中心弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		WorkCenterFormMap workCenterFormMap = new WorkCenterFormMap();
		workCenterFormMap.put("workcenter_no", "%"+inputValue+"%");			//模糊查询
		workCenterFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<WorkCenterFormMap> list = baseMapper.findByNames(workCenterFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("workcenter_no", "工作中心编号"));
		mTable.addHead(new MTableCol("workcenter_name", "工作中心名称"));
		mTable.addHead(new MTableCol("workcenter_description", "工作中心描述"));
		mTable.addHead(new MTableCol("workcenter_level", "工作中心层级"));
		mTable.addHead(new MTableCol("workcenter_version", "工作中心版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 车间弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryWorkCenter")
	public String queryWorkCenter(Model model, HttpServletRequest request){
		log.debug(">>> 进入车间弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		WorkCenterFormMap workCenterFormMap = new WorkCenterFormMap();
		workCenterFormMap.put("workcenter_no", "%"+inputValue+"%");			//模糊查询
		workCenterFormMap.put("workcenter_level", "workcenter");			//车间
		workCenterFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<WorkCenterFormMap> list = baseMapper.findByNames(workCenterFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("workcenter_no", "工作中心编号"));
		mTable.addHead(new MTableCol("workcenter_name", "工作中心名称"));
		mTable.addHead(new MTableCol("workcenter_description", "工作中心描述"));
		mTable.addHead(new MTableCol("workcenter_level", "工作中心层级"));
		mTable.addHead(new MTableCol("workcenter_version", "工作中心版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 产线弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryWorkLine")
	public String queryWorkLine(Model model, HttpServletRequest request){
		log.debug(">>> 进入产线弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		String workcenterId = request.getParameter("input_workcenter_id");
		
		WorkCenterFormMap workCenterFormMap = new WorkCenterFormMap();
		workCenterFormMap.put("workcenter_no", "%"+inputValue+"%");			//模糊查询
		workCenterFormMap.put("workcenter_level", "workline");				//产线
		if (!StringUtils.isEmpty(workcenterId)) {
			workCenterFormMap.put("workcenter_parent_id", workcenterId);		//车间ID
		}
		workCenterFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<WorkCenterFormMap> list = baseMapper.findByNames(workCenterFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("workcenter_no", "工作中心编号"));
		mTable.addHead(new MTableCol("workcenter_name", "工作中心名称"));
		mTable.addHead(new MTableCol("workcenter_description", "工作中心描述"));
		mTable.addHead(new MTableCol("workcenter_level", "工作中心层级"));
		mTable.addHead(new MTableCol("workcenter_version", "工作中心版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * POD功能弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllPodFunction")
	public String queryAllPodFunction(Model model, HttpServletRequest request){
		log.debug(">>> 进入POD功能弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		PodFunctionFormMap podFunctionFormMap = new PodFunctionFormMap();
		podFunctionFormMap.put("pod_function_no", "%"+inputValue+"%");			//模糊查询
		podFunctionFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<PodFunctionFormMap> list = baseMapper.findByNames(podFunctionFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("pod_function_no", "POD功能编号"));
		mTable.addHead(new MTableCol("pod_function_name", "POD功能名称"));
		mTable.addHead(new MTableCol("pod_function_url", "POD功能地址"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * POD功能弹出窗（不分站点）
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllPodFunctions")
	public String queryAllPodFunctions(Model model, HttpServletRequest request){
		log.debug(">>> 进入POD功能弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		PodFunctionFormMap podFunctionFormMap = new PodFunctionFormMap();
		podFunctionFormMap.put("pod_function_no", "%"+inputValue+"%");			//模糊查询
		List<PodFunctionFormMap> list = baseMapper.findByNames(podFunctionFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("pod_function_no", "POD功能编号"));
		mTable.addHead(new MTableCol("pod_function_name", "POD功能名称"));
		mTable.addHead(new MTableCol("pod_function_url", "POD功能地址"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * POD按钮弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllPodButton")
	public String queryAllPodButton(Model model, HttpServletRequest request){
		log.debug(">>> 进入POD按钮弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		PodButtonFormMap podButtonFormMap = new PodButtonFormMap();
		podButtonFormMap.put("pod_function_no", "%"+inputValue+"%");			//模糊查询
		podButtonFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<PodButtonFormMap> list = baseMapper.findByNames(podButtonFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("pod_button_no", "POD按钮编号"));
		mTable.addHead(new MTableCol("pod_button_name", "POD按钮名称"));
		mTable.addHead(new MTableCol("pod_function", "POD功能地址"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * POD按钮弹出窗（不区分站点）
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllPodButtons")
	public String queryAllPodButtons(Model model, HttpServletRequest request){
		log.debug(">>> 进入POD按钮弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		PodButtonFormMap podButtonFormMap = new PodButtonFormMap();
		podButtonFormMap.put("pod_function_no", "%"+inputValue+"%");			//模糊查询
		List<PodButtonFormMap> list = baseMapper.findByNames(podButtonFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("pod_button_no", "POD按钮编号"));
		mTable.addHead(new MTableCol("pod_button_name", "POD按钮名称"));
		mTable.addHead(new MTableCol("pod_function", "POD功能地址"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * POD面板弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllPodPanel")
	public String queryAllPodPanel(Model model, HttpServletRequest request){
		log.debug(">>> 进入POD面板弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		PodPanelFormMap podPanelFormMap = new PodPanelFormMap();
		podPanelFormMap.put("pod_function_no", "%"+inputValue+"%");			//模糊查询
		podPanelFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<PodPanelFormMap> list = baseMapper.findByNames(podPanelFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("pod_panel_no", "POD面板编号"));
		mTable.addHead(new MTableCol("pod_panel_name", "POD面板名称"));
		mTable.addHead(new MTableCol("pod_panel_desc", "POD面板描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * POD界面-操作弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryPodOperation")
	public String queryPodOperation(Model model, HttpServletRequest request){
		log.debug(">>> 进入POD界面操作弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		OperationFormMap operationFormMap = new OperationFormMap();
		operationFormMap.put("operation_no", "%"+inputValue+"%");			//模糊查询
		operationFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<OperationFormMap> list = baseMapper.findByNames(operationFormMap);
		for (OperationFormMap operationFormMap2 : list) {
			operationFormMap2.put("operation_id", operationFormMap2.getStr("id"));
			String defaultResource = operationFormMap2.getStr("default_resource");
			if (!StringUtils.isEmpty(defaultResource)) {
				List<WorkResourceFormMap> listWorkResourceFormMap = baseMapper.findByAttribute("id", defaultResource, WorkResourceFormMap.class);
				if (ListUtils.isNotNull(listWorkResourceFormMap)) {
					WorkResourceFormMap workResourceFormMap = listWorkResourceFormMap.get(0);
					operationFormMap2.put("resource_no", workResourceFormMap.getStr("resource_no"));
					operationFormMap2.put("resource_id", defaultResource);
				}
			}
		}
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "操作id", -1));
		mTable.addHead(new MTableCol("operation_id", "操作id", -1));
		mTable.addHead(new MTableCol("resource_id", "资源ID", -1));
		mTable.addHead(new MTableCol("resource_no", "默认资源NO", -1));
		mTable.addHead(new MTableCol("operation_no", "操作编号"));
		mTable.addHead(new MTableCol("operation_desc", "操作描述"));
		mTable.addHead(new MTableCol("operation_type", "操作类型"));
		mTable.addHead(new MTableCol("operation_version", "操作版本"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * POD界面-资源弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryPodResource")
	public String queryPodResource(Model model, HttpServletRequest request){
		log.debug(">>> 进入资源弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		String operation_no = request.getParameter("tbx_pod_operation");
		String operation_id = request.getParameter("input_operation_id");
		
		WorkResourceFormMap workResourceFormMap = new WorkResourceFormMap();
		workResourceFormMap.put("resource_no", "%"+inputValue+"%");			//模糊查询
		workResourceFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<WorkResourceFormMap> list = baseMapper.findByNames(workResourceFormMap);
		for (WorkResourceFormMap workResourceFormMap2 : list) {
			workResourceFormMap2.put("operation_no", operation_no);
			workResourceFormMap2.put("operation_id", operation_id);
			workResourceFormMap2.put("resource_id", workResourceFormMap2.getStr("id"));
		}
		
		MTable mTable = new MTable(); 
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "资源id", -1));
		mTable.addHead(new MTableCol("operation_no", "操作no", -1));
		mTable.addHead(new MTableCol("operation_id", "资源id", -1));
		mTable.addHead(new MTableCol("resource_id", "资源id", -1));
		mTable.addHead(new MTableCol("resource_no", "资源编号"));
		mTable.addHead(new MTableCol("resource_desc", "资源描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * podSFC弹出窗
	 * 根据资源ID找到产线ID，再根据产线ID和操作ID找到sfc_step中所有符合记录的SFC，
	 * 再根据SFC找到最后一笔记录，且状态为创建的全部记录。
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryOperationSfcByWorkResource")
	public String queryOperationSfcByWorkResource(Model model, HttpServletRequest request){
		log.debug(">>> 进入PODSFC弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		inputValue = "%"+inputValue+"%";
		
		String operationId = request.getParameter("input_operation_id");
		String resourceId = request.getParameter("input_resource_id");
		
		//取得产线ID
		List<WorkResourceFormMap> listWorkResourceFormMap = baseMapper.findByAttribute("id", resourceId, WorkResourceFormMap.class);
		WorkResourceFormMap workResourceFormMap = listWorkResourceFormMap.get(0);
		String workLineId = workResourceFormMap.getStr("workline_id");
		
		//获取当前操作、资源上排队的SFC列表
		List<SfcStepFormMap> listSfcStepFormMap = workPodPanelService.getSfcWaitOnOperation(operationId, resourceId, inputValue);
		
		for (SfcStepFormMap SfcStepFormMap : listSfcStepFormMap) {
			String itemid = SfcStepFormMap.getStr("item_id");
			List<ItemFormMap> listItemFormMap = baseMapper.findByAttribute("id", itemid, ItemFormMap.class);
			if (ListUtils.isNotNull(listItemFormMap)) {
				ItemFormMap itemFormMap = listItemFormMap.get(0);
				SfcStepFormMap.put("item_no", itemFormMap.getStr("item_no"));
			}
			
			String workshopId = SfcStepFormMap.getStr("workshop_id");
			List<WorkCenterFormMap> listWorkCenterFormMap = baseMapper.findByAttribute("id", workshopId, WorkCenterFormMap.class);
			if (ListUtils.isNotNull(listWorkCenterFormMap)) {
				WorkCenterFormMap workCenterFormMap = listWorkCenterFormMap.get(0);
				SfcStepFormMap.put("workshop_no", workCenterFormMap.getStr("workcenter_no"));
			}
			
			String worklineId = SfcStepFormMap.getStr("workline_id");
			List<WorkCenterFormMap> listWorkCenterFormMap2 = baseMapper.findByAttribute("id", worklineId, WorkCenterFormMap.class);
			if (ListUtils.isNotNull(listWorkCenterFormMap2)) {
				WorkCenterFormMap workCenterFormMap = listWorkCenterFormMap2.get(0);
				SfcStepFormMap.put("workline_no", workCenterFormMap.getStr("workcenter_no"));
			}
			
			String shiftId = SfcStepFormMap.getStr("shift_id");
			List<ShiftFormMap> listShiftFormMap = baseMapper.findByAttribute("id", shiftId, ShiftFormMap.class);
			if (ListUtils.isNotNull(listShiftFormMap)) {
				ShiftFormMap shiftFormMap = listShiftFormMap.get(0);
				SfcStepFormMap.put("shift_no", shiftFormMap.getStr("shift_no"));
			}
			
			String shoporderId = SfcStepFormMap.getStr("shoporder_id");
			List<ShoporderFormMap> listShoporderFormMap = baseMapper.findByAttribute("id", shoporderId, ShoporderFormMap.class);
			if (ListUtils.isNotNull(listShoporderFormMap)) {
				ShoporderFormMap shoporderFormMap = listShoporderFormMap.get(0);
				SfcStepFormMap.put("shoporder_no", shoporderFormMap.getStr("shoporder_no"));
			}
		}
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("sfc", "sfc"));
		mTable.addHead(new MTableCol("item_no", "物料"));
		mTable.addHead(new MTableCol("workshop_no", "车间"));
		mTable.addHead(new MTableCol("workline_no", "产线"));
		mTable.addHead(new MTableCol("shift_no", "班次"));
		mTable.addHead(new MTableCol("shoporder_no", "工单"));
		mTable.addHead(new MTableCol("status", "状态"));
		mTable.addHead(new MTableCol("create_time", "开始操作时间"));
		mTable.setTableData(listSfcStepFormMap);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * sfc弹窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllSfc")
	public String queryAllSfc(Model model, HttpServletRequest request){
		log.debug(">>> 进入sfc弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ShopOrderSfcFormMap shopOrderSfcFormMap = new ShopOrderSfcFormMap();
		shopOrderSfcFormMap.put("sfc", "%"+inputValue+"%");			//模糊查询
		shopOrderSfcFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<ShopOrderSfcFormMap> list = baseMapper.findByNames(shopOrderSfcFormMap);
		
		MTable mTable = new MTable(); 
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "sfc_id", -1));
		mTable.addHead(new MTableCol("sfc", "sfc"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.addHead(new MTableCol("create_time", "创建时间"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 容器弹出窗
	 * 检索出可用的容器
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAvailableContainer")
	public String queryAvailableContainer(Model model, HttpServletRequest request){
		log.debug(">>> 进入容器弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		ContainerTypeFormMap containerTypeFormMap = new ContainerTypeFormMap();
		containerTypeFormMap.put("container_type_no", "%"+inputValue+"%");			//模糊查询
		containerTypeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		containerTypeFormMap.put("status","1");
		List<ContainerTypeFormMap> list = baseMapper.findByNames(containerTypeFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("number_rule_id", "编号规则", -1));
		mTable.addHead(new MTableCol("container_type_no", "容器编号"));
		mTable.addHead(new MTableCol("container_type_desc", "容器描述"));
		mTable.addHead(new MTableCol("container_level", "容器类型"));
		mTable.addHead(new MTableCol("min_number", "最小总数量"));
		mTable.addHead(new MTableCol("max_number", "最大总数量"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 已接收物料组件SFC弹窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllInventorySfc")
	public String queryAllInventorySfc(Model model, HttpServletRequest request){
		log.debug(">>> 进入已接收物料组件SFC弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		String item_id = request.getParameter("item_sfc_id");
		String main_item_id = request.getParameter("main_item_id");
		
		WorkShopInventoryFormMap workshopInventoryFormMap = new WorkShopInventoryFormMap();
		
		StringBuffer param = new StringBuffer();
		param.append(" where item_sfc like '%"+inputValue+"%' ");
		param.append(" and item_id in  (SELECT DISTINCT mis.item_surrenal_id FROM mds_item_surrenal mis ");
		
		param.append(" WHERE 1=1 AND mis.item_id = '"+item_id+"' ");
		param.append(" AND mis.item_surrenal_usage = '"+main_item_id+"' ");
		param.append(" UNION  ALL SELECT '"+item_id+"' FROM DUAL) ");
		
		param.append(" and site_id = '"+ShiroSecurityHelper.getSiteId() + "'");
		param.append(" and item_sfc not in (");
		param.append(" select item_sfc from mds_sfc_assembly msa WHERE msa.`item_sfc` IS NOT NULL)");
		
		workshopInventoryFormMap.put("where", param.toString());
		
		List<WorkShopInventoryFormMap> list = baseMapper.findByWhere(workshopInventoryFormMap);
		
		MTable mTable = new MTable(); 
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "sfc_id", -1));
		mTable.addHead(new MTableCol("item_sfc", "item_sfc"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.addHead(new MTableCol("create_time", "创建时间"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 已接收物料组件批次弹窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllInventoryBatch")
	public String queryAllInventoryBatch(Model model, HttpServletRequest request){
		log.debug(">>> 进入已接收物料组件批次弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		String item_id = request.getParameter("item_sfc_id");
		String main_item_id = request.getParameter("main_item_id");
		
		Map<String,String> param = new HashMap<String, String>();
		param.put("item_id", item_id);
		param.put("main_item_id", main_item_id);
		param.put("site_id", ShiroSecurityHelper.getSiteId());
		param.put("item_inner_batch", "%"+inputValue+"%");
		
		List<WorkShopInventoryFormMap> list = workPodPanelService.getValidItemBatch(param);
		
		MTable mTable = new MTable(); 
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "sfc_id", -1));
		mTable.addHead(new MTableCol("item_inner_batch", "批次号"));
		mTable.addHead(new MTableCol("remanent_num", "剩余数量"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.addHead(new MTableCol("create_time", "创建时间"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 编号规则弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllNumberRule")
	public String queryAllNumberRule(Model model, HttpServletRequest request){
		log.debug(">>> 进入编号规则弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		NumberRuleFormMap numberRuleFormMap = new NumberRuleFormMap();
		numberRuleFormMap.put("number_rule", "%"+inputValue+"%");			//模糊查询
		numberRuleFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		List<NumberRuleFormMap> list = baseMapper.findByNames(numberRuleFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "编号规则id", -1));
		mTable.addHead(new MTableCol("number_rule", "编号规则"));
		mTable.addHead(new MTableCol("prefix", "前缀"));
		mTable.addHead(new MTableCol("postfix", "后缀"));
		mTable.addHead(new MTableCol("number_rule_desc", "描述"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.addHead(new MTableCol("create_time", "创建时间"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	
	/**
	 * 获取未包装但已经完成的SFC
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("getFinishAndUnPackingSfc")
	public String getFinishAndUnPackingSfc(Model model, HttpServletRequest request){
		log.debug(">>> 进入获取未包装但已经完成的SFC弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		List<ShopOrderSfcFormMap> list = packingMapper.getFinishAndUnPackingSfc("%"+inputValue+"%");
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("sfc", "sfc"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.addHead(new MTableCol("create_time", "创建时间"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 获取未包装的礼盒
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("getUnPackingBox")
	public String getUnPackingBox(Model model, HttpServletRequest request){
		log.debug(">>> 进入获取未包装的礼盒弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		List<ShopOrderSfcFormMap> list = packingMapper.getUnPackingBox("%"+inputValue+"%");
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("container_id", "礼盒"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * 获取未包装的中箱
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("getUnPackingCarton")
	public String getUnPackingCarton(Model model, HttpServletRequest request){
		log.debug(">>> 进入获取未包装的中箱弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		List<ShopOrderSfcFormMap> list = packingMapper.getUnPackingCarton("%"+inputValue+"%");
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "id", -1));
		mTable.addHead(new MTableCol("container_id", "中箱"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
	
	/**
	 * UI设计页弹出窗
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryAllBuilderPage")
	public String queryAllBuilderPage(Model model, HttpServletRequest request){
		log.debug(">>> 进入设计页弹出窗方法");
		
		String inputValue = request.getParameter("inputValue");
		String viewTitle = request.getParameter("viewTitle");
		
		PageSaveFormMap pageSaveFormMap = new PageSaveFormMap();
		pageSaveFormMap.put("page_name", "%"+inputValue+"%");			//模糊查询
		List<PageSaveFormMap> list = baseMapper.findByNames(pageSaveFormMap);
		
		MTable mTable = new MTable();
		mTable.setTableName(viewTitle);
		mTable.addHead(new MTableCol("id", "页面id", -1));
		mTable.addHead(new MTableCol("page_name", "页面名称"));
		mTable.addHead(new MTableCol("jsp路径", "page_file_jsp_path"));
		mTable.addHead(new MTableCol("js路径", "page_file_js_path"));
		mTable.addHead(new MTableCol("create_user", "创建人"));
		mTable.setTableData(list);
		
		model.addAttribute("mTable", mTable);
		return publicPopupUrl;
	}
}
