package com.iemes.controller.workshop_inventory;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.ItemBomRelationFormMap;
import com.iemes.entity.WorkShopInventoryFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.mapper.report.KanbanMapper;
import com.iemes.service.WorkshopInventory.WorkshopInventoryReceptionService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.entity.FormMap;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

/**
 * 
 */
@Controller
@RequestMapping("/workshop_inventory/workshop_inventory_reception/")
public class WorkshopInventoryReceptionController extends BaseController {

	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private KanbanMapper kanbanMapper;
	
	@Inject
	private WorkshopInventoryReceptionService workshopInventoryReceptionService;

	private Logger log = Logger.getLogger(this.getClass());
	private String workshopInventoryReceptionUrl = Common.BACKGROUND_PATH + "/workshop_inventory/workshop_inventory_reception/workshop_inventory_reception";

	// 页面跳转
	@RequestMapping("workshop_inventory_reception")
	public String Get_workshop_inventory_reception_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return workshopInventoryReceptionUrl;
	}

	/**
	 * 车间物料接收
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("receptItem")
	@SystemLog(module = "车间库存接收", methods = "车间库存接收-接收物料") // 凡需要处理业务逻辑的.都需要记录操作日志
	public String receptItem() {
		WorkShopInventoryFormMap workshopInventoryFormMap = getFormMap(WorkShopInventoryFormMap.class);
		workshopInventoryFormMap.put("create_time", DateUtils.getStringDateTime());
		workshopInventoryFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		workshopInventoryFormMap.put("site_id", ShiroSecurityHelper.getSiteId());

		try {
			receptItem(workshopInventoryFormMap);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	private void receptItem(WorkShopInventoryFormMap workshopInventoryFormMap)throws BusinessException {
		try {
			//1.如果物料sfc已存在，则不允许接收
			String itemSfc = workshopInventoryFormMap.getStr("item_sfc");
			String item_inner_batch = workshopInventoryFormMap.getStr("item_inner_batch");
			String item_save_type = workshopInventoryFormMap.getStr("item_save_type");
			
			WorkShopInventoryFormMap workshopInventoryFormMap2 = new WorkShopInventoryFormMap();
			workshopInventoryFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			if ("batch".equals(item_save_type)) {
				workshopInventoryFormMap2.put("item_inner_batch", item_inner_batch);
			}else if ("unit".equals(item_save_type)) {
				workshopInventoryFormMap2.put("item_sfc", itemSfc);
			}
			
			List<WorkShopInventoryFormMap> listWorkshopInventoryFormMap = baseMapper.findByNames(workshopInventoryFormMap2);
			
			if(ListUtils.isNotNull(listWorkshopInventoryFormMap)){
				throw new BusinessException("物料或批次已存在，不可重复接收！");
			}
			
			if ("unit".equals(item_save_type)) {
				workshopInventoryFormMap.put("receive_number", 1);
			}
			
			//2.如果物料超出了结存上限，则不允许接收
			//.获取已存在的数量
			/*String itemId = workshopInventoryFormMap.getStr("item_id");
			String workshopId = workshopInventoryFormMap.getStr("workshop_id");
			
			FormMap<String,String> formMap = new FormMap<String, String>();
			formMap.put("site_id", ShiroSecurityHelper.getSiteId());
			formMap.put("item_id", itemId);
			formMap.put("workshop_id", workshopId);
			List<Map<String, Object>> list = null;
			list = kanbanMapper.findKanBanData(formMap);
			//库存数
			int kc = 0;
			if(ListUtils.isNotNull(list)) {
				Object objectKc = list.get(0).get("kc");
				kc = Integer.valueOf(String.valueOf(objectKc));
			}
			//库存上限
			int kcUplimit = 0;
			ItemBomRelationFormMap itemBomRelationFormMap = new ItemBomRelationFormMap();
			itemBomRelationFormMap.put("item_id", itemId);
			List<ItemBomRelationFormMap> listItemBomRelationFormMap = baseMapper.findByNames(itemBomRelationFormMap);
			if(ListUtils.isNotNull(listItemBomRelationFormMap)) {
				Object objectKcUp = listItemBomRelationFormMap.get(0).get("balance_up");
				kcUplimit = Integer.valueOf(String.valueOf(objectKcUp));
			}
			//本次接收数量
			int receiveNumber = 0;
			//Object objectReceiveNumber = workshopInventoryFormMap.get("receive_number");
			//receiveNumber = Integer.valueOf(String.valueOf(objectReceiveNumber));
			//暂时只按sfc单个接收，数量固定为1
			receiveNumber = 1;
			
			if(receiveNumber <= 0) {
				throw new BusinessException("接收数量必须大于0！");
			}
			if(kc + receiveNumber > kcUplimit) {
				throw new BusinessException("库存数量 "+ kc + " 和当前接收数量 "+ receiveNumber + " 的和 " + (kc + receiveNumber)+ " 超过了库存上限 "+ kcUplimit);
			}
			*/
			workshopInventoryReceptionService.receptItem(workshopInventoryFormMap);
			
		}catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("车间库存接收失败！" + e.getMessage());
		}
	}
}