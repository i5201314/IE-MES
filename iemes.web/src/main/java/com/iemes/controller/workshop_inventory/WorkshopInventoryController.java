package com.iemes.controller.workshop_inventory;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.SiteFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.entity.UserGroupsFormMap;
import com.iemes.exception.SystemException;
import com.iemes.mapper.UserMapper;
import com.iemes.mapper.site.SiteMapper;
import com.iemes.plugin.PageView;
import com.iemes.util.Common;
import com.iemes.util.JsonUtils;
import com.iemes.util.POIUtils;
import com.iemes.util.PasswordHelper;

/**
 * 
 * @author mds
 * @Email: mds
 * @version 2.0v
 */
@Controller
@RequestMapping("/workshop_inventory/")
public class WorkshopInventoryController extends BaseController {
	
	//页面跳转
	@RequestMapping("workshop_inventory_reception")
	public String Get_aql_plan_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workshop_inventory/workshop_inventory_reception/workshop_inventory_reception";
	}
}