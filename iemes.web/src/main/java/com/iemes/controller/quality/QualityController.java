package com.iemes.controller.quality;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;

/**
 * 
 * @author mds
 * @Email: mds
 * @version 2.0v
 */
@Controller
@RequestMapping("/quality/")
public class QualityController extends BaseController {
	
	//页面跳转
	@RequestMapping("aql_plan")
	public String Get_aql_plan_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/quality/aql_plan/aql_plan";
	}
	
	@RequestMapping("oqc_manager")
	public String Get_oqc_manager_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/quality/oqc_manager/oqc_manager";
	}
	
	@RequestMapping("hold_product_manager")
	public String Get_hold_product_manager_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/quality/hold_product_manager/hold_product_manager";
	}
	
	@RequestMapping("ipqc_manager")
	public String Get_ipqc_manager_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/quality/ipqc_manager/ipqc_manager";
	}

	@RequestMapping("nc_product_handle")
	public String Get_nc_product_handle_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/quality/nc_product_handle/nc_product_handle";
	}
	
	@RequestMapping("summary_report")
	public String summaryReport() {
		return Common.BACKGROUND_PATH + "/quality/summary/summary_report";
	}
	
	@RequestMapping("progress_report")
	public String progressReport() {
		return Common.BACKGROUND_PATH + "/quality/progress/progress_report";
	}
}