package com.iemes.controller.workcenter_model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.UDefinedDataFieldFormMap;
import com.iemes.entity.UDefinedDataFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.workcenter_model.UserDefinedDataService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

/**
 * 用户自定义数据
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/workcenter_model/user_defined_data/")
public class UserDefinedDataController extends BaseController{

	@Inject
	private BaseExtMapper baseMapper;
	
	@Inject
	private UserDefinedDataService userDefinedDataService;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	private Map<String, String> map;
	
	@RequestMapping("udefined_data_maintenance")
	public String listUI(Model model,HttpServletRequest request) throws Exception {
		model.addAttribute("data_list", getDataList());
		handlePageRes(model,request);
		return Common.BACKGROUND_PATH + "/workcenter_model/user_defined_data/user_defined_data_maintenance";
	}
	
	public Map<String, String> getDataList(){
		if (map==null) {
			map = new HashMap<String, String>();
		}
		map.put("workcenter", "工作中心维护");
		map.put("work_resource", "资源维护");
		map.put("item_maintenance", "物料维护");
		map.put("item_bom_maintenance", "物料清单维护");
		map.put("operation_maintenance", "操作维护");
		map.put("routing", "工艺路线");
		map.put("nc_code_maintenance", "不良代码");
		map.put("container_manager", "容器管理");
		map.put("shift", "班次维护");
		map.put("shop_order_maintenance", "工单维护");
		map.put("pod_panel_maintenance", "生产操作员—面板维护");
		return map;
	}
	
	/**
	 * 保存自定义数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveUserDefinedData")
	@SystemLog(module="车间基础建模",methods="自定义数据-保存自定义数据")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveUserDefinedData(){
		UDefinedDataFormMap uDefinedDataFormMap = getFormMap(UDefinedDataFormMap.class);
		uDefinedDataFormMap.put("create_time", DateUtils.getStringDateTime());
		uDefinedDataFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		uDefinedDataFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		try {
			userDefinedDataService.saveWorkCenter(uDefinedDataFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索自定义数据
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryUserDefinedData")
	public String queryUserDefinedData(Model model,HttpServletRequest request) {
		String dataType = request.getParameter("data_type");
		try {
			UDefinedDataFormMap uDefinedDataFormMap = new UDefinedDataFormMap();
			uDefinedDataFormMap.put("data_type", dataType);
			uDefinedDataFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<UDefinedDataFormMap> list = baseMapper.findByNames(uDefinedDataFormMap);
			if (ListUtils.isNotNull(list)) {
				UDefinedDataFormMap uDefinedDataFormMap2 = list.get(0);
				String id = uDefinedDataFormMap2.getStr("id");
				
				UDefinedDataFieldFormMap uDefinedDataFieldFormMap = new UDefinedDataFieldFormMap();
				uDefinedDataFieldFormMap.put("udefined_data_id", id);
				uDefinedDataFieldFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
				uDefinedDataFieldFormMap.put("orderby", " order by data_level asc");
				List<UDefinedDataFieldFormMap> list2 = baseMapper.findByNames(uDefinedDataFieldFormMap);
				model.addAttribute("dataKeys", list2);
			}
			model.addAttribute("data_type", dataType);
			model.addAttribute("data_list", getDataList());
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			model.addAttribute("data_list", getDataList());
			handlePageRes(model,request);
			return Common.BACKGROUND_PATH + "/workcenter_model/user_defined_data/user_defined_data_maintenance";
		}
		model.addAttribute("successMessage", "检索成功");
		handlePageRes(model,request);
		return Common.BACKGROUND_PATH + "/workcenter_model/user_defined_data/user_defined_data_maintenance";
	}
	
	/**
	 * 删除自定义数据
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delUserDefinedData")
	public String delUserDefinedData(Model model,HttpServletRequest request) {
		String dataType = request.getParameter("data_type");
		try {
			if (StringUtils.isEmpty(dataType)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			//TODO
		}catch(BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			handlePageRes(model,request);
			return Common.BACKGROUND_PATH + "/workcenter_model/user_defined_data/user_defined_data_maintenance";
		}
		handlePageRes(model,request);
		return Common.BACKGROUND_PATH + "/workcenter_model/user_defined_data/user_defined_data_maintenance";
	}
}
