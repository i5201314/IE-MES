package com.iemes.controller.workcenter_model;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.WorkCenterFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.workcenter_model.WorkCenterService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/workcenter_model/workcenter/")
public class WorkCenterController extends BaseController{
	
	@Inject
	private WorkCenterService workCenterService;

	private Logger log = Logger.getLogger(this.getClass());
	
	private String uDefinedDataType = "workcenter";
	
	@RequestMapping("workcenter_maintenance")
	public String listUI(Model model,HttpServletRequest request) throws Exception {
		model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
		handlePageRes(model,request);
		return Common.BACKGROUND_PATH + "/workcenter_model/workcenter/workcenter_maintenance";
	}
	
	/**
	 * 保存工作中心
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveWorkCenter")
	@SystemLog(module="车间基础建模",methods="工作中心维护-保存工作中心")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveWorkCenter(){
		WorkCenterFormMap workCenterFormMap = getFormMap(WorkCenterFormMap.class);
		workCenterFormMap.put("create_time", DateUtils.getStringDateTime());
		workCenterFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		workCenterFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		try {
			workCenterService.saveWorkCenter(workCenterFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索工作中心
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryWorkCenter")
	public String queryWorkCenter(Model model,HttpServletRequest request) {
		String version = request.getParameter("version");
		String workcenterNo = request.getParameter("workcenter_no");
		String workcenterId = "";
		try {
			WorkCenterFormMap workCenterFormMap = new WorkCenterFormMap();
			workCenterFormMap.put("workcenter_no", workcenterNo);
			if (!StringUtils.isEmpty(version)) {
				workCenterFormMap.put("workcenter_version", version);
			}
			workCenterFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			workCenterFormMap.put("orderby", " order by workcenter_version");
			List<WorkCenterFormMap> list = baseMapper.findByNames(workCenterFormMap);
			
			if (!ListUtils.isNotNull(list)) {
				log.error("未检索到工作中心编号为："+workcenterNo.toUpperCase()+"的信息");
				model.addAttribute("errorMessage","未检索到工作中心编号为："+workcenterNo.toUpperCase()+"的信息");
				handlePageRes(model,request);
				return Common.BACKGROUND_PATH + "/workcenter_model/workcenter/workcenter_maintenance";
			}
			WorkCenterFormMap workCenterFormMap2 = list.get(0);
			workcenterId = workCenterFormMap2.getStr("id");
			if (!StringUtils.isEmpty(workCenterFormMap2.getStr("workcenter_parent_id"))) {
				List<WorkCenterFormMap> list2 = baseMapper.findByAttribute("id", workCenterFormMap2.getStr("workcenter_parent_id"), WorkCenterFormMap.class);
				model.addAttribute("parentWorkCenter", ListUtils.isNotNull(list2)?list2.get(0):null);
			}
			
			model.addAttribute("workcenter", workCenterFormMap2);
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
			model.addAttribute("errorMessage", e.getMessage());
			handlePageRes(model,request);
			return Common.BACKGROUND_PATH + "/workcenter_model/workcenter/workcenter_maintenance";
		}
		
		handlePageRes(model,request);
		getUDefinedDataValue(model,workcenterId,uDefinedDataType,"dataKeys");
		model.addAttribute("successMessage", "检索成功");
		return Common.BACKGROUND_PATH + "/workcenter_model/workcenter/workcenter_maintenance";
	}
	
	/**
	 * 删除工作中心
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delWorkCenter")
	public String delWorkCenter(Model model,HttpServletRequest request) {
		String workCenterId = request.getParameter("workcenter_id");
		try {
			if (StringUtils.isEmpty(workCenterId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			workCenterService.delWorkCenter(workCenterId);
			model.addAttribute("successMessage", "删除成功");
		}catch(BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
			model.addAttribute("errorMessage", e.getMessage());
			handlePageRes(model,request);
			return Common.BACKGROUND_PATH + "/workcenter_model/workcenter/workcenter_maintenance";
		}
		handlePageRes(model,request);
		model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
		return Common.BACKGROUND_PATH + "/workcenter_model/workcenter/workcenter_maintenance";
	}
}
