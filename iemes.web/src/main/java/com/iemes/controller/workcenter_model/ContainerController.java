package com.iemes.controller.workcenter_model;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.ContainerTypeFormMap;
import com.iemes.entity.NumberRuleFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.workcenter_model.ContainerService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/workcenter_model/container/")
public class ContainerController extends BaseController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private ContainerService containerService;
	
	private String containerManagerUrl = Common.BACKGROUND_PATH + "/workcenter_model/container/container_manager";

	//页面跳转
	@RequestMapping("container_manager")
	public String container_manager(Model model,HttpServletRequest request) throws Exception {
		model.addAttribute("dataKeys", getUDefinedDataField("container_manager"));
		handlePageRes(model,request);
		return containerManagerUrl;
	}
	
	/**
	 * 保存容器
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveContainer")
	@SystemLog(module="容器维护",methods="容器维护-保存容器")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveContainer() {
		ContainerTypeFormMap containerTypeFormMap = getFormMap(ContainerTypeFormMap.class);
		containerTypeFormMap.put("create_time", DateUtils.getStringDateTime());
		containerTypeFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		containerTypeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		
		try {
			containerService.saveContainer(containerTypeFormMap);	
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索容器
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryContainer")
	public String queryContainer(Model model, HttpServletRequest request) {
		String siteId = ShiroSecurityHelper.getSiteId();
		String containerTypeNo = request.getParameter("container_type_no");
		//1.查询物料清单基本信息
		ContainerTypeFormMap containerTypeFormMap = new ContainerTypeFormMap();
		containerTypeFormMap.put("container_type_no", containerTypeNo);
		containerTypeFormMap.put("site_id", siteId);
		List<ContainerTypeFormMap> listContainerFormMap = baseMapper.findByNames(containerTypeFormMap);
		if (!ListUtils.isNotNull(listContainerFormMap)) {
			String errMessage = "未检索到容器编号为：" + containerTypeNo.toUpperCase() + "的信息";
			log.error(errMessage);
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return containerManagerUrl;
		}
		containerTypeFormMap = listContainerFormMap.get(0);	
		
		String numberRule = containerTypeFormMap.getStr("number_rule");
		List<NumberRuleFormMap> list = baseMapper.findByAttribute("id", numberRule, NumberRuleFormMap.class);
		if (ListUtils.isNotNull(list)) {
			NumberRuleFormMap numberRuleFormMap = list.get(0);
			model.addAttribute("numberRuleFormMap", numberRuleFormMap);
		}
		
		//查询自定义数据信息
		getUDefinedDataValue(model,containerTypeFormMap.getStr("id"),"container_manager","dataKeys");
		
		model.addAttribute("containerTypeFormMap", containerTypeFormMap);
		model.addAttribute("successMessage", "检索成功");
		handlePageRes(model,request);
		return containerManagerUrl;
	}
	
	/**
	 * 删除物料清单
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delContainer")
	public String delContainer(Model model, HttpServletRequest request) {
		String containerTypeId = request.getParameter("container_type_id");
		try {
			if (StringUtils.isEmpty(containerTypeId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			containerService.delContainer(containerTypeId);
			model.addAttribute("dataKeys", getUDefinedDataField("container_manager"));
		} catch (BusinessException e) {
			String errMessage = e.getMessage();
			log.error(errMessage);
			model.addAttribute("dataKeys", getUDefinedDataField("container_manager"));
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return containerManagerUrl;
		}
		model.addAttribute("res", findByRes());
		model.addAttribute("successMessage", "删除容器信息成功");
		handlePageRes(model,request);
		return containerManagerUrl;
	}
}
