package com.iemes.mapper.WorkcenterModel;

import com.iemes.mapper.base.BaseMapper;


public interface WorkResourceMapper extends BaseMapper{

	public void updateWorkResourceTypeIdToEmptyByWorkResourceTypeId(String workResourceTypeId);
	
}
