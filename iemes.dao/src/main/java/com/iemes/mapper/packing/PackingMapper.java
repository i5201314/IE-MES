package com.iemes.mapper.packing;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.iemes.entity.ShopOrderSfcFormMap;

public interface PackingMapper {

	/**
	 * 获取已完成但未包装的SFC
	 * @param param
	 * @return
	 */
	List<ShopOrderSfcFormMap> getFinishAndUnPackingSfc(@Param("sfc")String sfc);
	
	/**
	 * 获取未包装的礼盒
	 * @param sfc
	 * @return
	 */
	List<ShopOrderSfcFormMap> getUnPackingBox(@Param("wrappage")String wrappage);
	
	/**
	 * 获取未包装的中箱
	 * @param sfc
	 * @return
	 */
	List<ShopOrderSfcFormMap> getUnPackingCarton(@Param("wrappage")String wrappage);
}
