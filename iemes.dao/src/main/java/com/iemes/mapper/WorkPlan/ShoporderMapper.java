package com.iemes.mapper.WorkPlan;

import java.util.List;
import java.util.Map;

import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.mapper.base.BaseMapper;

public interface ShoporderMapper extends BaseMapper{


	/**
	 * 获取工单的SFC列表
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getShoporderSfcList(Map<String,Object> map) throws Exception;
	
	/**
	 * 获取同一主物料下的所有SFC
	 * @param param
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getSfcByMainItemId(Map<String, String> param)throws Exception;
}
