package com.iemes.service.impl.system;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.ResRoleFormMap;
import com.iemes.entity.RoleFormMap;
import com.iemes.entity.RoleResFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.entity.UserGroupsFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.RoleMapper;
import com.iemes.service.system.RoleService;
import com.iemes.util.Common;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class RoleServiceImpl implements RoleService {
	
	@Inject
	private RoleMapper roleMapper;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveRole(RoleFormMap roleFormMap) throws BusinessException {
		try {
			String id = roleFormMap.getStr("id");
			String roleKey = roleFormMap.getStr("role_key");
			
			if(StringUtils.isEmpty(roleKey)) {
				throw new BusinessException("角色编号不能为空，请输入！");
			}
			
			RoleFormMap roleFormMap3 = new RoleFormMap();
			roleFormMap3.put("site", ShiroSecurityHelper.getSiteId());
			roleFormMap3.put("role_key", roleKey);
			List<RoleFormMap> list = roleMapper.findByNames(roleFormMap3);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				RoleFormMap roleFormMap2 = new RoleFormMap();
				roleFormMap2 = list.get(0);
				id = roleFormMap2.getStr("id");
				roleFormMap.set("id", id);
				
				roleMapper.editEntity(roleFormMap);
			}else {
				//新增
				roleFormMap.put("id", UUIDUtils.getUUID());
				roleMapper.addEntity(roleFormMap);
			}
					
			//添加或更新关联角色
			String userSelectGroups = roleFormMap.getStr("userSelectGroups");
			roleMapper.deleteByAttribute("role_id", roleFormMap.getStr("id"), UserGroupsFormMap.class);
			
			if (!Common.isEmpty(userSelectGroups)) {
				String[] txt = userSelectGroups.split(",");
				for (String user_id : txt) {
					UserGroupsFormMap userGroupsFormMap = new UserGroupsFormMap();
					userGroupsFormMap.put("user_id", user_id);
					userGroupsFormMap.put("role_id", roleFormMap.get("id"));
					roleMapper.addEntity(userGroupsFormMap);
				}
			}
			
			//添加或更新关联权限
			String tableData = roleFormMap.getStr("tableData");
			roleMapper.deleteByAttribute("roleId", roleFormMap.getStr("id"), RoleResFormMap.class);
			
			if (!Common.isEmpty(tableData)) {
				String[] rs = tableData.split(",");
				for (String res_id : rs) {
					ResRoleFormMap resRoleFormMap = new ResRoleFormMap();
					resRoleFormMap.put("resId", res_id);
					resRoleFormMap.put("roleId", roleFormMap.getStr("id"));
					roleMapper.addEntity(resRoleFormMap);
				}
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("角色保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delRole(String roleId) throws BusinessException {
		try {
			//超级管理员账号不可删除
			if (roleId.equals("0")) {
				throw new BusinessException("超级管理员账号不可删除！");
			}
			//删除角色表
			roleMapper.deleteByAttribute("id", roleId, RoleFormMap.class);
			//删除用户角色关联表
			roleMapper.deleteByAttribute("role_id", roleId, UserGroupsFormMap.class);
			//删除角色资源表
			roleMapper.deleteByAttribute("roleId", roleId, RoleResFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("角色删除失败！"+e.getMessage());
		}
	}

}
