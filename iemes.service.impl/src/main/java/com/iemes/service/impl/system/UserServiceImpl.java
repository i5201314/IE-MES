package com.iemes.service.impl.system;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.UserFormMap;
import com.iemes.entity.UserGroupsFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.UserMapper;
import com.iemes.service.system.UserService;
import com.iemes.util.Common;
import com.iemes.util.ListUtils;
import com.iemes.util.PasswordHelper;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class UserServiceImpl implements UserService {
	
	@Inject
	private UserMapper userMapper;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public String saveUser(UserFormMap userFormMap) throws BusinessException {
		try {
			String msg = "";
			String roleSelectGroups = userFormMap.getStr("roleSelectGroups");
			String id = userFormMap.getStr("id");
			String accountName = userFormMap.getStr("account_name");
			
			if(StringUtils.isEmpty(accountName)) {
				throw new BusinessException("用户编号不能为空，请输入！");
			}
			
			UserFormMap userFormMap3 = new UserFormMap();
			userFormMap3.put("account_name", accountName);
			userFormMap3.put("site_id", ShiroSecurityHelper.getSiteId());
			List<UserFormMap> list = userMapper.findByNames(userFormMap3);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				UserFormMap userFormMap2 = new UserFormMap();
				userFormMap2 = list.get(0);
				id = userFormMap2.getStr("id");
				userFormMap.set("id", id);
				
				userMapper.editEntity(userFormMap);
				msg = "用户信息保存成功";
			}else {
				//新增
				userFormMap.put("id", UUIDUtils.getUUID());
				userFormMap.set("password","123456");
				PasswordHelper passwordHelper = new PasswordHelper();
				passwordHelper.encryptPassword(userFormMap);
				userMapper.addEntity(userFormMap);
				msg = "用户信息保存成功，初始化密码为：123456";
			}
			
			//新增或更新用户角色信息
			userMapper.deleteByAttribute("user_id", userFormMap.getStr("id"), UserGroupsFormMap.class);
			
			UserGroupsFormMap userGroupsFormMap = new UserGroupsFormMap();
			if (!Common.isEmpty(roleSelectGroups)) {
				String[] txt = roleSelectGroups.split(",");
				for (String role_id : txt) {
					userGroupsFormMap.put("user_id", userFormMap.get("id"));
					userGroupsFormMap.put("role_id", role_id);
					userMapper.addEntity(userGroupsFormMap);
				}
			}
			return msg;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("用户保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delUser(String id) throws BusinessException {
		try {
			if ("0".equals(id)) {
				String msg = "系统超级管理员账号不能被删除！！！";
				log.error(msg);
				throw new BusinessException(msg);
			}
			List<UserFormMap> list = userMapper.findByAttribute("id", id, UserFormMap.class);
			if (!ListUtils.isNotNull(list)) {
				String msg = "未能查到相关的用户信息，无法执行删除用户操作";
				log.error(msg);
				throw new BusinessException(msg);
			}
			//删除用户信息
			userMapper.deleteByAttribute("id", id, UserFormMap.class);
			
			//删除用户角色关联信息
			userMapper.deleteByAttribute("user_id", id, UserGroupsFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("用户删除失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void resetPass(UserFormMap userFormMap) throws BusinessException {
		userFormMap.set("password","123456");
		PasswordHelper passwordHelper = new PasswordHelper();
		passwordHelper.encryptPassword(userFormMap);
		try {
			userMapper.editEntity(userFormMap);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("用户保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void updatePass(UserFormMap userFormMap) throws BusinessException {
		try {
			String username = ShiroSecurityHelper.getCurrentUsername();
			UserFormMap userFormMap2 = new UserFormMap();
			userFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			userFormMap2.put("account_name", username);
			List<UserFormMap> list = userMapper.findByNames(userFormMap2);
			if (!ListUtils.isNotNull(list)) {
				String msg = "未能查到相关的用户信息，不能执行修改密码操作";
				log.error(msg);
				throw new BusinessException(msg);
			}
			
			UserFormMap userFormMap3 = list.get(0);
			String newPassword = userFormMap.getStr("newPassword");
			String oldPassword = userFormMap.getStr("oldPassword");
			String credentials_salt = userFormMap3.getStr("credentials_salt");
			PasswordHelper passwordHelper = new PasswordHelper();
			
			String oldEnPassword = passwordHelper.getEncryptPassword(username, oldPassword, credentials_salt);
			if (!oldEnPassword.equals(userFormMap3.getStr("password"))) {
				String msg = "旧密码与原密码不匹配，请确认后再试！！！";
				log.error(msg);
				throw new BusinessException(msg);
			}
			
			userFormMap.put("id", userFormMap3.getStr("id"));
			userFormMap.put("account_name", username);
			userFormMap.put("password", newPassword);
			
			passwordHelper.encryptPassword(userFormMap);
			userMapper.editEntity(userFormMap);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("修改密码失败："+e.getMessage());
		}
	}
}
