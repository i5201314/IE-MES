package com.iemes.service.impl.work_plan;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iemes.entity.CustomerRejectSfcInfoFormMap;
import com.iemes.entity.FlowStepFormMap;
import com.iemes.entity.NumberRuleFormMap;
import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.entity.ShoporderIssuingHisoryFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.CommonService;
import com.iemes.service.work_plan.ShoporderIssuingService;
import com.iemes.service.workcenter_model.NumberRuleService;
import com.iemes.util.DateUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class ShoporderIssuingServiceImpl implements ShoporderIssuingService {
	@Inject
	private BaseMapper baseMapper;

	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void issuingShoporder(ShoporderFormMap shoporderFormMap, ShoporderFormMap shoporderFormMap2,
			NumberRuleFormMap numberRuleFormMap, FlowStepFormMap flowStepFormMap, List<String> listSfcNo,
			List<ShopOrderSfcFormMap> listExsitedShopOrderSfcFormMap) throws BusinessException {
		try {
			String id = shoporderFormMap.getStr("id");
			String siteId = ShiroSecurityHelper.getSiteId();
			String processWorkflowId = shoporderFormMap2.getStr("process_workflow_id");
			Object obshoporderIssuingNumberNow = shoporderFormMap.get("issuing_number");
			int shoporderIssuingNumberNow = Integer.valueOf(obshoporderIssuingNumberNow.toString());// 当次需要下达数量
			String workShopId = shoporderFormMap.getStr("workshop_id");
			String workLineId = shoporderFormMap.getStr("workline_id");
			// 5、判断工单SFC编号是否是自动生成，如果不是，则不作任何处理；如果是，向mds_shoporder_sfc表插入sfc信息、
			// 向mds_sfc_step表插入对应工艺路线的首操作，且状态为创建的记录。
			int isSfcAutoGenerate = shoporderFormMap2.getInt("is_sfc_auto_generate");
			String shoporder_type = shoporderFormMap2.getStr("shoporder_type");
			if (isSfcAutoGenerate == 1) {
				String firstStepOperationId = flowStepFormMap.getStr("operation_id");

				for (String sfcNo : listSfcNo) {
					// 需要判断sfcNo 是否已存在
					for (ShopOrderSfcFormMap eSfcMap : listExsitedShopOrderSfcFormMap) {
						if (eSfcMap.getStr("sfc").equals(sfcNo)) {
							throw new BusinessException("自动生成sfc重复，可能是有多人同时操作造成，请尝试重新下达工单或者联系系统管理员！");
						}
					}
					// 5.1.向mds_shoporder_sfc表插入sfc信息
					ShopOrderSfcFormMap shopOrderSfcFormMap = new ShopOrderSfcFormMap();
					String sfcId = UUIDUtils.getUUID();
					shopOrderSfcFormMap.put("id", sfcId);
					shopOrderSfcFormMap.put("shoporder_id", id);
					shopOrderSfcFormMap.put("sfc", sfcNo);
					shopOrderSfcFormMap.put("sfc_status", 0);
					shopOrderSfcFormMap.put("site_id", siteId);
					shopOrderSfcFormMap.put("workshop_id", workShopId);
					shopOrderSfcFormMap.put("workline_id", workLineId);
					shopOrderSfcFormMap.put("is_printed", -1);
					shopOrderSfcFormMap.put("create_time", DateUtils.getStringDateTime());
					shopOrderSfcFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());

					baseMapper.addEntity(shopOrderSfcFormMap);

					// 5.2.向mds_sfc_step表插入对应工艺路线的首操作，且状态为创建的记录。
					SfcStepFormMap sfcStepFormMap = new SfcStepFormMap();
					sfcStepFormMap.put("id", UUIDUtils.getUUID());
					sfcStepFormMap.put("sfc", sfcNo);
					sfcStepFormMap.put("shoporder_sfc_id", sfcId);
					sfcStepFormMap.put("process_workflow_id", processWorkflowId);
					sfcStepFormMap.put("operation_id", firstStepOperationId);
					sfcStepFormMap.put("status", 0);
					sfcStepFormMap.put("workline_id", workLineId);
					sfcStepFormMap.put("shoporder_id", id);
					sfcStepFormMap.put("site_id", siteId);
					sfcStepFormMap.put("create_time", DateUtils.getStringDateTime());
					sfcStepFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
					
					//增加 work_resource_id item_id shift_id workshop_id
					String itemId =  shoporderFormMap2.getStr("shoporder_item_id");
					sfcStepFormMap.put("item_id", itemId);
					sfcStepFormMap.put("workshop_id", workShopId);

					baseMapper.addEntity(sfcStepFormMap);
				}
				baseMapper.editEntity(numberRuleFormMap);
			}
			
			if ("rework".equals(shoporder_type)) {
				String firstStepOperationId = flowStepFormMap.getStr("operation_id");
				CustomerRejectSfcInfoFormMap customerRejectSfcInfoFormMap = new CustomerRejectSfcInfoFormMap();
				customerRejectSfcInfoFormMap.put("new_shoporder_id", id);
				customerRejectSfcInfoFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
				List<CustomerRejectSfcInfoFormMap> list = baseMapper.findByNames(customerRejectSfcInfoFormMap);
				for (CustomerRejectSfcInfoFormMap customerRejectSfcInfoFormMap2 : list) {
					
					String sfcNo = customerRejectSfcInfoFormMap2.getStr("new_sfc");
					
					// 5.1.向mds_shoporder_sfc表插入sfc信息
					ShopOrderSfcFormMap shopOrderSfcFormMap = new ShopOrderSfcFormMap();
					String sfcId = UUIDUtils.getUUID();
					shopOrderSfcFormMap.put("id", sfcId);
					shopOrderSfcFormMap.put("shoporder_id", id);
					shopOrderSfcFormMap.put("sfc", sfcNo);
					shopOrderSfcFormMap.put("sfc_status", 0);
					shopOrderSfcFormMap.put("site_id", siteId);
					shopOrderSfcFormMap.put("workshop_id", workShopId);
					shopOrderSfcFormMap.put("workline_id", workLineId);
					shopOrderSfcFormMap.put("is_printed", -1);
					shopOrderSfcFormMap.put("create_time", DateUtils.getStringDateTime());
					shopOrderSfcFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());

					baseMapper.addEntity(shopOrderSfcFormMap);

					// 5.2.向mds_sfc_step表插入对应工艺路线的首操作，且状态为创建的记录。
					SfcStepFormMap sfcStepFormMap = new SfcStepFormMap();
					sfcStepFormMap.put("id", UUIDUtils.getUUID());
					sfcStepFormMap.put("sfc", sfcNo);
					sfcStepFormMap.put("shoporder_sfc_id", sfcId);
					sfcStepFormMap.put("process_workflow_id", processWorkflowId);
					sfcStepFormMap.put("operation_id", firstStepOperationId);
					sfcStepFormMap.put("status", 0);
					sfcStepFormMap.put("workline_id", workLineId);
					sfcStepFormMap.put("shoporder_id", id);
					sfcStepFormMap.put("site_id", siteId);
					sfcStepFormMap.put("create_time", DateUtils.getStringDateTime());
					sfcStepFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
					
					//增加 work_resource_id item_id shift_id workshop_id
					String itemId =  shoporderFormMap2.getStr("shoporder_item_id");
					sfcStepFormMap.put("item_id", itemId);
					sfcStepFormMap.put("workshop_id", workShopId);
					baseMapper.addEntity(sfcStepFormMap);
				}
			}

			// 6、向mds_shoporder_issuing_history添加工单下达的历史记录。
			ShoporderIssuingHisoryFormMap shoporderIssuingHisoryFormMap = new ShoporderIssuingHisoryFormMap();
			shoporderIssuingHisoryFormMap.put("id", UUIDUtils.getUUID());
			shoporderIssuingHisoryFormMap.put("issuing_shoporder_id", id);
			shoporderIssuingHisoryFormMap.put("issuing_number", shoporderIssuingNumberNow);
			shoporderIssuingHisoryFormMap.put("issuing_workshop_id", workShopId);
			shoporderIssuingHisoryFormMap.put("issuing_workline_id", workLineId);
			shoporderIssuingHisoryFormMap.put("issuing_time", DateUtils.getStringDateTime());
			shoporderIssuingHisoryFormMap.put("site_id", siteId);
			shoporderIssuingHisoryFormMap.put("create_time", DateUtils.getStringDateTime());
			shoporderIssuingHisoryFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());

			baseMapper.addEntity(shoporderIssuingHisoryFormMap);

			// 7.最后更新工单的状态到数据库
			baseMapper.editEntity(shoporderFormMap2);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("工单下达失败！" + e.getMessage());
		}
	}
}
