package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.ItemBomFormMap;
import com.iemes.entity.ItemBomRelationFormMap;
import com.iemes.entity.ItemFormMap;
import com.iemes.entity.ItemSurrenalFormMap;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.WorkCenterFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.CommonService;
import com.iemes.service.workcenter_model.ItemService;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
public class ItemServiceImpl implements ItemService {

	@Inject
	private BaseExtMapper baseMapper;
	
	@Inject
	private CommonService commonService;

	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveItem(ItemFormMap itemFormMap) throws BusinessException {
		try {
			String id = itemFormMap.getStr("id");
			String itemNo = itemFormMap.getStr("item_no");
            String version = itemFormMap.getStr("item_version");
			
			if(StringUtils.isEmpty(itemNo)) {
				throw new BusinessException("物料编号不能为空，请输入！");
			}	
			if(StringUtils.isEmpty(version)) {
				throw new BusinessException("物料版本号不能为空，请输入！");
			}
			
			ItemFormMap itemFormMap2 = new ItemFormMap();
			itemFormMap2.put("item_no", itemNo);
			itemFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			itemFormMap2.put("item_version", version);
			List<ItemFormMap> list = baseMapper.findByNames(itemFormMap2);

			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				ItemFormMap itemFormMap3 = new ItemFormMap();
				itemFormMap3 = list.get(0);
				id = itemFormMap3.getStr("id");
				itemFormMap.set("id", id);
				
				baseMapper.editEntity(itemFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				itemFormMap.put("id", id);
				baseMapper.addEntity(itemFormMap);
			}
			
			//保存或更新自定义数据的值
			commonService.saveUDefinedDataValueS(itemFormMap.getStr("id"), itemFormMap.getStr("udata"));
			
			//保存替代物料
			baseMapper.deleteByAttribute("item_id", itemFormMap.getStr("id"), ItemSurrenalFormMap.class);
			String itemSurrenals = itemFormMap.getStr("item_surrenals");
			JSONArray itemSurrenalArr = JSONArray.fromObject(itemSurrenals);
			for (int i=0;i<itemSurrenalArr.size();i++) {
				JSONObject item = (JSONObject) itemSurrenalArr.get(i);
				ItemSurrenalFormMap itemSurrenalFormMap = new ItemSurrenalFormMap();
				itemSurrenalFormMap.put("item_surrenal_level", ((JSONArray)((JSONObject)item.get("item_surrenal_level")).get("value")).get(0));
				itemSurrenalFormMap.put("item_surrenal_id", ((JSONArray)((JSONObject)item.get("item_surrenal_id")).get("value")).get(1));
				itemSurrenalFormMap.put("item_surrenal_version", ((JSONArray)((JSONObject)item.get("item_surrenal_version")).get("value")).get(0));
				itemSurrenalFormMap.put("item_surrenal_usage", ((JSONArray)((JSONObject)item.get("item_surrenal_usage")).get("value")).get(1));
				itemSurrenalFormMap.put("item_surrenal_usage_version", ((JSONArray)((JSONObject)item.get("item_surrenal_usage_version")).get("value")).get(0));
				itemSurrenalFormMap.put("start_validity_term", ((JSONArray)((JSONObject)item.get("start_validity_term")).get("value")).get(0));
				itemSurrenalFormMap.put("end_validity_term", ((JSONArray)((JSONObject)item.get("end_validity_term")).get("value")).get(0));
				itemSurrenalFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
				itemSurrenalFormMap.put("create_time", DateUtils.getStringDateTime());
				itemSurrenalFormMap.put("item_id", itemFormMap.getStr("id"));
				itemSurrenalFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
				itemSurrenalFormMap.put("id", UUIDUtils.getUUID());
				baseMapper.addEntity(itemSurrenalFormMap);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("物料保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delItem(String itemId) throws BusinessException {
		try {
			//删除物料
			baseMapper.deleteByAttribute("id", itemId, ItemFormMap.class);
			
			//删除替代物料
			baseMapper.deleteByAttribute("item_id", itemId, ItemSurrenalFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("物料删除失败！"+e.getMessage());
		}
	}
}
