package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * 实体表
 */
@TableSeg(tableName = "mds_res_role", id="roleId")
public class ResRoleFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@date 2015年3月29日
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;

}
