package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * mds_page_save实体表
 */
@TableSeg(tableName = "mds_page_save", id="id")
public class PageSaveFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@author mds
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;

}
