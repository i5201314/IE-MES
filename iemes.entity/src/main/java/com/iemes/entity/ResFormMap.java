package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * 菜单实体表
 */
@TableSeg(tableName = "mds_resources", id="id")
public class ResFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@author mds
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;

}
