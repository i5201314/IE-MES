package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * work_resource_type实体表
 */
@TableSeg(tableName = "mds_work_resource_type", id="id")
public class WorkResourceTypeFormMap extends FormMap<String,Object>{

	/**

	 */
	private static final long serialVersionUID = 1L;

}
