package com.iemes.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 表头设置类
 * @author Administrator
 *
 */
public class MTable {

	private List<MTableCol> cols;						//表头
	
	private String tableName;							//标题
	
	private List<?> tableData;							//表数据
	
	public List<?> getTableData() {
		return tableData;
	}

	public void setTableData(List<?> tableData) {
		this.tableData = tableData;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public MTable() {
		cols = new ArrayList<MTableCol>();
	}

	public List<MTableCol> getCols() {
		return cols;
	}
	
	public void putHead(MTableCol col) {
		cols.add(col);
	}
	
	public void addHead(MTableCol col) {
		cols.add(col);
	}
}
