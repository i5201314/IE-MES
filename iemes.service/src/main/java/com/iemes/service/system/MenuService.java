package com.iemes.service.system;

import com.iemes.entity.ResFormMap;
import com.iemes.exception.BusinessException;

public interface MenuService {

	/**
	 * 保存用户（新增或更新）
	 * @param userFormMap
	 * @throws BusinessException
	 */
	void saveMenu(ResFormMap resFormMap)throws BusinessException;
	
	/**
	 * 删除用户
	 * @param id
	 * @throws BusinessException
	 */
	void delMenu(String id)throws BusinessException;
}
