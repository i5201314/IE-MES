package com.iemes.service.WorkScheduling;

import java.util.List;

import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.exception.BusinessException;

public interface WorkStepChangeService {
	
	/**
	 * 调整作业步骤
	 * @param shopOrderSfcFormMap...
	 * @throws BusinessException
	 */
	void changeWorkStep(SfcStepFormMap sfcStepFormMapBeforeChange, ShopOrderSfcFormMap shopOrderSfcFormMap,
			ShopOrderSfcFormMap shopOrderSfcFormMapInDb,ShoporderFormMap shoporderFormMap,
			List<ShopOrderSfcFormMap> listShopOrderSfcFormMapOfOneShoporder,
			int newOperationStatus)throws BusinessException;
}
